#include "headers.h"
#include <beth-api/beth_api.h>

#include "Controls.h"

using Control = beth::Control;

void Controls::Update() {
	std::lock_guard<std::mutex> l(m);
	if (!needApply) return;

	needApply = false;
	sd::EnablePlayerControls(state[(UInt32)Control::Movement] >= MIN_ENABLED_STATE,
		state[(UInt32)Control::Fighting] >= MIN_ENABLED_STATE,
		state[(UInt32)Control::CamSwitch] >= MIN_ENABLED_STATE,
		state[(UInt32)Control::Looking] >= MIN_ENABLED_STATE,
		state[(UInt32)Control::Sneaking] >= MIN_ENABLED_STATE,
		state[(UInt32)Control::Menu] >= MIN_ENABLED_STATE,
		state[(UInt32)Control::Activate] >= MIN_ENABLED_STATE,
		true,
		NULL);
	sd::DisablePlayerControls(state[(UInt32)Control::Movement] < MIN_ENABLED_STATE,
		state[(UInt32)Control::Fighting] < MIN_ENABLED_STATE,
		state[(UInt32)Control::CamSwitch] < MIN_ENABLED_STATE,
		state[(UInt32)Control::Looking] < MIN_ENABLED_STATE,
		state[(UInt32)Control::Sneaking] < MIN_ENABLED_STATE,
		state[(UInt32)Control::Menu] < MIN_ENABLED_STATE,
		state[(UInt32)Control::Activate] < MIN_ENABLED_STATE,
		false,
		NULL);
	sd::EnableFastTravel(state[(UInt32)Control::FastTravel] >= MIN_ENABLED_STATE);
	sd::SetInChargen(state[(UInt32)Control::SaveGame] < MIN_ENABLED_STATE,
		state[(UInt32)Control::Wait] < MIN_ENABLED_STATE,
		false);
	sd::SetBeastForm(state[(UInt32)Control::BeastForm] < MIN_ENABLED_STATE);
}

void Controls::SetEnabled(Control control, bool enabled, std::string systemfrom) {
	std::lock_guard<std::mutex> l(m);
	auto idx = (UInt32)control;
	if (enabled) this->disabled[systemfrom].erase(control);
	else this->disabled[systemfrom].insert(control);
	for (int i = 0; i != std::size(this->state); ++i) {
		this->state[i] = MIN_ENABLED_STATE;
	}
	for (auto pair : this->disabled) {
		for (Control control : pair.second) {
			this->state[(uint32_t)control] = MIN_ENABLED_STATE - 1;
		}
	}
	needApply = true;
}