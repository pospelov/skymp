#pragma once
#include <mutex>
#include <string>
#include <stdexcept>
#include <atomic>

#include <skymp-common/Look.h>

#include "FormIdGen.h"

class Actor;
class TESNPC;

class LookManager {
public:
	void TickHelper() noexcept;

	Look GetFrom(Actor *ac) noexcept;

	// Combination of ApplyTints and Apply to g_thePlayer in runtime
	// Takes ~1 second to execute
	// Throws ApplyError
	void ApplyToPlayer(const Look &look, FormIdGen &formIdGen);

	// Throws ApplyError
	TESNPC *Apply(const Look &lo, TESNPC *optionalPreallocatedNpc, FormIdGen &formIdGen);

	void ApplyTints(const Look &lo, Actor *optionalActor) noexcept;

	class ApplyError : public std::runtime_error {
	public:
		ApplyError(std::string s) : runtime_error(s) {};
	};

private:
	std::recursive_mutex m;
	std::atomic<bool> badTintsInPC = false;
	std::vector<Tint> lastTints;
};