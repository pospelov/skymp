// skymp3: Edited
#pragma once

#include "ess_structures.h"

class saveFile {
public:
	bool read(void *data, size_t length);
	bool save();
	saveFile(std::ostream &log, bool dbg, std::ostream &out);

	bool clean();

	bool DBG;
	long pos = 0;
	std::streampos size = 0;
	uint8_t* memblock = NULL;
	std::ostream &log;
	std::ostream &out;
	saveFile_t file;

	globalData_t* table1;

	miscStats* g0;
	playerLocation_t* g1;
	tes_t* g2;
	globalVariables* g3;
	createdObjects* g4;
	effects* g5;
	weather_t* g6;
	audio_t* g7;
	skyCells* g8;

	processList_t* g100;
	//combat* g101;
	uint8_t* g101;
	interface_t* g102;
	actorCauses_t* g103;
	//g104
	uint8_t* g104;
	detectionManager_t* g105;
	locationMetaData_t* g106;
	questStaticData_t* g107;
	storyTeller_t* g108;
	magicFavorites_t* g109;
	playerControls_t* g110;
	storyEventManager_t* g111;
	//byte* g111;
	ingredientShared_t* g112;
	menuControls_t* g113;
	menuTopicManager* g114;

	uint8_t* g1000;
	papyrus_t* g1001;
	animObjects_t* g1002;
	timer_t* g1003;
	uint8_t* g1004;
	uint8_t* g1005;

	float readFloat();
	uint32_t readUint32();
	uint16_t readUint16();
	uint8_t readUint8();
	int32_t readInt32();
	int16_t readInt16();

	std::string readString();
	std::string readString(unsigned int strlen);
	refID_t readRefID();
	uint8_t readByte();
	uint32_t readVsval();
	variable_t readVariable_t();
	globalData_t readGlobalData();

	void writeString(std::string str);
	void writeString(std::string str[], unsigned int length);
	void writeGlobalData(globalData_t data);
	void writeRefID(refID_t data);
	void writeRefID(refID_t data[], unsigned int length);
	void writeVsval(vsval data);
	void writeEnchantment(createdObjects::enchantment_t data[], unsigned int length);
	template <class T>void write(const T data);
	template <class T>void write(const T data[], unsigned int length);
	void writeVariable_t(variable_t data);
	void writeVariable_t(variable_t* data, unsigned int length);
};
