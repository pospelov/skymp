#pragma once
#include <memory>
#include <beth-api/beth_api.h>
#include "headers.h"

namespace skyrim_le_api {
	using namespace beth;

	class Features : public beth::IGameThrFeatures, public beth::IHelperThrFeatures {
	public:
		Features(std::shared_ptr<beth::IListener> listener) noexcept;

		void PrintNote(const char *fmt, ...) noexcept override;
		Form GetPlayer() noexcept override;
		Form GetParentCell(const Form &ref) noexcept override;
		std::vector<Form> GetLoadedRefs(const Form &filterCell = {}) noexcept override;
		TESFormType GetFormType(const Form &form) noexcept override;
		Form GetBase(const Form &form) noexcept override;
		uint32_t GetId(const Form &form) noexcept override;
		void DisableNoWait(const Form &ref, bool fadeOut) noexcept override;
		void Delete(const Form &ref) noexcept override;
		void SetLockLevel(const Form &ref, LockLevel lockLevel) noexcept override;
		void BlockActivation(const Form&ref, bool block) noexcept override;
		void SetMotionType(const Form &ref, MotionType mt) noexcept override;
		Point3D GetPosition(const Form &ref) noexcept override;
		Point3D GetRotation(const Form &ref) noexcept override;
		Form FindRandomActor(const Point3D &pos, float radius) noexcept override;
		bool IsInterior(const Form &cell) noexcept override;
		Form PlaceAtMe(const Form &refToPlaceAt, const Form &baseForm) noexcept override;
		bool Exist(const Form &form) noexcept override;
		Form GetWorldSpace(const Form &ref) noexcept override;
		bool Is3DLoaded(const Form &ref) noexcept override;
		bool GetAnimVariableBool(const Form &ref, const char *varName) noexcept override;
		int GetAnimVariableInt(const Form &ref, const char *varName) noexcept override;
		float GetAnimVariableFloat(const Form &ref, const char *varName) noexcept override;
		beth::ActorFlags GetActorFlags(const Form &actor) noexcept override;
		float GetCurrentAV(const Form &actor, ActorValue av) noexcept override;
		void SetCurrentAV(const Form &actor, ActorValue actorValue, float newValue) noexcept override;
		void ModCurrentAV(const Form &actor, ActorValue actorValue, float mod) noexcept override;
		void SetBaseAV(const Form &actor, ActorValue actorValue, float newValue) noexcept override;
		float GetAVPercentage(const Form &actor, ActorValue actorValue) noexcept override;
		float GetTotalItemWeight(const Form &ref) noexcept override;
		void SetPosition(const Form &ref, const Point3D &pos) noexcept;
		void SetRotation(const Form &ref, const Point3D &rot) noexcept;
		void SetHeadTracking(const Form &actor, bool headTraking) noexcept override;
		void SetName(const Form &ref, const char *name) noexcept override;
		void ClearLookAt(const Form &actor) noexcept override;
		void StopCombat(const Form &actor) noexcept override;
		void SetDeferredKill(const Form &actor, bool deferredKill) noexcept override;
		void Kill(const Form &actor) noexcept override;
		void KeepOffsetFromActor(const Form &actor, const Form &target, const Point3D &offset, float angleOffset, float catchupRadius, float followRadius) noexcept override;
		void ClearKeepOffsetFromActor(const Form &actor) noexcept override;
		void TranslateTo(const Form &ref, const Point3D &pos, const Point3D &rot, float speed, float maxRotSpeed) noexcept override;
		void SendAnimEvent(const Form &ref, const char *animEventName) noexcept override;
		void SetWeaponDrawn(const Form &actor, bool drawn) noexcept override;
		Look GetLook(const Form &actor) noexcept override;
		Form ApplyLook(const Look &look, const Form &optionalNpc) noexcept override;
		void ApplyLookTints(const Look &look, const Form &actor) noexcept override;
		void ApplyLookInRuntime(const Look &look, const Form &actor) noexcept override;
		void ApplyEquipment(const Equipment &eq, const Form &actorOrNpc) noexcept override;
		bool HasLOS(const Form &ref) noexcept override;
		void ShowRaceMenu() noexcept override;
		void SetControlEnabled(beth::Control control, bool enabled, const char *systemFrom) noexcept override;
		beth::Form GetDefaultNpc() noexcept override;
		Point3D GetNodeWorldPosition(const Form &actor, SkeletonNode node) noexcept override;
		beth::Form GetProduce(const Form &refOrBase) noexcept override;
		void SetSneaking(const Form &actor, bool sneaking) noexcept override;

		bool Load(uint32_t cellOrWorld, const Point3D &pos, float degreesRotZ) noexcept override;
		bool IsMenuOpen(Menu menu) const noexcept override;
		void SetMenuOpen(Menu menu, bool open) const noexcept override;
		Point3D WorldPointToScreenPoint(const Point3D &point) noexcept override;

		void SDMain();
		static void HelperThreadMain(void *featuresPtr);

		const char *SendAnimEventHook(uint32_t id, const char *animEvent);
		bool SetWeaponDrawnHook(uint32_t id, bool draw);

	private:
		void BeforeAnyTick() noexcept;
		void AfterAnyTick();
		void ForceDeleteSaves();

		struct Impl;
		std::shared_ptr<Impl> pImpl;
	};
}