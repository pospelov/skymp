#pragma once
#include <cstdint>
#include <mutex>

class FormIdGen {
public:
	// Threadsafe
	uint32_t NewFormId() noexcept;

private:
	uint32_t id = ~0;
	std::mutex mutex;
};