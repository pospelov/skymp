#define VERSION_MAJOR 0
#define VERSION_MINOR 2

#include "ess_file.h"

using namespace std;

string getOpcodeDesc(int opcode) {
	switch (opcode) {
	case 0x00:
		return "0x00 - NOOP";
	case 0x01:
		return "0x01 - IAdd(SII)";
	case 0x02:
		return "0x02 - FAdd(SFF)";
	case 0x03:
		return "0x03 - ISubstract(SII)";
	case 0x04:
		return "0x04 - FSubstract(SFF)";
	case 0x05:
		return "0x05 - IMultiply(SII)";
	case 0x06:
		return "0x06 - FMultiply(SFF)";
	case 0x07:
		return "0x07 - IDivide(SII)";
	case 0x08:
		return "0x08 - FDivide(SFF)";
	case 0x09:
		return "0x09 - IMod(SII)";
	case 0x0a:
		return "0x0a - Not(SA)";
	case 0x0b:
		return "0x0b - INegate(SI)";
	case 0x0c:
		return "0x0c - FNegate(SF)";
	case 0x0d:
		return "0x0d - Assign(SA)";
	case 0x0e:
		return "0x0e - Cast(SA)";
	case 0x0f:
		return "0x0f - CompareEQ(SAA)";
	case 0x10:
		return "0x10 - CompareLT(SAA)";
	case 0x11:
		return "0x11 - CompareLTE(SAA)";
	case 0x12:
		return "0x12 - CompareGT(SAA)";
	case 0x13:
		return "0x13 - Compare GTE(SAA)";
	case 0x14:
		return "0x14 - Jump(L)";
	case 0x15:
		return "0x15 - JumpT(AL)";
	case 0x16:
		return "0x16 - JumpF(AL)";
	case 0x17:
		return "0x17 - CallMethod(NSS*)";
	case 0x18:
		return "0x18 - CallParent(NS*)";
	case 0x19:
		return "0x19 - CallStatic(NNS*)";
	case 0x1a:
		return "0x1a - Return(A)";
	case 0x1b:
		return "0x1b - StrCat(SQQ)";
	case 0x1c:
		return "0x1c - PropGet(NSS)";
	case 0x1d:
		return "0x1d - PropSet(NSA)";
	case 0x1e:
		return "0x1e - ArrayCreate(Su)";
	case 0x1f:
		return "0x1f - ArrayLength(SS)";
	case 0x20:
		return "0x20 - ArrayGetElement(SSI)";
	case 0x21:
		return "0x21 - ArraySetElement(SIA)";
	case 0x22:
		return "0x23 - ArrayFindElement(SSAI)";
	case 0x23:
		return "0x24 - ArrayRFindElement(SSAI)";
	case 0x24:
		return "0x25 - INVALID";
	default:
		throw "ERROR! unknown opcode! dec: " + opcode;
	}
}

int getOpcodeLength(int opcode) {
	switch (opcode) {
	case 0x14:
	case 0x1a:
		return 1;

	case 0x0a:
	case 0x0b:
	case 0x0c:
	case 0x0d:
	case 0x0e:
	case 0x15:
	case 0x16:
	case 0x1e:
	case 0x1f:
		return 2;

	case 0x01:
	case 0x02:
	case 0x03:
	case 0x04:
	case 0x05:
	case 0x06:
	case 0x07:
	case 0x08:
	case 0x09:
	case 0x0f:
	case 0x10:
	case 0x11:
	case 0x12:
	case 0x13:
	case 0x18:
	case 0x1b:
	case 0x1c:
	case 0x1d:
	case 0x20:
	case 0x21:
		return 3;

	case 0x17:
	case 0x19:
	case 0x22:
	case 0x23:
		return 4;

	case 0x24:
		throw "ERROR! invalid opcode! (0x24 - INVALID)";

	case 0x00:
		throw "ERROR! invalid opcode! (0x00 - NOOP)";

	default:
		throw "ERROR! nunknown opcode! dec: " + opcode;
	}
}

saveFile::saveFile(std::ostream &log_, bool dbg_, std::ostream &out_) : log(log_), out(out_) {
	this->DBG = dbg_;
}

string refIDtoString(refID_t a) {
	string str;
	stringstream stream;

	switch (a.byte0 >> 6) {
	case 0:
		str = "formIDArray index";
		break;

	case 1:
		str = "default";
		break;

	case 2:
		str = "created";
		break;

	case 3:
		str = "???????";
		break;

	default:
		return "invalid refID!";
	}

	stream << (int)a.byte0 << " " << (int)a.byte1 << " " << (int)a.byte2 << ", " << str << " : 0x" << std::right << std::setfill('0') << std::setw(8) << std::hex << (int)(((a.byte0 & 0b00111111) << 16) | (a.byte1 < 8) | a.byte2);// << std::dec << " - raw data: b0: " << (int) a.byte0 << " b1: " << (int) a.byte1 << " b2: " << (int) a.byte2 << endl;
	return stream.str();
}

string variableToString(variable_t a, const string* strings, int strcount) {
	try {
		switch (a.type) {
		case 0:
			return "Null - 00000000 00000000 00000000 00000000";

		case 1:
			return "refID - " + refIDtoString(static_cast<refID_t*>(a.data)[0]) + '\t' + refIDtoString(static_cast<refID_t*>(a.data)[1]);

		case 2:
			if (*static_cast<uint16_t*>(a.data) >= strcount) {
				throw "index > strcount!";
			}
			return "string - " + strings[*static_cast<uint16_t*>(a.data)] + "(" + to_string(*static_cast<uint16_t*>(a.data)) + ")";

		case 3:
			return "integer - " + to_string(*static_cast<uint32_t*>(a.data));

		case 4:
			return "float - " + to_string(*static_cast<float*>(a.data));

		case 5:
			if (*static_cast<uint32_t*>(a.data) == 0)
				return "boolean - false";

			return "boolean - true";

		case 11:
			return "refID array - type(?): " + to_string(static_cast<refID6*>(a.data)->type) + ", array ID: " + to_string(static_cast<refID6*>(a.data)->id);

		case 12:
			return "string array ID - " + to_string(*static_cast<uint16_t*>(a.data));

		case 13:
			return "integer array ID - " + to_string(*static_cast<uint16_t*>(a.data));

		case 14:
			return "float array ID - " + to_string(*static_cast<uint16_t*>(a.data));

		case 15:
			return "boolean array ID- " + to_string(*static_cast<uint16_t*>(a.data));

		default:
			return "INVALID VARIABLE_T VALUE!";
		}
	}
	catch (out_of_range e) {
		throw e;
	}
}

string variableToString(variable_t a) {
	switch (a.type) {
	case 0:
		return "Null - 00000000 00000000 00000000 00000000";

	case 1:
		return "refID - " + refIDtoString(static_cast<refID_t*>(a.data)[0]) + '\t' + refIDtoString(static_cast<refID_t*>(a.data)[1]);

	case 2:
		return "string reference - " + to_string(*static_cast<uint16_t*>(a.data));

	case 3:
		return "integer - " + to_string(*static_cast<uint32_t*>(a.data));

	case 4:
		return "float - " + to_string(*static_cast<float*>(a.data));

	case 5:
		if (*static_cast<bool*>(a.data) == 0)
			return "boolean - false";

		return "boolean - true";

	case 11:
		return "refID array - type(?): " + to_string(static_cast<refID6*>(a.data)->type) + ", array ID: " + to_string(static_cast<refID6*>(a.data)->id);

	case 12:
		return "string array ID - " + to_string(*static_cast<uint16_t*>(a.data));

	case 13:
		return "integer array ID - " + to_string(*static_cast<uint16_t*>(a.data));

	case 14:
		return "float array ID - " + to_string(*static_cast<uint16_t*>(a.data));

	case 15:
		return "boolean array ID- " + to_string(*static_cast<uint16_t*>(a.data));

	default:
		return "INVALID VARIABLE_T VALUE!";
	}

}

float saveFile::readFloat() {
	if ((pos + 4) > size) {
		throw "unexpectedly reached EOF!";
	}

	uint8_t buff[4];
	buff[3] = readUint8();
	buff[2] = readUint8();
	buff[1] = readUint8();
	buff[0] = readUint8();

	int exponent = (((buff[0] & 127) << 1) + (buff[1] >> 7)) - 127;

	float ret = (float)((((buff[1] & 64) >> 6) / 2.0) + (((buff[1] & 32) >> 5) / 4.0) + (((buff[1] & 16) >> 4) / 8.0) + (((buff[1] & 8) >> 3) / 16.0) + (((buff[1] & 4) >> 2) / 32.0) + (((buff[1] & 2) >> 1) / 64.0) + ((buff[1] & 1) / 128.0)
		+ (((buff[2] & 128) >> 7) / 256.0) + (((buff[2] & 64) >> 6) / 512.0) + (((buff[2] & 32) >> 5) / 1024.0) + (((buff[2] & 16) >> 4) / 2048.0) + (((buff[2] & 8) >> 3) / 4096.0)
		+ (((buff[2] & 4) >> 2) / 8192.0) + (((buff[2] & 2) >> 1) / 16384.0) + ((buff[2] & 1) / 32768.0) + (((buff[3] & 128) >> 7) / 65536.0)
		+ (((buff[3] & 64) >> 6) / 131072.0) + (((buff[3] & 32) >> 5) / 262144.0) + (((buff[3] & 16) >> 4) / 524288.0) + (((buff[3] & 8) >> 3) / 1048576.0)
		+ (((buff[3] & 4) >> 2) / 2097152.0) + (((buff[3] & 2) >> 1) / 4194304.0) + ((buff[3] & 1) / 8388608.0));
	if (exponent != -127) {
		ret += 1;
	}
	if ((buff[0] >> 7) == 1) {
		ret *= -1;
	}
	ret *= (float)pow(2, exponent);

	return ret;
}

uint8_t saveFile::readByte() {
	if ((pos + 1) > size) {
		throw "unexpectedly reached EOF!";
	}
	return memblock[pos++];
}

uint32_t saveFile::readUint32() {
	if ((pos + 4) > size) {
		throw "unexpectedly reached EOF!";
	}

	const auto byte0 = readByte();
	const auto byte1 = readByte();
	const auto byte2 = readByte();
	const auto byte3 = readByte();

	return (((byte3 & 0xff) << 24) | ((byte2 & 0xff) << 16) | ((byte1 & 0xff) << 8) | (byte0 & 0xff));
}

uint16_t saveFile::readUint16() {
	if ((pos + 2) > size) {
		throw "unexpectedly reached EOF!";
	}

	const auto  byte0 = readByte();
	const auto byte1 = readByte();

	return (uint16_t)(((byte1 & 0xff) << 8) | (byte0 & 0xff));
}

uint8_t saveFile::readUint8() {
	if ((pos + 1) > size) {
		throw "unexpectedly reached EOF!";
	}
	return (uint8_t)readByte();
}

int32_t saveFile::readInt32() {
	if ((pos + 4) > size) {
		throw "unexpectedly reached EOF!";
	}

	const auto byte0 = readByte();
	const auto byte1 = readByte();
	const auto byte2 = readByte();
	const auto byte3 = readByte();

	return (((byte3 & 0xff) << 24) | ((byte2 & 0xff) << 16) | ((byte1 & 0xff) << 8) | (byte0 & 0xff));
}

int16_t saveFile::readInt16() {
	if ((pos + 2) > size) {
		throw "unexpectedly reached EOF!";
	}

	const auto byte0 = readByte();
	const auto byte1 = readByte();

	return (int16_t)(((byte1 & 0xff) << 8) | (byte0 & 0xff));
}

vsval saveFile::readVsval() {
	try {
		uint8_t byte0 = readUint8();
		uint8_t byte1, byte2, byte3;

		vsval ret;

		if ((byte0 & 0b00000011) == 0b00000000) {
			ret = (byte0 >> 2);
		}
		else if ((byte0 & 0b00000011) == 0b00000001) {
			byte1 = readUint8();
			ret = ((byte0 | (byte1 << 8)) >> 2);
		}
		else if ((byte0 & 0b00000011) == 0b00000010) {
			byte1 = readUint8();
			byte2 = readUint8();
			byte3 = readUint8();
			ret = ((byte0 | (byte1 << 8) | (byte2 << 16) | (byte3 << 24)) >> 2);
		}
		else {
			throw "error reading vsval! byte0 lower 2 bits value: " + to_string(byte0 & 0b00000011);
		}

		return ret;

	}
	catch (const char* str) {
		throw str;
	}
}

string saveFile::readString() {
	try {
		uint16_t strlen = readUint16();

		if ((pos + strlen) > size) {
			throw "unexpectedly reached EOF!";
		}

		char* str = new char[strlen + 1];

		for (int i = 0; i < strlen; i++) {
			str[i] = readByte();
		}
		str[strlen] = '\0';
		string ret = (string)str;
		delete[] str;
		return ret;
	}
	catch (const char* a) {
		throw a;
	}
}

string saveFile::readString(unsigned int strlen) {
	if ((pos + strlen) > size) {
		throw "unexpectedly reached EOF!";
	}

	char* str = new char[strlen + 1];

	for (unsigned int i = 0; i < strlen; i++) {
		str[i] = readByte();
	}

	str[strlen] = '\0';
	string ret = str;
	delete[] str;
	return ret;
}

refID_t saveFile::readRefID() {

	if ((pos + 3) > size) {
		throw "unexpectedly reached EOF!";
	}

	refID_t ret;
	ret.byte0 = readByte();
	ret.byte1 = readByte();
	ret.byte2 = readByte();

	return ret;
}

variable_t saveFile::readVariable_t() {
	try {
		//ofstream a;
		//a.open("log.txt", ios::app);
		variable_t ret;
		ret.type = readUint8();
		//a << ret.type << ", data: ";
		switch (ret.type) {
		case 0: { //NULL (4 bytes of zero)
			ret.data = new uint8_t[4];
			uint8_t tmpByte = 0;

			for (int i = 0; i < 4; i++) {
				tmpByte = readByte();

				if (tmpByte != 0) {
					throw "invalid value! should be 0, but is " + to_string(tmpByte) + " (variable_t type 0)";
				}
			}

			static_cast<uint8_t*>(ret.data)[0] = 0;
			static_cast<uint8_t*>(ret.data)[1] = 0;
			static_cast<uint8_t*>(ret.data)[2] = 0;
			static_cast<uint8_t*>(ret.data)[3] = 0;

			break;
		}

		case 1: { //RefID (6 byte) -> maybe 2??
			ret.data = new refID_t[2];
			static_cast<refID_t*>(ret.data)[0] = readRefID();
			static_cast<refID_t*>(ret.data)[1] = readRefID();
			//a << static_cast<refID_t*>(ret.data)[0].byte0 << " " << static_cast<refID_t*>(ret.data)[0].byte1 << " " << static_cast<refID_t*>(ret.data)[0].byte2 << ", " << static_cast<refID_t*>(ret.data)[1].byte0 << " " << static_cast<refID_t*>(ret.data)[1].byte1 << " " << static_cast<refID_t*>(ret.data)[1].byte2 << endl;
			break;
		}

		case 2: { //String (2 byte) -> string array reference??
			ret.data = new uint16_t;
			*static_cast<uint16_t*>(ret.data) = readUint16();
			//a << *static_cast<uint16_t*>(ret.data) << endl;
			break;
		}

		case 3: { //Integer (4 byte)
			ret.data = new uint32_t;
			*static_cast<uint32_t*>(ret.data) = readUint32();
			//a << *static_cast<uint32_t*>(ret.data) << endl;
			break;
		}

		case 4: { //Float (4 byte)
			ret.data = new float;
			*static_cast<float*>(ret.data) = readFloat();
			//a << *static_cast<float*>(ret.data) << endl;
			break;
		}

		case 5: { //Boolean (4 byte)
			ret.data = new bool;
			uint32_t tmp2 = readUint32();

			if (tmp2 == 0)
				*static_cast<bool*>(ret.data) = false;
			else if (tmp2 == 1)
				*static_cast<bool*>(ret.data) = true;
			else {
				throw "wrong value?? boolean scriptdata.member.data";
			}
			//a << *static_cast<bool*>(ret.data) << endl;
			break;
		}

		case 11: { //RefID Array (6 byte -> 2 for type and 4 for array ID) ??
			ret.data = new refID6;
			static_cast<refID6*>(ret.data)->type = readUint16();
			static_cast<refID6*>(ret.data)->id = readUint32();

			break;
		}

		case 12: { //String Array (4 byte) -> array ID
			ret.data = new uint32_t;
			*static_cast<uint32_t*>(ret.data) = readUint32();

			break;
		}

		case 13: { //Integer Array (4 byte) -> array ID
			ret.data = new uint32_t;
			*static_cast<uint32_t*>(ret.data) = readUint32();

			break;
		}

		case 14: { //Float Array (4 byte) -> array ID
			ret.data = new uint32_t;
			*static_cast<uint32_t*>(ret.data) = readUint32();

			break;
		}

		case 15: { //Boolean Array (4 byte) -> array ID
			ret.data = new uint32_t;
			*static_cast<uint32_t*>(ret.data) = readUint32();

			break;
		}

		default:
			throw "invalid variable_t.type!";
		}

		return ret;
	}
	catch (const char* str) {
		throw str;
	}

}

globalData_t saveFile::readGlobalData() {
	try {
		globalData_t data;
		data.type = readUint32();
		data.length = readUint32();
		long pos_start;

		switch (data.type) {
		case 0: { //misc stats

			g0 = new miscStats;
			pos_start = pos;
			g0->count = readUint32();
			g0->stats = new miscStats::miscStat_t[g0->count];

			for (unsigned int i = 0; i < g0->count; i++) {
				g0->stats[i].name = readString();
				g0->stats[i].category = readUint8();

				if (g0->stats[i].category < 0 || g0->stats[i].category > 6) {
					throw "unknown category!";
				}

				g0->stats[i].value = readInt32();
			}

			if (DBG) {
				log << "misc stats(" << g0->count << "):" << endl;
				log << "\tlength - " << data.length << endl;
				for (unsigned int i = 0; i < g0->count; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\tname - " << g0->stats[i].name << endl;
					log << "\t\t\tcategory - ";

					switch (g0->stats[i].category) {
					case 0:
						log << "General" << endl;
						break;

					case 1:
						log << "Quest" << endl;
						break;

					case 2:
						log << "Combat" << endl;
						break;

					case 3:
						log << "Magic" << endl;
						break;

					case 4:
						log << "Crafting" << endl;
						break;

					case 5:
						log << "Crime" << endl;
						break;

					case 6:
						log << "DLC Stats" << endl;
						break;

					default:
						throw "unknown miscStat category!";
					}
					log << "\t\t\tvalue - " << (unsigned int)g0->stats[i].value << endl << endl;
				}
			}

			if ((unsigned int)(pos - pos_start) != data.length) {
				throw "wrong position after reading globalData.miscStats!";
			}

			data.data = static_cast<void*>(g0);
			break;
		}

		case 1: { //player location
			g1 = new playerLocation_t;
			pos_start = pos;
			g1->nextObjectId = readInt32();
			g1->worldspace1 = readRefID();
			g1->coorX = readInt32();
			g1->coorY = readInt32();
			g1->worldspace2 = readRefID();
			g1->posX = readFloat();
			g1->posY = readFloat();
			g1->posZ = readFloat();
			if (this->file.header.version == 9)
				g1->unknown = readUint8();

			if (DBG) {
				log << "player location:" << endl;
				log << "\tlength - " << data.length << endl;
				log << "\tnextObjectID - " << (unsigned int)g1->nextObjectId << endl;
				log << "\tworldspace1 - " << refIDtoString(g1->worldspace1) << endl;
				log << "\tcoorX - " << (unsigned int)g1->coorX << endl;
				log << "\tcoorY - " << (unsigned int)g1->coorY << endl;
				log << "\tworldspace2 - " << refIDtoString(g1->worldspace2) << endl;
				log << "\tposX - " << g1->posX << endl;
				log << "\tposY - " << g1->posY << endl;
				log << "\tposZ - " << g1->posZ << endl;
				if (this->file.header.version == 9) {
					log << "\tversion - " << this->file.header.version << endl;
					log << "\tunknown - " << (unsigned int)g1->unknown << endl << endl;
				}
			}

			if ((unsigned int)(pos - pos_start) != data.length) {
				throw "wrong position after reading globalData.playerLocation!";
			}

			data.data = static_cast<void*>(g1);
			break;
		}

		case 2: { //TES
			pos_start = pos;
			g2 = new tes_t;
			g2->count1 = readVsval();
			g2->unknown1 = new tes_t::unknown0_t[g2->count1];
			for (unsigned int i = 0; i < g2->count1; i++) {
				g2->unknown1[i].formID = readRefID();
				g2->unknown1[i].unknown = readUint16();
			}

			g2->count2 = readUint32();
			g2->unknown2 = new refID_t[g2->count2 * g2->count2];

			for (unsigned int i = 0; i < (g2->count2 * g2->count2); i++) {
				g2->unknown2[i] = readRefID();
			}

			g2->count3 = readVsval();
			g2->unknown3 = new refID_t[g2->count3];

			for (unsigned int i = 0; i < g2->count3; i++) {
				g2->unknown3[i] = readRefID();
			}

			if (DBG) {
				log << "TES:" << endl;
				log << "\tlength - " << data.length << endl;
				log << "\tcount1 - " << g2->count1 << endl;
				for (unsigned int i = 0; i < g2->count1; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\tformID - " << refIDtoString(g2->unknown1[i].formID) << endl;
					log << "\t\t\tunknown - " << (unsigned int)g2->unknown1[i].unknown << endl;
				}
				log << "\tcount2 - " << g2->count2 << endl;
				for (unsigned int i = 0; i < (g2->count2 * g2->count2); i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\tformID - " << refIDtoString(g2->unknown2[i]) << endl;
				}
				log << "\tcount3 - " << (unsigned int)g2->count3 << endl;
				for (unsigned int i = 0; i < g2->count3; i++) {
					log << "\t\tunknown3[" << i << "] - " << refIDtoString(g2->unknown3[i]) << endl;
				}
				log << endl;
			}

			if ((unsigned int)(pos - pos_start) != data.length) {
				throw "wrong position after reading globaldata.TES!";
			}

			data.data = static_cast<void*>(g2);
			break;
		}

		case 3: { //global variables
			g3 = new globalVariables;
			pos_start = pos;
			g3->count = readVsval();
			g3->globals = new globalVariables::globalVariable_t[g3->count];

			for (unsigned int i = 0; i < g3->count; i++) {
				g3->globals[i].formID = readRefID();
				g3->globals[i].value = readFloat();
			}

			if (DBG) {
				log << "global variables:" << endl;
				log << "\tlength - " << data.length << endl;
				log << "\tcount - " << (unsigned int)g3->count << endl;
				for (unsigned int i = 0; i < g3->count; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\tformID - " << refIDtoString(g3->globals[i].formID) << endl;
					log << "\t\t\tvalue - " << (unsigned int)g3->globals[i].value << endl;
				}
				log << endl;
			}

			if ((unsigned int)(pos - pos_start) != data.length) {
				throw "wrong position after reading globalData.globalVariables!";
			}

			data.data = static_cast<void*>(g3);
			break;
		}

		case 4: { //created objects
			g4 = new createdObjects;
			pos_start = pos;
			g4->weaponCount = readVsval();
			g4->weaponEnchTable = new createdObjects::enchantment_t[g4->weaponCount];

			for (unsigned int i = 0; i < g4->weaponCount; i++) {
				g4->weaponEnchTable[i].refID = readRefID();
				g4->weaponEnchTable[i].timesUsed = readUint32();
				g4->weaponEnchTable[i].count = readVsval();
				g4->weaponEnchTable[i].effects = new createdObjects::enchantment_t::magicEffect_t[g4->weaponEnchTable[i].count];

				for (unsigned int j = 0; j < g4->weaponEnchTable[i].count; j++) {
					g4->weaponEnchTable[i].effects[j].effectID = readRefID();
					g4->weaponEnchTable[i].effects[j].info.magnitude = readFloat();
					g4->weaponEnchTable[i].effects[j].info.duration = readUint32();
					g4->weaponEnchTable[i].effects[j].info.area = readUint32();
					g4->weaponEnchTable[i].effects[j].price = readFloat();
				}
			}

			g4->armourCount = readVsval();
			g4->armourEnchTable = new createdObjects::enchantment_t[g4->armourCount];

			for (unsigned int i = 0; i < g4->armourCount; i++) {
				g4->armourEnchTable[i].refID = readRefID();
				g4->armourEnchTable[i].timesUsed = readUint32();
				g4->armourEnchTable[i].count = readVsval();
				g4->armourEnchTable[i].effects = new createdObjects::enchantment_t::magicEffect_t[g4->armourEnchTable[i].count];

				for (unsigned int j = 0; j < g4->armourEnchTable[i].count; j++) {
					g4->armourEnchTable[i].effects[j].effectID = readRefID();
					g4->armourEnchTable[i].effects[j].info.magnitude = readFloat();
					g4->armourEnchTable[i].effects[j].info.duration = readUint32();
					g4->armourEnchTable[i].effects[j].info.area = readUint32();
					g4->armourEnchTable[i].effects[j].price = readFloat();
				}
			}

			g4->potionCount = readVsval();
			g4->potionTable = new createdObjects::enchantment_t[g4->potionCount];

			for (unsigned int i = 0; i < g4->potionCount; i++) {
				g4->potionTable[i].refID = readRefID();
				g4->potionTable[i].timesUsed = readUint32();
				g4->potionTable[i].count = readVsval();
				g4->potionTable[i].effects = new createdObjects::enchantment_t::magicEffect_t[g4->potionTable[i].count];

				for (unsigned int j = 0; j < g4->potionTable[i].count; j++) {
					g4->potionTable[i].effects[j].effectID = readRefID();
					g4->potionTable[i].effects[j].info.magnitude = readFloat();
					g4->potionTable[i].effects[j].info.duration = readUint32();
					g4->potionTable[i].effects[j].info.area = readUint32();
					g4->potionTable[i].effects[j].price = readFloat();
				}
			}

			g4->poisonCount = readVsval();
			g4->poisonTable = new createdObjects::enchantment_t[g4->poisonCount];

			for (unsigned int i = 0; i < g4->poisonCount; i++) {
				g4->poisonTable[i].refID = readRefID();
				g4->poisonTable[i].timesUsed = readUint32();
				g4->poisonTable[i].count = readVsval();
				g4->poisonTable[i].effects = new createdObjects::enchantment_t::magicEffect_t[g4->poisonTable[i].count];

				for (unsigned int j = 0; j < g4->poisonTable[i].count; j++) {
					g4->poisonTable[i].effects[j].effectID = readRefID();
					g4->poisonTable[i].effects[j].info.magnitude = readFloat();
					g4->poisonTable[i].effects[j].info.duration = readUint32();
					g4->poisonTable[i].effects[j].info.area = readUint32();
					g4->poisonTable[i].effects[j].price = readFloat();
				}
			}

			if (DBG) {
				log << "created objects:" << endl;
				log << "\tlength - " << data.length << endl;
				log << "\tweapons(" << g4->weaponCount << "):" << endl;
				for (unsigned int i = 0; i < g4->weaponCount; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\trefID - " << refIDtoString(g4->weaponEnchTable[i].refID) << endl;
					log << "\t\t\ttimesUsed - " << g4->weaponEnchTable[i].timesUsed << endl;
					log << "\t\t\teffects(" << g4->weaponEnchTable[i].count << "):" << endl;
					for (unsigned int j = 0; j < g4->weaponEnchTable[i].count; j++) {
						log << "\t\t\t\t" << j << ":" << endl;
						log << "\t\t\t\t\teffectID - " << refIDtoString(g4->weaponEnchTable[i].effects[j].effectID) << endl;
						log << "\t\t\t\t\tinfo.magnitude - " << g4->weaponEnchTable[i].effects[j].info.magnitude << endl;
						log << "\t\t\t\t\tinfo.duration - " << g4->weaponEnchTable[i].effects[j].info.duration << endl;
						log << "\t\t\t\t\tinfo.area - " << g4->weaponEnchTable[i].effects[j].info.area << endl;
						log << "\t\t\t\t\tprice - " << g4->weaponEnchTable[i].effects[j].price << endl;
					}
				}
				log << "\tarmours(" << g4->armourCount << "):" << endl;
				for (unsigned int i = 0; i < g4->armourCount; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\trefID - " << refIDtoString(g4->armourEnchTable[i].refID) << endl;
					log << "\t\t\ttimesUsed - " << g4->armourEnchTable[i].timesUsed << endl;
					log << "\t\t\teffects(" << g4->armourEnchTable[i].count << "):" << endl;
					for (unsigned int j = 0; j < g4->armourEnchTable[i].count; j++) {
						log << "\t\t\t\t" << j << ":" << endl;
						log << "\t\t\t\t\teffectID - " << refIDtoString(g4->armourEnchTable[i].effects[j].effectID) << endl;
						log << "\t\t\t\t\tinfo.magnitude - " << g4->armourEnchTable[i].effects[j].info.magnitude << endl;
						log << "\t\t\t\t\tinfo.duration - " << g4->armourEnchTable[i].effects[j].info.duration << endl;
						log << "\t\t\t\t\tinfo.area - " << g4->armourEnchTable[i].effects[j].info.area << endl;
						log << "\t\t\t\t\tprice - " << g4->armourEnchTable[i].effects[j].price << endl;
					}
				}
				log << "\tpotions(" << g4->potionCount << "):" << endl;
				for (unsigned int i = 0; i < g4->potionCount; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\trefID - " << refIDtoString(g4->potionTable[i].refID) << endl;
					log << "\t\t\ttimesUsed - " << g4->potionTable[i].timesUsed << endl;
					log << "\t\t\teffects(" << g4->potionTable[i].count << "):" << endl;
					for (unsigned int j = 0; j < g4->potionTable[i].count; j++) {
						log << "\t\t\t\t" << j << ":" << endl;
						log << "\t\t\t\t\teffectID - " << refIDtoString(g4->potionTable[i].effects[j].effectID) << endl;
						log << "\t\t\t\t\tinfo.magnitude - " << g4->potionTable[i].effects[j].info.magnitude << endl;
						log << "\t\t\t\t\tinfo.duration - " << g4->potionTable[i].effects[j].info.duration << endl;
						log << "\t\t\t\t\tinfo.area - " << g4->potionTable[i].effects[j].info.area << endl;
						log << "\t\t\t\t\tprice - " << g4->potionTable[i].effects[j].price << endl;
					}
				}
				log << "\tpoisons(" << g4->poisonCount << "):" << endl;
				for (unsigned int i = 0; i < g4->poisonCount; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\trefID - " << refIDtoString(g4->poisonTable[i].refID) << endl;
					log << "\t\t\ttimesUsed - " << g4->poisonTable[i].timesUsed << endl;
					log << "\t\t\teffects(" << g4->poisonTable[i].count << ")" << endl;
					for (unsigned int j = 0; j < g4->poisonTable[i].count; j++) {
						log << "\t\t\t\t" << j << ":" << endl;
						log << "\t\t\t\t\teffectID - " << refIDtoString(g4->poisonTable[i].effects[j].effectID) << endl;
						log << "\t\t\t\t\tinfo.magnitude - " << g4->poisonTable[i].effects[j].info.magnitude << endl;
						log << "\t\t\t\t\tinfo.duration - " << g4->poisonTable[i].effects[j].info.duration << endl;
						log << "\t\t\t\t\tinfo.area - " << g4->poisonTable[i].effects[j].info.area << endl;
						log << "\t\t\t\t\tprice - " << g4->poisonTable[i].effects[j].price << endl;
					}
				}
				log << endl;
			}

			if ((unsigned int)(pos - pos_start) != data.length) {
				throw "wrong position after reading globalData.createdObjects!";
			}

			data.data = static_cast<void*>(g4);
			break;
		}

		case 5: { //Effects
			pos_start = pos;
			g5 = new effects;
			g5->count = readVsval();
			g5->imageSpaceModifiers = new effects::effect_t[g5->count];

			for (unsigned int i = 0; i < g5->count; i++) {
				g5->imageSpaceModifiers[i].strength = readFloat();
				g5->imageSpaceModifiers[i].timestamp = readFloat();
				g5->imageSpaceModifiers[i].unknown = readUint32();
				g5->imageSpaceModifiers[i].effectID = readRefID();
			}

			g5->unknown1 = readFloat();
			g5->unknown2 = readFloat();

			if (DBG) {
				log << "effects:" << endl;
				log << "\tlength - " << data.length << endl;
				log << "\timage space modifiers(" << g5->count << "):" << endl;
				for (unsigned int i = 0; i < g5->count; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\tstrength - " << g5->imageSpaceModifiers[i].strength << endl;
					log << "\t\t\ttimestamp - " << g5->imageSpaceModifiers[i].timestamp << endl;
					log << "\t\t\tunknown - " << g5->imageSpaceModifiers[i].unknown << endl;
					log << "\t\t\teffectID - " << refIDtoString(g5->imageSpaceModifiers[i].effectID) << endl;
				}
				log << "\tunknown1 - " << g5->unknown1 << endl;
				log << "\tunknown2 - " << g5->unknown2 << endl << endl;
			}

			if ((unsigned int)(pos - pos_start) != data.length) {
				throw "wrong position after reading globaldata.effects!";
			}

			data.data = static_cast<void*>(g5);
			break;
		}

		case 6: { //weather
			if (DBG) {
				log << "weather:" << endl;
				log << "\tlength - " << data.length << endl;
				//log << "warning! not enough informations to process this data type!!" << endl;
			}
			//pos += data.length;
			//break;


			g6 = new weather_t;
			pos_start = pos;
			g6->climate = readRefID();
			g6->weather = readRefID();
			g6->prevWeather = readRefID();
			g6->unknownWeather1 = readRefID();
			g6->unknownWeather2 = readRefID();
			g6->regnWeather = readRefID();
			g6->curTime = readFloat();
			g6->begTime = readFloat();
			g6->weatherPct = readFloat();
			g6->unknown1 = readUint32();
			g6->unknown2 = readUint32();
			g6->unknown3 = readUint32();
			g6->unknown4 = readUint32();
			g6->unknown5 = readUint32();
			g6->unknown6 = readUint32();
			g6->unknown7 = readFloat();
			g6->unknown8 = readUint32();
			g6->flags = readUint8();
			//g6->unknown9 =
			//g6->unknown10 =
			if (DBG) {
				log << "\t\tclimate - " << refIDtoString(g6->climate) << endl;
				log << "\t\tweather - " << refIDtoString(g6->weather) << endl;
				log << "\t\tprevWeather - " << refIDtoString(g6->prevWeather) << endl;
				log << "\t\tunknownWeather1 - " << refIDtoString(g6->unknownWeather1) << endl;
				log << "\t\tunknownWeather2 - " << refIDtoString(g6->unknownWeather2) << endl;
				log << "\t\tregnWeather - " << refIDtoString(g6->regnWeather) << endl;
				log << "\t\tcurTime - " << g6->curTime << endl;
				log << "\t\tbegTime - " << g6->begTime << endl;
				log << "\t\tweatherPct - " << g6->weatherPct << endl;
				log << "\t\tunknown1 - " << g6->unknown1 << endl;
				log << "\t\tunknown2 - " << g6->unknown2 << endl;
				log << "\t\tunknown3 - " << g6->unknown3 << endl;
				log << "\t\tunknown4 - " << g6->unknown4 << endl;
				log << "\t\tunknown5 - " << g6->unknown5 << endl;
				log << "\t\tunknown6 - " << g6->unknown6 << endl;
				log << "\t\tunknown7 - " << g6->unknown7 << endl;
				log << "\t\tunknown8 - " << g6->unknown8 << endl;
				log << "\t\tflags - " << (int)g6->flags << endl;
				//log << "\t\tunknown9 - " << g6->unknown9 << endl;
				//log << "\t\tunknown10 - " << g6->unknown10 << endl;
				log << endl;
			}

			if (((g6->flags | 0b11111110) == 0b11111111) || ((g6->flags | 0b11111101) == 0b11111111)) {
				if (DBG) {
					log << "\t\tcurrent position: " << hex << pos << endl;
					log << "\t\tend of data: " << hex << data.length << endl;
				}
				throw "unknown variable!";
			}

			if ((unsigned int)(pos - pos_start) != data.length) {
				throw "wrong position after reading globaldata.weather!";
			}
			data.data = static_cast<void*>(g6);
			break;
		}

		case 7: { //audio
			g7 = new audio_t;
			pos_start = pos;
			g7->unknown = readRefID();
			g7->tracksCount = readVsval();
			g7->tracks = new refID_t[g7->tracksCount];

			for (unsigned int i = 0; i < g7->tracksCount; i++) {
				g7->tracks[i] = readRefID();
			}

			g7->bgm = readRefID();

			if (DBG) {
				log << "audio:" << endl;
				log << "\tlength - " << data.length << endl;
				log << "\tunknown - " << refIDtoString(g7->unknown) << endl;
				log << "\ttracks(" << g7->tracksCount << "):" << endl;
				for (unsigned int i = 0; i < g7->tracksCount; i++) {
					log << "\t" << i << ":" << endl;
					log << "\t\t" << refIDtoString(g7->tracks[i]) << endl;
				}
				log << "\tbgm - " << refIDtoString(g7->bgm) << endl;
				log << endl;
			}

			if ((unsigned int)(pos - pos_start) != data.length) {
				throw "wrong position after reading globaldata.audio!";
			}

			data.data = static_cast<void*>(g7);
			break;
		}

		case 8: { //skyCells
			g8 = new skyCells;
			pos_start = pos;
			g8->count = readVsval();
			g8->unknown = new skyCells::unknown0_t[g8->count];

			for (unsigned int i = 0; i < g8->count; i++) {
				g8->unknown[i].unknown1 = readRefID();
				g8->unknown[i].unknown2 = readRefID();
			}

			if (DBG) {
				log << "skyCells:" << endl;
				log << "\tlength - " << data.length << endl;
				log << "\tunknown(" << g8->count << "):" << endl;
				for (unsigned int i = 0; i < g8->count; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\tunknown1 - " << refIDtoString(g8->unknown[i].unknown1) << endl;
					log << "\t\t\tunknown2 - " << refIDtoString(g8->unknown[i].unknown2) << endl;
				}
				log << endl;
			}

			if ((unsigned int)(pos - pos_start) != data.length) {
				throw "wrong position after reading globaldata.skyCells!";
			}

			data.data = static_cast<void*>(g8);
			break;
		}

		case 100: { //process lists
			g100 = new processList_t;
			pos_start = pos;
			g100->unknown1 = readFloat();
			g100->unknown2 = readFloat();
			g100->unknown3 = readFloat();
			g100->nextNum = readUint32();

			for (int i = 0; i < 7; i++) {
				g100->allCrimes[i].count = readVsval();
				g100->allCrimes[i].crimes = new processList_t::crimeType::crime_t[g100->allCrimes[i].count];

				for (unsigned int j = 0; j < g100->allCrimes[i].count; j++) {
					g100->allCrimes[i].crimes[j].witnessNum = readUint32();
					g100->allCrimes[i].crimes[j].crimeType = readUint32();
					g100->allCrimes[i].crimes[j].unknown1 = readUint8();
					g100->allCrimes[i].crimes[j].quantity = readUint32();
					g100->allCrimes[i].crimes[j].serialNum = readUint32();
					g100->allCrimes[i].crimes[j].unknown2 = readUint8();
					g100->allCrimes[i].crimes[j].unknown3 = readUint32();
					g100->allCrimes[i].crimes[j].elapsedTime = readFloat();
					g100->allCrimes[i].crimes[j].victimID = readRefID();
					g100->allCrimes[i].crimes[j].criminalID = readRefID();
					g100->allCrimes[i].crimes[j].itemBaseID = readRefID();
					g100->allCrimes[i].crimes[j].ownershipID = readRefID();
					g100->allCrimes[i].crimes[j].count = readVsval();
					g100->allCrimes[i].crimes[j].witnesses = new refID_t[g100->allCrimes[i].crimes[j].count];

					for (unsigned int x = 0; x < g100->allCrimes[i].crimes[j].count; x++) {
						g100->allCrimes[i].crimes[j].witnesses[x] = readRefID();
					}
					g100->allCrimes[i].crimes[j].bounty = readUint32();
					g100->allCrimes[i].crimes[j].crimeFactionID = readRefID();
					g100->allCrimes[i].crimes[j].isCleared = readUint8();
					g100->allCrimes[i].crimes[j].unknown4 = readUint16();
				}
			}

			if (DBG) {
				log << "process lists:" << endl;
				log << "\tlength - " << data.length << endl;
				log << "\tunknown1 - " << g100->unknown1 << endl;
				log << "\tunknown2 - " << g100->unknown2 << endl;
				log << "\tunknown3 - " << g100->unknown3 << endl;
				log << "\tnextNum - " << g100->nextNum << endl;
				log << "\tall crimes(7):" << endl;
				for (int i = 0; i < 7; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\tcrimes(" << g100->allCrimes[i].count << "):" << endl;
					for (unsigned int j = 0; j < g100->allCrimes[i].count; j++) {
						log << "\t\t\t\t" << j << ":" << endl;
						log << "\t\t\t\t\twitnessNum - " << (unsigned int)g100->allCrimes[i].crimes[j].witnessNum << endl;
						log << "\t\t\t\t\tcrimeType - ";
						switch (g100->allCrimes[i].crimes[j].crimeType) {
						case 0:
							log << "theft" << endl;
							break;
						case 1:
							log << "pickpocket" << endl;
							break;
						case 2:
							log << "trespassing" << endl;
							break;
						case 3:
							log << "assault" << endl;
							break;
						case 4:
							log << "murder" << endl;
							break;
						case 5:
							log << "unknown(vampirism??)" << endl;
							break;
						case 6:
							log << "lycanthropy" << endl;
							break;
						}
						log << "\t\t\t\t\tunknown1 - " << (unsigned int)g100->allCrimes[i].crimes[j].unknown1 << endl;
						log << "\t\t\t\t\tquantity - " << (unsigned int)g100->allCrimes[i].crimes[j].quantity << endl;
						log << "\t\t\t\t\tserialNum - " << (unsigned int)g100->allCrimes[i].crimes[j].serialNum << endl;
						log << "\t\t\t\t\tunknown2 - " << (unsigned int)g100->allCrimes[i].crimes[j].unknown2 << endl;
						log << "\t\t\t\t\tunknown3 - " << (unsigned int)g100->allCrimes[i].crimes[j].unknown3 << endl;
						log << "\t\t\t\t\telapsedTime - " << (unsigned int)g100->allCrimes[i].crimes[j].elapsedTime << endl;
						log << "\t\t\t\t\tvictimID - " << refIDtoString(g100->allCrimes[i].crimes[j].victimID) << endl;
						log << "\t\t\t\t\tcriminalID - " << refIDtoString(g100->allCrimes[i].crimes[j].criminalID) << endl;
						log << "\t\t\t\t\titemBaseID - " << refIDtoString(g100->allCrimes[i].crimes[j].itemBaseID) << endl;
						log << "\t\t\t\t\townershipID - " << refIDtoString(g100->allCrimes[i].crimes[j].ownershipID) << endl;
						log << "\t\t\t\t\twitnesses(" << (unsigned int)g100->allCrimes[i].crimes[j].count << "):" << endl;
						for (unsigned int x = 0; x < g100->allCrimes[i].crimes[j].count; x++) {
							log << "\t\t\t\t\t\t" << x << ":" << endl;
							log << "\t\t\t\t\t\t\t" << refIDtoString(g100->allCrimes[i].crimes[j].witnesses[x]) << endl;
						}
						log << "\t\t\t\t\tbounty - " << (unsigned int)g100->allCrimes[i].crimes[j].bounty << endl;
						log << "\t\t\t\t\tcrimeFactionID - " << refIDtoString(g100->allCrimes[i].crimes[j].crimeFactionID) << endl;
						log << "\t\t\t\t\tisCleared - " << (unsigned int)g100->allCrimes[i].crimes[j].isCleared << endl;
						log << "\t\t\t\t\tunknown4 - " << (unsigned int)g100->allCrimes[i].crimes[j].unknown4 << endl;
					}
				}
				log << endl;
			}

			if ((unsigned int)(pos - pos_start) != data.length) {
				throw "wrong position after reading globaldata.actorCauses! (interface.unknown3 not present?)";
			}

			data.data = static_cast<void*>(g100);
			break;
		}

		case 101: { //combat
			pos_start = pos;
			g101 = new uint8_t[data.length];
			for (unsigned int i = 0; i < data.length; i++) {
				g101[i] = readByte();
			}

			/*g101 = new combat;

			g101->nextNum = readUint32();
			g101->count0 = readVsval();
			g101->unknown0 = new combat::unknown0_t[g101->count0];

			for (unsigned int i = 0; i < g101->count0; i++) {
				g101->unknown0[i].unknown1 = readUint32();
				g101->unknown0[i].serialNum = readUint32();
				g101->unknown0[i].unknown2.count0 = readVsval();
				g101->unknown0[i].unknown2.unknown0 = new combat::unknown0_0_t::unknown0_0_0_t[g101->unknown0[i].unknown2.count0];
				for (unsigned int j = 0; j < g101->unknown0[i].unknown2.count0; j++) {
					g101->unknown0[i].unknown2.unknown0[j].unknown1 = readRefID();
					g101->unknown0[i].unknown2.unknown0[j].unknown2 = readUint32();
					g101->unknown0[i].unknown2.unknown0[j].unknown3 = readFloat();
					g101->unknown0[i].unknown2.unknown0[j].unknown4 = readUint16();
					g101->unknown0[i].unknown2.unknown0[j].unknown5 = readUint16();
					g101->unknown0[i].unknown2.unknown0[j].target = readRefID();
					g101->unknown0[i].unknown2.unknown0[j].unknown6.x = readFloat();
					g101->unknown0[i].unknown2.unknown0[j].unknown6.y = readFloat();
					g101->unknown0[i].unknown2.unknown0[j].unknown6.z = readFloat();
					g101->unknown0[i].unknown2.unknown0[j].unknown6.cellID = readRefID();
					g101->unknown0[i].unknown2.unknown0[j].unknown7.x = readFloat();
					g101->unknown0[i].unknown2.unknown0[j].unknown7.y = readFloat();
					g101->unknown0[i].unknown2.unknown0[j].unknown7.z = readFloat();
					g101->unknown0[i].unknown2.unknown0[j].unknown7.cellID = readRefID();
					g101->unknown0[i].unknown2.unknown0[j].unknown8.x = readFloat();
					g101->unknown0[i].unknown2.unknown0[j].unknown8.y = readFloat();
					g101->unknown0[i].unknown2.unknown0[j].unknown8.z = readFloat();
					g101->unknown0[i].unknown2.unknown0[j].unknown8.cellID = readRefID();
					g101->unknown0[i].unknown2.unknown0[j].unknown9.x = readFloat();
					g101->unknown0[i].unknown2.unknown0[j].unknown9.y = readFloat();
					g101->unknown0[i].unknown2.unknown0[j].unknown9.z = readFloat();
					g101->unknown0[i].unknown2.unknown0[j].unknown9.cellID = readRefID();
					g101->unknown0[i].unknown2.unknown0[j].unknown10.x = readFloat();
					g101->unknown0[i].unknown2.unknown0[j].unknown10.y = readFloat();
					g101->unknown0[i].unknown2.unknown0[j].unknown10.z = readFloat();
					g101->unknown0[i].unknown2.unknown0[j].unknown10.cellID = readRefID();
					g101->unknown0[i].unknown2.unknown0[j].unknown11 = readFloat();
					g101->unknown0[i].unknown2.unknown0[j].unknown12 = readFloat();
					g101->unknown0[i].unknown2.unknown0[j].unknown13 = readFloat();
					g101->unknown0[i].unknown2.unknown0[j].unknown14 = readFloat();
					g101->unknown0[i].unknown2.unknown0[j].unknown15 = readFloat();
					g101->unknown0[i].unknown2.unknown0[j].unknown16 = readFloat();
				}
				g101->unknown0[i].unknown2.count1 = readVsval();
				g101->unknown0[i].unknown2.unknown1 = new combat::unknown0_0_t::unknown0_0_1_t[g101->unknown0[i].unknown2.count1];
				for (unsigned int j = 0; j < g101->unknown0[i].unknown2.count1; j++) {
					g101->unknown0[i].unknown2.unknown1[j].unknown1 = readRefID();
					g101->unknown0[i].unknown2.unknown1[j].unknown2 = readFloat();
					g101->unknown0[i].unknown2.unknown1[j].unknown3 = readFloat();
				}

				g101->unknown0[i].unknown2.unknown2.unknown1 = readFloat();
				g101->unknown0[i].unknown2.unknown2.unknown2 = readFloat();
				g101->unknown0[i].unknown2.unknown3.unknown1 = readFloat();
				g101->unknown0[i].unknown2.unknown3.unknown2 = readFloat();
				g101->unknown0[i].unknown2.unknown4.unknown1 = readFloat();
				g101->unknown0[i].unknown2.unknown4.unknown2 = readFloat();
				g101->unknown0[i].unknown2.unknown5.unknown1 = readFloat();
				g101->unknown0[i].unknown2.unknown5.unknown2 = readFloat();
				for (unsigned int j = 0; j < 11; j++) {
					g101->unknown0[i].unknown2.unknown6[j].unknown1 = readFloat();
					g101->unknown0[i].unknown2.unknown6[j].unknown2 = readFloat();
				}
				g101->unknown0[i].unknown2.unknownFlag = readUint32();
				if (g101->unknown0[i].unknown2.unknownFlag != 0) {
					g101->unknown0[i].unknown2.unknown7.unknown1 = readRefID();
					g101->unknown0[i].unknown2.unknown7.unknown2.unknown1 = readFloat();
					g101->unknown0[i].unknown2.unknown7.unknown2.unknown2 = readFloat();
					g101->unknown0[i].unknown2.unknown7.unknown3.unknown1 = readFloat();
					g101->unknown0[i].unknown2.unknown7.unknown3.unknown2 = readFloat();
					g101->unknown0[i].unknown2.unknown7.unknown4 = readFloat();
					g101->unknown0[i].unknown2.unknown7.unknown5.x = readFloat();
					g101->unknown0[i].unknown2.unknown7.unknown5.y = readFloat();
					g101->unknown0[i].unknown2.unknown7.unknown5.z = readFloat();
					g101->unknown0[i].unknown2.unknown7.unknown5.cellID = readRefID();
					g101->unknown0[i].unknown2.unknown7.unknown6 = readFloat();
					g101->unknown0[i].unknown2.unknown7.count0 = readVsval();
					g101->unknown0[i].unknown2.unknown7.unknown7 = new combat::unknown0_0_t::unknown0_0_2_t::unknown0_0_2_0_t[g101->unknown0[i].unknown2.unknown7.count0];
					for (unsigned int j = 0; j < g101->unknown0[i].unknown2.unknown7.count0; j++) {
						g101->unknown0[i].unknown2.unknown7.unknown7[j].unknown1.x = readFloat();
						g101->unknown0[i].unknown2.unknown7.unknown7[j].unknown1.y = readFloat();
						g101->unknown0[i].unknown2.unknown7.unknown7[j].unknown1.z = readFloat();
						g101->unknown0[i].unknown2.unknown7.unknown7[j].unknown1.cellID = readRefID();
						g101->unknown0[i].unknown2.unknown7.unknown7[j].unknown2 = readUint32();
						g101->unknown0[i].unknown2.unknown7.unknown7[j].unknown3 = readFloat();
					}
					g101->unknown0[i].unknown2.unknown7.count1 = readVsval();
					g101->unknown0[i].unknown2.unknown7.unknown8 = new combat::unknown0_0_t::unknown0_0_2_t::unknown0_0_2_1_t[g101->unknown0[i].unknown2.unknown7.count1];
					for (unsigned int j = 0; j < g101->unknown0[i].unknown2.unknown7.count1; j++) {
						g101->unknown0[i].unknown2.unknown7.unknown8[j].unknown1 = readRefID();
						g101->unknown0[i].unknown2.unknown7.unknown8[j].unknown2 = readRefID();
						g101->unknown0[i].unknown2.unknown7.unknown8[j].unknown3 = readUint8();
						g101->unknown0[i].unknown2.unknown7.unknown8[j].unknown4 = readUint8();
						g101->unknown0[i].unknown2.unknown7.unknown8[j].unknown5 = readUint8();
					}
					g101->unknown0[i].unknown2.unknown7.unknownFlag = readUint8();
					if (g101->unknown0[i].unknown2.unknown7.unknownFlag != 0) {
						g101->unknown0[i].unknown2.unknown7.unknown9.unknown1 = readUint32();
						g101->unknown0[i].unknown2.unknown7.unknown9.unknown2 = readUint32();
						g101->unknown0[i].unknown2.unknown7.unknown9.count0 = readUint32();
						g101->unknown0[i].unknown2.unknown7.unknown9.unknown3 = new combat::unknown0_0_t::unknown0_0_2_t::unknown0_0_2_2_t::unknown0_0_2_2_0_t[g101->unknown0[i].unknown2.unknown7.unknown9.count0];
						for (unsigned int j = 0; j < g101->unknown0[i].unknown2.unknown7.unknown9.count0; j++) {
							g101->unknown0[i].unknown2.unknown7.unknown9.unknown3[j].unknown1 = readUint8();
							g101->unknown0[i].unknown2.unknown7.unknown9.unknown3[j].count0 = readUint32();
							g101->unknown0[i].unknown2.unknown7.unknown9.unknown3[j].unknown2 = new uint8_t[g101->unknown0[i].unknown2.unknown7.unknown9.unknown3[j].count0];
							for (unsigned int k = 0; k < g101->unknown0[i].unknown2.unknown7.unknown9.unknown3[j].count0; k++) {
								g101->unknown0[i].unknown2.unknown7.unknown9.unknown3[j].unknown2[k] = readUint8();
							}
							g101->unknown0[i].unknown2.unknown7.unknown9.unknown3[j].unknown3 = readRefID();
							g101->unknown0[i].unknown2.unknown7.unknown9.unknown3[j].unknown4 = readUint32();
						}
						g101->unknown0[i].unknown2.unknown7.unknown9.unknown4 = readRefID();
						g101->unknown0[i].unknown2.unknown7.unknown9.unknown5 = readFloat();
						g101->unknown0[i].unknown2.unknown7.unknown9.unknown6 = readFloat();
						g101->unknown0[i].unknown2.unknown7.unknown9.unknown7 = readFloat();
						g101->unknown0[i].unknown2.unknown7.unknown9.unknown8 = readFloat();
						g101->unknown0[i].unknown2.unknown7.unknown9.unknown9 = readFloat();
						g101->unknown0[i].unknown2.unknown7.unknown9.unknown10 = readRefID();
						g101->unknown0[i].unknown2.unknown7.unknown9.unknown11 = readFloat();
						g101->unknown0[i].unknown2.unknown7.unknown9.unknown12 = readRefID();
						g101->unknown0[i].unknown2.unknown7.unknown9.unknown13 = readUint16();
						g101->unknown0[i].unknown2.unknown7.unknown9.unknown14 = readUint8();
						g101->unknown0[i].unknown2.unknown7.unknown9.unknown15 = readUint8();
						g101->unknown0[i].unknown2.unknown7.unknown9.unknown16 = readFloat();
						g101->unknown0[i].unknown2.unknown7.unknown9.unknown17 = readFloat();
					}
				}
				g101->unknown0[i].unknown2.unknown8.unknown1 = readFloat();
				g101->unknown0[i].unknown2.unknown8.unknown2 = readFloat();
				g101->unknown0[i].unknown2.unknown9 = readFloat();
				g101->unknown0[i].unknown2.unknown10 = readFloat();
				g101->unknown0[i].unknown2.unknown11 = readFloat();
				g101->unknown0[i].unknown2.unknown12 = readFloat();
				g101->unknown0[i].unknown2.unknown13.unknown1 = readFloat();
				g101->unknown0[i].unknown2.unknown13.unknown2 = readFloat();
				g101->unknown0[i].unknown2.unknown14 = readUint8();
			}
			g101->count1 = readVsval();
			g101->unknown1 = new combat::unknown1_t[g101->count1];
			for (unsigned int i = 0; i < g101->count1; i++) {
				g101->unknown1[i].unknown1 = readRefID();
				g101->unknown1[i].unknown2 = readFloat();
				g101->unknown1[i].unknown3 = readRefID();
				g101->unknown1[i].unknown4 = readRefID();
				g101->unknown1[i].unknown5 = readFloat();
				g101->unknown1[i].unknown6 = readFloat();
				g101->unknown1[i].unknown7 = readFloat();
				g101->unknown1[i].x = readFloat();
				g101->unknown1[i].y = readFloat();
				g101->unknown1[i].z = readFloat();
				g101->unknown1[i].unknown8 = readFloat();
				g101->unknown1[i].unknown9 = readFloat();
				g101->unknown1[i].unknown10 = readFloat();
				g101->unknown1[i].unknown11 = readFloat();
				g101->unknown1[i].unknown12 = readFloat();
				g101->unknown1[i].unknown13 = readFloat();
				g101->unknown1[i].unknown14 = readFloat();
				g101->unknown1[i].unknown15 = readFloat();
				g101->unknown1[i].unknown16 = readRefID();
			}
			g101->unknown2 = readFloat();
			g101->unknown3 = readVsval();
			g101->count2 = readVsval();
			g101->unknown4 = new refID_t[g101->count2];
			for (unsigned int i = 0; i < g101->count2; i++) {
				g101->unknown4[i] = readRefID();
			}
			g101->unknown5 = readFloat();
			g101->unknown6.unknown1 = readFloat();
			g101->unknown6.unknown2 = readFloat();
			g101->unknown7.unknown1 = readFloat();
			g101->unknown7.unknown2 = readFloat();

			if (DBG) {
				log << "combat:" << endl;
				log << "\tlength - " << data.length << endl;
				log << "\tnextnum - " << g101->nextNum << endl;
				log << "\tunknown0(" << g101->count0 << "):" << endl;
				for (unsigned int i = 0; i < g101->count0; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\tunknown1 - " << g101->unknown0[i].unknown1 << endl;
					log << "\t\t\tunknown2(" << g101->unknown0[i].serialNum << "):" << endl;
					for (unsigned int j = 0; j < g101->unknown0[i].unknown2.count0; j++) {
						log << "\t\t\t\t" << j << ":" << endl;
						log << "\t\t\t\t\tunknown1 - " << refIDtoString(g101->unknown0[i].unknown2.unknown0[j].unknown1) << endl;
						log << "\t\t\t\t\tunknown2 - " << g101->unknown0[i].unknown2.unknown0[j].unknown2 << endl;
						log << "\t\t\t\t\tunknown3 - " << g101->unknown0[i].unknown2.unknown0[j].unknown3 << endl;
						log << "\t\t\t\t\tunknown4 - " << g101->unknown0[i].unknown2.unknown0[j].unknown4 << endl;
						log << "\t\t\t\t\tunknown5 - " << g101->unknown0[i].unknown2.unknown0[j].unknown5 << endl;
						log << "\t\t\t\t\ttarget - " << refIDtoString(g101->unknown0[i].unknown2.unknown0[j].target) << endl;
						log << "\t\t\t\t\tunknown6.x - " << g101->unknown0[i].unknown2.unknown0[j].unknown6.x << endl;
						log << "\t\t\t\t\tunknown6.y - " << g101->unknown0[i].unknown2.unknown0[j].unknown6.y << endl;
						log << "\t\t\t\t\tunknown6.z - " << g101->unknown0[i].unknown2.unknown0[j].unknown6.z << endl;
						log << "\t\t\t\t\tunknown6.cellID - " << refIDtoString(g101->unknown0[i].unknown2.unknown0[j].unknown6.cellID) << endl;
						log << "\t\t\t\t\tunknown7.x - " << g101->unknown0[i].unknown2.unknown0[j].unknown7.x << endl;
						log << "\t\t\t\t\tunknown7.y - " << g101->unknown0[i].unknown2.unknown0[j].unknown7.y << endl;
						log << "\t\t\t\t\tunknown7.z - " << g101->unknown0[i].unknown2.unknown0[j].unknown7.z << endl;
						log << "\t\t\t\t\tunknown7.cellID - " << refIDtoString(g101->unknown0[i].unknown2.unknown0[j].unknown7.cellID) << endl;
						log << "\t\t\t\t\tunknown8.x - " << g101->unknown0[i].unknown2.unknown0[j].unknown8.x << endl;
						log << "\t\t\t\t\tunknown8.y - " << g101->unknown0[i].unknown2.unknown0[j].unknown8.y << endl;
						log << "\t\t\t\t\tunknown8.z - " << g101->unknown0[i].unknown2.unknown0[j].unknown8.z << endl;
						log << "\t\t\t\t\tunknown8.cellID - " << refIDtoString(g101->unknown0[i].unknown2.unknown0[j].unknown8.cellID) << endl;
						log << "\t\t\t\t\tunknown9.x - " << g101->unknown0[i].unknown2.unknown0[j].unknown9.x << endl;
						log << "\t\t\t\t\tunknown9.y - " << g101->unknown0[i].unknown2.unknown0[j].unknown9.y << endl;
						log << "\t\t\t\t\tunknown9.z - " << g101->unknown0[i].unknown2.unknown0[j].unknown9.z << endl;
						log << "\t\t\t\t\tunknown9.cellID - " << refIDtoString(g101->unknown0[i].unknown2.unknown0[j].unknown9.cellID) << endl;
						log << "\t\t\t\t\tunknown10.x - " << g101->unknown0[i].unknown2.unknown0[j].unknown10.x << endl;
						log << "\t\t\t\t\tunknown10.y - " << g101->unknown0[i].unknown2.unknown0[j].unknown10.y << endl;
						log << "\t\t\t\t\tunknown10.z - " << g101->unknown0[i].unknown2.unknown0[j].unknown10.z << endl;
						log << "\t\t\t\t\tunknown10.cellID - " << refIDtoString(g101->unknown0[i].unknown2.unknown0[j].unknown10.cellID) << endl;
						log << "\t\t\t\t\tunknown11 - " << g101->unknown0[i].unknown2.unknown0[j].unknown11 << endl;
						log << "\t\t\t\t\tunknown12 - " << g101->unknown0[i].unknown2.unknown0[j].unknown12 << endl;
						log << "\t\t\t\t\tunknown13 - " << g101->unknown0[i].unknown2.unknown0[j].unknown13 << endl;
						log << "\t\t\t\t\tunknown14 - " << g101->unknown0[i].unknown2.unknown0[j].unknown14 << endl;
						log << "\t\t\t\t\tunknown15 - " << g101->unknown0[i].unknown2.unknown0[j].unknown15 << endl;
						log << "\t\t\t\t\tunknown16 - " << g101->unknown0[i].unknown2.unknown0[j].unknown16 << endl;
					}
					log << "\t\t\tcount1 - " << g101->unknown0[i].unknown2.count1 << endl;
					for (unsigned int j = 0; j < g101->unknown0[i].unknown2.count1; j++) {
						log << "\t\t\t\t" << j << ":" << endl;
						log << "\t\t\t\t\tunknown1 - " << refIDtoString(g101->unknown0[i].unknown2.unknown1[j].unknown1) << endl;
						log << "\t\t\t\t\tunknown2 - " << g101->unknown0[i].unknown2.unknown1[j].unknown2 << endl;
						log << "\t\t\t\t\tunknown3 - " << g101->unknown0[i].unknown2.unknown1[j].unknown3 << endl;
					}
					log << "\t\t\tunknown2.unknown2.1 - " << g101->unknown0[i].unknown2.unknown2.unknown1 << endl;
					log << "\t\t\tunknown2.unknown2.2 - " << g101->unknown0[i].unknown2.unknown2.unknown2 << endl;
					log << "\t\t\tunknown2.unknown3.1 - " << g101->unknown0[i].unknown2.unknown3.unknown1 << endl;
					log << "\t\t\tunknown2.unknown3.2 - " << g101->unknown0[i].unknown2.unknown3.unknown2 << endl;
					log << "\t\t\tunknown2.unknown4.1 - " << g101->unknown0[i].unknown2.unknown4.unknown1 << endl;
					log << "\t\t\tunknown2.unknown4.2 - " << g101->unknown0[i].unknown2.unknown4.unknown2 << endl;
					log << "\t\t\tunknown2.unknown5.1 - " << g101->unknown0[i].unknown2.unknown5.unknown1 << endl;
					log << "\t\t\tunknown2.unknown5.2 - " << g101->unknown0[i].unknown2.unknown5.unknown2 << endl;
					for (unsigned int j = 0; j < 11; j++) {
						log << "\t\t\t\t" << j << ":" << endl;
						log << "\t\t\t\t\tunknown1 - " << g101->unknown0[i].unknown2.unknown6[j].unknown1 << endl;
						log << "\t\t\t\t\tunknown2 - " << g101->unknown0[i].unknown2.unknown6[j].unknown2 << endl;
					}
					log << "\t\t\tflag - " << g101->unknown0[i].unknown2.unknownFlag << endl;
					if (g101->unknown0[i].unknown2.unknownFlag != 0) {
						log << "\t\t\tunknown2.unknown7.unknown1 - " << refIDtoString(g101->unknown0[i].unknown2.unknown7.unknown1) << endl;
						log << "\t\t\tunknown2.unknown7.unknown2.1 - " << g101->unknown0[i].unknown2.unknown7.unknown2.unknown1 << endl;
						log << "\t\t\tunknown2.unknown7.unknown2.2 - " << g101->unknown0[i].unknown2.unknown7.unknown2.unknown2 << endl;
						log << "\t\t\tunknown2.unknown7.unknown3.1 - " << g101->unknown0[i].unknown2.unknown7.unknown3.unknown1 << endl;
						log << "\t\t\tunknown2.unknown7.unknown3.2 - " << g101->unknown0[i].unknown2.unknown7.unknown3.unknown2 << endl;
						log << "\t\t\tunknown2.unknown7.unknown4 - " << g101->unknown0[i].unknown2.unknown7.unknown4 << endl;
						log << "\t\t\tunknown2.unknown7.unknown5.x - " << g101->unknown0[i].unknown2.unknown7.unknown5.x << endl;
						log << "\t\t\tunknown2.unknown7.unknown5.y - " << g101->unknown0[i].unknown2.unknown7.unknown5.y << endl;
						log << "\t\t\tunknown2.unknown7.unknown5.z - " << g101->unknown0[i].unknown2.unknown7.unknown5.z << endl;
						log << "\t\t\tunknown2.unknown7.unknown5.cellID - " << refIDtoString(g101->unknown0[i].unknown2.unknown7.unknown5.cellID) << endl;
						log << "\t\t\tunknown2.unknown7.unknown6 - " << g101->unknown0[i].unknown2.unknown7.unknown6 << endl;
						log << "\t\t\tunknown2.unknown7.count0 - " << g101->unknown0[i].unknown2.unknown7.count0 << endl;
						for (unsigned int j = 0; j < g101->unknown0[i].unknown2.unknown7.count0; j++) {
							log << "\t\t\t\t" << j << ":" << endl;
							log << "\t\t\t\t\tunknown1.x - " << g101->unknown0[i].unknown2.unknown7.unknown7[j].unknown1.x << endl;
							log << "\t\t\t\t\tunknown1.y - " << g101->unknown0[i].unknown2.unknown7.unknown7[j].unknown1.y << endl;
							log << "\t\t\t\t\tunknown1.z - " << g101->unknown0[i].unknown2.unknown7.unknown7[j].unknown1.z << endl;
							log << "\t\t\t\t\tunknown1.cellID - " << refIDtoString(g101->unknown0[i].unknown2.unknown7.unknown7[j].unknown1.cellID) << endl;
							log << "\t\t\t\t\tunknown2 - " << g101->unknown0[i].unknown2.unknown7.unknown7[j].unknown2 << endl;
							log << "\t\t\t\t\tunknown3 - " << g101->unknown0[i].unknown2.unknown7.unknown7[j].unknown3 << endl;
						}
						log << "\t\t\tunknown2.unknown7.count1 - " << g101->unknown0[i].unknown2.unknown7.count1 << endl;
						for (unsigned int j = 0; j < g101->unknown0[i].unknown2.unknown7.count1; j++) {
							log << "\t\t\t\t" << j << ":" << endl;
							log << "\t\t\t\t\tunknown1 - " << refIDtoString(g101->unknown0[i].unknown2.unknown7.unknown8[j].unknown1) << endl;
							log << "\t\t\t\t\tunknown2 - " << refIDtoString(g101->unknown0[i].unknown2.unknown7.unknown8[j].unknown2) << endl;
							log << "\t\t\t\t\tunknown3 - " << (int)g101->unknown0[i].unknown2.unknown7.unknown8[j].unknown3 << endl;
							log << "\t\t\t\t\tunknown4 - " << (int)g101->unknown0[i].unknown2.unknown7.unknown8[j].unknown4 << endl;
							log << "\t\t\t\t\tunknown5 - " << (int)g101->unknown0[i].unknown2.unknown7.unknown8[j].unknown5 << endl;
						}
						log << "\t\t\tunknown7.unknownFlag - " << g101->unknown0[i].unknown2.unknown7.unknownFlag << endl;
						if (g101->unknown0[i].unknown2.unknown7.unknownFlag != 0) {
							log << "\t\t\tunknown7.unknown9.1 - " << g101->unknown0[i].unknown2.unknown7.unknown9.unknown1 << endl;
							log << "\t\t\tunknown7.unknown9.2 - " << g101->unknown0[i].unknown2.unknown7.unknown9.unknown2 << endl;
							log << "\t\t\tunknown7.unknown9.count0 - " << g101->unknown0[i].unknown2.unknown7.unknown9.count0 << endl;
							for (unsigned int j = 0; j < g101->unknown0[i].unknown2.unknown7.unknown9.count0; j++) {
								log << "\t\t\t\t" << j << ":" << endl;
								log << "\t\t\t\t\tunknown1" << (int)g101->unknown0[i].unknown2.unknown7.unknown9.unknown3[j].unknown1 << endl;
								log << "\t\t\t\t\tcount0" << g101->unknown0[i].unknown2.unknown7.unknown9.unknown3[j].count0 << endl;
								for (unsigned int k = 0; k < g101->unknown0[i].unknown2.unknown7.unknown9.unknown3[j].count0; k++) {
									log << "\t\t\t\t\t\t" << k << ":" << endl;
									log << "\t\t\t\t\t\t\t" << (int)g101->unknown0[i].unknown2.unknown7.unknown9.unknown3[j].unknown2[k] << endl;
								}
								log << "\t\t\t\t\tunknown3 - " << refIDtoString(g101->unknown0[i].unknown2.unknown7.unknown9.unknown3[j].unknown3) << endl;
								log << "\t\t\t\t\tunknown4 - " << g101->unknown0[i].unknown2.unknown7.unknown9.unknown3[j].unknown4 << endl;
							}
							log << "\t\t\tunknown2.unknown7.unknown9.unknown4 - " << refIDtoString(g101->unknown0[i].unknown2.unknown7.unknown9.unknown4) << endl;
							log << "\t\t\tunknown2.unknown7.unknown9.unknown5 - " << g101->unknown0[i].unknown2.unknown7.unknown9.unknown5 << endl;
							log << "\t\t\tunknown2.unknown7.unknown9.unknown6 - " << g101->unknown0[i].unknown2.unknown7.unknown9.unknown6 << endl;
							log << "\t\t\tunknown2.unknown7.unknown9.unknown7 - " << g101->unknown0[i].unknown2.unknown7.unknown9.unknown7 << endl;
							log << "\t\t\tunknown2.unknown7.unknown9.unknown8 - " << g101->unknown0[i].unknown2.unknown7.unknown9.unknown8 << endl;
							log << "\t\t\tunknown2.unknown7.unknown9.unknown9 - " << g101->unknown0[i].unknown2.unknown7.unknown9.unknown9 << endl;
							log << "\t\t\tunknown2.unknown7.unknown9.unknown10 - " << refIDtoString(g101->unknown0[i].unknown2.unknown7.unknown9.unknown10) << endl;
							log << "\t\t\tunknown2.unknown7.unknown9.unknown11 - " << g101->unknown0[i].unknown2.unknown7.unknown9.unknown11 << endl;
							log << "\t\t\tunknown2.unknown7.unknown9.unknown12 - " << refIDtoString(g101->unknown0[i].unknown2.unknown7.unknown9.unknown12) << endl;
							log << "\t\t\tunknown2.unknown7.unknown9.unknown13 - " << g101->unknown0[i].unknown2.unknown7.unknown9.unknown13 << endl;
							log << "\t\t\tunknown2.unknown7.unknown9.unknown14 - " << g101->unknown0[i].unknown2.unknown7.unknown9.unknown14 << endl;
							log << "\t\t\tunknown2.unknown7.unknown9.unknown15 - " << g101->unknown0[i].unknown2.unknown7.unknown9.unknown15 << endl;
							log << "\t\t\tunknown2.unknown7.unknown9.unknown16 - " << g101->unknown0[i].unknown2.unknown7.unknown9.unknown16 << endl;
							log << "\t\t\tunknown2.unknown7.unknown9.unknown17 - " << g101->unknown0[i].unknown2.unknown7.unknown9.unknown17 << endl;
						}
					}
					log << "\t\t\tunknown2.unknown8.unknown1 - " << g101->unknown0[i].unknown2.unknown8.unknown1 << endl;
					log << "\t\t\tunknown2.unknown8.unknown2 - " << g101->unknown0[i].unknown2.unknown8.unknown2 << endl;
					log << "\t\t\tunknown2.unknown9 - " << g101->unknown0[i].unknown2.unknown9 << endl;
					log << "\t\t\tunknown2.unknown10 - " << g101->unknown0[i].unknown2.unknown10 << endl;
					log << "\t\t\tunknown2.unknown11 - " << g101->unknown0[i].unknown2.unknown11 << endl;
					log << "\t\t\tunknown2.unknown12 - " << g101->unknown0[i].unknown2.unknown12 << endl;
					log << "\t\t\tunknown2.unknown13.unknown1 - " << g101->unknown0[i].unknown2.unknown13.unknown1 << endl;
					log << "\t\t\tunknown2.unknown13.unknown2 - " << g101->unknown0[i].unknown2.unknown13.unknown2 << endl;
					log << "\t\t\tunknown2.unknown14 - " << (int)g101->unknown0[i].unknown2.unknown14 << endl;
				}
				log << "\tcount1 - " << g101->count1 << endl;
				for (unsigned int i = 0; i < g101->count1; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\tunknown1 - " << refIDtoString(g101->unknown1[i].unknown1) << endl;
					log << "\t\t\tunknown2 - " << g101->unknown1[i].unknown2 << endl;
					log << "\t\t\tunknown3 - " << refIDtoString(g101->unknown1[i].unknown3) << endl;
					log << "\t\t\tunknown4 - " << refIDtoString(g101->unknown1[i].unknown4) << endl;
					log << "\t\t\tunknown5 - " << g101->unknown1[i].unknown5 << endl;
					log << "\t\t\tunknown6 - " << g101->unknown1[i].unknown6 << endl;
					log << "\t\t\tunknown7 - " << g101->unknown1[i].unknown7 << endl;
					log << "\t\t\tx - " << g101->unknown1[i].x << endl;
					log << "\t\t\ty - " << g101->unknown1[i].y << endl;
					log << "\t\t\tz - " << g101->unknown1[i].z << endl;
					log << "\t\t\tunknow8 - " << g101->unknown1[i].unknown8 << endl;
					log << "\t\t\tunknown9 - " << g101->unknown1[i].unknown9 << endl;
					log << "\t\t\tunknown10 - " << g101->unknown1[i].unknown10 << endl;
					log << "\t\t\tunknown11- " << g101->unknown1[i].unknown11 << endl;
					log << "\t\t\tunknown12 - " << g101->unknown1[i].unknown12 << endl;
					log << "\t\t\tunknown13 - " << g101->unknown1[i].unknown13 << endl;
					log << "\t\t\tunknown14 - " << g101->unknown1[i].unknown14 << endl;
					log << "\t\t\tunknown15 - " << g101->unknown1[i].unknown15 << endl;
					log << "\t\t\tunknown16 - " << refIDtoString(g101->unknown1[i].unknown16) << endl;
				}
				log << "\tunknown2 - " << g101->unknown2 << endl;
				log << "\tunknown3 - " << g101->unknown3 << endl;
				log << "\tcount2 - " << g101->count2 << endl;
				for (unsigned int i = 0; i < g101->count2; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\tdata: " << refIDtoString(g101->unknown4[i]) << endl;
				}

				log << "\tunknown5 - " << g101->unknown5 << endl;
				log << "\tunknown6.unknown1 - " << g101->unknown6.unknown1 << endl;
				log << "\tunknown6.unknown2 - " << g101->unknown6.unknown2 << endl;
				log << "\tunknown7.unknown1 - " << g101->unknown7.unknown1 << endl;
				log << "\tunknown7.unknown2 - " << g101->unknown7.unknown2 << endl << endl;
			}*/

			if ((unsigned int)(pos - pos_start) != data.length) {
				throw "wrong position after reading globaldata.combat!";
			}

			data.data = static_cast<void*>(g101);
			break;
		}

		case 102: { //interface
			pos_start = pos;
			g102 = new interface_t;
			g102->shownHelpMsgCount = readUint32();
			g102->shownHelpMsg = new uint32_t[g102->shownHelpMsgCount];

			for (unsigned int i = 0; i < g102->shownHelpMsgCount; i++) {
				g102->shownHelpMsg[i] = readUint32();
			}

			g102->unknown1 = readUint8();
			g102->lastUsedWeaponsCount = readVsval();
			g102->lastUsedWeapons = new refID_t[g102->lastUsedWeaponsCount];

			for (unsigned int i = 0; i < g102->lastUsedWeaponsCount; i++) {
				g102->lastUsedWeapons[i] = readRefID();
			}

			g102->lastUsedSpellsCount = readVsval();
			g102->lastUsedSpells = new refID_t[g102->lastUsedSpellsCount];

			for (unsigned int i = 0; i < g102->lastUsedSpellsCount; i++) {
				g102->lastUsedSpells[i] = readRefID();
			}

			g102->lastUsedShoutsCount = readVsval();
			g102->lastUsedShouts = new refID_t[g102->lastUsedShoutsCount];

			for (unsigned int i = 0; i < g102->lastUsedShoutsCount; i++) {
				g102->lastUsedShouts[i] = readRefID();
			}

			g102->unknown2 = readUint8();
			g102->unknown3.count1 = readVsval();
			g102->unknown3.unknown1 = new interface_t::unknown0_t::unknown0_0_t[g102->unknown3.count1];

			for (unsigned int i = 0; i < g102->unknown3.count1; i++) {
				g102->unknown3.unknown1[i].unknown1 = readString();
				g102->unknown3.unknown1[i].unknown2 = readString();
				g102->unknown3.unknown1[i].unknown3 = readUint32();
				g102->unknown3.unknown1[i].unknown4 = readUint32();
				g102->unknown3.unknown1[i].unknown5 = readUint32();
				g102->unknown3.unknown1[i].unknown6 = readUint32();
			}

			g102->unknown3.count2 = readVsval();
			g102->unknown3.unknown2 = new string[g102->unknown3.count2];

			for (unsigned int i = 0; i < g102->unknown3.count2; i++) {
				g102->unknown3.unknown2[i] = readString();
			}

			g102->unknown3.unknown3 = readUint32();

			if (DBG) {
				log << "interface:" << endl;
				log << "\tlength - " << data.length << endl;
				log << "\tshown help messages(" << g102->shownHelpMsgCount << ")" << endl;
				for (unsigned int i = 0; i < g102->shownHelpMsgCount; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\t" << g102->shownHelpMsg[i] << endl;
				}
				log << "\tunknown1 - " << (unsigned int)g102->unknown1 << endl;
				log << "\tlastUsedWeapons(" << g102->lastUsedWeaponsCount << "):" << endl;
				for (unsigned int i = 0; i < g102->lastUsedWeaponsCount; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\t" << refIDtoString(g102->lastUsedWeapons[i]) << endl;
				}
				log << "\tlastUsedSpells(" << g102->lastUsedSpellsCount << "):" << endl;
				for (unsigned int i = 0; i < g102->lastUsedSpellsCount; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\t" << refIDtoString(g102->lastUsedSpells[i]) << endl;
				}
				log << "\tlastUsedShouts(" << g102->lastUsedShoutsCount << "):" << endl;
				for (unsigned int i = 0; i < g102->lastUsedShoutsCount; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\t" << refIDtoString(g102->lastUsedShouts[i]) << endl;
				}
				log << "\tunknown2 - " << (unsigned int)g102->unknown2 << endl;
				log << "\tunknown3.count1 - " << (unsigned int)g102->unknown3.count1 << endl;
				for (unsigned int i = 0; i < g102->unknown3.count1; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\tunknown1 - " << g102->unknown3.unknown1[i].unknown1 << endl;
					log << "\t\t\tunknown2 - " << g102->unknown3.unknown1[i].unknown2 << endl;
					log << "\t\t\tunknown3 - " << g102->unknown3.unknown1[i].unknown3 << endl;
					log << "\t\t\tunknown4 - " << g102->unknown3.unknown1[i].unknown4 << endl;
					log << "\t\t\tunknown5 - " << g102->unknown3.unknown1[i].unknown5 << endl;
					log << "\t\t\tunknown6 - " << g102->unknown3.unknown1[i].unknown6 << endl;
				}
				log << "\tunknown3.count2 - " << g102->unknown3.count2 << endl;
				for (unsigned int i = 0; i < g102->unknown3.count2; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\t" << g102->unknown3.unknown2[i] << endl;
				}
				log << "\tunknown3.unknown3 - " << g102->unknown3.unknown3 << endl << endl;
			}

			if ((unsigned int)(pos - pos_start) != data.length) {
				throw "wrong position after reading globaldata.actorCauses! (interface.unknown3 not present?)";
			}

			data.data = static_cast<void*>(g102);
			break;
		}

		case 103: { //actor causes
			g103 = new actorCauses_t;
			pos_start = pos;
			g103->nextNum = readUint32();
			g103->count = readVsval();
			g103->unknown = new actorCauses_t::unknown0_t[g103->count];

			for (unsigned int i = 0; i < g103->count; i++) {
				g103->unknown[i].x = readFloat();
				g103->unknown[i].y = readFloat();
				g103->unknown[i].z = readFloat();
				g103->unknown[i].serialNum = readUint32();
				g103->unknown[i].actorID = readRefID();
			}

			if (DBG) {
				log << "actorCauses:" << endl;
				log << "\tlength - " << data.length << endl;
				log << "\tnextNum - " << g103->nextNum << endl;
				log << "\tcount - " << g103->count << endl;
				for (unsigned int i = 0; i < g103->count; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\tx - " << g103->unknown[i].x << endl;
					log << "\t\t\ty - " << g103->unknown[i].y << endl;
					log << "\t\t\tz - " << g103->unknown[i].z << endl;
					log << "\t\t\tserialNum - " << g103->unknown[i].serialNum << endl;
					log << "\t\t\tactorID - " << refIDtoString(g103->unknown[i].actorID) << endl;
				}
				log << endl;
			}

			if ((unsigned int)(pos - pos_start) != data.length) {
				throw "wrong position after reading globaldata.actorCauses!";
			}
			data.data = static_cast<void*>(g103);
			break;
		}

		case 104: { //unknown 104
			//pos_start = pos;
			pos_start = pos;
			g104 = new uint8_t[data.length];
			for (unsigned int i = 0; i < data.length; i++) {
				g104[i] = readByte();
			}

			if (DBG) {
				log << "unknown104:" << endl;
				log << "\tlength - " << data.length << endl;
				log << "\tdata:" << endl;
				for (unsigned int i = 0; i < data.length; i++) {
					log << "\t\t" << (int)g104[i] << endl;
				}
			}
			if ((unsigned int)(pos - pos_start) != data.length) {
				throw "wrong position after reading globaldata.unknown104!";
			}
			data.data = static_cast<void*>(g104);
			break;
		}

		case 105: { //detection manager
			pos_start = pos;
			g105 = new detectionManager_t;
			g105->count = readVsval();
			g105->unknown = new detectionManager_t::unknown_t[g105->count];

			for (unsigned int i = 0; i < g105->count; i++) {
				g105->unknown[i].unknown1 = readRefID();
				g105->unknown[i].unknown2 = readUint32();
				g105->unknown[i].unknown3 = readUint32();
			}

			if (DBG) {
				log << "detection manager:" << endl;
				log << "\tlength - " << data.length << endl;
				log << "\tcount - " << g105->count << endl;
				for (unsigned int i = 0; i < g105->count; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\tunknown1 - " << refIDtoString(g105->unknown[i].unknown1) << endl;
					log << "\t\t\tunknown2 - " << g105->unknown[i].unknown2 << endl;
					log << "\t\t\tunknown3 - " << g105->unknown[i].unknown3 << endl;
				}
				log << endl;
			}

			if ((unsigned int)(pos - pos_start) != data.length) {
				throw "wrong position after reading globaldata.detectionManager!";
			}
			data.data = static_cast<void*>(g105);
			break;
		}

		case 106: { //location metadata
			pos_start = pos;
			g106 = new locationMetaData_t;
			g106->count = readVsval();

			g106->unknown = new locationMetaData_t::unknown_t[g106->count];

			for (unsigned int i = 0; i < g106->count; i++) {
				g106->unknown[i].unknown1 = readRefID();
				g106->unknown[i].unknown2 = readInt32();
			}

			if (DBG) {
				log << "location metadata:" << endl;
				log << "\tlength - " << data.length << endl;
				log << "\tcount - " << g106->count << endl;

				for (unsigned int i = 0; i < g106->count; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\tunknown1 - " << refIDtoString(g106->unknown[i].unknown1) << endl;
					log << "\t\t\tunknown2 - " << g106->unknown[i].unknown2 << endl;
				}
				log << endl;
			}

			if ((unsigned int)(pos - pos_start) != data.length) {
				throw "wrong position after reading globaldata.locationMetadata!";
			}

			data.data = static_cast<void*>(g106);
			break;
		}

		case 107: { //quest static data
			pos_start = pos;
			g107 = new questStaticData_t;

			g107->count0 = readUint32();
			g107->unknown0 = new questStaticData_t::QuestRunData_3[g107->count0];

			for (unsigned int i = 0; i < g107->count0; i++) {
				g107->unknown0[i].unknown0 = readUint32();
				g107->unknown0[i].unknown1 = readFloat();
				g107->unknown0[i].count = readUint32();
				for (unsigned int j = 0; j < g107->unknown0[i].count; j++) {
					g107->unknown0[i].questRunData_item[j].type = readUint32();
					switch (g107->unknown0[i].questRunData_item[j].type) {
					case 1:
					case 2:
					case 4: {
						refID_t tmp_ = readRefID();
						g107->unknown0[i].questRunData_item[j].unknown = &tmp_;
						break;
					}
					case 3:
						g107->unknown0[i].questRunData_item[j].unknown = new uint32_t(readUint32());
						break;
					default:
						throw "invalid globaldata.questStaticData.unknown0.questRunData_item.type!\n";
					}
				}
			}
			g107->count1 = readUint32();
			g107->unknown1 = new questStaticData_t::QuestRunData_3[g107->count1];
			for (unsigned int i = 0; i < g107->count1; i++) {
				g107->unknown1[i].unknown0 = readUint32();
				g107->unknown1[i].unknown1 = readFloat();
				g107->unknown1[i].count = readUint32();
				for (unsigned int j = 0; j < g107->unknown1[i].count; j++) {
					g107->unknown1[i].questRunData_item[j].type = readUint32();
					switch (g107->unknown1[i].questRunData_item[j].type) {
					case 1:
					case 2:
					case 4: {
						refID_t tmp_ = readRefID();
						g107->unknown1[i].questRunData_item[j].unknown = &tmp_;
						break;
					}
					case 3:
						g107->unknown1[i].questRunData_item[j].unknown = new uint32_t(readUint32());
						break;
					default:
						throw "invalid globaldata.questStaticData.unknown1.questRunData_item.type!\n";
					}
				}
			}

			g107->count2 = readUint32();
			g107->unknown2 = new refID_t[g107->count2];
			for (unsigned int i = 0; i < g107->count2; i++) {
				g107->unknown2[i] = readRefID();
			}
			g107->count3 = readUint32();
			g107->unknown3 = new refID_t[g107->count3];
			for (unsigned int i = 0; i < g107->count3; i++) {
				g107->unknown3[i] = readRefID();
			}

			g107->count4 = readUint32();
			g107->unknown4 = new refID_t[g107->count4];
			for (unsigned int i = 0; i < g107->count4; i++) {
				g107->unknown4[i] = readRefID();
			}

			g107->count5 = readVsval();
			g107->unknown5 = new questStaticData_t::unknown0_t[g107->count5];
			for (unsigned int i = 0; i < g107->count5; i++) {
				g107->unknown5[i].unknown0 = readRefID();
				g107->unknown5[i].count = readVsval();
				g107->unknown5[i].unknown1 = new questStaticData_t::unknown0_t::unknown0_1_t[g107->unknown5[i].count];
				for (unsigned int j = 0; j < g107->unknown5[i].count; j++) {
					g107->unknown5[i].unknown1[j].unknown0 = readUint32();
					g107->unknown5[i].unknown1[j].unknown1 = readUint32();
				}
			}

			g107->unknown6 = readUint8();

			if (DBG) {
				log << "quest static data:" << endl;
				log << "\tlength - " << data.length << endl;
				log << "\tcount0 - " << g107->count0 << endl;
				for (unsigned int i = 0; i < g107->count0; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\tunknown0 - " << g107->unknown0[i].unknown0 << endl;
					log << "\t\t\tunknown1 - " << g107->unknown0[i].unknown1 << endl;
					log << "\t\t\tcount - " << g107->unknown0[i].count << endl;
					for (unsigned int j = 0; j < g107->unknown0[i].count; j++) {
						log << "\t\t\t\t" << j << ":" << endl;
						log << "\t\t\t\t\ttype - " << g107->unknown0[i].questRunData_item[j].type;
						switch (g107->unknown0[i].questRunData_item[j].type) {
						case 1:
						case 2:
						case 4:
							log << " (refID)" << endl;
							log << "\t\t\t\t\tunknown - " << refIDtoString(*static_cast<refID_t*>(g107->unknown0[i].questRunData_item[j].unknown)) << endl;
							break;
						case 3:
							log << " (uint32)" << endl;
							log << "\t\t\t\t\tunknown - " << *static_cast<uint32_t*>(g107->unknown0[i].questRunData_item[j].unknown) << endl;
						}
					}
				}
				log << "\tcount1 - " << g107->count1 << endl;
				for (unsigned int i = 0; i < g107->count1; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\tunknown0 - " << g107->unknown1[i].unknown0 << endl;
					log << "\t\t\tunknown1 - " << g107->unknown1[i].unknown1 << endl;
					log << "\t\t\tquest run data(" << g107->unknown1[i].count << "):" << endl;
					for (unsigned int j = 0; j < g107->unknown1[i].count; j++) {
						log << "\t\t\t\t" << j << ":" << endl;
						log << "\t\t\t\t\ttype - " << g107->unknown1[i].questRunData_item[j].type;
						switch (g107->unknown1[i].questRunData_item[j].type) {
						case 1:
						case 2:
						case 4:
							log << " (refID)" << endl;
							log << "\t\t\t\t\tunknown - " << refIDtoString(*static_cast<refID_t*>(g107->unknown1[i].questRunData_item[j].unknown)) << endl;
							break;
						case 3:
							log << " (uint32)" << endl;
							log << "\t\t\t\t\tunknown - " << *static_cast<uint32_t*>(g107->unknown1[i].questRunData_item[j].unknown) << endl;
						}
					}
				}
				log << "\tcount2 - " << g107->count2 << endl;
				for (unsigned int i = 0; i < g107->count2; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\t" << refIDtoString(g107->unknown2[i]) << endl;
				}
				log << "\tcount3 - " << g107->count3 << endl;
				for (unsigned int i = 0; i < g107->count3; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\t" << refIDtoString(g107->unknown3[i]) << endl;
				}
				log << "\tcount4 - " << g107->count4 << endl;
				for (unsigned int i = 0; i < g107->count4; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\t" << refIDtoString(g107->unknown4[i]) << endl;
				}
				log << "\tcount5 - " << g107->count5 << endl;
				for (unsigned int i = 0; i < g107->count5; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\tunknown0 - " << refIDtoString(g107->unknown5[i].unknown0) << endl;
					log << "\t\t\tcount - " << g107->unknown5[i].count << endl;
					for (unsigned int j = 0; j < g107->unknown5[i].count; j++) {
						log << "\t\t\t\t" << j << ":" << endl;
						log << "\t\t\t\t\tunknown0 - " << g107->unknown5[i].unknown1[j].unknown0 << endl;
						log << "\t\t\t\t\tunknown1 - " << g107->unknown5[i].unknown1[j].unknown1 << endl;
					}
				}
				log << "\tunknown6 - " << (int)g107->unknown6 << endl << endl;
			}

			if ((unsigned int)(pos - pos_start) != data.length) {
				throw "wrong position after reading globaldata.questStaticData!";
			}

			data.data = static_cast<void*>(g107);
			break;
		}

		case 108: { //storyteller
			g108 = new storyTeller_t;
			pos_start = pos;
			g108->flag = readUint8();

			if (DBG) {
				log << "storyTeller:" << endl;
				log << "\tlength - " << data.length << endl;
				log << "\tflag - " << (unsigned int)g108->flag << endl;
				log << endl;
			}

			if ((unsigned int)(pos - pos_start) != data.length) {
				throw "wrong position after reading globaldata.locationMetadata!";
			}

			data.data = static_cast<void*>(g108);
			break;
		}

		case 109: { //magic favorites
			pos_start = pos;
			g109 = new magicFavorites_t;
			g109->count0 = readVsval();
			g109->favoritedMagics = new refID_t[g109->count0];

			for (unsigned int i = 0; i < g109->count0; i++) {
				g109->favoritedMagics[i] = readRefID();
			}

			g109->count1 = readVsval();
			g109->magicHotKeys = new refID_t[g109->count1];

			for (unsigned int i = 0; i < g109->count1; i++) {
				g109->magicHotKeys[i] = readRefID();
			}

			if (DBG) {
				log << "magic favorites:" << endl;
				log << "\tlength - " << data.length << endl;
				log << "\tfavorited magics(" << g109->count0 << "):" << endl;
				for (unsigned int i = 0; i < g109->count0; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\t" << refIDtoString(g109->favoritedMagics[i]) << endl;
				}
				log << "\tmagichotkeys(" << g109->count1 << "):" << endl;
				for (unsigned int i = 0; i < g109->count1; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\t" << refIDtoString(g109->magicHotKeys[i]) << endl;
				}
				log << endl;
			}

			if ((unsigned int)(pos - pos_start) != data.length) {
				throw "wrong position after reading globaldata.magicFavorites!";
			}

			data.data = static_cast<void*>(g109);
			break;
		}

		case 110: { //player controls
			pos_start = pos;
			g110 = new playerControls_t;
			g110->unknown1 = readUint8();
			g110->unknown2 = readUint8();
			g110->unknown3 = readUint8();
			g110->unknown4 = readUint16();
			g110->unknown5 = readUint8();

			if (DBG) {
				log << "player controls:" << endl;
				log << "\tlength - " << data.length << endl;
				log << "\tunknown1 - " << (unsigned int)g110->unknown1 << endl;
				log << "\tunknown2 - " << (unsigned int)g110->unknown2 << endl;
				log << "\tunknown3 - " << (unsigned int)g110->unknown3 << endl;
				log << "\tunknown4 - " << (unsigned int)g110->unknown4 << endl;
				log << "\tunknown5 - " << (unsigned int)g110->unknown5 << endl << endl;
			}

			if ((unsigned int)(pos - pos_start) != data.length) {
				throw "wrong position after reading globaldata.playerControls!";
			}

			data.data = static_cast<void*>(g110);
			break;
		}

		case 111: { //story event manager
			pos_start = pos;

			g111 = new storyEventManager_t;
			//g111->unknown0 = readUint32();
			//g111->count = readVsval();
			//int bytes_read = pos - pos_start;

			g111->data = new uint8_t[data.length];
			for (unsigned int i = 0; i < data.length; i++) {
				g111->data[i] = readByte();
			}

			if (DBG) {
				log << "story event manager:" << endl;
				//log << "\tlength - " << data.length << endl;
				//log << "\tunknown0 - " << g111->unknown0 << endl;
				//log << "\tcount - " << g111->count << endl;
				log << "\tdata(" << (data.length) << "):" << endl;
				for (unsigned int i = 0; i < data.length; i++) {
					log << "\t\t" << (int)g111->data[i] << endl;
				}
			}


			if ((unsigned int)(pos - pos_start) != data.length) {
				throw "wrong position after reading globaldata.storyEventManager!";
			}

			data.data = static_cast<void*>(g111);
			break;
		}

		case 112: { //ingredient shared
			pos_start = pos;
			g112 = new ingredientShared_t;
			g112->count = readUint32();
			g112->ingredientsCombined = new ingredientShared_t::ingredientsCombined_t[g112->count];

			for (unsigned int i = 0; i < g112->count; i++) {
				g112->ingredientsCombined[i].ingredient0 = readRefID();
				g112->ingredientsCombined[i].ingredient1 = readRefID();
			}

			if (DBG) {
				log << "ingredientShared:" << endl;
				log << "\tlength - " << data.length << endl;
				log << "\tingredients combined(" << g112->count << "):" << endl;
				for (unsigned int i = 0; i < g112->count; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\tingredient0 - " << refIDtoString(g112->ingredientsCombined[i].ingredient0) << endl;
					log << "\t\t\tingredient1 - " << refIDtoString(g112->ingredientsCombined[i].ingredient1) << endl;
				}
				log << endl;
			}

			if ((unsigned int)(pos - pos_start) != data.length) {
				log << ("wrong position after reading globaldata.playerControls!");
				throw "wrong position after reading globaldata.playerControls!";
			}

			data.data = static_cast<void*>(g112);
			break;
		}

		case 113: { //menuControls
			pos_start = pos;
			g113 = new menuControls_t;
			g113->unknown1 = readUint8();
			g113->unknown2 = readUint8();

			if (DBG) {
				log << "menu controls:" << endl;
				log << "\tlength - " << data.length << endl;
				log << "\tunknown1 - " << (unsigned int)g113->unknown1 << endl;
				log << "\tunknown2 - " << (unsigned int)g113->unknown2 << endl << endl;

			}

			if ((unsigned int)(pos - pos_start) != data.length) {
				log << ("wrong position after reading globaldata.playerControls!");
				throw "wrong position after reading globaldata.playerControls!";
			}

			data.data = static_cast<void*>(g113);
			break;
		}

		case 114: { //menuTopicManager
			pos_start = pos;
			g114 = new menuTopicManager;
			g114->unknown1 = readRefID();
			g114->unknown2 = readRefID();

			if (DBG) {
				log << "menuTopicManager:" << endl;
				log << "\tlength - " << data.length << endl;
				log << "\tunknown1 - " << refIDtoString(g114->unknown1) << endl;
				log << "\tunknown2 - " << refIDtoString(g114->unknown2) << endl << endl;
			}

			if ((unsigned int)(pos - pos_start) != data.length) {
				throw "wrong position after reading globaldata.menutopicmanager!";
			}

			data.data = static_cast<void*>(g114);
			break;
		}

		case 1000: { //temp effects
			pos_start = pos;
			g1000 = new uint8_t[data.length];
			for (unsigned int i = 0; i < data.length; i++) {
				g1000[i] = readByte();
			}

			if (DBG) {
				log << "temp effects:" << endl;
				log << "\tlength - " << data.length << endl;
				log << "\tdata(" << data.length << "):" << endl;
				for (unsigned int i = 0; i < data.length; i++) {
					log << "\t\t" << (int)g1000[i] << endl;
				}
			}
			if ((unsigned int)(pos - pos_start) != data.length) {
				throw "wrong position after reading globaldata.tempeffects!";
			}
			break;
		}

		case 1001: { //Papyrus
			pos_start = pos;
			g1001 = new papyrus_t;
			g1001->header = readUint16();
			g1001->strCount = readUint16();
			g1001->strings = new string[g1001->strCount];
			for (int i = 0; i < g1001->strCount; i++) {
				g1001->strings[i] = readString();
			}
			g1001->scriptCount = readUint32();
			g1001->script = new papyrus_t::script_t[g1001->scriptCount];

			for (unsigned int i = 0; i < g1001->scriptCount; i++) {
				g1001->script[i].scriptName = readUint16();
				g1001->script[i].type = readUint16();
				g1001->script[i].memberCount = readUint32();
				g1001->script[i].memberData = new papyrus_t::script_t::memberData_t[g1001->script[i].memberCount];

				for (unsigned int j = 0; j < g1001->script[i].memberCount; j++) {
					g1001->script[i].memberData[j].memberName = readUint16();
					g1001->script[i].memberData[j].memberType = readUint16();
				}
			}

			g1001->scriptInstanceCount = readUint32();
			g1001->scriptInstance = new papyrus_t::scriptInstance_t[g1001->scriptInstanceCount];

			for (unsigned int i = 0; i < g1001->scriptInstanceCount; i++) {
				g1001->scriptInstance[i].scriptID = readUint32();
				g1001->scriptInstance[i].scriptName = readUint16();
				g1001->scriptInstance[i].unknown2bits = readUint16();
				g1001->scriptInstance[i].unknown = readInt16();
				//if(!(g1001->scriptInstance[i].unknown == -1))
				g1001->scriptInstance[i].refID = readRefID();
				g1001->scriptInstance[i].unknown2 = readUint8();
			}

			g1001->referenceCount = readUint32();
			g1001->reference = new reference_t[g1001->referenceCount];

			for (unsigned int i = 0; i < g1001->referenceCount; i++) {
				g1001->reference[i].referenceID = readUint32();
				g1001->reference[i].type = readUint16();
			}

			g1001->arrayInfoCount = readUint32();
			g1001->arrayInfo = new papyrus_t::arrayInfo_t[g1001->arrayInfoCount];

			for (unsigned int i = 0; i < g1001->arrayInfoCount; i++) {
				g1001->arrayInfo[i].arrayID = readUint32();
				g1001->arrayInfo[i].type = readUint8();

				//if ((g1001->arrayInfo[i].type != 1) && (g1001->arrayInfo[i].type != 2) && (g1001->arrayInfo[i].type != 3) && (g1001->arrayInfo[i].type != 4) && (g1001->arrayInfo[i].type != 5)) {
				//	throw "invalid array info type!";
				//}

				if (g1001->arrayInfo[i].type == 1) {
					g1001->arrayInfo[i].refType = readUint16();
				}
				else
					g1001->arrayInfo[i].refType = 0;

				g1001->arrayInfo[i].length = readUint32();
			}

			g1001->papyrusRuntime = readUint32();
			g1001->activeScriptCount = readUint32();
			g1001->activeScript = new papyrus_t::activeScript_t[g1001->activeScriptCount];

			for (unsigned int i = 0; i < g1001->activeScriptCount; i++) {
				g1001->activeScript[i].scriptID = readUint32();
				g1001->activeScript[i].scriptType = readUint8();
			}

			g1001->scriptData = new papyrus_t::scriptData_t[g1001->scriptInstanceCount];

			for (unsigned int i = 0; i < g1001->scriptInstanceCount; i++) {
				g1001->scriptData[i].scriptID = readUint32();
				if (g1001->scriptData[i].scriptID != g1001->scriptInstance[i].scriptID) {//check if scriptIDs match
					throw "script data/instance script ids don't match!";
				}
				g1001->scriptData[i].flag = readUint8();
				g1001->scriptData[i].type = readUint16();
				g1001->scriptData[i].unknown1 = readUint32();

				if ((g1001->scriptData[i].flag & 0x04) == 0x04) {
					g1001->scriptData[i].unknown2 = readUint32();
				}
				else
					g1001->scriptData[i].unknown2 = 0;

				g1001->scriptData[i].memberCount = readUint32();
				g1001->scriptData[i].member = new variable_t[g1001->scriptData[i].memberCount];

				for (unsigned int j = 0; j < g1001->scriptData[i].memberCount; j++) {
					g1001->scriptData[i].member[j] = readVariable_t();
				}
			}

			g1001->referenceData = new papyrus_t::referenceData_t[g1001->referenceCount];

			for (unsigned int i = 0; i < g1001->referenceCount; i++) {
				g1001->referenceData[i].referenceID = readUint32();
				g1001->referenceData[i].flag = readUint8();
				g1001->referenceData[i].type = readUint16();
				g1001->referenceData[i].unknown1 = readUint32();

				if ((g1001->referenceData[i].unknown1 & 0x04) == 0x04) {
					g1001->referenceData[i].unknown2 = readUint32();
				}

				g1001->referenceData[i].memberCount = readUint32();
				g1001->referenceData[i].member = new variable_t[g1001->referenceData[i].memberCount];

				for (unsigned int j = 0; j < g1001->referenceData[i].memberCount; j++) {
					g1001->referenceData[i].member[j] = readVariable_t();
				}
			}

			g1001->arrayData = new papyrus_t::arrayData_t[g1001->arrayInfoCount];

			for (unsigned int i = 0; i < g1001->arrayInfoCount; i++) {
				g1001->arrayData[i].arrayID = readUint32();

				if (g1001->arrayInfo[i].arrayID != g1001->arrayData[i].arrayID) {
					throw "globaldata.papyrus.arraydata.arrayID error";
				}

				g1001->arrayData[i].data = new variable_t[g1001->arrayInfo[i].length];

				for (unsigned int j = 0; j < g1001->arrayInfo[i].length; j++) {
					g1001->arrayData[i].data[j] = readVariable_t();
				}
			}

			if (DBG) {
				log << "papyrus:" << endl;
				log << "\tlength - " << data.length << endl;
				log << "\theader - " << (unsigned int)g1001->header << endl;
				log << "\tstrings(" << g1001->strCount << ")" << endl;
				for (unsigned int i = 0; i < g1001->strCount; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\t" << g1001->strings[i] << endl;
				}
				log << "\tscripts(" << (unsigned int)g1001->scriptCount << "):" << endl;
				for (unsigned int i = 0; i < g1001->scriptCount; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\tscriptName - " << g1001->strings[g1001->script[i].scriptName] << endl;
					log << "\t\t\ttype - " << g1001->strings[g1001->script[i].type] << endl;
					log << "\t\t\tmemberData(" << (unsigned int)g1001->script[i].memberCount << "):" << endl;
					for (unsigned int j = 0; j < g1001->script[i].memberCount; j++) {
						log << "\t\t\t\t" << j << ":" << endl;
						log << "\t\t\t\t\t" << g1001->strings[g1001->script[i].memberData[j].memberType] << ": " << g1001->strings[g1001->script[i].memberData[j].memberName] << endl;
					}
				}
				log << "\tscript instances(" << (unsigned int)g1001->scriptInstanceCount << "):" << endl;
				for (unsigned int i = 0; i < g1001->scriptInstanceCount; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\tscriptID - " << (unsigned int)g1001->scriptInstance[i].scriptID << endl;
					log << "\t\t\tscriptName - " << g1001->strings[g1001->scriptInstance[i].scriptName] << endl;
					log << "\t\t\tunknown2bits - " << (unsigned int)g1001->scriptInstance[i].unknown2bits << endl;
					log << "\t\t\tunknown2bits used value - " << (unsigned int)(g1001->scriptInstance[i].unknown2bits >> 14) << endl;
					log << "\t\t\tunknown - " << (int16_t)g1001->scriptInstance[i].unknown << endl;
					log << "\t\t\tunknown used value - " << (int16_t)((g1001->scriptInstance[i].unknown2bits >> 14) * 1000 + g1001->scriptInstance[i].unknown) << endl;
					//if(!g1001->scriptInstance[i].unknown == -1)
					log << "\t\t\trefID - " << refIDtoString(g1001->scriptInstance[i].refID) << endl;
					log << "\t\t\tunknown2 - " << (unsigned int)g1001->scriptInstance[i].unknown2 << endl << endl;
				}
				log << "\treferences(" << (unsigned int)g1001->referenceCount << "):" << endl;
				for (unsigned int i = 0; i < g1001->referenceCount; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\treferenceID - " << (unsigned int)g1001->reference[i].referenceID << endl;
					log << "\t\t\ttype - " << (unsigned int)g1001->reference[i].type << endl << endl;
				}
				log << "\tarray info(" << (unsigned int)g1001->arrayInfoCount << "):" << endl;
				for (unsigned int i = 0; i < g1001->arrayInfoCount; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\tarrayID - " << (unsigned int)g1001->arrayInfo[i].arrayID << endl;
					log << "\t\t\ttype - ";

					switch (g1001->arrayInfo[i].type) {
					case 1:
						log << "refID";
						break;

					case 2:
						log << "String";
						break;

					case 3:
						log << "Integer";
						break;

					case 4:
						log << "Float";
						break;

					case 5:
						log << "Boolean";
						break;

					case 0:
						log << "NULL";
						break;

					default:
						throw "\ninvalid array info type!";
					}

					if (g1001->arrayInfo[i].type == 1) {
						log << endl << "\t\t\trefType - " << g1001->strings[g1001->arrayInfo[i].refType];
					}

					log << endl << "\t\t\tlength - " << (unsigned int)g1001->arrayInfo[i].length << endl << endl;
				}
				log << "\tpapyrusRuntime - " << (unsigned int)g1001->papyrusRuntime << endl;
				log << "\tactive scripts(" << (unsigned int)g1001->activeScriptCount << "):" << endl;
				for (unsigned int i = 0; i < g1001->activeScriptCount; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\tscriptID - " << g1001->activeScript[i].scriptID << endl;
					log << "\t\t\tscriptType - " << (unsigned int)g1001->activeScript[i].scriptType << endl;
				}
				log << "\tscript instances(" << g1001->scriptInstanceCount << "):" << endl;
				for (unsigned int i = 0; i < g1001->scriptInstanceCount; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\tscriptID - " << (unsigned int)g1001->scriptData[i].scriptID << endl;
					log << "\t\t\tflag - " << (unsigned int)g1001->scriptData[i].flag << endl;
					log << "\t\t\ttype - " << (unsigned int)g1001->scriptData[i].type << endl;
					log << "\t\t\tunknown1 - " << (unsigned int)g1001->scriptData[i].unknown1 << endl;
					log << "\t\t\tunknown2 - " << (unsigned int)g1001->scriptData[i].unknown2 << endl;
					log << "\t\t\tmemberCount - " << (unsigned int)g1001->scriptData[i].memberCount << endl;
				}
				log << "\treference data(" << g1001->referenceCount << "):" << endl;
				for (unsigned int i = 0; i < g1001->referenceCount; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\treferenceID - " << g1001->referenceData[i].referenceID << endl;
					log << "\t\t\tflag - " << (int)g1001->referenceData[i].flag << endl;
					log << "\t\t\ttype - " << g1001->referenceData[i].type << endl;
					log << "\t\t\tunknown1 - " << g1001->referenceData[i].unknown1 << endl;

					if ((g1001->referenceData[i].unknown1 & 0x04) == 0x04) {
						log << "\t\t\tunknown2 - " << g1001->referenceData[i].unknown2 << endl;
					}
					log << "\t\t\tmembers(" << g1001->referenceData[i].memberCount << "):" << endl;
					for (unsigned int j = 0; j < g1001->referenceData[i].memberCount; j++) {
						log << "\t\t\t\t" << variableToString(g1001->referenceData[i].member[j]) << endl;
					}
				}
				log << "\tarray data(" << g1001->arrayInfoCount << "):" << endl;
				for (unsigned int i = 0; i < g1001->arrayInfoCount; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\tarrayID - " << (unsigned int)g1001->arrayData[i].arrayID << endl;
					log << "\t\t\tdata(" << g1001->arrayInfo[i].length << "):" << endl;
					for (unsigned int j = 0; j < g1001->arrayInfo[i].length; j++) {
						log << "\t\t\t\t" << variableToString(g1001->arrayData[i].data[j], g1001->strings, g1001->strCount) << endl;
					}
				}

				log << "\tactive script data(" << g1001->activeScriptCount << "):" << endl;
			}

			g1001->activeScriptData = new papyrus_t::activeScriptData_t[g1001->activeScriptCount];

			for (unsigned int i = 0; i < g1001->activeScriptCount; i++) {

				g1001->activeScriptData[i].scriptID = readUint32();

				if (g1001->activeScriptData[i].scriptID != g1001->activeScript[i].scriptID) {
					throw "activescriptdata.scriptID != activescript.scriptID!!";
				}

				g1001->activeScriptData[i].majorVersion = readUint8();

				/*if (g1001->activeScriptData[i].majorVersion != 3) {
					log << "globaldata.papyrus.activeScriptData.majorVersion - wrong value! should be 3, is  " + to_string(g1001->activeScriptData[i].majorVersion) << endl;;
				}*/

				g1001->activeScriptData[i].minorVersion = readUint8();

				if (g1001->activeScriptData[i].minorVersion != 1 && g1001->activeScriptData[i].minorVersion != 2) {
					throw "globaldata.papyrus.activeScriptData.minorVersion - wrong value! should be 1 or 2, is " + to_string(g1001->activeScriptData[i].minorVersion);
				}

				g1001->activeScriptData[i].unknown = readVariable_t();
				g1001->activeScriptData[i].flag = readUint8();
				g1001->activeScriptData[i].unknownByte = readUint8();

				if (DBG) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\tscriptID - " << (unsigned int)g1001->activeScriptData[i].scriptID << endl;
					log << "\t\t\tmajorVersion - " << (unsigned int)g1001->activeScriptData[i].majorVersion << endl;
					log << "\t\t\tminorVersion - " << (unsigned int)g1001->activeScriptData[i].minorVersion << endl;
					log << "\t\t\tflag - " << (unsigned int)g1001->activeScriptData[i].flag << endl;
					log << "\t\t\tunknown2 - " << (unsigned int)g1001->activeScriptData[i].unknownByte << endl;
				}

				if ((g1001->activeScriptData[i].flag & 0x01) == 0x01) {
					g1001->activeScriptData[i].unknown2 = readUint32();

					if (DBG) {
						log << "\t\t\tunknown2_ - " << (unsigned int)g1001->activeScriptData[i].unknown2 << endl;
					}
				}
				else {
					g1001->activeScriptData[i].unknown2 = 0;
				}

				g1001->activeScriptData[i].unknown3 = readUint8();

				if (DBG) {
					log << "\t\t\tunknown3 - " << (unsigned int)g1001->activeScriptData[i].unknown3 << endl;
				}

				if ((g1001->activeScriptData[i].unknown3 >= 1) && (g1001->activeScriptData[i].unknown3 <= 3)) {
					switch (g1001->activeScriptData[i].unknown3) {
					case 1:
						g1001->activeScriptData[i].unknown4 = new papyrus_t::activeScriptData_t::unknown4_1;
						static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->count = readUint32();
						static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->str = new char[static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->count + 1];
						for (unsigned int x = 0; x < static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->count; x++) {
							static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->str[x] = (char)readByte();
						}
						static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->str[static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->count] = '\0';

						if (DBG) {
							log << "\t\t\tunknown4.count - " << static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->count << endl;
							log << "\t\t\tunknown4.str - " << static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->str << endl;
						}

						if (strcmp("TopicInfo", static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->str) == 0) {
							static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->data = NULL;
						}
						else if (strcmp("QuestStage", static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->str) == 0) {
							static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->data = new papyrus_t::activeScriptData_t::questStage;
							static_cast<papyrus_t::activeScriptData_t::questStage*>(static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->data)->refID = readRefID();
							static_cast<papyrus_t::activeScriptData_t::questStage*>(static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->data)->string = readUint16();
							static_cast<papyrus_t::activeScriptData_t::questStage*>(static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->data)->unknown = readUint8();

							if (DBG) {
								log << "\t\t\tunknown4.data.refID - " << refIDtoString(static_cast<papyrus_t::activeScriptData_t::questStage*>(static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->data)->refID) << endl;
								log << "\t\t\tunknown4.data.string - " << g1001->strings[static_cast<papyrus_t::activeScriptData_t::questStage*>(static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->data)->string] << endl;
								log << "\t\t\tunknown4.data.unknown - " << (int)static_cast<papyrus_t::activeScriptData_t::questStage*>(static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->data)->unknown << endl;;
							}
						}
						else if ((strcmp("ScenePhaseResults", static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->str) == 0) || (strcmp("SceneActionResults", static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->str) == 0)) {
							static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->data = new papyrus_t::activeScriptData_t::scenePhaseResults;
							static_cast<papyrus_t::activeScriptData_t::scenePhaseResults*>(static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->data)->refID = readRefID();
							static_cast<papyrus_t::activeScriptData_t::scenePhaseResults*>(static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->data)->unknown = readUint32();

							if (DBG) {
								log << "\t\t\tunknown4.refID - " << refIDtoString(static_cast<papyrus_t::activeScriptData_t::scenePhaseResults*>(static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->data)->refID) << endl;
								log << "\t\t\tunknown4.unknown - " << static_cast<papyrus_t::activeScriptData_t::scenePhaseResults*>(static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->data)->unknown << endl;
							}
						}
						else if (strcmp("SceneResults", static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->str) == 0) {
							static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->data = new papyrus_t::activeScriptData_t::sceneResults;
							static_cast<papyrus_t::activeScriptData_t::sceneResults*>(static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->data)->refID = readRefID();

							if (DBG) {
								log << "\t\t\tunknown4.refID - " << refIDtoString(static_cast<papyrus_t::activeScriptData_t::sceneResults*>(static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->data)->refID) << endl;
							}
						}
						else {
							throw "papyrus.activeScriptData.unknown4 - invalid value!";
						}

						break;

					case 2:
						g1001->activeScriptData[i].unknown4 = new variable_t;
						*static_cast<variable_t*>(g1001->activeScriptData[i].unknown4) = readVariable_t();

						if (DBG) {
							log << "\t\t\tunknown4 - " << variableToString(*static_cast<variable_t*>(g1001->activeScriptData[i].unknown4), g1001->strings, g1001->strCount) << endl;
						}

						break;

					case 3:
						g1001->activeScriptData[i].unknown4 = new papyrus_t::activeScriptData_t::unknown4_3;
						static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->count = readUint32();

						static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->str = new char[static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->count + 1];
						for (unsigned int x = 0; x < static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->count; x++) {
							static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->str[x] = (char)readUint8();
						}
						static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->str[static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->count] = '\0';

						if (DBG) {
							log << "\t\t\tunknown4.str - " << static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->str << endl;
						}

						if (strcmp("TopicInfo", static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->str) == 0) {
							static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->data = NULL;
						}
						else if (strcmp("QuestStage", static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->str) == 0) {
							static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->data = new papyrus_t::activeScriptData_t::questStage;
							static_cast<papyrus_t::activeScriptData_t::questStage*>(static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->data)->refID = readRefID();
							static_cast<papyrus_t::activeScriptData_t::questStage*>(static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->data)->string = readUint16();
							static_cast<papyrus_t::activeScriptData_t::questStage*>(static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->data)->unknown = readUint8();

							if (DBG) {
								log << "\t\t\tunknown4.refID - " << refIDtoString(static_cast<papyrus_t::activeScriptData_t::questStage*>(static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->data)->refID) << endl;
								log << "\t\t\tunknown4.string - " << g1001->strings[static_cast<papyrus_t::activeScriptData_t::questStage*>(static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->data)->string] << endl;
								log << "\t\t\tunknown4.unknown - " << static_cast<papyrus_t::activeScriptData_t::questStage*>(static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->data)->unknown << endl;
							}
						}
						else if ((strcmp("ScenePhaseResults", static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->str)) == 0 || strcmp("SceneActionResults", static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->str) == 0) {
							static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->data = new papyrus_t::activeScriptData_t::scenePhaseResults;
							static_cast<papyrus_t::activeScriptData_t::scenePhaseResults*>(static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->data)->refID = readRefID();
							static_cast<papyrus_t::activeScriptData_t::scenePhaseResults*>(static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->data)->unknown = readUint32();

							if (DBG) {
								log << "\t\t\tunknown4.refID - " << refIDtoString(static_cast<papyrus_t::activeScriptData_t::scenePhaseResults*>(static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->data)->refID) << endl;
								log << "\t\t\tunknown4.unknown - " << static_cast<papyrus_t::activeScriptData_t::scenePhaseResults*>(static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->data)->unknown << endl;
							}
						}
						else if (strcmp("SceneResults", static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->str) == 0) {
							static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->data = new papyrus_t::activeScriptData_t::sceneResults;
							static_cast<papyrus_t::activeScriptData_t::sceneResults*>(static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->data)->refID = readRefID();

							if (DBG) {
								log << "\t\t\tunknown4.refID - " << refIDtoString(static_cast<papyrus_t::activeScriptData_t::sceneResults*>(static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->data)->refID) << endl;
							}
						}
						else {
							throw "globaldata.papyrus.activeScriptData.unknown4 - invalid value!";
						}

						static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->unknown = readVariable_t();

						if (DBG) {
							log << "\t\t\tunknown4.unknown - " << variableToString(static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->unknown, g1001->strings, g1001->strCount) << endl;
						}
						break;

					default:
						throw "g1001->activeScriptData.unknown3 - invalid value!";
					}

				}

				g1001->activeScriptData[i].stackframecount = readUint32();

				if (DBG) {
					log << "\t\t\tstackframes(" << (unsigned int)g1001->activeScriptData[i].stackframecount << "):" << endl;
				}

				g1001->activeScriptData[i].stackframe = new papyrus_t::activeScriptData_t::stackframe_t[g1001->activeScriptData[i].stackframecount];

				for (unsigned int j = 0; j < g1001->activeScriptData[i].stackframecount; j++) {
					g1001->activeScriptData[i].stackframe[j].variablecount = readUint32();
					g1001->activeScriptData[i].stackframe[j].flag = readUint8();
					g1001->activeScriptData[i].stackframe[j].functionType = readUint8();
					g1001->activeScriptData[i].stackframe[j].scriptName = readUint16();
					g1001->activeScriptData[i].stackframe[j].scriptBaseName = readUint16();
					g1001->activeScriptData[i].stackframe[j].event = readUint16();

					if (((g1001->activeScriptData[i].stackframe[j].flag & 0x01) == 0) && (g1001->activeScriptData[i].stackframe[j].functionType == 0)) {
						g1001->activeScriptData[i].stackframe[j].status = readUint16();
					}
					else {
						g1001->activeScriptData[i].stackframe[j].status = 0;
					}
					g1001->activeScriptData[i].stackframe[j].opcodeVersion = readUint8();
					g1001->activeScriptData[i].stackframe[j].opcodeMinorVersion = readUint8();
					g1001->activeScriptData[i].stackframe[j].returnType = readUint16();
					g1001->activeScriptData[i].stackframe[j].functionDocstring = readUint16();
					g1001->activeScriptData[i].stackframe[j].functionUserflags = readUint32();
					g1001->activeScriptData[i].stackframe[j].functionFlags = readUint8();
					g1001->activeScriptData[i].stackframe[j].functionParameterCount = readUint16();

					if (DBG) {
						log << "\t\t\t\t" << j << ":" << endl;
						log << "\t\t\t\t\tvariablecount - " << (unsigned int)g1001->activeScriptData[i].stackframe[j].variablecount << endl;
						log << "\t\t\t\t\tflag - " << (unsigned int)g1001->activeScriptData[i].stackframe[j].flag << endl;
						log << "\t\t\t\t\tfunctionType - " << (unsigned int)g1001->activeScriptData[i].stackframe[j].functionType << endl;
						log << "\t\t\t\t\tscriptName - " << g1001->strings[g1001->activeScriptData[i].stackframe[j].scriptName] << "(" << (unsigned int)g1001->activeScriptData[i].stackframe[j].scriptName << ")" << endl;
						log << "\t\t\t\t\tscriptBaseName - " << g1001->strings[g1001->activeScriptData[i].stackframe[j].scriptBaseName] << endl;
						log << "\t\t\t\t\tevent - " << g1001->strings[g1001->activeScriptData[i].stackframe[j].event] << endl;

						if (((g1001->activeScriptData[i].stackframe[j].flag & 0x01) == 0) && (g1001->activeScriptData[i].stackframe[j].functionType == 0))
							log << "\t\t\t\t\tstatus - " << g1001->strings[g1001->activeScriptData[i].stackframe[j].status] << "(" << (unsigned int)g1001->activeScriptData[i].stackframe[j].status << ")" << endl;

						log << "\t\t\t\t\topcodeVersion - " << (unsigned int)g1001->activeScriptData[i].stackframe[j].opcodeVersion << endl;
						log << "\t\t\t\t\topcodeMinorVersion - " << (unsigned int)g1001->activeScriptData[i].stackframe[j].opcodeMinorVersion << endl;
						log << "\t\t\t\t\treturnType - " << g1001->strings[g1001->activeScriptData[i].stackframe[j].returnType] << "(" << (unsigned int)g1001->activeScriptData[i].stackframe[j].returnType << ")" << endl;
						log << "\t\t\t\t\tfunctionDocstring - " << g1001->strings[g1001->activeScriptData[i].stackframe[j].functionDocstring] << "(" << (unsigned int)g1001->activeScriptData[i].stackframe[j].functionDocstring << ")" << endl;
						log << "\t\t\t\t\tfunctionUserflags - " << (unsigned int)g1001->activeScriptData[i].stackframe[j].functionUserflags << endl;
						log << "\t\t\t\t\tfunctionFlags - " << (unsigned int)g1001->activeScriptData[i].stackframe[j].functionFlags << endl;
						log << "\t\t\t\t\tfunction parameters(" << (unsigned int)g1001->activeScriptData[i].stackframe[j].functionParameterCount << "):" << endl;
					}

					g1001->activeScriptData[i].stackframe[j].functionParam = new papyrus_t::activeScriptData_t::stackframe_t::functionParam_t[g1001->activeScriptData[i].stackframe[j].functionParameterCount];

					for (unsigned int x = 0; x < g1001->activeScriptData[i].stackframe[j].functionParameterCount; x++) {
						g1001->activeScriptData[i].stackframe[j].functionParam[x].functionParamName = readUint16();
						g1001->activeScriptData[i].stackframe[j].functionParam[x].functionParamType = readUint16();

						if (DBG) {
							log << "\t\t\t\t\t\t" << x << ":" << endl;
							log << "\t\t\t\t\t\t\tfunctionParamName - " << g1001->strings[g1001->activeScriptData[i].stackframe[j].functionParam[x].functionParamName] << endl;
							log << "\t\t\t\t\t\t\tfunctionParamType - " << g1001->strings[g1001->activeScriptData[i].stackframe[j].functionParam[x].functionParamType] << endl;
						}
					}

					g1001->activeScriptData[i].stackframe[j].functionLocalsCount = readUint16();

					if (DBG) {
						log << "\t\t\t\t\tfunctionLocals(" << g1001->activeScriptData[i].stackframe[j].functionLocalsCount << "):" << endl;
					}

					g1001->activeScriptData[i].stackframe[j].functionLocal = new papyrus_t::activeScriptData_t::stackframe_t::functionLocal_t[g1001->activeScriptData[i].stackframe[j].functionLocalsCount];

					for (unsigned int x = 0; x < g1001->activeScriptData[i].stackframe[j].functionLocalsCount; x++) {
						g1001->activeScriptData[i].stackframe[j].functionLocal[x].functionLocalName = readUint16();
						g1001->activeScriptData[i].stackframe[j].functionLocal[x].functionTypeName = readUint16();
						if (DBG) {
							log << "\t\t\t\t\t\t" << x << ":" << endl;
							log << "\t\t\t\t\t\t\tfunctionLocalName - " << g1001->strings[g1001->activeScriptData[i].stackframe[j].functionLocal[x].functionLocalName] << endl;
							log << "\t\t\t\t\t\t\tfunctionTypeName - " << g1001->strings[g1001->activeScriptData[i].stackframe[j].functionLocal[x].functionTypeName] << endl;
						}
					}

					g1001->activeScriptData[i].stackframe[j].opcodeCount = readUint16();

					if (DBG) {
						log << "\t\t\t\t\topcode data(" << (unsigned int)g1001->activeScriptData[i].stackframe[j].opcodeCount << "):" << endl;
					}

					g1001->activeScriptData[i].stackframe[j].opcodeData = new papyrus_t::activeScriptData_t::stackframe_t::opcodeData_t[g1001->activeScriptData[i].stackframe[j].opcodeCount];
					//process opcodeData array
					for (unsigned int x = 0; x < g1001->activeScriptData[i].stackframe[j].opcodeCount; x++) {
						g1001->activeScriptData[i].stackframe[j].opcodeData[x].opcode = readUint8();

						if (DBG) {
							log << "\t\t\t\t\t\t" << x << ":" << endl;
							log << "\t\t\t\t\t\t\topcode - " << getOpcodeDesc(g1001->activeScriptData[i].stackframe[j].opcodeData[x].opcode) << ", parameters:" << endl;
						}
						int count = getOpcodeLength(g1001->activeScriptData[i].stackframe[j].opcodeData[x].opcode);

						g1001->activeScriptData[i].stackframe[j].opcodeData[x].parameter = new papyrus_t::activeScriptData_t::stackframe_t::opcodeData_t::parameter_t[count];

						for (int y = 0; y < count; y++) {
							g1001->activeScriptData[i].stackframe[j].opcodeData[x].parameter[y].type = readUint8();
							if (DBG) {
								log << "\t\t\t\t\t\t\t\t" << y << ":" << endl;
							}
							switch (g1001->activeScriptData[i].stackframe[j].opcodeData[x].parameter[y].type) {
							case 1:
							case 2:
								g1001->activeScriptData[i].stackframe[j].opcodeData[x].parameter[y].data = new uint16_t;
								*static_cast<uint16_t*>(g1001->activeScriptData[i].stackframe[j].opcodeData[x].parameter[y].data) = readUint16();

								if (DBG) {
									log << "\t\t\t\t\t\t\t\t\tstring: " << g1001->strings[*static_cast<uint16_t*>(g1001->activeScriptData[i].stackframe[j].opcodeData[x].parameter[y].data)] << endl;
								}

								break;

							case 3:
								g1001->activeScriptData[i].stackframe[j].opcodeData[x].parameter[y].data = new uint32_t;
								*static_cast<uint32_t*>(g1001->activeScriptData[i].stackframe[j].opcodeData[x].parameter[y].data) = readUint32();

								if (DBG) {
									log << "\t\t\t\t\t\t\t\t\tuint32: " << *(unsigned int*)(g1001->activeScriptData[i].stackframe[j].opcodeData[x].parameter[y].data) << endl;
								}

								break;

							case 4:
								g1001->activeScriptData[i].stackframe[j].opcodeData[x].parameter[y].data = new float;
								*static_cast<float*>(g1001->activeScriptData[i].stackframe[j].opcodeData[x].parameter[y].data) = readFloat();

								if (DBG) {
									log << "\t\t\t\t\t\t\t\t\tfloat: " << *(float*)(g1001->activeScriptData[i].stackframe[j].opcodeData[x].parameter[y].data) << endl;
								}

								break;

							case 5:
								g1001->activeScriptData[i].stackframe[j].opcodeData[x].parameter[y].data = new uint8_t;
								*static_cast<uint8_t*>(g1001->activeScriptData[i].stackframe[j].opcodeData[x].parameter[y].data) = readUint8();

								if (DBG) {
									log << "\t\t\t\t\t\t\t\t\tuint8: " << *(unsigned int*)(g1001->activeScriptData[i].stackframe[j].opcodeData[x].parameter[y].data) << endl;
								}

								break;

							case 0:
								g1001->activeScriptData[i].stackframe[j].opcodeData[x].parameter[y].data = NULL;
								if (DBG) {
									log << "\t\t\t\t\t\t\t\t\tNULL" << endl;
								}
								break;
							default:
								if (DBG)
									log << "error! unknown parameter.type! - " + to_string(g1001->activeScriptData[i].stackframe[j].opcodeData[x].parameter[y].type);
								throw "error! unknown type!";
							}
							//check if additional parameters are present and process them
							if ((((g1001->activeScriptData[i].stackframe[j].opcodeData[x].opcode == 0x17) || (g1001->activeScriptData[i].stackframe[j].opcodeData[x].opcode == 0x18) || (g1001->activeScriptData[i].stackframe[j].opcodeData[x].opcode == 0x19)) && (y == (count - 1)))) {
								if (g1001->activeScriptData[i].stackframe[j].opcodeData[x].parameter[y].type != 3) {
									throw "invalid additional parameter count type!";
								}
								if (DBG) {
									log << "\t\t\t\t\t\t\t\tadditional parameters(" << *static_cast<uint32_t*>(g1001->activeScriptData[i].stackframe[j].opcodeData[x].parameter[y].data) << "):" << endl;
								}
								g1001->activeScriptData[i].stackframe[j].opcodeData[x].extra_parameter = new papyrus_t::activeScriptData_t::stackframe_t::opcodeData_t::parameter_t[*static_cast<uint32_t*>(g1001->activeScriptData[i].stackframe[j].opcodeData[x].parameter[y].data)];
								for (unsigned int a = 0; a < *static_cast<uint32_t*>(g1001->activeScriptData[i].stackframe[j].opcodeData[x].parameter[y].data); a++) {
									g1001->activeScriptData[i].stackframe[j].opcodeData[x].extra_parameter[a].type = readUint8();
									if (DBG) {
										log << "\t\t\t\t\t\t\t\t\t" << a << ":" << endl;
									}
									switch (g1001->activeScriptData[i].stackframe[j].opcodeData[x].extra_parameter[a].type) {
									case 1:
									case 2://parameter is uint16 string table reference
										g1001->activeScriptData[i].stackframe[j].opcodeData[x].extra_parameter[a].data = new uint16_t;
										*static_cast<uint16_t*>(g1001->activeScriptData[i].stackframe[j].opcodeData[x].extra_parameter[a].data) = readUint16();
										if (DBG)
											log << "\t\t\t\t\t\t\t\t\t\tstring: " << g1001->strings[*static_cast<uint16_t*>(g1001->activeScriptData[i].stackframe[j].opcodeData[x].extra_parameter[a].data)] << endl;
										break;
									case 3://parameter is uint32
										g1001->activeScriptData[i].stackframe[j].opcodeData[x].extra_parameter[a].data = new uint32_t;
										*static_cast<uint32_t*>(g1001->activeScriptData[i].stackframe[j].opcodeData[x].extra_parameter[a].data) = readUint32();
										if (DBG)
											log << "\t\t\t\t\t\t\t\t\t\tuint32: " << *static_cast<uint32_t*>(g1001->activeScriptData[i].stackframe[j].opcodeData[x].extra_parameter[a].data) << endl;
										break;
									case 4://parameter is float
										g1001->activeScriptData[i].stackframe[j].opcodeData[x].extra_parameter[a].data = new float;
										*static_cast<float*>(g1001->activeScriptData[i].stackframe[j].opcodeData[x].extra_parameter[a].data) = readFloat();
										if (DBG)
											log << "\t\t\t\t\t\t\t\t\t\tfloat: " << *static_cast<float*>(g1001->activeScriptData[i].stackframe[j].opcodeData[x].extra_parameter[a].data) << endl;
										break;
									case 5://parameter is uint8
										g1001->activeScriptData[i].stackframe[j].opcodeData[x].extra_parameter[a].data = new uint8_t;
										*static_cast<uint8_t*>(g1001->activeScriptData[i].stackframe[j].opcodeData[x].extra_parameter[a].data) = readUint8();
										if (DBG)
											log << "\t\t\t\t\t\t\t\t\t\tuint8: " << (int)*static_cast<uint8_t*>(g1001->activeScriptData[i].stackframe[j].opcodeData[x].extra_parameter[a].data) << endl;
										break;
									case 0://parameter is null????
										g1001->activeScriptData[i].stackframe[j].opcodeData[x].extra_parameter[a].data = NULL;
										if (DBG) {
											log << "\t\t\t\t\t\t\t\t\tNULL" << endl;
										}
										break;
									default:
										if (DBG) {
											log << "invalid extra parameter type: " << (int)g1001->activeScriptData[i].stackframe[j].opcodeData[x].extra_parameter[a].type << endl;
										}
										throw "invalid extra parameter type!";
									}
								}
							}
							else {
								g1001->activeScriptData[i].stackframe[j].opcodeData[x].extra_parameter = NULL;
							}
						}
					}

					g1001->activeScriptData[i].stackframe[j].unknown3 = readUint32();
					g1001->activeScriptData[i].stackframe[j].unknown4 = readVariable_t();

					if (DBG) {
						log << "\t\t\t\t\tunknown3 - " << (unsigned int)g1001->activeScriptData[i].stackframe[j].unknown3 << endl;
						log << "\t\t\t\t\tunknown4 - " << variableToString(g1001->activeScriptData[i].stackframe[j].unknown4, g1001->strings, g1001->strCount) << endl;
						log << "\t\t\t\t\tunknown5(" << g1001->activeScriptData[i].stackframe[j].variablecount << "):" << endl;
					}

					g1001->activeScriptData[i].stackframe[j].unknown5 = new variable_t[g1001->activeScriptData[i].stackframe[j].variablecount];

					for (unsigned int x = 0; x < g1001->activeScriptData[i].stackframe[j].variablecount; x++) {
						g1001->activeScriptData[i].stackframe[j].unknown5[x] = readVariable_t();

						if (DBG) {
							log << "\t\t\t\t\t\t" << variableToString(g1001->activeScriptData[i].stackframe[j].unknown5[x], g1001->strings, g1001->strCount) << endl;
						}
					}
				}

				if (g1001->activeScriptData[i].stackframecount != 0) {
					g1001->activeScriptData[i].unknown5 = readUint8();

					if (DBG)
						log << "\t\t\tunknown5 - " << (unsigned int)g1001->activeScriptData[i].unknown5 << endl;
				}
			}

			g1001->functionMessageCount = readUint32();

			if (DBG) {
				log << "\tfunction messages(" << g1001->functionMessageCount << "):" << endl;
			}

			g1001->functionMessages = new papyrus_t::functionMessage_t[g1001->functionMessageCount];

			for (unsigned int i = 0; i < g1001->functionMessageCount; i++) {
				g1001->functionMessages[i].unknown = readUint8();

				if (g1001->functionMessages[i].unknown <= 2) {
					g1001->functionMessages[i].id = readUint32();  //maybe compare to g1001->activeScripts[i].id ???
					g1001->functionMessages[i].flag = readUint8();
					if (g1001->functionMessages[i].flag > 0) {
						g1001->functionMessages[i].message.unknown0 = readUint8();
						g1001->functionMessages[i].message.scriptName = readUint16();
						g1001->functionMessages[i].message.event = readUint16();
						g1001->functionMessages[i].message.unknown1 = readVariable_t();
						g1001->functionMessages[i].message.variablecount = readUint32();
						g1001->functionMessages[i].message.unknown2 = new variable_t[g1001->functionMessages[i].message.variablecount];
						for (unsigned int j = 0; j < g1001->functionMessages[i].message.variablecount; j++) {
							g1001->functionMessages[i].message.unknown2[j] = readVariable_t();
						}
					}

					if (DBG) {
						log << "\t\t" << i << ":" << endl;
						log << "\t\t\tid - " << g1001->functionMessages[i].id << endl;
						if (g1001->functionMessages[i].id != g1001->activeScript[i].scriptID) {
							log << "\t\t\twarning - id doesn't match activescript.scriptID!" << endl;
						}
						log << "\t\t\tflag - " << (unsigned int)g1001->functionMessages[i].flag << endl;
						if (g1001->functionMessages[i].flag > 0) {
							log << "\t\t\tunknown0 - " << (unsigned int)g1001->functionMessages[i].message.unknown0 << endl;
							log << "\t\t\tscriptName - " << g1001->strings[g1001->functionMessages[i].message.scriptName] << endl;
							log << "\t\t\tevent - " << g1001->strings[g1001->functionMessages[i].message.event] << endl;
							log << "\t\t\tunknown1 - " << variableToString(g1001->functionMessages[i].message.unknown1, g1001->strings, g1001->strCount) << endl;
							log << "\t\t\tvariable(" << g1001->functionMessages[i].message.variablecount << "):" << endl;
							for (unsigned int j = 0; j < g1001->functionMessages[i].message.variablecount; j++) {
								log << "\t\t\t\t" << variableToString(g1001->functionMessages[i].message.unknown2[j], g1001->strings, g1001->strCount) << endl;
							}
						}
					}
				}
			}

			g1001->suspendedStackCount1 = readUint32();
			if (DBG) {
				log << "\tsuspended stacks(" << g1001->suspendedStackCount1 << "):" << endl;
			}
			g1001->suspendedStacks1 = new papyrus_t::suspendedStack_t[g1001->suspendedStackCount1];
			for (unsigned int i = 0; i < g1001->suspendedStackCount1; i++) {
				g1001->suspendedStacks1[i].id = readUint32();
				g1001->suspendedStacks1[i].flag = readUint8();
				if (DBG) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\tid - " << g1001->suspendedStacks1[i].id << endl;
					if (g1001->suspendedStacks1[i].id != g1001->activeScript[i].scriptID) {
						log << "\t\t\twarning - id != active script id!" << endl;
					}
					log << "\t\t\tflag - " << (unsigned int)g1001->suspendedStacks1[i].flag << endl;
				}
				if (g1001->suspendedStacks1[i].flag > 0) {
					g1001->suspendedStacks1[i].message.unknown0 = readUint8();
					g1001->suspendedStacks1[i].message.scriptName = readUint16();
					g1001->suspendedStacks1[i].message.event = readUint16();
					g1001->suspendedStacks1[i].message.unknown1 = readVariable_t();
					g1001->suspendedStacks1[i].message.variablecount = readUint32();
					if (DBG) {
						log << "\t\t\tunknown0 - " << (unsigned int)g1001->suspendedStacks1[i].message.unknown0 << endl;
						log << "\t\t\tscriptName - " << g1001->strings[g1001->suspendedStacks1[i].message.scriptName] << endl;
						log << "\t\t\tevent - " << g1001->strings[g1001->suspendedStacks1[i].message.event] << endl;
						log << "\t\t\tunknown1 - " << variableToString(g1001->suspendedStacks1[i].message.unknown1) << endl;
						log << "\t\t\tvariable(" << g1001->suspendedStacks1[i].message.variablecount << "):" << endl;
					}
					g1001->suspendedStacks1[i].message.unknown2 = new variable_t[g1001->suspendedStacks1[i].message.variablecount];
					for (unsigned int j = 0; j < g1001->suspendedStacks1[i].message.variablecount; j++) {
						g1001->suspendedStacks1[i].message.unknown2[j] = readVariable_t();
						log << "\t\t\t\t" << variableToString(g1001->suspendedStacks1[i].message.unknown2[j]) << endl;
					}
				}
			}

			g1001->suspendedStackCount2 = readUint32();
			if (DBG) {
				log << "\tsuspended stacks2(" << g1001->suspendedStackCount2 << "):" << endl;
			}
			g1001->suspendedStacks2 = new papyrus_t::suspendedStack_t[g1001->suspendedStackCount2];
			for (unsigned int i = 0; i < g1001->suspendedStackCount2; i++) {
				g1001->suspendedStacks2[i].id = readUint32();
				g1001->suspendedStacks2[i].flag = readUint8();
				if (DBG) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\tid - " << g1001->suspendedStacks2[i].id << endl;
					if (g1001->suspendedStacks2[i].id != g1001->activeScript[i].scriptID) {
						log << "\t\t\twarning - id != active script id!" << endl;
					}
					log << "\t\t\tflag - " << (unsigned int)g1001->suspendedStacks2[i].flag << endl;
				}
				if (g1001->suspendedStacks2[i].flag > 0) {
					g1001->suspendedStacks2[i].message.unknown0 = readUint8();
					g1001->suspendedStacks2[i].message.scriptName = readUint16();
					g1001->suspendedStacks2[i].message.event = readUint16();
					g1001->suspendedStacks2[i].message.unknown1 = readVariable_t();
					g1001->suspendedStacks2[i].message.variablecount = readUint32();
					if (DBG) {
						log << "\t\t\tunknown0 - " << (unsigned int)g1001->suspendedStacks2[i].message.unknown0 << endl;
						log << "\t\t\tscriptName - " << g1001->strings[g1001->suspendedStacks2[i].message.scriptName] << endl;
						log << "\t\t\tevent - " << g1001->strings[g1001->suspendedStacks2[i].message.event] << endl;
						log << "\t\t\tunknown1 - " << variableToString(g1001->suspendedStacks2[i].message.unknown1) << endl;
						log << "\t\t\tvariable(" << g1001->suspendedStacks2[i].message.variablecount << "):" << endl;
					}
					g1001->suspendedStacks2[i].message.unknown2 = new variable_t[g1001->suspendedStacks2[i].message.variablecount];
					for (unsigned int j = 0; j < g1001->suspendedStacks2[i].message.variablecount; j++) {
						g1001->suspendedStacks2[i].message.unknown2[j] = readVariable_t();
						log << "\t\t\t\t" << variableToString(g1001->suspendedStacks2[i].message.unknown2[j]) << endl;
					}
				}
			}

			g1001->unknown1 = readUint32();
			if (g1001->unknown1 != 0) { //also check if skyrim vm version > 2??
				g1001->unknown2 = readUint32();
			}

			g1001->unknown0Count = readUint32();
			g1001->unknowns0 = new uint32_t[g1001->unknown0Count];
			for (unsigned int i = 0; i < g1001->unknown0Count; i++) {
				g1001->unknowns0[i] = readUint32();
			}

			g1001->queuedUnbindCount = readUint32();//also check if skyrim vm version > 4??

			g1001->queuedUnbinds = new papyrus_t::queudUnbind_t[g1001->queuedUnbindCount];
			for (unsigned int i = 0; i < g1001->queuedUnbindCount; i++) {//also check if skyrim vm version > 4??
				g1001->queuedUnbinds[i].id = readUint32();
				if (g1001->queuedUnbinds[i].id != g1001->scriptInstance[i].scriptID) {
					//throw "queuedunbinds.id != scriptinstance.id";
					log << "queuedunbinds.id != scriptinstance.id" << endl;
				}
				g1001->queuedUnbinds[i].unknown = readUint32();
			}

			g1001->saveFileVersion = readInt16();
			if (g1001->saveFileVersion == -1) {
				throw "error - invalid save! (papyrus.saveFileVersion)";
			}

			g1001->arrayCount1 = readUint32();
			g1001->array1 = new papyrus_t::array1_t[g1001->arrayCount1];
			for (unsigned int i = 0; i < g1001->arrayCount1; i++) {
				g1001->array1[i].unknown0 = readUint32();
				g1001->array1[i].unknown1 = readUint32();
			}

			g1001->arrayCount1a = readUint32();
			g1001->array1a = new papyrus_t::array1a_t[g1001->arrayCount1a];
			for (unsigned int i = 0; i < g1001->arrayCount1a; i++) {
				g1001->array1a[i].unknown0 = readUint32();
				g1001->array1a[i].unknown1 = readUint32();
			}

			g1001->arrayCount2 = readUint32();
			g1001->array2 = new papyrus_t::array2_t[g1001->arrayCount2];
			for (unsigned int i = 0; i < g1001->arrayCount2; i++) {
				g1001->array2[i].unknown0 = readUint32();
				g1001->array2[i].unknown1 = readUint32();
			}

			g1001->arrayCount3 = readUint32();
			g1001->array3 = new papyrus_t::array3_t[g1001->arrayCount3];
			for (unsigned int i = 0; i < g1001->arrayCount3; i++) {
				g1001->array3[i].type = readUint8();
				g1001->array3[i].string0 = readUint16();
				g1001->array3[i].unknown0 = readUint16();
				g1001->array3[i].string1 = readUint16();
				g1001->array3[i].unknown1 = readUint32();
				g1001->array3[i].flags = readUint16();
				g1001->array3[i].reference = readRefID();
			}

			g1001->arrayCount4 = readUint32();
			g1001->array4 = new papyrus_t::array4_t[g1001->arrayCount4];
			for (unsigned int i = 0; i < g1001->arrayCount4; i++) {
				g1001->array4[i].string0 = readUint16();
				g1001->array4[i].unknown0 = readUint16();
				g1001->array4[i].unknown1 = readUint8();
				g1001->array4[i].string1 = readUint16();
				g1001->array4[i].unknown2 = readUint32();
				g1001->array4[i].flags = readUint16();
				g1001->array4[i].reference = readRefID();
			}

			g1001->scriptListCount = readUint32();
			g1001->scriptList = new w32string_t[g1001->scriptListCount];
			for (unsigned int i = 0; i < g1001->scriptListCount; i++) {
				g1001->scriptList[i].length = readUint32();
				g1001->scriptList[i].string = new uint8_t[g1001->scriptList[i].length];
				for (unsigned int j = 0; j < g1001->scriptList[i].length; j++) {
					g1001->scriptList[i].string[j] = readUint8();
				}
			}

			g1001->arrayCount4a = readUint32();
			g1001->arrayCount4b = readUint32();
			g1001->array4b = new papyrus_t::array4b_t[g1001->arrayCount4b];
			for (unsigned int i = 0; i < g1001->arrayCount4b; i++) {
				g1001->array4b[i].unknown0 = readUint8();
				g1001->array4b[i].unknown1 = readUint16();
				g1001->array4b[i].unknown2 = readUint16();
				g1001->array4b[i].reference0 = readRefID();
				g1001->array4b[i].reference1 = readRefID();
				g1001->array4b[i].reference2 = readRefID();
				g1001->array4b[i].reference3 = readRefID();
			}

			g1001->arrayCount4c = readUint32();
			g1001->array4c = new papyrus_t::array4c_t[g1001->arrayCount4c];
			for (unsigned int i = 0; i < g1001->arrayCount4c; i++) {
				g1001->array4c[i].flag = readUint8();
				g1001->array4c[i].unknown0 = readUint32();
				g1001->array4c[i].reference0 = readRefID();
				if (g1001->array4c[i].flag == 0 || g1001->array4c[i].flag == 6) {
					for (unsigned char j = 0; j < 3; j++) {
						g1001->array4c[i].unknown1[j] = readUint32();
					}
				}
				if (g1001->array4c[i].flag == 0) {
					for (unsigned char j = 0; j < 4; j++) {
						g1001->array4c[i].unknown2[j] = readUint32();
					}
				}
				if (g1001->array4c[i].flag == 0 || g1001->array4c[i].flag == 3) {
					g1001->array4c[i].unknown3 = readUint8();
				}
			}

			g1001->arrayCount4d = readUint32();
			g1001->array4d = new papyrus_t::array4d_t[g1001->arrayCount4d];
			for (unsigned int i = 0; i < g1001->arrayCount4d; i++) {
				g1001->array4d[i].flag0 = readUint8();
				g1001->array4d[i].unknown = readUint32();
				g1001->array4d[i].flag1 = readUint8();
				g1001->array4d[i].reference = readRefID();
			}

			g1001->arrayCount5 = readUint32();
			g1001->array5 = new papyrus_t::array5_t[g1001->arrayCount5];
			for (unsigned int i = 0; i < g1001->arrayCount5; i++) {
				g1001->array5[i].unknown0 = readUint16();
				g1001->array5[i].unknown1 = readUint16();
				g1001->array5[i].reference0 = readRefID();
				g1001->array5[i].reference1 = readRefID();
				g1001->array5[i].reference2 = readRefID();
				g1001->array5[i].unknown2 = readUint16();
			}

			g1001->arrayCount6 = readUint32();
			g1001->array6 = new papyrus_t::array6_t[g1001->arrayCount6];
			for (unsigned int i = 0; i < g1001->arrayCount6; i++) {
				g1001->array6[i].unknown = readUint16();
				g1001->array6[i].flags = readUint16();
				g1001->array6[i].reference = readRefID();
			}

			g1001->arrayCount7 = readUint32();
			g1001->array7 = new papyrus_t::array7_t[g1001->arrayCount7];
			for (unsigned int i = 0; i < g1001->arrayCount7; i++) {
				g1001->array7[i].unknown = readUint16();
				g1001->array7[i].flags = readUint16();
				g1001->array7[i].reference = readRefID();
			}

			g1001->arrayCount8 = readUint32();
			g1001->array8 = new papyrus_t::array8_t[g1001->arrayCount8];
			for (unsigned int i = 0; i < g1001->arrayCount8; i++) {
				g1001->array8[i].unknown = readUint16();
				g1001->array8[i].type = readUint16();
				g1001->array8[i].reference = readRefID();
				g1001->array8[i].count0 = readUint32();
				g1001->array8[i].count1 = readUint32();
				g1001->array8[i].reference0 = new refID_t[g1001->array8[i].count0];
				for (unsigned int j = 0; j < g1001->array8[i].count0; j++) {
					g1001->array8[i].reference0[j] = readRefID();
				}
				g1001->array8[i].reference1 = new refID_t[g1001->array8[i].count1];
				for (unsigned int j = 0; j < g1001->array8[i].count1; j++) {
					g1001->array8[i].reference1[j] = readRefID();
				}
			}

			g1001->arrayCount9 = readUint32();
			g1001->array9 = new uint32_t[g1001->arrayCount9];
			for (unsigned int i = 0; i < g1001->arrayCount9; i++) {
				g1001->array9[i] = readUint32();
			}

			g1001->arrayCount10 = readUint32();
			g1001->array10 = new papyrus_t::array10_t[g1001->arrayCount10];
			for (unsigned int i = 0; i < g1001->arrayCount10; i++) {
				g1001->array10[i].reference = readRefID();
				g1001->array10[i].count = readUint32();
				g1001->array10[i].elements = new papyrus_t::array10_t::array10element_t[g1001->array10[i].count];
				for (unsigned int j = 0; j < g1001->array10[i].count; j++) {
					g1001->array10[i].elements[j].name.length = readUint32();
					g1001->array10[i].elements[j].name.string = new uint8_t[g1001->array10[i].elements[j].name.length];
					for (unsigned int k = 0; k < g1001->array10[i].elements[j].name.length; k++) {
						g1001->array10[i].elements[j].name.string[k] = readUint8();
					}

					g1001->array10[i].elements[j].count0 = readUint32();
					g1001->array10[i].elements[j].elements0 = new papyrus_t::array10_t::array10element_t::array10element1_t[g1001->array10[i].elements[j].count0];
					for (unsigned int k = 0; k < g1001->array10[i].elements[j].count0; k++) {
						g1001->array10[i].elements[j].elements0[k].count = readUint32();
						g1001->array10[i].elements[j].elements0[k].elements = new papyrus_t::array10_t::array10element_t::array10element1_t::array10element1data_t[g1001->array10[i].elements[j].elements0[k].count];
						for (unsigned int l = 0; l < g1001->array10[i].elements[j].elements0[k].count; l++) {
							g1001->array10[i].elements[j].elements0[k].elements[l].string = readUint16();
							g1001->array10[i].elements[j].elements0[k].elements[l].flag = readUint16();
						}
					}
					g1001->array10[i].elements[j].count1 = readUint32();
					g1001->array10[i].elements[j].elements1 = new papyrus_t::array10_t::array10element_t::array10element2_t[g1001->array10[i].elements[j].count1];
					for (unsigned int k = 0; k < g1001->array10[i].elements[j].count1; k++) {
						g1001->array10[i].elements[j].elements1[k].unknown = readUint16();
						g1001->array10[i].elements[j].elements1[k].flag = readUint16();
						g1001->array10[i].elements[j].elements1[k].reference = readRefID();
					}
				}
			}

			g1001->arrayCount11 = readUint32();
			g1001->array11 = new papyrus_t::array11_t[g1001->arrayCount11];
			for (unsigned int i = 0; i < g1001->arrayCount11; i++) {
				g1001->array11[i].unknown0 = readUint32();
				g1001->array11[i].reference = readRefID();
				g1001->array11[i].unknown1 = readUint8();
			}

			g1001->arrayCount12 = readUint32();
			g1001->array12 = new papyrus_t::array12_t[g1001->arrayCount12];
			for (unsigned int i = 0; i < g1001->arrayCount12; i++) {
				g1001->array12[i].reference = readRefID();
				g1001->array12[i].count = readUint32();
				g1001->array12[i].elements = new papyrus_t::array12_t::element_t[g1001->array12[i].count];
				for (unsigned int j = 0; j < g1001->array12[i].count; j++) {
					g1001->array12[i].elements[j].unknown0 = readUint32();
					g1001->array12[i].elements[j].unknown1 = readUint8();
					g1001->array12[i].elements[j].unknown2 = readUint16();
					g1001->array12[i].elements[j].unknown3 = readUint16();
				}
			}

			g1001->arrayCount13 = readUint32();
			g1001->array13 = new papyrus_t::array13_t[g1001->arrayCount13];
			for (unsigned int i = 0; i < g1001->arrayCount13; i++) {
				g1001->array13[i].reference = readRefID();
				g1001->array13[i].count = readUint32();
				g1001->array13[i].elements = new papyrus_t::array13_t::element_t[g1001->array13[i].count];
				for (unsigned int j = 0; j < g1001->array13[i].count; j++) {
					g1001->array13[i].elements[j].unknown0 = readUint32();
					g1001->array13[i].elements[j].count0 = readUint32();
					g1001->array13[i].elements[j].data0 = new uint32_t[g1001->array13[i].elements[j].count0];
					for (unsigned int k = 0; k < g1001->array13[i].elements[j].count0; k++) {
						g1001->array13[i].elements[j].data0[k] = readUint32();
					}
					g1001->array13[i].elements[j].count1 = readUint32();
					g1001->array13[i].elements[j].data1 = new papyrus_t::array13_t::element_t::data_t[g1001->array13[i].elements[j].count1];
					for (unsigned int k = 0; k < g1001->array13[i].elements[j].count1; k++) {
						for (int l = 0; l < 5; l++) {
							g1001->array13[i].elements[j].data1[k].data[l] = readUint8();
						}
					}
					g1001->array13[i].elements[j].unknown1 = readUint32();
				}

			}

			g1001->arrayCount14 = readUint32();
			g1001->array14 = new papyrus_t::array14_t[g1001->arrayCount14];
			for (unsigned int i = 0; i < g1001->arrayCount14; i++) {
				g1001->array14[i].reference = readRefID();
				g1001->array14[i].count = readUint32();
				g1001->array14[i].elements = new papyrus_t::array14_t::data_t[g1001->array14[i].count];
				for (unsigned int j = 0; j < g1001->array14[i].count; j++) {
					for (unsigned char k = 0; k < 5; k++) {
						g1001->array14[i].elements[j].data[k] = readUint8();
					}
				}
				g1001->array14[i].data = readUint32();
			}

			g1001->arrayCount15 = readUint32();
			g1001->array15 = new papyrus_t::array15_t[g1001->arrayCount15];
			for (unsigned int i = 0; i < g1001->arrayCount15; i++) {
				g1001->array15[i].data = readUint32();
				g1001->array15[i].count = readUint32();
				g1001->array15[i].elements = new uint32_t[g1001->array15[i].count];
				for (unsigned int j = 0; j < g1001->array15[i].count; j++) {
					g1001->array15[i].elements[j] = readUint32();
				}
			}

			if (DBG) {
				log << "\tunknown1 - " << g1001->unknown1 << endl;
				if (g1001->unknown1 != 0) {
					log << "\tunknown2 - " << g1001->unknown2 << endl;
				}
				log << "\tunknown0(" << g1001->unknown0Count << "):" << endl;
				for (unsigned int i = 0; i < g1001->unknown0Count; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\t" << g1001->unknowns0[i] << endl;
				}
				log << "\tqueued unbinds(" << g1001->queuedUnbindCount << "):" << endl;
				for (unsigned int i = 0; i < g1001->queuedUnbindCount; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\tid - " << g1001->queuedUnbinds[i].id << endl;
					log << "\t\t\tname - " << g1001->strings[g1001->scriptInstance[i].scriptName] << endl;
					log << "\t\t\tunknown - " << g1001->queuedUnbinds[i].unknown << endl;
				}
				log << "\tsaveFileVersion - " << g1001->saveFileVersion << endl;
				log << "\tarray1(" << g1001->arrayCount1 << "):" << endl;
				for (unsigned int i = 0; i < g1001->arrayCount1; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\tunknown0 - " << g1001->array1[i].unknown0 << endl;
					log << "\t\t\tunknown1 - " << g1001->array1[i].unknown1 << endl;
				}
				log << "\tarray1a(" << g1001->arrayCount1a << "):" << endl;
				for (unsigned int i = 0; i < g1001->arrayCount1a; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\tunknown0 - " << g1001->array1a[i].unknown0 << endl;
					log << "\t\t\tunknown1 - " << g1001->array1a[i].unknown1 << endl;
				}
				log << "\tarray2(" << g1001->arrayCount2 << "):" << endl;
				for (unsigned int i = 0; i < g1001->arrayCount2; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\tunknown0 - " << g1001->array2[i].unknown0 << endl;
					log << "\t\t\tunknown1 - " << g1001->array2[i].unknown1 << endl;
				}
				log << "\tarray3(" << g1001->arrayCount3 << "):" << endl;
				for (unsigned int i = 0; i < g1001->arrayCount3; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\ttype - " << (unsigned int)g1001->array3[i].type << endl;
					//log << "\t\t\tstring0 - " << g1001->strings.at(g1001->array3[i].string0) << "(" << g1001->array3[i].string1 << ")" << endl;
					log << "\t\t\tstring0 - " << g1001->array3[i].string0 << endl;
					log << "\t\t\tunknown0 - " << g1001->array3[i].unknown0 << endl;
					//log << "\t\t\tstring1 - " << g1001->strings.at(g1001->array3[i].string1) << "(" << g1001->array3[i].string1 << ")" << endl;
					log << "\t\t\tstring1 - " << g1001->array3[i].string1 << endl;
					log << "\t\t\tunknown1 - " << g1001->array3[i].unknown1 << endl;
					log << "\t\t\tflags - " << g1001->array3[i].flags << endl;
					log << "\t\t\treference - " << refIDtoString(g1001->array3[i].reference) << endl;
				}
				log << "\tarray4(" << g1001->arrayCount4 << "):" << endl;
				for (unsigned int i = 0; i < g1001->arrayCount4; i++) {
					log << "\t\t" << i << ":" << endl;
					//log << "\t\t\tstring0 - " << g1001->strings.at(g1001->array4[i].string0) << "(" << g1001->array4[i].string0 << ")" << endl;
					log << "\t\t\tstring0 - " << g1001->array4[i].string0 << endl;
					log << "\t\t\tunknown0 - " << g1001->array4[i].unknown0 << endl;
					log << "\t\t\tunknown1 - " << (unsigned int)g1001->array4[i].unknown1 << endl;
					//log << "\t\t\tstring1 - " << g1001->strings.at(g1001->array4[i].string1) << "(" << g1001->array4[i].string1 << ")" << endl;
					log << "\t\t\tstring1 - " << g1001->array4[i].string1 << endl;
					log << "\t\t\tunknown2 - " << g1001->array4[i].unknown2 << endl;
					log << "\t\t\tflags - " << g1001->array4[i].flags << endl;
					log << "\t\t\treference - " << refIDtoString(g1001->array4[i].reference) << endl;
				}
				log << "\tscript list(" << g1001->scriptListCount << "):" << endl;
				for (unsigned int i = 0; i < g1001->scriptListCount; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\tlength - " << g1001->scriptList[i].length << endl;
					log << "\t\t\tstring - ";
					for (unsigned int j = 0; j < g1001->scriptList[i].length; j++) {
						log << g1001->scriptList[i].string[j];
					}
					log << endl;
				}
				log << "\tarrayCount4a - " << g1001->arrayCount4a << endl;
				log << "\tarray4b(" << g1001->arrayCount4b << "):" << endl;
				for (unsigned int i = 0; i < g1001->arrayCount4b; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\tunknown0 - " << (unsigned int)g1001->array4b[i].unknown0 << endl;
					log << "\t\t\tunknown1 - " << g1001->array4b[i].unknown1 << endl;
					log << "\t\t\tunknown2 - " << g1001->array4b[i].unknown2 << endl;
					log << "\t\t\treference0 - " << refIDtoString(g1001->array4b[i].reference0) << endl;
					log << "\t\t\treference1 - " << refIDtoString(g1001->array4b[i].reference1) << endl;
					log << "\t\t\treference2 - " << refIDtoString(g1001->array4b[i].reference2) << endl;
					log << "\t\t\treference3 - " << refIDtoString(g1001->array4b[i].reference3) << endl;
				}
				log << "\tarray4c(" << g1001->arrayCount4c << "):" << endl;
				for (unsigned int i = 0; i < g1001->arrayCount4c; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\tflag - " << (unsigned int)g1001->array4c[i].flag << endl;
					log << "\t\t\tunknown0 - " << g1001->array4c[i].unknown0 << endl;
					log << "\t\t\treference0 - " << refIDtoString(g1001->array4c[i].reference0) << endl;

					if (g1001->array4c[i].flag == 0 || g1001->array4c[i].flag == 6) {
						log << "\t\t\tunknown1(3):" << endl;
						for (unsigned char j = 0; j < 3; j++) {
							log << "\t\t\t\t" << (int)j << ":" << endl;
							log << "\t\t\t\t\t" << g1001->array4c[i].unknown1[j] << endl;
						}
					}
					if (g1001->array4c[i].flag == 0) {
						log << "\t\t\tunknown2(4):" << endl;
						for (unsigned char j = 0; j < 4; j++) {
							log << "\t\t\t\t" << (unsigned int)j << ":" << endl;
							log << "\t\t\t\t\t" << g1001->array4c[i].unknown2[j] << endl;
						}
					}
					if (g1001->array4c[i].flag == 0 || g1001->array4c[i].flag == 3) {
						log << "\t\t\tunknown3 - " << (unsigned int)g1001->array4c[i].unknown3 << endl;
					}
				}
				log << "\tarray4d(" << g1001->arrayCount4d << "):" << endl;
				for (unsigned int i = 0; i < g1001->arrayCount4d; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\tflag0 - " << (unsigned int)g1001->array4d[i].flag0 << endl;
					log << "\t\t\tunknown - " << g1001->array4d[i].unknown << endl;
					log << "\t\t\tflag1 - " << (unsigned int)g1001->array4d[i].flag1 << endl;
					log << "\t\t\treference - " << refIDtoString(g1001->array4d[i].reference) << endl;
				}
				log << "\tarray5(" << g1001->arrayCount5 << "):" << endl;
				for (unsigned int i = 0; i < g1001->arrayCount5; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\tunknown0 - " << g1001->array5[i].unknown0 << endl;
					log << "\t\t\tunknown1 - " << g1001->array5[i].unknown1 << endl;
					log << "\t\t\treference0 - " << refIDtoString(g1001->array5[i].reference0) << endl;
					log << "\t\t\treference1 - " << refIDtoString(g1001->array5[i].reference1) << endl;
					log << "\t\t\treference2 - " << refIDtoString(g1001->array5[i].reference2) << endl;
					log << "\t\t\tunknown2 - " << g1001->array5[i].unknown2 << endl;
				}
				log << "\tarray6(" << g1001->arrayCount6 << "):" << endl;
				for (unsigned int i = 0; i < g1001->arrayCount6; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\tunknown - " << g1001->array6[i].unknown << endl;
					log << "\t\t\tflags - " << g1001->array6[i].flags << endl;
					log << "\t\t\treference - " << refIDtoString(g1001->array6[i].reference) << endl;
				}
				log << "\tarray7(" << g1001->arrayCount7 << "):" << endl;
				for (unsigned int i = 0; i < g1001->arrayCount7; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\tunknown - " << g1001->array7[i].unknown << endl;
					log << "\t\t\tflags - " << g1001->array7[i].flags << endl;
					log << "\t\t\treference - " << refIDtoString(g1001->array7[i].reference) << endl;
				}
				log << "\tarray8(" << g1001->arrayCount8 << "):" << endl;
				for (unsigned int i = 0; i < g1001->arrayCount8; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\tunknown - " << g1001->array8[i].unknown << endl;
					log << "\t\t\ttype - " << g1001->array8[i].type << endl;
					log << "\t\t\tcount0 - " << g1001->array8[i].count0 << endl;
					log << "\t\t\tcount1 - " << g1001->array8[i].count1 << endl;
					log << "\t\t\treferences(" << g1001->array8[i].count0 + g1001->array8[i].count1 << "):" << endl;
					for (unsigned int j = 0; j < g1001->array8[i].count0; j++) {
						log << "\t\t\t\t" << j << ":" << endl;
						log << "\t\t\t\t\t" << refIDtoString(g1001->array8[i].reference0[j]) << endl;
					}
					for (unsigned int j = 0; j < g1001->array8[i].count1; j++) {
						log << "\t\t\t\t" << j + g1001->array8[i].count0 << ":" << endl;
						log << "\t\t\t\t\t" << refIDtoString(g1001->array8[i].reference1[j]) << endl;
					}
				}
				log << "\tarray9(" << g1001->arrayCount9 << "):" << endl;
				for (unsigned int i = 0; i < g1001->arrayCount9; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\t" << g1001->array9[i] << endl;
				}
				log << "\tarray10(" << g1001->arrayCount10 << "):" << endl;
				for (unsigned int i = 0; i < g1001->arrayCount10; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\treference - " << refIDtoString(g1001->array10[i].reference) << endl;
					log << "\t\t\telements(" << g1001->array10[i].count << "):" << endl;
					for (unsigned int j = 0; j < g1001->array10[i].count; j++) {
						log << "\t\t\t\t" << j << ":" << endl;
						log << "\t\t\t\t\tname.length - " << g1001->array10[i].elements[j].name.length << endl;
						log << "\t\t\t\t\tname.string - ";
						for (unsigned int k = 0; k < g1001->array10[i].elements[j].name.length; k++) {
							log << g1001->array10[i].elements[j].name.string[k];
						}
						log << endl << "\t\t\t\t\tsubelements0(" << g1001->array10[i].elements[j].count0 << "):" << endl;
						for (unsigned int k = 0; k < g1001->array10[i].elements[j].count0; k++) {
							log << "\t\t\t\t\t\t" << k << ":" << endl;
							log << "\t\t\t\t\t\t\telement data(" << g1001->array10[i].elements[j].elements0[k].count << "):" << endl;
							for (unsigned int l = 0; l < g1001->array10[i].elements[j].elements0[k].count; l++) {
								log << "\t\t\t\t\t\t\t\t" << l << ":" << endl;
								log << "\t\t\t\t\t\t\t\t\tstring - " << g1001->strings[g1001->array10[i].elements[j].elements0[k].elements[l].string] << "(" << g1001->array10[i].elements[j].elements0[k].elements[l].string << ")" << endl;
								log << "\t\t\t\t\t\t\t\t\tflag - " << g1001->array10[i].elements[j].elements0[k].elements[l].flag << endl;

							}

						}
						log << "\t\t\t\t\tsubelements1(" << g1001->array10[i].elements[j].count1 << "):" << endl;
						for (unsigned int k = 0; k < g1001->array10[i].elements[j].count1; k++) {
							log << "\t\t\t\t\t\t" << k << ":" << endl;
							log << "\t\t\t\t\t\t\tunknown - " << g1001->array10[i].elements[j].elements1[k].unknown << endl;
							log << "\t\t\t\t\t\t\tflag - " << g1001->array10[i].elements[j].elements1[k].flag << endl;
							log << "\t\t\t\t\t\t\treference - " << refIDtoString(g1001->array10[i].elements[j].elements1[k].reference) << endl;
						}
					}
				}
				log << "\tarray11(" << g1001->arrayCount11 << "):" << endl;
				for (unsigned int i = 0; i < g1001->arrayCount11; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\tunknown0 - " << g1001->array11[i].unknown0 << endl;
					log << "\t\t\treference - " << refIDtoString(g1001->array11[i].reference) << endl;
					log << "\t\t\tunknown1 - " << g1001->array11[i].unknown1 << endl;
				}
				log << "\tarray12(" << g1001->arrayCount12 << "):" << endl;
				for (unsigned int i = 0; i < g1001->arrayCount12; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\treference - " << refIDtoString(g1001->array12[i].reference) << endl;
					log << "\t\t\telements(" << g1001->array12[i].count << "):" << endl;
					for (unsigned int j = 0; j < g1001->array12[i].count; j++) {
						log << "\t\t\t\t" << j << ":" << endl;
						log << "\t\t\t\t\t" << g1001->array12[i].elements[j].unknown0 << endl;
						log << "\t\t\t\t\t" << (unsigned int)g1001->array12[i].elements[j].unknown1 << endl;
						log << "\t\t\t\t\t" << g1001->array12[i].elements[j].unknown2 << endl;
						log << "\t\t\t\t\t" << g1001->array12[i].elements[j].unknown3 << endl;
					}
				}
				log << "\tarray13(" << g1001->arrayCount13 << "):" << endl;
				for (unsigned int i = 0; i < g1001->arrayCount13; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\treference - " << refIDtoString(g1001->array13[i].reference) << endl;
					log << "\t\t\telements0(" << g1001->array13[i].count << "):" << endl;
					for (unsigned int j = 0; j < g1001->array13[i].count; j++) {
						log << "\t\t\t\t" << j << ":" << endl;
						log << "\t\t\t\t\tunknown0 - " << g1001->array13[i].elements[j].unknown0 << endl;
						log << "\t\t\t\t\tdata0(" << g1001->array13[i].elements[j].unknown0 << "):" << endl;
						for (unsigned int k = 0; k < g1001->array13[i].elements[j].count0; k++) {
							log << "\t\t\t\t\t" << k << ":" << endl;
							log << "\t\t\t\t\t\t" << g1001->array13[i].elements[j].data0[k] << endl;
						}
						log << "\t\t\t\tdata1(" << g1001->array13[i].elements[j].count1 << "):" << endl;
						for (unsigned int k = 0; k < g1001->array13[i].elements[j].count1; k++) {
							log << "\t\t\t\t\t" << k << ":" << endl;
							for (int l = 0; l < 5; l++) {
								log << "\t\t\t\t\t" << (unsigned int)g1001->array13[i].elements[j].data1[k].data[l] << endl;
							}

						}
						log << "\t\t\t\t\tunknown1 - " << g1001->array13[i].elements[j].unknown1 << endl;
					}
				}
				log << "\tarray14(" << g1001->arrayCount14 << "):" << endl;
				for (unsigned int i = 0; i < g1001->arrayCount14; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\treference - " << refIDtoString(g1001->array14[i].reference) << endl;
					log << "\t\t\telements(" << g1001->array14[i].count << "):" << endl;
					for (unsigned int j = 0; j < g1001->array14[i].count; j++) {
						log << "\t\t\t\t" << j << ":" << endl;
						for (unsigned char k = 0; k < 5; k++) {
							log << "\t\t\t\t\t" << (unsigned int)g1001->array14[i].elements[j].data[k] << endl;
						}


					}
					log << "\t\t\tdata - " << g1001->array14[i].data << endl;
				}
				log << "\tarray15(" << g1001->arrayCount15 << "):" << endl;
				for (unsigned int i = 0; i < g1001->arrayCount15; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\tdata - " << g1001->array15[i].data << endl;
					log << "\t\t\telements(" << g1001->array15[i].count << "):" << endl;
					for (unsigned int j = 0; j < g1001->array15[i].count; j++) {
						log << "\t\t\t\t" << j << ":" << endl;
						log << "\t\t\t\t\t" << g1001->array15[i].elements[j] << endl;
					}
				}
			}

			if ((unsigned int)(pos - pos_start) != data.length) {
				throw "wrong position after reading globaldata.papyrus!";
			}

			data.data = static_cast<void*>(g1001);
			break;
		}

		case 1002: { //anim objects
			g1002 = new animObjects_t;
			pos_start = pos;
			g1002->count = readUint32();
			g1002->objects = new animObjects_t::animObject_t[g1002->count];

			for (unsigned int i = 0; i < g1002->count; i++) {
				g1002->objects[i].achr = readRefID();
				g1002->objects[i].anim = readRefID();
				g1002->objects[i].unknown = readUint8();
			}

			if (DBG) {
				log << "anim objects:" << endl;
				log << "\tlength - " << data.length << endl;
				log << "\tobjects(" << g1002->count << "):" << endl;
				for (unsigned int i = 0; i < g1002->count; i++) {
					log << "\t\t" << i << ":" << endl;
					log << "\t\t\tachr - " << refIDtoString(g1002->objects[i].achr) << endl;
					log << "\t\t\tanim - " << refIDtoString(g1002->objects[i].anim) << endl;
					log << "\t\t\tunknown - " << (unsigned int)g1002->objects[i].unknown << endl;
				}
			}

			if ((unsigned int)(pos - pos_start) != data.length) {
				throw "wrong position after reading globaldata.animObjects!";
			}

			data.data = static_cast<void*>(g1002);
			break;
		}

		case 1003: { //timer
			pos_start = pos;
			g1003 = new timer_t;
			g1003->unknown1 = readUint32();
			g1003->unknown2 = readUint32();

			if (DBG) {
				log << "timer:" << endl;
				log << "\tlength - " << data.length << endl;
				log << "\tunknown1 - " << g1003->unknown1 << endl;
				log << "\tunknown2 - " << g1003->unknown2 << endl;
			}

			if ((unsigned int)(pos - pos_start) != data.length) {
				throw "wrong position after reading globaldata.timer!";
			}

			data.data = static_cast<void*>(g1003);
			break;
		}

		case 1004: { //synchronized animations
			g1004 = new uint8_t[data.length];
			for (unsigned int i = 0; i < data.length; i++) {
				g1004[i] = readByte();
			}
			if (DBG) {
				log << "synchronized animations:" << endl;
				log << "\tdata(" << data.length << "):" << endl;
				for (unsigned int i = 0; i < data.length; i++) {
					log << "\t\t" << (unsigned int)g1004[i] << endl;
				}
				log << endl;
			}

			break;
		}

		case 1005: { //main
			g1005 = new uint8_t[data.length];
			for (unsigned int i = 0; i < data.length; i++) {
				g1005[i] = readByte();
			}
			if (DBG) {
				log << "main:" << endl;
				log << "\tdata(" << data.length << "):" << endl;
				for (unsigned int i = 0; i < data.length; i++) {
					log << "\t\t" << (unsigned int)g1005[i] << endl;
				}
				log << endl;
			}

			break;
		}

		default:
			cout << "invalid globaldata type\n";
			throw "invalid globaldata type!!";

		}

		return data;
	}
	catch (const char* str) {
		throw str;
	}
	catch (exception ex) {
		throw (string)ex.what();
	}
	catch (...) {
		throw "fatal error!";
	}
}

bool saveFile::read(void *data, size_t length) {

	cout << "reading file..." << endl;

	this->memblock = (uint8_t *)data;
	this->size = length;

	try {
		for (int i = 0; i < 13; i++) {
			file.magic[i] = (char)readByte();
		}

		if (strncmp(file.magic, "TESV_SAVEGAME", 13) != 0) {
			throw "File is not a Skyrim savegame!";
		}

		file.headerSize = readUint32();
		file.header.version = readUint32();


		if (file.header.version < 7 || file.header.version > 9) {
			throw "unsupported skyrim version, should be between 7 and 9, but is " + to_string(file.header.version);
		}

		file.header.saveNumber = readUint32();
		file.header.playerName = readString();
		file.header.playerLevel = readUint32();
		file.header.playerLocation = readString();
		file.header.gameDate = readString();
		file.header.playerRaceEditorId = readString();
		file.header.playerSex = readUint16();

		if (file.header.playerSex != 0 && file.header.playerSex != 1) {
			throw "invalid player sex in header!";
		}

		file.header.playerCurExp = readFloat();
		file.header.playerLvlUpExp = readFloat();
		//file.header.filetime = new byte[8];
		for (uint8_t i = 0; i < 8; i++) {
			file.header.filetime[i] = readByte();
		}
		//file.header.filetime.dwHighDateTime = readUint32();
		//file.header.filetime.dwLowDateTime = readUint32();
		file.header.shotWidth = readUint32();
		file.header.shotHeight = readUint32();

		if ((unsigned long)(pos - 17) != file.headerSize) {
			throw ("position after reading header seems to be wrong, should be " + to_string(file.headerSize + 17) + ", but is " + to_string(pos));
		}

		file.screenshotData = new uint8_t[file.header.shotHeight * file.header.shotWidth * 3];

		for (unsigned int i = 0; i < file.header.shotHeight * file.header.shotWidth * 3; i++) {
			file.screenshotData[i] = readUint8();
		}

		file.formVersion = readUint8();
		file.pluginInfoSize = readUint32();
		file.pluginInfo.pluginCount = readUint8();
		file.pluginInfo.plugins = new string[file.pluginInfo.pluginCount];

		for (int i = 0; i < file.pluginInfo.pluginCount; i++) {
			file.pluginInfo.plugins[i] = readString();
		}

		file.fileLocationTable.formIDArrayCountOffset = readUint32();
		file.fileLocationTable.unknownTable3Offset = readUint32();
		file.fileLocationTable.globalDataTable1Offset = readUint32();
		file.fileLocationTable.globalDataTable2Offset = readUint32();
		file.fileLocationTable.changeFormsOffset = readUint32();
		file.fileLocationTable.globalDataTable3Offset = readUint32();
		file.fileLocationTable.globalDataTable1Count = readUint32();
		file.fileLocationTable.globalDataTable2Count = readUint32();
		file.fileLocationTable.globalDataTable3Count = readUint32();
		file.fileLocationTable.changeFormCount = readUint32();
		for (short i = 0; i < 15; i++) {
			file.fileLocationTable.unused[i] = readUint32();
		}

		if (DBG) {
			log << "headerSize - " << (unsigned int)file.headerSize << endl;
			log << "header:" << endl;
			log << "\tversion - " << (unsigned int)file.header.version << endl;
			log << "\tsaveNumber - " << (unsigned int)file.header.saveNumber << endl;
			log << "\tplayerName - " << file.header.playerName << endl;
			log << "\tplayerLevel - " << file.header.playerLevel << endl;
			log << "\tplayerLocation - " << file.header.playerLocation << endl;
			log << "\tgameDate - " << file.header.gameDate << endl;
			log << "\tplayerRaceEditorID - " << file.header.playerRaceEditorId << endl;
			log << "\tplayerSex - ";
			(file.header.playerSex == 0) ? log << "male" << endl : log << "female" << endl;
			log << "\tplayerCurExp - " << file.header.playerCurExp << endl;
			log << "\tplayerLvlUpExp - " << file.header.playerLvlUpExp << endl;
			log << "\tfiletime - ";
			for (uint8_t i = 0; i < 8; i++) {
				log << (int)file.header.filetime[i] << " ";
			}
			log << endl;
			//log << "\tfiletime.dwHighDateTime - " << (unsigned int)file.header.filetime.dwHighDateTime << endl;
			//log << "\tfiletime.dwLowDateTime - " << (unsigned int)file.header.filetime.dwLowDateTime << endl;
			log << "\tshotWidth - " << (unsigned int)file.header.shotWidth << endl;
			log << "\tshotHeight - " << (unsigned int)file.header.shotHeight << endl;
			log << "formVersion - " << (unsigned int)file.formVersion << endl;
			log << "pluginInfoSize - " << (unsigned int)file.pluginInfoSize << endl;
			log << "pluginInfo(" << (unsigned int)file.pluginInfo.pluginCount << "):" << endl;

			for (unsigned int i = 0; i < file.pluginInfo.pluginCount; i++)
				log << "\t" << file.pluginInfo.plugins[i] << endl;

			log << "\nfile location table:" << endl;
			log << "\tformIDArrayCountOffset - " << (unsigned int)file.fileLocationTable.formIDArrayCountOffset << endl;
			log << "\tunknownTable3Offset - " << (unsigned int)file.fileLocationTable.unknownTable3Offset << endl;
			log << "\tglobalDataTable1Offset - " << (unsigned int)file.fileLocationTable.globalDataTable1Offset << endl;
			log << "\tglobalDataTable2Offset - " << (unsigned int)file.fileLocationTable.globalDataTable2Offset << endl;
			log << "\tchangeFormsOffset - " << (unsigned int)file.fileLocationTable.changeFormsOffset << endl;
			log << "\tglobalDataTable3Offset - " << (unsigned int)file.fileLocationTable.globalDataTable3Offset << endl;
			log << "\tglobalDataTable1Count - " << (unsigned int)file.fileLocationTable.globalDataTable1Count << endl;
			if (file.fileLocationTable.globalDataTable1Count != 9)
				log << "!!Warning!!, globalDataTable1Count should be 9, but is " << file.fileLocationTable.globalDataTable1Count << endl;
			log << "\tglobalDataTable2Count - " << (unsigned int)file.fileLocationTable.globalDataTable2Count << endl;
			if (file.fileLocationTable.globalDataTable2Count != 14)
				log << "!!Warning!!, globalDataTable2Count should be 14, but is " << file.fileLocationTable.globalDataTable2Count << endl;
			log << "\tglobalDataTable3Count - " << (unsigned int)file.fileLocationTable.globalDataTable3Count << endl;
			if (file.fileLocationTable.globalDataTable3Count != 5)
				log << "!!Warning!!, globalDataTable3Count should be 5, but is " << file.fileLocationTable.globalDataTable3Count << endl;
			log << "\tchangeFormsCount - " << (unsigned int)file.fileLocationTable.changeFormCount << endl << endl;
		}

		for (short i = 0; i < 15; i++) {
			if (file.fileLocationTable.unused[i] != 0) {
				throw "unused != 0!";
			}
		}

		if ((unsigned long)pos != file.fileLocationTable.globalDataTable1Offset) {
			throw ("globalDataTable1: current position seems to be wrong, should be " + to_string(file.fileLocationTable.globalDataTable1Offset) + ", but is " + to_string(pos));
		}

		file.globalDataTable1 = new globalData_t[file.fileLocationTable.globalDataTable1Count];
		for (unsigned int i = 0; i < file.fileLocationTable.globalDataTable1Count; i++) {
			file.globalDataTable1[i] = readGlobalData();
		}

		if ((unsigned long)pos != file.fileLocationTable.globalDataTable2Offset) {
			throw "wrong position after reading globalDataTable1!\n";
		}

		file.globalDataTable2 = new globalData_t[file.fileLocationTable.globalDataTable2Count];
		for (unsigned int i = 0; i < file.fileLocationTable.globalDataTable2Count; i++) {
			file.globalDataTable2[i] = readGlobalData();
		}

		if ((unsigned long)pos != file.fileLocationTable.changeFormsOffset) {
			throw "wrong position after reading globalDataTable2!\n";
		}

		file.changeForms = new changeForm_t[file.fileLocationTable.changeFormCount];

		for (unsigned int i = 0; i < file.fileLocationTable.changeFormCount; i++) {
			file.changeForms[i].formID = readRefID();
			file.changeForms[i].changeFlags = readUint32();
			file.changeForms[i].type = readUint8();
			file.changeForms[i].version = readUint8();

			if (file.changeForms[i].version != 74 && file.changeForms[i].version != 73 && file.changeForms[i].version != 64 && file.changeForms[i].version != 57) {
				throw ("changeForms: unknown skyrim version in changeforms; should be 57, 64, 73 or 74, but is " + to_string(file.changeForms[i].version));
			}

			if (file.changeForms[i].type > 0x3F && file.changeForms[i].type <= 0x7F) {
				file.changeForms[i].length1 = readUint16();
				file.changeForms[i].length2 = readUint16();
			}
			else if (file.changeForms[i].type > 0x7F) {
				file.changeForms[i].length1 = readUint32();
				file.changeForms[i].length2 = readUint32();
			}
			else {
				file.changeForms[i].length1 = readUint8();
				file.changeForms[i].length2 = readUint8();
			}

			file.changeForms[i].data = new uint8_t[file.changeForms[i].length1];

			for (unsigned int j = 0; j < file.changeForms[i].length1; j++) {
				file.changeForms[i].data[j] = readUint8();
			}

		}

		if ((unsigned long)pos != file.fileLocationTable.globalDataTable3Offset) {
			throw "wrong position after reading changeForms!\n";
		}

		//file.globalDataTable3 = new globalData_t[file.fileLocationTable.globalDataTable3Count];
		file.globalDataTable3 = new globalData_t[6];
		//for (unsigned int i = 0; i < file.fileLocationTable.globalDataTable3Count; i++) {
		for (unsigned int i = 0; i < 6; i++) {
			file.globalDataTable3[i] = readGlobalData();
		}

		if ((unsigned long)pos != file.fileLocationTable.formIDArrayCountOffset) {
			throw ((const char*)"FormIDArray: wrong position " + to_string(pos) + ", should be " + to_string(file.fileLocationTable.formIDArrayCountOffset));
		}

		file.formIDArrayCount = readUint32();
		file.formIDArray = new uint32_t[file.formIDArrayCount];
		for (unsigned int i = 0; i < file.formIDArrayCount; i++) {
			file.formIDArray[i] = readUint32();
		}
		if (DBG) {
			log << "form IDs (" << file.formIDArrayCount << "):" << endl;
			for (unsigned int i = 0; i < file.formIDArrayCount; i++) {
				log << "\t" << i << ":" << endl;
				log << "\t\t" << file.formIDArray[i] << endl;
			}
			log << endl;
		}

		file.visitedWorldspaceArrayCount = readUint32();
		file.visitedWorldspaceArray = new uint32_t[file.visitedWorldspaceArrayCount];
		for (unsigned int i = 0; i < file.visitedWorldspaceArrayCount; i++) {
			file.visitedWorldspaceArray[i] = readUint32();
		}

		if (DBG) {
			log << "visited worldspaces (" << file.visitedWorldspaceArrayCount << "):" << endl;
			for (unsigned int i = 0; i < file.visitedWorldspaceArrayCount; i++) {
				log << "\t" << i << ":" << endl;
				log << "\t\t" << file.visitedWorldspaceArray[i] << endl;
			}
			log << endl;
		}


		if ((unsigned long)pos != file.fileLocationTable.unknownTable3Offset) {
			throw ("wrong position after reading visited worldspaces! current: " + to_string(pos) + ", expected: " + to_string(file.fileLocationTable.unknownTable3Offset));
		}
		file.unknown3TableSize = readUint32();
		file.unknown3Table.count = readUint32();
		long pos_start = pos;
		file.unknown3Table.unknown = new string[file.unknown3Table.count];
		for (unsigned int i = 0; i < file.unknown3Table.count; i++) {
			file.unknown3Table.unknown[i] = readString();
			pos_start++;
		}
		if (DBG) {
			log << "unknown table (" << file.unknown3Table.count << "):" << endl;
			for (unsigned int i = 0; i < file.unknown3Table.count; i++) {
				log << "\t" << i << ":" << endl;
				log << "\t\t" << file.unknown3Table.unknown[i] << endl;
			}
		}
		if ((pos_start != pos) || (pos != this->size)) {
			log << "wrong position after reading unknown3table!" << endl;
		}
		if (DBG) {
			log << "DONE" << endl;
		}
		cout << "done" << endl;
	}
	catch (const char* e) {
		throw e;
	}
	catch (string e) {
		throw e;
	}
	catch (...) {
		throw "exception cought!";
	}
	return true;
}

bool saveFile::clean() {
	cout << "cleaning file..." << endl;

	//1: clear pluginInfo
	int diff = file.pluginInfoSize - 73;
	file.pluginInfoSize = 73;
	delete[] file.pluginInfo.plugins;
	file.pluginInfo.pluginCount = 5;
	file.pluginInfo.plugins = new string[5]{ "Skyrim.esm", "Update.esm", "Dawnguard.esm", "HearthFires.esm", "Dragonborn.esm" };

	//1.1: fix offsets
	file.fileLocationTable.formIDArrayCountOffset -= diff;
	file.fileLocationTable.unknownTable3Offset -= diff;
	file.fileLocationTable.globalDataTable1Offset -= diff;
	file.fileLocationTable.globalDataTable2Offset -= diff;
	file.fileLocationTable.changeFormsOffset -= diff;
	file.fileLocationTable.globalDataTable3Offset -= diff;


	//2: set player location to Breezehome
	diff = file.header.playerLocation.length() - 10;
	file.header.playerLocation = "Breezehome";

	g1->nextObjectId = 4278195454;
	g1->worldspace1.byte0 = 65;
	g1->worldspace1.byte1 = 162;
	g1->worldspace1.byte2 = 111;
	g1->coorX = 4294967295;
	g1->coorY = 4294967295;
	g1->worldspace2.byte0 = 65;
	g1->worldspace2.byte1 = 101;
	g1->worldspace2.byte2 = 168;
	g1->posX = (float)-385.445;
	g1->posY = (float)-672.066;
	g1->posZ = (float)64.35;
	g1->unknown = 0;


	//2.1: fix offsets
	file.headerSize -= diff;
	file.fileLocationTable.formIDArrayCountOffset -= diff;
	file.fileLocationTable.unknownTable3Offset -= diff;
	file.fileLocationTable.globalDataTable1Offset -= diff;
	file.fileLocationTable.globalDataTable2Offset -= diff;
	file.fileLocationTable.changeFormsOffset -= diff;
	file.fileLocationTable.globalDataTable3Offset -= diff;


	//3: clear createdObjects
	g4->weaponCount = 0;
	delete[] g4->weaponEnchTable;
	g4->weaponEnchTable = NULL;
	g4->armourCount = 0;
	delete[] g4->armourEnchTable;
	g4->armourEnchTable = NULL;
	g4->potionCount = 0;
	delete[] g4->potionTable;
	g4->potionTable = NULL;
	g4->poisonCount = 0;
	delete[] g4->poisonTable;
	g4->poisonTable = NULL;

	//3.1: fix offsets
	for (unsigned int i = 0; i < file.fileLocationTable.globalDataTable1Count; i++) {
		if (file.globalDataTable1[i].type == 4) {
			diff = file.globalDataTable1[i].length - 4;
			file.globalDataTable1[i].length = 4;
		}
	}
	file.fileLocationTable.formIDArrayCountOffset -= diff;
	file.fileLocationTable.unknownTable3Offset -= diff;
	file.fileLocationTable.globalDataTable2Offset -= diff;
	file.fileLocationTable.changeFormsOffset -= diff;
	file.fileLocationTable.globalDataTable3Offset -= diff;


	//4: clear processlist
	for (int i = 0; i < 7; i++) {
		g100->allCrimes[i].count = 0;
		delete[] g100->allCrimes[i].crimes;
		g100->allCrimes[i].crimes = NULL;
	}

	//4.1: fix offsets
	for (unsigned int i = 0; i < file.fileLocationTable.globalDataTable2Count; i++) {
		if (file.globalDataTable2[i].type == 100) {
			diff = file.globalDataTable2[i].length - 23;
			file.globalDataTable2[i].length = 23;
			break;
		}
	}
	file.fileLocationTable.formIDArrayCountOffset -= diff;
	file.fileLocationTable.unknownTable3Offset -= diff;
	file.fileLocationTable.changeFormsOffset -= diff;
	file.fileLocationTable.globalDataTable3Offset -= diff;


	//5: clear interface.last used weapons/spells/shouts
	diff = g102->lastUsedWeaponsCount * 3 + g102->lastUsedSpellsCount * 3 + g102->lastUsedShoutsCount * 3;
	if ((g102->lastUsedWeaponsCount <= 0x3fff) && (g102->lastUsedWeaponsCount > 0x3f)) {
		diff += 1;
	}
	else if (g102->lastUsedWeaponsCount > 0x3fff) {
		diff += 2;
	}
	if ((g102->lastUsedSpellsCount <= 0x3fff) && (g102->lastUsedSpellsCount > 0x3f)) {
		diff += 1;
	}
	else if (g102->lastUsedSpellsCount > 0x3fff) {
		diff += 2;
	}
	if ((g102->lastUsedShoutsCount <= 0x3fff) && (g102->lastUsedShoutsCount > 0x3f)) {
		diff += 1;
	}
	else if (g102->lastUsedShoutsCount > 0x3fff) {
		diff += 2;
	}
	g102->lastUsedWeaponsCount = 0;
	delete[] g102->lastUsedWeapons;
	g102->lastUsedWeapons = NULL;
	g102->lastUsedSpellsCount = 0;
	delete[] g102->lastUsedSpells;
	g102->lastUsedSpells = NULL;
	g102->lastUsedShoutsCount = 0;
	delete[] g102->lastUsedShouts;
	g102->lastUsedShouts = NULL;

	//5.1: fix offsets
	for (unsigned int i = 0; i < file.fileLocationTable.globalDataTable2Count; i++) {
		if (file.globalDataTable2[i].type == 102) {
			file.globalDataTable2[i].length -= diff;
			break;
		}
	}
	file.fileLocationTable.formIDArrayCountOffset -= diff;
	file.fileLocationTable.unknownTable3Offset -= diff;
	file.fileLocationTable.changeFormsOffset -= diff;
	file.fileLocationTable.globalDataTable3Offset -= diff;


	cout << "done" << endl;
	return true;
}

template <class T>void saveFile::write(const T data) {
	out.write((char*)&data, sizeof(data));
}

template <class T>void saveFile::write(const T data[], unsigned int length) {
	for (unsigned int i = 0; i < length; i++) {
		out.write((char*)&data[i], sizeof(data[i]));
	}
}

void saveFile::writeString(string str) {
	uint16_t length = (uint16_t)str.size();
	out.write((char*)&length, sizeof(length));
	for (int i = 0; i < length; i++) {
		out.put(str.at(i));
	}
}

void saveFile::writeString(std::string str[], unsigned int length) {
	for (unsigned int i = 0; i < length; i++) {
		writeString(str[i]);
	}
}

void saveFile::writeRefID(refID_t data) {
	write(data.byte0);
	write(data.byte1);
	write(data.byte2);
}

void saveFile::writeRefID(refID_t data[], uint32_t length) {
	for (unsigned int i = 0; i < length; i++) {
		writeRefID(data[i]);
	}
}

void saveFile::writeVsval(vsval data) {
	if (data <= 0x3f) { //value is uint8 _0x3f
		write(static_cast<uint8_t>(data << 2));
	}
	else if (data <= 0x3fff) {//value is uint16
		write(static_cast<uint16_t>((data << 2) | 1));
	}
	else {//value is uint32
		write((data << 2) | 2);
	}

}

void saveFile::writeEnchantment(createdObjects::enchantment_t data[], unsigned int length) {
	for (unsigned int i = 0; i < length; i++) {
		writeRefID(data[i].refID);
		write(data[i].timesUsed);
		writeVsval(data[i].count);
		for (unsigned int j = 0; j < data[i].count; j++) {
			writeRefID(data[i].effects[j].effectID);
			write(data[i].effects[j].info.magnitude);
			write(data[i].effects[j].info.duration);
			write(data[i].effects[j].info.area);
			write(data[i].effects[j].price);
		}
	}
}

void saveFile::writeVariable_t(variable_t data) {
	write(data.type);
	switch (data.type) {
	case 0:
		out.put(0);
		out.put(0);
		out.put(0);
		out.put(0);
		break;

	case 1:
		writeRefID(static_cast<refID_t*>(data.data), 2);
		break;

	case 2:
		write(*static_cast<uint16_t*>(data.data));
		break;

	case 3:
		write(*static_cast<uint32_t*>(data.data));
		break;

	case 4:
		write(*static_cast<float*>(data.data));
		break;

	case 5:
		if (*static_cast<bool*>(data.data)) {
			write(static_cast<uint32_t>(1));
		}
		else {
			write(static_cast<uint32_t>(0));
		}
		break;

	case 11:
		write(static_cast<refID6*>(data.data)->type);
		write(static_cast<refID6*>(data.data)->id);
		break;

	case 12:
	case 13:
	case 15:
		write(*static_cast<uint32_t*>(data.data));
		break;

	case 14:
		write(*static_cast<float*>(data.data));
		break;

	default:
		throw "invalid variable_t.type!";

	}
}

void saveFile::writeVariable_t(variable_t* data, unsigned int length) {
	try {
		for (unsigned int i = 0; i < length; i++) {
			writeVariable_t(data[i]);
		}
	}
	catch (const char* e) {
		throw e;
	}
}

bool saveFile::save() {
	cout << "writing file..." << endl;
	out.write("TESV_SAVEGAME", strlen("TESV_SAVEGAME"));
	write(file.headerSize);
	write(file.header.version);
	write(file.header.saveNumber);
	writeString(file.header.playerName);
	write(file.header.playerLevel);
	writeString(file.header.playerLocation);
	writeString(file.header.gameDate);
	writeString(file.header.playerRaceEditorId);
	write(file.header.playerSex);
	write(file.header.playerCurExp);
	write(file.header.playerLvlUpExp);
	write(file.header.filetime, 8);
	write(file.header.shotWidth);
	write(file.header.shotHeight);
	write(file.screenshotData, (file.header.shotWidth * file.header.shotHeight * 3));
	write(file.formVersion);
	write(file.pluginInfoSize);
	write(file.pluginInfo.pluginCount);
	for (int i = 0; i < file.pluginInfo.pluginCount; i++) {
		writeString(file.pluginInfo.plugins[i]);
	}
	write(file.fileLocationTable.formIDArrayCountOffset);
	write(file.fileLocationTable.unknownTable3Offset);
	write(file.fileLocationTable.globalDataTable1Offset);
	write(file.fileLocationTable.globalDataTable2Offset);
	write(file.fileLocationTable.changeFormsOffset);
	write(file.fileLocationTable.globalDataTable3Offset);
	write(file.fileLocationTable.globalDataTable1Count);
	write(file.fileLocationTable.globalDataTable2Count);
	write(file.fileLocationTable.globalDataTable3Count);
	write(file.fileLocationTable.changeFormCount);
	for (int i = 0; i < 15 * 4; i++) {//write 15 uint32 containing 0
		out.put(0);
	}
	for (unsigned int i = 0; i < file.fileLocationTable.globalDataTable1Count; i++) {
		writeGlobalData(file.globalDataTable1[i]);
	}
	for (unsigned int i = 0; i < file.fileLocationTable.globalDataTable2Count; i++) {
		writeGlobalData(file.globalDataTable2[i]);
	}
	for (unsigned int i = 0; i < file.fileLocationTable.changeFormCount; i++) {
		writeRefID(file.changeForms[i].formID);
		write(file.changeForms[i].changeFlags);
		write(file.changeForms[i].type);
		write(file.changeForms[i].version);
		if (file.changeForms[i].type > 0x3F && file.changeForms[i].type <= 0x7F) {
			write(static_cast<uint16_t>(file.changeForms[i].length1));
			write(static_cast<uint16_t>(file.changeForms[i].length2));
		}
		else if (file.changeForms[i].type > 0x7F) {
			write(static_cast<uint32_t>(file.changeForms[i].length1));
			write(static_cast<uint32_t>(file.changeForms[i].length2));
		}
		else {
			write(static_cast<uint8_t>(file.changeForms[i].length1));
			write(static_cast<uint8_t>(file.changeForms[i].length2));
		}
		for (unsigned int j = 0; j < file.changeForms[i].length1; j++) {
			write(file.changeForms[i].data[j]);
		}
	}
	//for (unsigned int i = 0; i < file.fileLocationTable.globalDataTable3Count; i++) {
	for (unsigned int i = 0; i < 6; i++) {
		writeGlobalData(file.globalDataTable3[i]);
	}
	write(file.formIDArrayCount);
	write(file.formIDArray, file.formIDArrayCount);
	write(file.visitedWorldspaceArrayCount);
	write(file.visitedWorldspaceArray, file.visitedWorldspaceArrayCount);
	write(file.unknown3TableSize);
	write(file.unknown3Table.count);
	for (unsigned int i = 0; i < file.unknown3Table.count; i++) {
		writeString(file.unknown3Table.unknown[i]);
	}
	cout << "done" << endl;
	return true;
}

void saveFile::writeGlobalData(globalData_t data) {
	write(data.type);
	write(data.length);
	switch (data.type) {
	case 0://misc stats
		write(g0->count);
		for (unsigned int i = 0; i < g0->count; i++) {
			writeString(g0->stats[i].name);
			write(g0->stats[i].category);
			write(g0->stats[i].value);
		}
		break;

	case 1://player location
		write(g1->nextObjectId);
		writeRefID(g1->worldspace1);
		write(g1->coorX);
		write(g1->coorY);
		writeRefID(g1->worldspace2);
		write(g1->posX);
		write(g1->posY);
		write(g1->posZ);
		if (this->file.header.version == 9)
			write(g1->unknown);
		break;

	case 2://TES
		writeVsval(g2->count1);
		for (unsigned int i = 0; i < g2->count1; i++) {
			writeRefID(g2->unknown1[i].formID);
			write(g2->unknown1[i].unknown);
		}
		write(g2->count2);
		writeRefID(g2->unknown2, g2->count2 * g2->count2);
		writeVsval(g2->count3);
		writeRefID(g2->unknown3, g2->count3);
		break;

	case 3://global variables
		writeVsval(g3->count);
		for (unsigned int i = 0; i < g3->count; i++) {
			writeRefID(g3->globals[i].formID);
			write(g3->globals[i].value);
		}
		break;

	case 4://created objects
		writeVsval(g4->weaponCount);
		writeEnchantment(g4->weaponEnchTable, g4->weaponCount);
		writeVsval(g4->armourCount);
		writeEnchantment(g4->armourEnchTable, g4->armourCount);
		writeVsval(g4->potionCount);
		writeEnchantment(g4->potionTable, g4->potionCount);
		writeVsval(g4->poisonCount);
		writeEnchantment(g4->poisonTable, g4->poisonCount);
		break;

	case 5://effects
		writeVsval(g5->count);
		for (unsigned int i = 0; i < g5->count; i++) {
			write(g5->imageSpaceModifiers[i].strength);
			write(g5->imageSpaceModifiers[i].timestamp);
			write(g5->imageSpaceModifiers[i].unknown);
			writeRefID(g5->imageSpaceModifiers[i].effectID);
		}
		write(g5->unknown1);
		write(g5->unknown2);
		break;

	case 6://weather
		writeRefID(g6->climate);
		writeRefID(g6->weather);
		writeRefID(g6->prevWeather);
		writeRefID(g6->unknownWeather1);
		writeRefID(g6->unknownWeather2);
		writeRefID(g6->regnWeather);
		write(g6->curTime);
		write(g6->begTime);
		write(g6->weatherPct);
		write(g6->unknown1);
		write(g6->unknown2);
		write(g6->unknown3);
		write(g6->unknown4);
		write(g6->unknown5);
		write(g6->unknown6);
		write(g6->unknown7);
		write(g6->unknown8);
		write(g6->flags);
		if ((g6->flags & 0b00000001) == 1) {
			write(g6->unknown9);
		}
		if ((g6->flags & 0b00000010) == 2) {
			write(g6->unknown10);
		}
		break;

	case 7://audio
		writeRefID(g7->unknown);
		writeVsval(g7->tracksCount);
		writeRefID(g7->tracks, g7->tracksCount);
		writeRefID(g7->bgm);
		break;

	case 8://skycells
		writeVsval(g8->count);
		for (unsigned int i = 0; i < g8->count; i++) {
			writeRefID(g8->unknown[i].unknown1);
			writeRefID(g8->unknown[i].unknown2);
		}
		break;

	case 100://process lists
		write(g100->unknown1);
		write(g100->unknown2);
		write(g100->unknown3);
		write(g100->nextNum);
		for (int i = 0; i < 7; i++) {
			writeVsval(g100->allCrimes[i].count);
			for (unsigned int j = 0; j < g100->allCrimes[i].count; j++) {
				write(g100->allCrimes[i].crimes[j].witnessNum);
				write(g100->allCrimes[i].crimes[j].crimeType);
				write(g100->allCrimes[i].crimes[j].unknown1);
				write(g100->allCrimes[i].crimes[j].quantity);
				write(g100->allCrimes[i].crimes[j].serialNum);
				write(g100->allCrimes[i].crimes[j].unknown2);
				write(g100->allCrimes[i].crimes[j].unknown3);
				write(g100->allCrimes[i].crimes[j].elapsedTime);
				writeRefID(g100->allCrimes[i].crimes[j].victimID);
				writeRefID(g100->allCrimes[i].crimes[j].criminalID);
				writeRefID(g100->allCrimes[i].crimes[j].itemBaseID);
				writeRefID(g100->allCrimes[i].crimes[j].ownershipID);
				writeVsval(g100->allCrimes[i].crimes[j].count);
				writeRefID(g100->allCrimes[i].crimes[j].witnesses, g100->allCrimes[i].crimes[j].witnessNum);
				write(g100->allCrimes[i].crimes[j].bounty);
				writeRefID(g100->allCrimes[i].crimes[j].crimeFactionID);
				write(g100->allCrimes[i].crimes[j].isCleared);
				write(g100->allCrimes[i].crimes[j].unknown4);
			}
		}
		break;

	case 101://combat
		write(g101, data.length);
		/*write(g101->nextNum);
		writeVsval(g101->count0);
		for (unsigned int i = 0; i < g101->count0; i++) {
			write(g101->unknown0[i].unknown1);
			write(g101->unknown0[i].serialNum);
			writeVsval(g101->unknown0[i].unknown2.count0);
			for (unsigned int j = 0; j < g101->unknown0[i].unknown2.count0; j++) {
				writeRefID(g101->unknown0[i].unknown2.unknown0[j].unknown1);
				write(g101->unknown0[i].unknown2.unknown0[j].unknown2);
				write(g101->unknown0[i].unknown2.unknown0[j].unknown3);
				write(g101->unknown0[i].unknown2.unknown0[j].unknown4);
				write(g101->unknown0[i].unknown2.unknown0[j].unknown5);
				writeRefID(g101->unknown0[i].unknown2.unknown0[j].target);
				write(g101->unknown0[i].unknown2.unknown0[j].unknown6.x);
				write(g101->unknown0[i].unknown2.unknown0[j].unknown6.y);
				write(g101->unknown0[i].unknown2.unknown0[j].unknown6.z);
				writeRefID(g101->unknown0[i].unknown2.unknown0[j].unknown6.cellID);
				write(g101->unknown0[i].unknown2.unknown0[j].unknown7.x);
				write(g101->unknown0[i].unknown2.unknown0[j].unknown7.y);
				write(g101->unknown0[i].unknown2.unknown0[j].unknown7.z);
				writeRefID(g101->unknown0[i].unknown2.unknown0[j].unknown7.cellID);
				write(g101->unknown0[i].unknown2.unknown0[j].unknown8.x);
				write(g101->unknown0[i].unknown2.unknown0[j].unknown8.y);
				write(g101->unknown0[i].unknown2.unknown0[j].unknown8.z);
				writeRefID(g101->unknown0[i].unknown2.unknown0[j].unknown8.cellID);
				write(g101->unknown0[i].unknown2.unknown0[j].unknown9.x);
				write(g101->unknown0[i].unknown2.unknown0[j].unknown9.y);
				write(g101->unknown0[i].unknown2.unknown0[j].unknown9.z);
				writeRefID(g101->unknown0[i].unknown2.unknown0[j].unknown9.cellID);
				write(g101->unknown0[i].unknown2.unknown0[j].unknown10.x);
				write(g101->unknown0[i].unknown2.unknown0[j].unknown10.y);
				write(g101->unknown0[i].unknown2.unknown0[j].unknown10.z);
				writeRefID(g101->unknown0[i].unknown2.unknown0[j].unknown10.cellID);
				write(g101->unknown0[i].unknown2.unknown0[j].unknown11);
				write(g101->unknown0[i].unknown2.unknown0[j].unknown12);
				write(g101->unknown0[i].unknown2.unknown0[j].unknown13);
				write(g101->unknown0[i].unknown2.unknown0[j].unknown14);
				write(g101->unknown0[i].unknown2.unknown0[j].unknown15);
				write(g101->unknown0[i].unknown2.unknown0[j].unknown16);
			}
			writeVsval(g101->unknown0[i].unknown2.count1);
			for (unsigned int j = 0; j < g101->unknown0[i].unknown2.count1; j++) {
				writeRefID(g101->unknown0[i].unknown2.unknown1[j].unknown1);
				write(g101->unknown0[i].unknown2.unknown1[j].unknown2);
				write(g101->unknown0[i].unknown2.unknown1[j].unknown3);
			}
			write(g101->unknown0[i].unknown2.unknown2.unknown1);
			write(g101->unknown0[i].unknown2.unknown2.unknown2);
			write(g101->unknown0[i].unknown2.unknown3.unknown1);
			write(g101->unknown0[i].unknown2.unknown3.unknown2);
			write(g101->unknown0[i].unknown2.unknown4.unknown1);
			write(g101->unknown0[i].unknown2.unknown4.unknown2);
			write(g101->unknown0[i].unknown2.unknown5.unknown1);
			write(g101->unknown0[i].unknown2.unknown5.unknown2);
			for (int j = 0; j < 11; j++) {
				write(g101->unknown0[i].unknown2.unknown6[j].unknown1);
				write(g101->unknown0[i].unknown2.unknown6[j].unknown2);
			}
			write(g101->unknown0[i].unknown2.unknownFlag);
			if (g101->unknown0[i].unknown2.unknownFlag != 0) {//unknown0_0_2_t
				writeRefID(g101->unknown0[i].unknown2.unknown7.unknown1);
				write(g101->unknown0[i].unknown2.unknown7.unknown2.unknown1);
				write(g101->unknown0[i].unknown2.unknown7.unknown2.unknown2);
				write(g101->unknown0[i].unknown2.unknown7.unknown3.unknown1);
				write(g101->unknown0[i].unknown2.unknown7.unknown3.unknown2);
				write(g101->unknown0[i].unknown2.unknown7.unknown4);
				write(g101->unknown0[i].unknown2.unknown7.unknown5.x);
				write(g101->unknown0[i].unknown2.unknown7.unknown5.y);
				write(g101->unknown0[i].unknown2.unknown7.unknown5.z);
				writeRefID(g101->unknown0[i].unknown2.unknown7.unknown5.cellID);
				write(g101->unknown0[i].unknown2.unknown7.unknown6);
				writeVsval(g101->unknown0[i].unknown2.unknown7.count0);
				for (unsigned int j = 0; j < g101->unknown0[i].unknown2.unknown7.count0; j++) {
					write(g101->unknown0[i].unknown2.unknown7.unknown7[j].unknown1.x);
					write(g101->unknown0[i].unknown2.unknown7.unknown7[j].unknown1.y);
					write(g101->unknown0[i].unknown2.unknown7.unknown7[j].unknown1.z);
					writeRefID(g101->unknown0[i].unknown2.unknown7.unknown7[j].unknown1.cellID);
					write(g101->unknown0[i].unknown2.unknown7.unknown7[j].unknown2);
					write(g101->unknown0[i].unknown2.unknown7.unknown7[j].unknown3);
				}
				writeVsval(g101->unknown0[i].unknown2.unknown7.count1);
				for (unsigned int j = 0; j < g101->unknown0[i].unknown2.unknown7.count1; j++) {
					writeRefID(g101->unknown0[i].unknown2.unknown7.unknown8[j].unknown1);
					writeRefID(g101->unknown0[i].unknown2.unknown7.unknown8[j].unknown2);
					write(g101->unknown0[i].unknown2.unknown7.unknown8[j].unknown3);
					write(g101->unknown0[i].unknown2.unknown7.unknown8[j].unknown4);
					write(g101->unknown0[i].unknown2.unknown7.unknown8[j].unknown5);
				}
				write(g101->unknown0[i].unknown2.unknown7.unknownFlag);
				if (g101->unknown0[i].unknown2.unknown7.unknownFlag != 0) {
					write(g101->unknown0[i].unknown2.unknown7.unknown9.unknown1);
					write(g101->unknown0[i].unknown2.unknown7.unknown9.unknown2);
					writeVsval(g101->unknown0[i].unknown2.unknown7.unknown9.count0);
					for (unsigned int j = 0; j < g101->unknown0[i].unknown2.unknown7.unknown9.count0; j++) {//unknown_0_0_2_2_0_t
						write(g101->unknown0[i].unknown2.unknown7.unknown9.unknown3[j].unknown1);
						write(g101->unknown0[i].unknown2.unknown7.unknown9.unknown3[j].count0);
						write(g101->unknown0[i].unknown2.unknown7.unknown9.unknown3[j].unknown2, g101->unknown0[i].unknown2.unknown7.unknown9.unknown3[j].count0);
						writeRefID(g101->unknown0[i].unknown2.unknown7.unknown9.unknown3[j].unknown3);
						write(g101->unknown0[i].unknown2.unknown7.unknown9.unknown3[j].unknown4);
					}
					writeRefID(g101->unknown0[i].unknown2.unknown7.unknown9.unknown4);
					write(g101->unknown0[i].unknown2.unknown7.unknown9.unknown5);
					write(g101->unknown0[i].unknown2.unknown7.unknown9.unknown6);
					write(g101->unknown0[i].unknown2.unknown7.unknown9.unknown7);
					write(g101->unknown0[i].unknown2.unknown7.unknown9.unknown8);
					write(g101->unknown0[i].unknown2.unknown7.unknown9.unknown9);
					writeRefID(g101->unknown0[i].unknown2.unknown7.unknown9.unknown10);
					write(g101->unknown0[i].unknown2.unknown7.unknown9.unknown11);
					writeRefID(g101->unknown0[i].unknown2.unknown7.unknown9.unknown12);
					write(g101->unknown0[i].unknown2.unknown7.unknown9.unknown13);
					write(g101->unknown0[i].unknown2.unknown7.unknown9.unknown14);
					write(g101->unknown0[i].unknown2.unknown7.unknown9.unknown15);
					write(g101->unknown0[i].unknown2.unknown7.unknown9.unknown16);
					write(g101->unknown0[i].unknown2.unknown7.unknown9.unknown17);
				}

			}
			write(g101->unknown0[i].unknown2.unknown8.unknown1);
			write(g101->unknown0[i].unknown2.unknown8.unknown2);
			write(g101->unknown0[i].unknown2.unknown9);
			write(g101->unknown0[i].unknown2.unknown10);
			write(g101->unknown0[i].unknown2.unknown11);
			write(g101->unknown0[i].unknown2.unknown12);
			write(g101->unknown0[i].unknown2.unknown13.unknown1);
			write(g101->unknown0[i].unknown2.unknown13.unknown2);
			write(g101->unknown0[i].unknown2.unknown14);
		}
		writeVsval(g101->count1);
		for (unsigned int i = 0; i < g101->count1; i++) {
			writeRefID(g101->unknown1[i].unknown1);
			write(g101->unknown1[i].unknown2);
			writeRefID(g101->unknown1[i].unknown3);
			writeRefID(g101->unknown1[i].unknown4);
			write(g101->unknown1[i].unknown5);
			write(g101->unknown1[i].unknown6);
			write(g101->unknown1[i].unknown7);
			write(g101->unknown1[i].x);
			write(g101->unknown1[i].y);
			write(g101->unknown1[i].z);
			write(g101->unknown1[i].unknown8);
			write(g101->unknown1[i].unknown9);
			write(g101->unknown1[i].unknown10);
			write(g101->unknown1[i].unknown11);
			write(g101->unknown1[i].unknown12);
			write(g101->unknown1[i].unknown13);
			write(g101->unknown1[i].unknown14);
			write(g101->unknown1[i].unknown15);
			writeRefID(g101->unknown1[i].unknown16);
		}
		write(g101->unknown2);
		writeVsval(g101->unknown3);
		writeVsval(g101->count2);
		writeRefID(g101->unknown4, g101->count2);
		write(g101->unknown5);
		write(g101->unknown6.unknown1);
		write(g101->unknown6.unknown2);
		write(g101->unknown7.unknown1);
		write(g101->unknown7.unknown2);*/
		break;

	case 102://interface
		write(g102->shownHelpMsgCount);
		write(g102->shownHelpMsg, g102->shownHelpMsgCount);
		write(g102->unknown1);
		writeVsval(g102->lastUsedWeaponsCount);
		writeRefID(g102->lastUsedWeapons, g102->lastUsedWeaponsCount);
		writeVsval(g102->lastUsedSpellsCount);
		writeRefID(g102->lastUsedSpells, g102->lastUsedSpellsCount);
		writeVsval(g102->lastUsedShoutsCount);
		writeRefID(g102->lastUsedShouts, g102->lastUsedShoutsCount);
		write(g102->unknown2);
		writeVsval(g102->unknown3.count1);
		for (unsigned int i = 0; i < g102->unknown3.count1; i++) {
			writeString(g102->unknown3.unknown1[i].unknown1);
			writeString(g102->unknown3.unknown1[i].unknown2);
			write(g102->unknown3.unknown1[i].unknown3);
			write(g102->unknown3.unknown1[i].unknown4);
			write(g102->unknown3.unknown1[i].unknown5);
			write(g102->unknown3.unknown1[i].unknown6);
		}
		writeVsval(g102->unknown3.count2);
		writeString(g102->unknown3.unknown2, g102->unknown3.count2);
		write(g102->unknown3.unknown3);
		break;

	case 103://actor causes
		write(g103->nextNum);
		writeVsval(g103->count);
		for (unsigned int i = 0; i < g103->count; i++) {
			write(g103->unknown[i].x);
			write(g103->unknown[i].y);
			write(g103->unknown[i].z);
			write(g103->unknown[i].serialNum);
			writeRefID(g103->unknown[i].actorID);
		}
		break;

	case 104://unknown 104
		write(g104, data.length);
		break;

	case 105://detection manager
		writeVsval(g105->count);
		for (unsigned int i = 0; i < g105->count; i++) {
			writeRefID(g105->unknown[i].unknown1);
			write(g105->unknown[i].unknown2);
			write(g105->unknown[i].unknown3);
		}
		break;

	case 106://location metadata
		writeVsval(g106->count);
		for (unsigned int i = 0; i < g106->count; i++) {
			writeRefID(g106->unknown[i].unknown1);
			write(g106->unknown[i].unknown2);
		}
		break;

	case 107://quest static data
		write(g107->count0);
		for (unsigned int i = 0; i < g107->count0; i++) {
			write(g107->unknown0[i].unknown0);
			write(g107->unknown0[i].unknown1);
			write(g107->unknown0[i].count);
			for (unsigned int j = 0; j < g107->unknown0[i].count; j++) {
				write(g107->unknown0[i].questRunData_item[j].type);
				switch (g107->unknown0[i].questRunData_item[j].type) {
				case 1:
				case 2:
				case 4:
					writeRefID(*static_cast<refID_t*>(g107->unknown0[i].questRunData_item[j].unknown));
					break;
				case 3:
					write(*static_cast<uint32_t*>(g107->unknown0[i].questRunData_item[j].unknown));
					break;
				default:
					throw "internal error(w:c107_0 invalid type)";
				}
			}
		}
		write(g107->count1);
		for (unsigned int i = 0; i < g107->count1; i++) {
			write(g107->unknown1[i].unknown0);
			write(g107->unknown1[i].unknown1);
			write(g107->unknown1[i].count);
			for (unsigned int j = 0; j < g107->unknown1[i].count; j++) {
				write(g107->unknown1[i].questRunData_item[j].type);
				switch (g107->unknown1[i].questRunData_item[j].type) {
				case 1:
				case 2:
				case 4:
					writeRefID(*static_cast<refID_t*>(g107->unknown1[i].questRunData_item[j].unknown));
					break;
				case 3:
					write(*static_cast<uint32_t*>(g107->unknown1[i].questRunData_item[j].unknown));
					break;
				default:
					throw "internal error(w:c107_1 invalid type)";
				}
			}
		}
		write(g107->count2);
		writeRefID(g107->unknown2, g107->count2);
		write(g107->count3);
		writeRefID(g107->unknown3, g107->count3);
		write(g107->count4);
		writeRefID(g107->unknown4, g107->count4);
		writeVsval(g107->count5);
		for (unsigned int i = 0; i < g107->count5; i++) {
			writeRefID(g107->unknown5[i].unknown0);
			writeVsval(g107->unknown5[i].count);
			for (unsigned int j = 0; j < g107->unknown5[i].count; j++) {
				write(g107->unknown5[i].unknown1[j].unknown0);
				write(g107->unknown5[i].unknown1[j].unknown1);
			}
		}
		write(g107->unknown6);
		break;

	case 108://storyteller
		write(g108->flag);
		break;

	case 109://magic favorites
		writeVsval(g109->count0);
		writeRefID(g109->favoritedMagics, g109->count0);
		writeVsval(g109->count1);
		writeRefID(g109->magicHotKeys, g109->count1);
		break;

	case 110://playercontrols
		write(g110->unknown1);
		write(g110->unknown2);
		write(g110->unknown3);
		write(g110->unknown4);
		write(g110->unknown5);
		break;

	case 111://story event manager
		write(g111->data, data.length);
		break;

	case 112://ingredient shared
		write(g112->count);
		for (unsigned int i = 0; i < g112->count; i++) {
			writeRefID(g112->ingredientsCombined[i].ingredient0);
			writeRefID(g112->ingredientsCombined[i].ingredient1);
		}
		break;

	case 113://menucontrols
		write(g113->unknown1);
		write(g113->unknown2);
		break;

	case 114://menutopicmanager
		writeRefID(g114->unknown1);
		writeRefID(g114->unknown2);
		break;

	case 1000://temp effects
		write(g1000, data.length);
		break;

	case 1001://papyrus
		write(g1001->header);
		write(g1001->strCount);
		writeString(g1001->strings, g1001->strCount);
		write(g1001->scriptCount);
		for (unsigned int i = 0; i < g1001->scriptCount; i++) {
			write(g1001->script[i].scriptName);
			write(g1001->script[i].type);
			write(g1001->script[i].memberCount);
			for (unsigned int j = 0; j < g1001->script[i].memberCount; j++) {
				write(g1001->script[i].memberData[j].memberName);
				write(g1001->script[i].memberData[j].memberType);
			}
		}
		write(g1001->scriptInstanceCount);
		for (unsigned int i = 0; i < g1001->scriptInstanceCount; i++) {
			write(g1001->scriptInstance[i].scriptID);
			write(g1001->scriptInstance[i].scriptName);
			write(g1001->scriptInstance[i].unknown2bits);
			write(g1001->scriptInstance[i].unknown);
			writeRefID(g1001->scriptInstance[i].refID);
			write(g1001->scriptInstance[i].unknown2);
		}
		write(g1001->referenceCount);
		for (unsigned int i = 0; i < g1001->referenceCount; i++) {
			write(g1001->reference[i].referenceID);
			write(g1001->reference[i].type);
		}
		write(g1001->arrayInfoCount);
		for (unsigned int i = 0; i < g1001->arrayInfoCount; i++) {
			write(g1001->arrayInfo[i].arrayID);
			write(g1001->arrayInfo[i].type);
			if (g1001->arrayInfo[i].type == 1) {
				write(g1001->arrayInfo[i].refType);
			}
			write(g1001->arrayInfo[i].length);
		}
		write(g1001->papyrusRuntime);
		write(g1001->activeScriptCount);
		for (unsigned int i = 0; i < g1001->activeScriptCount; i++) {
			write(g1001->activeScript[i].scriptID);
			write(g1001->activeScript[i].scriptType);
		}
		for (unsigned int i = 0; i < g1001->scriptInstanceCount; i++) {
			write(g1001->scriptData[i].scriptID);
			write(g1001->scriptData[i].flag);
			write(g1001->scriptData[i].type);
			write(g1001->scriptData[i].unknown1);
			if ((g1001->scriptData[i].flag & 0x04) == 0x04)
				write(g1001->scriptData[i].unknown2);
			write(g1001->scriptData[i].memberCount);
			for (unsigned int j = 0; j < g1001->scriptData[i].memberCount; j++)
				writeVariable_t(g1001->scriptData[i].member[j]);
		}
		for (unsigned int i = 0; i < g1001->referenceCount; i++) {
			write(g1001->referenceData[i].referenceID);
			write(g1001->referenceData[i].flag);
			write(g1001->referenceData[i].type);
			write(g1001->referenceData[i].unknown1);
			if ((g1001->referenceData[i].flag & 0x04) == 0x04) {
				write(g1001->referenceData[i].unknown2);
			}
			write(g1001->referenceData[i].memberCount);
			writeVariable_t(g1001->referenceData[i].member, g1001->referenceData[i].memberCount);
		}
		for (unsigned int i = 0; i < g1001->arrayInfoCount; i++) {
			write(g1001->arrayData[i].arrayID);
			writeVariable_t(g1001->arrayData[i].data, g1001->arrayInfo[i].length);
		}
		for (unsigned int i = 0; i < g1001->activeScriptCount; i++) {
			write(g1001->activeScriptData[i].scriptID);
			write(g1001->activeScriptData[i].majorVersion);
			write(g1001->activeScriptData[i].minorVersion);
			writeVariable_t(g1001->activeScriptData[i].unknown);
			write(g1001->activeScriptData[i].flag);
			write(g1001->activeScriptData[i].unknownByte);
			if ((g1001->activeScriptData[i].flag & 0x01) == 0x01) {
				write(g1001->activeScriptData[i].unknown2);
			}
			write(g1001->activeScriptData[i].unknown3);
			if ((g1001->activeScriptData[i].unknown3 >= 1) && (g1001->activeScriptData[i].unknown3 <= 3)) {
				switch (g1001->activeScriptData[i].unknown3) {
				case 1:
					write(static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->count);
					write(static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->str, static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->count);
					if (strcmp("QuestStage", static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->str) == 0) {
						writeRefID(static_cast<papyrus_t::activeScriptData_t::questStage*>(static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->data)->refID);
						write(static_cast<papyrus_t::activeScriptData_t::questStage*>(static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->data)->string);
						write(static_cast<papyrus_t::activeScriptData_t::questStage*>(static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->data)->unknown);
					}
					else if ((strcmp("ScenePhaseResults", static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->str) == 0) || (strcmp("SceneActionResults", static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->str) == 0)) {
						writeRefID(static_cast<papyrus_t::activeScriptData_t::scenePhaseResults*>(static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->data)->refID);
						write(static_cast<papyrus_t::activeScriptData_t::scenePhaseResults*>(static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->data)->unknown);
					}
					else if (strcmp("SceneResults", static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->str) == 0) {
						writeRefID(static_cast<papyrus_t::activeScriptData_t::sceneResults*>(static_cast<papyrus_t::activeScriptData_t::unknown4_1*>(g1001->activeScriptData[i].unknown4)->data)->refID);
					}
					break;

				case 2:
					writeVariable_t(*static_cast<variable_t*>(g1001->activeScriptData[i].unknown4));
					break;

				case 3:
					write(static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->count);
					write(static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->str, static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->count);
					if (strcmp("QuestStage", static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->str) == 0) {
						writeRefID(static_cast<papyrus_t::activeScriptData_t::questStage*>(static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->data)->refID);
						write(static_cast<papyrus_t::activeScriptData_t::questStage*>(static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->data)->string);
						write(static_cast<papyrus_t::activeScriptData_t::questStage*>(static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->data)->unknown);
					}
					else if ((strcmp("ScenePhaseResults", static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->str)) == 0 || strcmp("SceneActionResults", static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->str) == 0) {
						writeRefID(static_cast<papyrus_t::activeScriptData_t::scenePhaseResults*>(static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->data)->refID);
						write(static_cast<papyrus_t::activeScriptData_t::scenePhaseResults*>(static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->data)->unknown);
					}
					else if (strcmp("SceneResults", static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->str) == 0) {
						writeRefID(static_cast<papyrus_t::activeScriptData_t::sceneResults*>(static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->data)->refID);
					}
					writeVariable_t(static_cast<papyrus_t::activeScriptData_t::unknown4_3*>(g1001->activeScriptData[i].unknown4)->unknown);
					break;
				}
			}
			write(g1001->activeScriptData[i].stackframecount);
			for (unsigned int j = 0; j < g1001->activeScriptData[i].stackframecount; j++) {
				write(g1001->activeScriptData[i].stackframe[j].variablecount);
				write(g1001->activeScriptData[i].stackframe[j].flag);
				write(g1001->activeScriptData[i].stackframe[j].functionType);
				write(g1001->activeScriptData[i].stackframe[j].scriptName);
				write(g1001->activeScriptData[i].stackframe[j].scriptBaseName);
				write(g1001->activeScriptData[i].stackframe[j].event);
				if (((g1001->activeScriptData[i].stackframe[j].flag & 0x01) == 0) && (g1001->activeScriptData[i].stackframe[j].functionType == 0)) {
					write(g1001->activeScriptData[i].stackframe[j].status);
				}
				write(g1001->activeScriptData[i].stackframe[j].opcodeVersion);
				write(g1001->activeScriptData[i].stackframe[j].opcodeMinorVersion);
				write(g1001->activeScriptData[i].stackframe[j].returnType);
				write(g1001->activeScriptData[i].stackframe[j].functionDocstring);
				write(g1001->activeScriptData[i].stackframe[j].functionUserflags);
				write(g1001->activeScriptData[i].stackframe[j].functionFlags);
				write(g1001->activeScriptData[i].stackframe[j].functionParameterCount);
				for (unsigned int k = 0; k < g1001->activeScriptData[i].stackframe[j].functionParameterCount; k++) {
					write(g1001->activeScriptData[i].stackframe[j].functionParam[k].functionParamName);
					write(g1001->activeScriptData[i].stackframe[j].functionParam[k].functionParamType);
				}
				write(g1001->activeScriptData[i].stackframe[j].functionLocalsCount);
				for (unsigned int k = 0; k < g1001->activeScriptData[i].stackframe[j].functionLocalsCount; k++) {
					write(g1001->activeScriptData[i].stackframe[j].functionLocal[k].functionLocalName);
					write(g1001->activeScriptData[i].stackframe[j].functionLocal[k].functionTypeName);
				}
				write(g1001->activeScriptData[i].stackframe[j].opcodeCount);
				for (unsigned int x = 0; x < g1001->activeScriptData[i].stackframe[j].opcodeCount; x++) {
					write(g1001->activeScriptData[i].stackframe[j].opcodeData[x].opcode);
					int count = getOpcodeLength(g1001->activeScriptData[i].stackframe[j].opcodeData[x].opcode);
					for (int y = 0; y < count; y++) {
						write(g1001->activeScriptData[i].stackframe[j].opcodeData[x].parameter[y].type);
						switch (g1001->activeScriptData[i].stackframe[j].opcodeData[x].parameter[y].type) {
						case 1:
						case 2:
							write(*static_cast<uint16_t*>(g1001->activeScriptData[i].stackframe[j].opcodeData[x].parameter[y].data));
							break;
						case 3:
							write(*static_cast<uint32_t*>(g1001->activeScriptData[i].stackframe[j].opcodeData[x].parameter[y].data));
							break;
						case 4:
							write(*static_cast<float*>(g1001->activeScriptData[i].stackframe[j].opcodeData[x].parameter[y].data));
							break;
						case 5:
							write(*static_cast<uint8_t*>(g1001->activeScriptData[i].stackframe[j].opcodeData[x].parameter[y].data));
							break;
						}
						if ((((g1001->activeScriptData[i].stackframe[j].opcodeData[x].opcode == 0x17) || (g1001->activeScriptData[i].stackframe[j].opcodeData[x].opcode == 0x18) || (g1001->activeScriptData[i].stackframe[j].opcodeData[x].opcode == 0x19)) && (y == (count - 1)))) {
							for (unsigned int a = 0; a < *static_cast<uint32_t*>(g1001->activeScriptData[i].stackframe[j].opcodeData[x].parameter[y].data); a++) {
								write(g1001->activeScriptData[i].stackframe[j].opcodeData[x].extra_parameter[a].type);
								switch (g1001->activeScriptData[i].stackframe[j].opcodeData[x].extra_parameter[a].type) {
								case 1:
								case 2:
									write(*static_cast<uint16_t*>(g1001->activeScriptData[i].stackframe[j].opcodeData[x].extra_parameter[a].data));
									break;
								case 3:
									write(*static_cast<uint32_t*>(g1001->activeScriptData[i].stackframe[j].opcodeData[x].extra_parameter[a].data));
									break;
								case 4:
									write(*static_cast<float*>(g1001->activeScriptData[i].stackframe[j].opcodeData[x].extra_parameter[a].data));
									break;
								case 5:
									write(*static_cast<uint8_t*>(g1001->activeScriptData[i].stackframe[j].opcodeData[x].extra_parameter[a].data));
									break;
								}
							}
						}
					}
				}
				write(g1001->activeScriptData[i].stackframe[j].unknown3);
				writeVariable_t(g1001->activeScriptData[i].stackframe[j].unknown4);
				writeVariable_t(g1001->activeScriptData[i].stackframe[j].unknown5, g1001->activeScriptData[i].stackframe[j].variablecount);

			}
			if (g1001->activeScriptData[i].stackframecount != 0) {
				write(g1001->activeScriptData[i].unknown5);
			}
		}
		write(g1001->functionMessageCount);
		for (unsigned int i = 0; i < g1001->functionMessageCount; i++) {
			write(g1001->functionMessages[i].unknown);
			write(g1001->functionMessages[i].id);
			write(g1001->functionMessages[i].flag);
			if (g1001->functionMessages[i].flag > 0) {
				write(g1001->functionMessages[i].message.unknown0);
				write(g1001->functionMessages[i].message.scriptName);
				write(g1001->functionMessages[i].message.event);
				writeVariable_t(g1001->functionMessages[i].message.unknown1);
				write(g1001->functionMessages[i].message.variablecount);
				writeVariable_t(g1001->functionMessages[i].message.unknown2, g1001->functionMessages[i].message.variablecount);
			}
		}
		write(g1001->suspendedStackCount1);
		for (unsigned int i = 0; i < g1001->suspendedStackCount1; i++) {
			write(g1001->suspendedStacks1[i].id);
			write(g1001->suspendedStacks1[i].flag);
			if (g1001->suspendedStacks1[i].flag > 0) {
				write(g1001->suspendedStacks1[i].message.unknown0);
				write(g1001->suspendedStacks1[i].message.scriptName);
				write(g1001->suspendedStacks1[i].message.event);
				writeVariable_t(g1001->suspendedStacks1[i].message.unknown1);
				write(g1001->suspendedStacks1[i].message.variablecount);
				writeVariable_t(g1001->suspendedStacks1[i].message.unknown2, g1001->suspendedStacks1[i].message.variablecount);
			}
		}
		write(g1001->suspendedStackCount2);
		for (unsigned int i = 0; i < g1001->suspendedStackCount2; i++) {
			write(g1001->suspendedStacks2[i].id);
			write(g1001->suspendedStacks2[i].flag);
			if (g1001->suspendedStacks2[i].flag > 0) {
				write(g1001->suspendedStacks2[i].message.unknown0);
				write(g1001->suspendedStacks2[i].message.scriptName);
				write(g1001->suspendedStacks2[i].message.event);
				writeVariable_t(g1001->suspendedStacks2[i].message.unknown1);
				write(g1001->suspendedStacks2[i].message.variablecount);
				writeVariable_t(g1001->suspendedStacks2[i].message.unknown2, g1001->suspendedStacks2[i].message.variablecount);
			}
		}
		write(g1001->unknown1);
		if (g1001->unknown1 != 0) {
			write(g1001->unknown2);
		}
		write(g1001->unknown0Count);
		write(g1001->unknowns0, g1001->unknown0Count);
		write(g1001->queuedUnbindCount);
		for (unsigned int i = 0; i < g1001->queuedUnbindCount; i++) {
			write(g1001->queuedUnbinds[i].id);
			write(g1001->queuedUnbinds[i].unknown);
		}
		write(g1001->saveFileVersion);
		write(g1001->arrayCount1);
		for (unsigned int i = 0; i < g1001->arrayCount1; i++) {
			write(g1001->array1[i].unknown0);
			write(g1001->array1[i].unknown1);
		}
		write(g1001->arrayCount1a);
		for (unsigned int i = 0; i < g1001->arrayCount1a; i++) {
			write(g1001->array1a[i].unknown0);
			write(g1001->array1a[i].unknown1);
		}
		write(g1001->arrayCount2);
		for (unsigned int i = 0; i < g1001->arrayCount2; i++) {
			write(g1001->array2[i].unknown0);
			write(g1001->array2[i].unknown1);
		}
		write(g1001->arrayCount3);
		for (unsigned int i = 0; i < g1001->arrayCount3; i++) {
			write(g1001->array3[i].type);
			write(g1001->array3[i].string0);
			write(g1001->array3[i].unknown0);
			write(g1001->array3[i].string1);
			write(g1001->array3[i].unknown1);
			write(g1001->array3[i].flags);
			writeRefID(g1001->array3[i].reference);
		}
		write(g1001->arrayCount4);
		for (unsigned int i = 0; i < g1001->arrayCount4; i++) {
			write(g1001->array4[i].string0);
			write(g1001->array4[i].unknown0);
			write(g1001->array4[i].unknown1);
			write(g1001->array4[i].string1);
			write(g1001->array4[i].unknown2);
			write(g1001->array4[i].flags);
			writeRefID(g1001->array4[i].reference);
		}
		write(g1001->scriptListCount);
		for (unsigned int i = 0; i < g1001->scriptListCount; i++) {
			write(g1001->scriptList[i].length);
			write(g1001->scriptList[i].string, g1001->scriptList[i].length);
		}
		write(g1001->arrayCount4a);
		write(g1001->arrayCount4b);
		for (unsigned int i = 0; i < g1001->arrayCount4b; i++) {
			write(g1001->array4b[i].unknown0);
			write(g1001->array4b[i].unknown1);
			write(g1001->array4b[i].unknown2);
			writeRefID(g1001->array4b[i].reference0);
			writeRefID(g1001->array4b[i].reference1);
			writeRefID(g1001->array4b[i].reference2);
			writeRefID(g1001->array4b[i].reference3);
		}
		write(g1001->arrayCount4c);
		for (unsigned int i = 0; i < g1001->arrayCount4c; i++) {
			write(g1001->array4c[i].flag);
			write(g1001->array4c[i].unknown0);
			writeRefID(g1001->array4c[i].reference0);
			if (g1001->array4c[i].flag == 0) {
				write(g1001->array4c[i].unknown1);
				write(g1001->array4c[i].unknown2);
				write(g1001->array4c[i].unknown3);
			}
			else if (g1001->array4c[i].flag == 6) {
				write(g1001->array4c[i].unknown1);
			}
			else if (g1001->array4c[i].flag == 3) {
				write(g1001->array4c[i].unknown3);
			}
		}
		write(g1001->arrayCount4d);
		for (unsigned int i = 0; i < g1001->arrayCount4d; i++) {
			write(g1001->array4d[i].flag0);
			write(g1001->array4d[i].unknown);
			write(g1001->array4d[i].flag1);
			writeRefID(g1001->array4d[i].reference);
		}
		write(g1001->arrayCount5);
		for (unsigned int i = 0; i < g1001->arrayCount5; i++) {
			write(g1001->array5[i].unknown0);
			write(g1001->array5[i].unknown1);
			writeRefID(g1001->array5[i].reference0);
			writeRefID(g1001->array5[i].reference1);
			writeRefID(g1001->array5[i].reference2);
			write(g1001->array5[i].unknown2);
		}
		write(g1001->arrayCount6);
		for (unsigned int i = 0; i < g1001->arrayCount6; i++) {
			write(g1001->array6[i].unknown);
			write(g1001->array6[i].flags);
			writeRefID(g1001->array6[i].reference);
		}
		write(g1001->arrayCount7);
		for (unsigned int i = 0; i < g1001->arrayCount7; i++) {
			write(g1001->array7[i].unknown);
			write(g1001->array7[i].flags);
			writeRefID(g1001->array7[i].reference);
		}
		write(g1001->arrayCount8);
		for (unsigned int i = 0; i < g1001->arrayCount8; i++) {
			write(g1001->array8[i].unknown);
			write(g1001->array8[i].type);
			writeRefID(g1001->array8[i].reference);
			write(g1001->array8[i].count0);
			write(g1001->array8[i].count1);
			writeRefID(g1001->array8[i].reference0, g1001->array8[i].count0);
			writeRefID(g1001->array8[i].reference1, g1001->array8[i].count1);
		}
		write(g1001->arrayCount9);
		write(g1001->array9, g1001->arrayCount9);
		write(g1001->arrayCount10);
		for (unsigned int i = 0; i < g1001->arrayCount10; i++) {
			writeRefID(g1001->array10[i].reference);
			write(g1001->array10[i].count);
			for (unsigned int j = 0; j < g1001->array10[i].count; j++) {
				write(g1001->array10[i].elements[j].name.length);
				write(g1001->array10[i].elements[j].name.string, g1001->array10[i].elements[j].name.length);
				write(g1001->array10[i].elements[j].count0);
				for (unsigned int k = 0; k < g1001->array10[i].elements[j].count0; k++) {
					write(g1001->array10[i].elements[j].elements0[k].count);
					for (unsigned int l = 0; l < g1001->array10[i].elements[j].elements0[k].count; l++) {
						write(g1001->array10[i].elements[j].elements0[k].elements[l].string);
						write(g1001->array10[i].elements[j].elements0[k].elements[l].flag);
					}
				}
				write(g1001->array10[i].elements[j].count1);
				for (unsigned int k = 0; k < g1001->array10[i].elements[j].count1; k++) {
					write(g1001->array10[i].elements[j].elements1[k].unknown);
					write(g1001->array10[i].elements[j].elements1[k].flag);
					writeRefID(g1001->array10[i].elements[j].elements1[k].reference);
				}

			}
		}
		write(g1001->arrayCount11);
		for (unsigned int i = 0; i < g1001->arrayCount11; i++) {
			write(g1001->array11[i].unknown0);
			writeRefID(g1001->array11[i].reference);
			write(g1001->array11[i].unknown1);
		}
		write(g1001->arrayCount12);
		for (unsigned int i = 0; i < g1001->arrayCount12; i++) {
			writeRefID(g1001->array12[i].reference);
			write(g1001->array12[i].count);
			for (unsigned int j = 0; j < g1001->array12[i].count; j++) {
				write(g1001->array12[i].elements[j].unknown0);
				write(g1001->array12[i].elements[j].unknown1);
				write(g1001->array12[i].elements[j].unknown2);
				write(g1001->array12[i].elements[j].unknown3);
			}
		}
		write(g1001->arrayCount13);
		for (unsigned int i = 0; i < g1001->arrayCount13; i++) {
			writeRefID(g1001->array13[i].reference);
			write(g1001->array13[i].count);
			for (unsigned int j = 0; j < g1001->array13[i].count; j++) {
				write(g1001->array13[i].elements[j].unknown0);
				write(g1001->array13[i].elements[j].count0);
				write(g1001->array13[i].elements[j].data0, g1001->array13[i].elements[j].count0);
				write(g1001->array13[i].elements[j].count1);
				for (unsigned int k = 0; k < g1001->array13[i].elements[j].count1; k++) {
					write(g1001->array13[i].elements[j].data1, 5);
				}
				write(g1001->array13[i].elements[j].unknown1);
			}
		}
		write(g1001->arrayCount14);
		for (unsigned int i = 0; i < g1001->arrayCount14; i++) {
			writeRefID(g1001->array14[i].reference);
			write(g1001->array14[i].count);
			for (unsigned int j = 0; j < g1001->array14[i].count; j++) {
				write(g1001->array14[i].elements[j].data, 5);
			}
			write(g1001->array14[i].data);
		}
		write(g1001->arrayCount15);
		for (unsigned int i = 0; i < g1001->arrayCount15; i++) {
			write(g1001->array15[i].data);
			write(g1001->array15[i].count);
			write(g1001->array15[i].elements, g1001->array15[i].count);
		}
		break;

	case 1002://anim objects
		write(g1002->count);
		for (unsigned int i = 0; i < g1002->count; i++) {
			writeRefID(g1002->objects[i].achr);
			writeRefID(g1002->objects[i].anim);
			write(g1002->objects[i].unknown);
		}
		break;

	case 1003://timer
		write(g1003->unknown1);
		write(g1003->unknown2);
		break;

	case 1004://synchronized animations
		write(g1004, data.length);
		break;

	case 1005://main
		write(g1005, data.length);
		break;

	default:
		throw "internal error(w:gd invalid type)";
	}

}
