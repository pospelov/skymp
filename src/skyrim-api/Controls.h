// Legacy from 0.9 or earlier (November 2017)

#pragma once
#include <string>
#include <mutex>
#include <map>
#include <set>
#include <cstdint>
#include <beth-api/beth_api.h>

// All methods must be threadsafe
// Upd (March 2019): In skymp3 we do not use this class from different threads

using Control = beth::Control;

class Controls {
public:
	void SetEnabled(Control control, bool enabled, std::string srcsystem);
	void RequestApply() { std::lock_guard<std::mutex> l(m); needApply = true; }

	void Update();

private:
	enum {
		MIN_ENABLED_STATE = 0
	};
	std::mutex m;
	std::vector<int64_t> state = std::vector<int64_t>((int)Control::COUNT, MIN_ENABLED_STATE);
	std::map<std::string, std::set<Control>> disabled;
	bool needApply = true;
};