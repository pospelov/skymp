// Legacy from 2.1 (January 2019)
#include "headers.h"
#include "EquipmentManager.h"

static constexpr uint32_t g_torchId = 0x0001D4EC;

inline bool TorchEquipped(Actor *ac) {
	return (sd::GetEquippedItemType(ac, 0) == 11);// left is 0 in this function
}

void EquipmentManager::Init() {
	if (!this->fakeIronArrow) {
		this->fakeIronArrow = (TESAmmo *)FormHeap_Allocate(sizeof TESAmmo);
		const auto ironArrow = (TESAmmo *)LookupFormByID(ID_TESAmmo::IronArrow);
		memcpy(this->fakeIronArrow, ironArrow, sizeof TESAmmo);
		this->fakeIronArrow->texSwap.SetModelName("");
	}
}

Equipment EquipmentManager::GetFrom(Actor *ac) noexcept {
	this->Init();

	Equipment eq;
	eq.armor.reserve(8);
	const int n = papyrusObjectReference::GetNumItems(ac);
	for (int i = 0; i < n; ++i) {
		auto form = papyrusObjectReference::GetNthForm(ac, i);
		if (form && sd::IsEquipped(ac, form)) {
			switch (form->formType) {
			case kFormType_Armor:
			case kFormType_Ammo:
				eq.armor.push_back(form->formID);
				break;
			case kFormType_Weapon: // TODO: both hands
				eq.special[Equipment::RightHand] = form->formID;
				break;
			}
		}
	}
	if (TorchEquipped(ac)) {
		eq.armor.push_back(g_torchId);
	}

	return eq;
}

BGSOutfit *EquipmentManager::Apply(const Equipment &eq) noexcept {
	this->Init();

	const auto baseOutfit = (BGSOutfit *)LookupFormByID(ID_BGSOutfit::ArmorBladesOutfitNoHelmet); // TODO: choose another base outfit?
	const auto outfit = (BGSOutfit *)FormHeap_Allocate(sizeof(BGSOutfit));
	assert(outfit);
	if (outfit) {
		memcpy(outfit, baseOutfit, sizeof(BGSOutfit));

		auto &array = outfit->armorOrLeveledItemArray;

		std::vector<TESForm *> forms;
		bool ammoAdded = false;
		forms.reserve(eq.armor.size());
		for (auto &id : eq.armor) {
			if (const auto f = LookupFormByID(id)) {
				if (f->formType == kFormType_Armor)
					forms.push_back(f);
				else if (f->formType == kFormType_Ammo) {
					forms.push_back(f);
					ammoAdded = true;
				}
			}
		}
		if (!ammoAdded) {
			forms.push_back(this->fakeIronArrow);
			assert(forms.back());
			assert(forms.back()->formType == kFormType_Ammo);
		}
		if (const auto f = LookupFormByID(eq.special[Equipment::RightHand])) {
			if (f->formType == kFormType_Weapon) {
				forms.push_back(f);
			}
		}
		if (forms.size()) {
			array.Allocate(forms.size());
			for (size_t i = 0; i < forms.size(); ++i) array[i] = forms[i];
		}
		else {
			return nullptr;
		}
	}
	return outfit;
}

static TESForm *GetAmmo(const Equipment &eq) {
	for (auto id : eq.armor) {
		const auto form = LookupFormByID(id);
		if (form && form->formType == kFormType_Ammo) {
			return form;
		}
	}
	return nullptr;
}

static bool IsBowOrCrossbow(TESForm *form) {
	TESObjectWEAP *weap = (TESObjectWEAP *)form;
	if (weap && weap->formType == kFormType_Weapon) {
		switch (weap->type()) {
		case TESObjectWEAP::GameData::kType_Bow:
		case TESObjectWEAP::GameData::kType_Bow2:
		case TESObjectWEAP::GameData::kType_CrossBow:
			return true;
		default:
			return false;
		}
	}
	return false;
}

void EquipmentManager::Apply(Actor *ac, const Equipment &eq) noexcept {
	auto eqWas = this->GetFrom(ac);

	// Hands
	for (int i = 0; i < 1; ++i) { // TODO: both hands
		auto form = LookupFormByID(eq.special[i]);
		auto formWas = (TESForm *)sd::GetEquippedWeapon(ac, (bool)i);
		if (!formWas) formWas = sd::GetEquippedSpell(ac, !i);
		if (formWas != form) {
			if (!form) form = LookupFormByID(0x1f4); // unarmed
			if (form) {
				if (form->formType != FormType::kFormType_Spell) {
					sd::RemoveItem(ac, formWas, -1, true, nullptr);

					if (!IsBowOrCrossbow(form) || GetAmmo(eqWas) != nullptr) {
						sd::AddItem(ac, form, 1, true);
					}
				}
				else {
					// Uncomment to enable magic equipment
					///sd::EquipSpell(ac, (SpellItem *)form, !i);
				}
			}
		}
	}

	// Armor
	std::set<uint32_t> toAdd, toRemove;

	for (auto element : eq.armor) {
		if (std::find(eqWas.armor.begin(), eqWas.armor.end(), element) == eqWas.armor.end()) toAdd.insert(element);
	}
	for (auto element : eqWas.armor) {
		if (std::find(eq.armor.begin(), eq.armor.end(), element) == eq.armor.end()) toRemove.insert(element);
	}

	for (auto &id : toRemove) {
		const auto form = LookupFormByID(id);
		if (form && (form->formType == kFormType_Armor || /*form->formType == kFormType_Ammo ||*/ form->formType == kFormType_Light)) {
			sd::UnequipItem(ac, form, false, true);
			sd::RemoveItem(ac, form, -1, true, nullptr);
		}
	}
	for (auto &id : toAdd) {
		const auto form = LookupFormByID(id);
		if (form && (form->formType == kFormType_Armor || form->formType == kFormType_Ammo || form->formType == kFormType_Light)) {
			if (form->formType == kFormType_Ammo) sd::AddItem(ac, form, 1000, true);
			sd::EquipItem(ac, form, true, true);
		}
	}
}