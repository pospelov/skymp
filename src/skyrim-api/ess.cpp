#include <sstream>
#include <zlib.h>
#include <cmrc/cmrc.hpp>
#include "ess_file.h"
#include "assets.h"

#include "ess.h"

static_assert(sizeof(char) == 1);

class BinarySave : public ess::IBinarySave {
public:
	BinarySave(const std::string &str_) : str(str_) {};

	const void *GetData() const noexcept override {
		return str.data();
	}

	size_t GetDataLength() const noexcept override {
		return str.size();
	}

	const std::string str;
};

// https://gist.github.com/arq5x/5315739

void ZlibDecompress(const void *in, size_t inSize, void *out, size_t outSize) {
	z_stream infstream;
	infstream.zalloc = Z_NULL;
	infstream.zfree = Z_NULL;
	infstream.opaque = Z_NULL;

	infstream.avail_in = (uInt)(inSize); // size of input
	infstream.next_in = (Bytef *)in; // input char array
	infstream.avail_out = (uInt)outSize; // size of output
	infstream.next_out = (Bytef *)out; // output char array

	inflateInit(&infstream);


	int res = inflate(&infstream, Z_NO_FLUSH);
	if (res < Z_OK) throw ess::CreateError("inflate() failed with code " + std::to_string(res));
	res = inflateEnd(&infstream);
	if (res < Z_OK) throw ess::CreateError("inflateEnd() failed with code " + std::to_string(res));
}

size_t ZlibCompress(const void *in, size_t inSize, void *out, size_t outMaxSize) {
	z_stream defstream;
	defstream.zalloc = Z_NULL;
	defstream.zfree = Z_NULL;
	defstream.opaque = Z_NULL;

	defstream.avail_in = (uInt)inSize;
	defstream.next_in = (Bytef *)in; // input char array
	defstream.avail_out = (uInt)outMaxSize; // size of output
	defstream.next_out = (Bytef *)out; // output char array

	// the actual compression work.
	deflateInit(&defstream, Z_BEST_COMPRESSION);
	int res = deflate(&defstream, Z_FINISH);
	if (res < Z_OK) throw ess::CreateError("deflate() failed with code " + std::to_string(res));
	const auto outputSize = defstream.next_out - (uint8_t *)out;
	res = deflateEnd(&defstream);
	if (res < Z_OK) throw ess::CreateError("deflateEnd() failed with code " + std::to_string(res));
	return outputSize;
}

#pragma pack(push, 1)
struct RefId {
	uint8_t v[3] = { 0,0,0 };
};
static_assert(sizeof(RefId) == 3);
#pragma pack(pop)

RefId CreateRefId(saveFile &savetool, uint32_t formId) {
	RefId res;
	if (formId < 0x01000000) {
		res.v[0] = uint8_t((formId / 65536) % 256) | uint8_t(0x40); // 0b1000000
		res.v[1] = (formId / 256) % 256;
		res.v[2] = formId % 256;
	}
	else {
		const auto countWas = savetool.file.formIDArrayCount;
		uint32_t *newFormIDArray = new uint32_t[countWas + 1];

		memcpy(newFormIDArray, savetool.file.formIDArray, countWas);
		newFormIDArray[countWas] = formId;

		delete[] savetool.file.formIDArray;
		savetool.file.formIDArray = newFormIDArray;
		savetool.file.formIDArrayCount = countWas + 1;

		// fix offset
		savetool.file.fileLocationTable.unknownTable3Offset += 4;

		// 255 => 00 00 FF
		// 256 => 00 01 00
		// 65536 => error
		const auto index = countWas + 1; // as uesp.net says, formIDArray index starts in 1
		if (index >= 65536) throw ess::CreateError("too many elements was in FormIDArray (" + std::to_string(countWas) + ")");
		res.v[0] = 0;
		res.v[1] = (index / 256) % 256;
		res.v[2] = index % 256;
	}
	return res;
}

std::shared_ptr<ess::IBinarySave> ess::Create(const SaveData &saveData) {
	std::shared_ptr<IBinarySave> res;
	std::stringstream log, out;
	saveFile savetool(log, false, out);

	auto [templateEss, size] = assets_get_file("template.ess");
	std::vector<uint8_t> templCopy(size);
	memcpy(templCopy.data(), templateEss, size);
	try {
		if (!savetool.read(templCopy.data(), size)) {
			throw CreateError("read() returned false");
		}
		if (!savetool.clean()) {
			throw CreateError("clean() returned false");
		}

		const RefId wpRefId = CreateRefId(savetool, saveData.worldSpaceId);

		auto &g1 = savetool.g1;
		g1->nextObjectId = 4278195454;
		g1->worldspace1.byte0 = wpRefId.v[0];
		g1->worldspace1.byte1 = wpRefId.v[1];
		g1->worldspace1.byte2 = wpRefId.v[2];
		g1->coorX = (int)saveData.posX / 4096;
		g1->coorY = (int)saveData.posY / 4096;
		g1->worldspace2.byte0 = wpRefId.v[0];
		g1->worldspace2.byte1 = wpRefId.v[1];
		g1->worldspace2.byte2 = wpRefId.v[2];
		g1->posX = (float)saveData.posX;
		g1->posY = (float)saveData.posY;
		g1->posZ = (float)saveData.posZ;
		g1->unknown = 0;

		auto &changeForms = savetool.file.changeForms;
		const auto count = savetool.file.fileLocationTable.changeFormCount;
		for (size_t i = 0; i < count; ++i) {
			auto &chForm = changeForms[i];
			const bool isPlayer = chForm.formID.byte0 == 0x40 && chForm.formID.byte1 == 0x00 && chForm.formID.byte2 == 0x14;
			if (isPlayer) {
				if (chForm.length2 != 0) {
					const size_t lengthCompressed = chForm.length1;
					const size_t lengthUncompressed = chForm.length2;
					auto uncompressed = (std::vector<uint8_t>(lengthUncompressed));
					const auto compressed = chForm.data;
					ZlibDecompress(compressed, lengthCompressed, uncompressed.data(), lengthUncompressed);

					// modify changeform
					auto d = uncompressed.data();
					*(RefId *)(d + 0) = wpRefId;
					float *pos = (float *)(d + 3);
					float *rot = (float *)(d + 15);
					pos[0] = saveData.posX;
					pos[1] = saveData.posY;
					pos[2] = saveData.posZ;
					rot[0] = 0.f;
					rot[1] = 0.f;
					rot[2] = saveData.degreesRotZ;

					const size_t newCompressedSizeMax = lengthUncompressed;
					std::vector<uint8_t> newCompressed(newCompressedSizeMax, 0);
					const auto newCompressedSize = ZlibCompress(uncompressed.data(), uncompressed.size(), newCompressed.data(), newCompressedSizeMax);

					if (newCompressedSize > lengthCompressed) {
						std::stringstream errSs;
						errSs << "newCompressedSize is greater than lengthCompressed (" << newCompressedSize << " > " << lengthCompressed << ')';
						throw CreateError(errSs.str());
					}

					// write new version of change form
					chForm.length1 = newCompressedSize;
					chForm.length2 = uncompressed.size();
					chForm.data = new uint8_t[chForm.length2];
					memcpy(chForm.data, newCompressed.data(), chForm.length1);

					// fix offsets
					const int diff = lengthCompressed - newCompressedSize;
					savetool.file.fileLocationTable.formIDArrayCountOffset -= diff;
					savetool.file.fileLocationTable.unknownTable3Offset -= diff;
					savetool.file.fileLocationTable.globalDataTable3Offset -= diff;
				}
			}
		}

		if (!savetool.save()) {
			throw CreateError("save() returned false");
		}
	}
	catch (const CreateError &) {
		throw;
	}
	catch (const std::exception &e) {
		throw CreateError(e.what());
	}
	catch (std::string e) {
		throw CreateError(e);
	}
	catch (const char *e) {
		throw CreateError(e);
	}
	catch (...) {
		throw CreateError("unknown error");
	}

	res.reset(new BinarySave(out.str()));
	return res;
}
