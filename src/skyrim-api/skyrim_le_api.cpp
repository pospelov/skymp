#include "headers.h"
#include <nlohmann/json.hpp>

#include "ess.h"
#include "LookManager.h"
#include "EquipmentManager.h"
#include "Controls.h"
#include "FormIdGen.h"

#include "skyrim_le_api.h"

namespace fs = std::filesystem;
using json = nlohmann::json;
using namespace skyrim_le_api;

// Helpers and utils:

#pragma comment(lib, "shell32.lib")
#include <shlobj.h>

namespace {
	// Range [0xFE000000;0xFF000000] is reserved for fake ids
	// g_fakeId must be reseted with Reset() method every game thread init
	class FakeIdManager {
	public:
		// Returns 0 on failure
		uint32_t Insert(TESForm *f) {
			std::lock_guard l(this->m);
			uint64_t id = data.size() + 0xfe000000;
			if (id >= 0xff000000) {
				assert(0 && "Too many fake ids generated");
				return 0;
			}
			data.push_back(f);
			return uint32_t(id);
		}

		TESForm *Find(uint32_t id) {
			std::lock_guard l(this->m);
			if (id < 0xfe000000 || id >= 0xff000000) return nullptr;
			try {
				return data.at(id - 0xfe000000);
			}
			catch (...) {
				return nullptr;
			}
		}

		void Reset() {
			std::lock_guard l(this->m);
			this->data.clear();
		}

	private:
		std::vector<TESForm *> data;
		std::mutex m;
	};

	// Any thread:
	static std::mutex g_uiM;

	static LookManager g_lookManager;

	static FormIdGen g_formIdGen;

	// Game thread only:
	static std::vector<Tint> g_lastTints;
	static std::unique_ptr<Controls> g_controls;
	static BSFixedString *g_animEvent = nullptr;
	static EquipmentManager g_eqManager;
	static std::vector<beth::Form> g_loadedRefsCache;

	static FakeIdManager g_fakeId; 

	static std::wstring GetPathToMyDocuments() {
		PWSTR   ppszPath;    // variable to receive the path memory block pointer.
		HRESULT hr = SHGetKnownFolderPath(FOLDERID_Documents, 0, NULL, &ppszPath);
		std::wstring myPath;
		if (SUCCEEDED(hr)) {
			myPath = ppszPath;
		}
		CoTaskMemFree(ppszPath);    // free up the path memory block
		return myPath;
	}

	// https://stackoverflow.com/questions/2573834/c-convert-string-or-char-to-wstring-or-wchar-t
	static std::wstring StringToWstring(std::string s) {
		std::wstring ws(s.size(), L' '); // Overestimate number of code points.
		ws.resize(std::mbstowcs(&ws[0], s.c_str(), s.size())); // Shrink to fit.
		return ws;
	}

	struct RaceMenuExitEvent {
		bool lookChanged = false;
	};
}

struct Features::Impl {
	std::shared_ptr<beth::IListener> listener;

	std::unique_ptr<RaceMenuExitEvent> raceMenuExit;
	struct {
		std::recursive_mutex m;
		std::vector<fs::path> essGarbage;
		std::string essName;
		bool taskEssGarbageCollect = false;
	} share;

	struct {
		std::recursive_mutex m;
		std::string lastAe;
		std::vector<std::string> animEvents;
	} share2;

	struct {
		std::recursive_mutex m;
		std::set<uint32_t> drawn;
	} share3;
};

Features::Features(std::shared_ptr<beth::IListener> listener) noexcept {
	pImpl.reset(new Impl);
	pImpl->listener = listener;
}

void Features::PrintNote(const char *fmt, ...) noexcept {
	char text[1024];
	va_list lst;
	va_start(lst, fmt);
	vsprintf_s(text, fmt, lst);
	va_end(lst);
	OutputDebugStringA(text);
	sd::Notification(text);
}

namespace {
	template <class T>
	T *Get(const beth::Form &pForm) {

		T *form = (T *)g_fakeId.Find(pForm.id);
		if (!form) {
			form = (T *)LookupFormByID(pForm.id);
		}
		if (form) {
			if constexpr (std::is_same<T, TESObjectREFR>::value) {
				form = DYNAMIC_CAST((TESForm *)form, TESForm, TESObjectREFR);
			}
			else {
				if constexpr (std::is_same<T, TESForm>::value == false) {
					if (form->formType != T::kTypeID) {
						form = nullptr;
					}
				}
			}
		}
		return form;
	}
}

beth::Form Features::GetPlayer() noexcept {
	return 0x14;
}

beth::Form Features::GetParentCell(const beth::Form &pRef) noexcept {
	if (auto ref = Get<TESObjectREFR>(pRef)) {
		return sd::GetParentCell(ref);
	}
	return nullptr;
}

std::vector<beth::Form> Features::GetLoadedRefs(const beth::Form &pCellFilter) noexcept {
	const auto targetCell = Get<TESObjectCELL>(pCellFilter);

	if (!targetCell && !g_loadedRefsCache.empty()) {
		return g_loadedRefsCache;
	}

	std::vector<beth::Form> res;
	res.reserve(65536);
	auto currentCell = sd::GetParentCell(*g_thePlayer);
	if (currentCell) {
		const auto formID = currentCell->formID;
		enum { CELL_SEARCH_DEEPTH = 40 };
		for (auto tmpFormID = (formID > CELL_SEARCH_DEEPTH) ? formID - CELL_SEARCH_DEEPTH : 0; tmpFormID != formID + CELL_SEARCH_DEEPTH; ++tmpFormID) {
			auto tmpCell = (TESObjectCELL *)LookupFormByID(tmpFormID);
			if (tmpCell && tmpCell->formType == kFormType_Cell) {
				if (!targetCell || tmpCell == targetCell) {
					for (size_t i = 0; i != tmpCell->objectList.count; ++i) {
						auto refr = tmpCell->objectList[i];
						if (refr && !(refr->flags & TESForm::kFlagUnk_0x800)) {// not disabled
							if (refr->formID < 0xff000000) {
								res.push_back(beth::Form(refr->formID));
							}
						}
					}
				}
			}
		}
	}

	if (targetCell) {
		g_loadedRefsCache = res;
	}
	return res;
}

TESFormType Features::GetFormType(const beth::Form &pForm) noexcept {
	if (auto form = Get<TESForm>(pForm)) {
		return (TESFormType)form->formType;
	}
	return TESFormType::None;
}

beth::Form Features::GetBase(const beth::Form &pForm) noexcept {
	if (auto form = Get<TESObjectREFR>(pForm)) {
		return form->baseForm;
	}
	return {};
}

uint32_t Features::GetId(const beth::Form &pForm) noexcept {
	if (g_fakeId.Find(pForm.id) != nullptr) {
		return pForm.id;
	}
	if (auto form = Get<TESForm>(pForm)) {
		return form->formID;
	}
	return 0x00000000;
}

void Features::DisableNoWait(const beth::Form &pRef, bool fadeOut) noexcept {
	if (auto ref = Get<TESObjectREFR>(pRef)) {
		sd::DisableNoWait(ref, fadeOut);
	}
}

void Features::Delete(const beth::Form &pRef) noexcept {
	if (auto ref = Get<TESObjectREFR>(pRef)) {
		sd::Delete(ref);
	}
}

void Features::SetLockLevel(const beth::Form &pRef, beth::LockLevel lockLevel) noexcept {
	if (auto ref = Get<TESObjectREFR>(pRef)) {
		if (ref->formType != kFormType_Character) {
			sd::Lock(ref, lockLevel != LockLevel::None, false);
			switch (lockLevel) {
			case LockLevel::None:
				break;
			case LockLevel::Novice:
				sd::SetLockLevel(ref, 1);
				break;
			case LockLevel::Apprentice:
				sd::SetLockLevel(ref, 25);
				break;
			case LockLevel::Adept:
				sd::SetLockLevel(ref, 50);
				break;
			case LockLevel::Expert:
				sd::SetLockLevel(ref, 75);
				break;
			case LockLevel::Master:
				sd::SetLockLevel(ref, 100);
				break;
			case LockLevel::RequiresKey:
				sd::SetLockLevel(ref, 255);
				break;
			}
		}
	}
}

void Features::BlockActivation(const beth::Form &pRef, bool block) noexcept {
	if (auto ref = Get<TESObjectREFR>(pRef)) {
		sd::BlockActivation(ref, block);
	}
}

void Features::SetMotionType(const beth::Form &pRef, beth::MotionType mt) noexcept {
	if (auto ref = Get<TESObjectREFR>(pRef)) {
		if (ref->formType != kFormType_Character) {
			if (mt == beth::MotionType::Dynamic) {
				sd::SetMotionType(ref, 1, true);
			}
			else if (mt == beth::MotionType::Keyframed) {
				sd::SetMotionType(ref, 4, true);
			}
		}
	}
}

Point3D Features::GetPosition(const beth::Form &pRef) noexcept {
	if (auto ref = Get<TESObjectREFR>(pRef)) {
		auto &pos = ref->pos;
		return Point3D(pos.x, pos.y, pos.z);
	}
	return {};
}

Point3D Features::GetRotation(const beth::Form &pRef) noexcept {
	if (auto ref = Get<TESObjectREFR>(pRef)) {
		return Point3D(sd::GetAngleX(ref), sd::GetAngleY(ref), sd::GetAngleZ(ref));
	}
	return {};
}

beth::Form Features::FindRandomActor(const Point3D &pos, float radius) noexcept {
	if (radius <= 0) return {};
	auto ac = sd::FindRandomActor(pos.x, pos.y, pos.z, radius);
	if (!ac) return {};
	return ac->formID;
}

bool Features::IsInterior(const beth::Form &pCell) noexcept {
	if (auto cell = Get<TESObjectCELL>(pCell)) {
		return cell->IsInterior();
	}
	return false;
}

beth::Form Features::PlaceAtMe(const beth::Form &pAt, const beth::Form &pBase) noexcept {
	if (auto at = Get<TESObjectREFR>(pAt)) {
		if (auto base = Get<TESForm>(pBase)) {
			auto ac = sd::PlaceAtMe(at, base, 1, true, false);
			if (ac) {
				return ac->formID;
			}
		}
	}
	return {};
}

bool Features::Exist(const beth::Form &pForm) noexcept {
	return Get<TESForm>(pForm) != nullptr;
}

beth::Form Features::GetWorldSpace(const beth::Form &pRef) noexcept {
	if (auto ref = Get<TESObjectREFR>(pRef)) {
		return sd::GetWorldSpace(ref);
	}
	return {};
}

bool Features::Is3DLoaded(const beth::Form &pRef) noexcept {
	if (auto ref = Get<TESObjectREFR>(pRef)) {
		return sd::Is3DLoaded(ref);
	}
	return false;
}

bool Features::GetAnimVariableBool(const beth::Form &pRef, const char *varName) noexcept {
	if (auto ref = Get<TESObjectREFR>(pRef)) {
		uint8_t res = 0;
		BSFixedString s(varName);
		ref->animGraphHolder.GetVariableBool(&s, &res);
		return res != 0;
	}
	return false;
}

int Features::GetAnimVariableInt(const beth::Form &pRef, const char *varName) noexcept {
	if (auto ref = Get<TESObjectREFR>(pRef)) {
		uint32_t res = 0;
		BSFixedString s(varName);
		ref->animGraphHolder.GetVariableInt(&s, (UInt32 *)&res);
		return (int)res;
	}
	return 0;
}

float Features::GetAnimVariableFloat(const beth::Form &pRef, const char *varName) noexcept {
	if (auto ref = Get<TESObjectREFR>(pRef)) {
		float res = 0;
		BSFixedString s(varName);
		ref->animGraphHolder.GetVariableFloat(&s, &res);
		return res;
	}
	return 0;
}

beth::ActorFlags Features::GetActorFlags(const beth::Form &pActor) noexcept {
	beth::ActorFlags res;
	if (auto ac = Get<Actor>(pActor)) {
		res.isSneaking = sd::IsSneaking(ac);
		res.isWeapDrawn = sd::IsWeaponDrawn(ac);
		res.isBlocking = sd::Obscript::IsBlocking(ac) > 0.01f;
		res.isDead = sd::IsDead(ac);
		res.isSprinting = sd::IsSprinting(ac);
		res.isRunning = (ac == *g_thePlayer) ? !!PlayerControls::GetSingleton()->runMode : sd::IsRunning(ac);
	}
	return res;
}

float Features::GetCurrentAV(const beth::Form &pActor, ActorValue av) noexcept {
	if (auto ac = Get<Actor>(pActor)) {
		return sd::GetActorValue(ac, (char *)ActorValueUtils::GetActorValueName(av));
	}
	return 0.f;
}

void Features::SetCurrentAV(const beth::Form &pActor, ActorValue av, float newVal) noexcept {
	if (auto ac = Get<Actor>(pActor)) {
		sd::ForceActorValue(ac, (char *)ActorValueUtils::GetActorValueName(av), newVal);
	}
}

void Features::ModCurrentAV(const beth::Form &pActor, ActorValue av, float newVal) noexcept {
	if (auto ac = Get<Actor>(pActor)) {
		if (newVal > 0) {
			sd::RestoreActorValue(ac, (char *)ActorValueUtils::GetActorValueName(av), newVal);
		}
		else if (newVal < 0) {
			sd::DamageActorValue(ac, (char *)ActorValueUtils::GetActorValueName(av), -newVal);
		}
	}
}

void Features::SetBaseAV(const beth::Form &pActor, ActorValue av, float newVal) noexcept {
	if (auto ac = Get<Actor>(pActor)) {
		sd::SetActorValue(ac, (char *)ActorValueUtils::GetActorValueName(av), newVal);
	}
}

float Features::GetAVPercentage(const beth::Form &pActor, ActorValue av) noexcept {
	if (auto ac = Get<Actor>(pActor)) {
		return sd::GetActorValuePercentage(ac, (char *)ActorValueUtils::GetActorValueName(av));
	}
	return 0.f;
}

float Features::GetTotalItemWeight(const beth::Form &pRef) noexcept {
	if (auto ac = Get<TESObjectREFR>(pRef)) {
		return papyrusObjectReference::GetTotalItemWeight(ac);
	}
	return 0.f;
}

void Features::SetPosition(const beth::Form &pRef, const Point3D &pos) noexcept {
	if (auto ref = Get<TESObjectREFR>(pRef)) {
		sd::SetPosition(ref, pos.x, pos.y, pos.z);
	}
}

void Features::SetRotation(const beth::Form &pRef, const Point3D &rot) noexcept {
	if (auto ref = Get<TESObjectREFR>(pRef)) {
		sd::SetAngle(ref, rot.x, rot.y, rot.z);
	}
}

void Features::SetHeadTracking(const beth::Form &pActor, bool headTraking) noexcept {
	if (auto ac = Get<Actor>(pActor)) {
		sd::SetHeadTracking(ac, headTraking);
	}
}

void Features::SetName(const beth::Form &pRef, const char *name) noexcept {
	if (auto ref = Get<TESObjectREFR>(pRef)) {
		if (ref == *g_thePlayer) {
			const auto len = strlen(name);
			auto buf = new char[len + 1];
			memcpy(buf, name, len + 1);
			papyrusForm::SetName(sd::GetPlayer()->baseForm, *new BSFixedString(buf));
		}
		else {
			papyrusObjectReference::SetDisplayName(ref, name, true);
		}
	}
}

void Features::ClearLookAt(const beth::Form &pActor) noexcept {
	if (auto ac = Get<Actor>(pActor)) {
		sd::ClearLookAt(ac);
	}
}

void Features::StopCombat(const beth::Form &pActor) noexcept {
	if (auto ac = Get<Actor>(pActor)) {
		sd::StopCombat(ac);
	}
}

void Features::SetDeferredKill(const beth::Form &pActor, bool deferredKill) noexcept {
	if (auto ac = Get<Actor>(pActor)) {
		if (deferredKill) {
			sd::StartDeferredKill(ac);
		}
		else {
			sd::EndDeferredKill(ac);
		}
	}
}

void Features::Kill(const beth::Form &pActor) noexcept {
	if (auto ac = Get<Actor>(pActor)) {
		sd::Kill(ac, nullptr);
	}
}

void Features::KeepOffsetFromActor(const beth::Form &pActor, const beth::Form &pTarget, const Point3D &offset, float angleOffset, float catchupRadius, float followRadius) noexcept {
	if (auto ac = Get<Actor>(pActor)) {
		if (auto target = Get<Actor>(pTarget)) { // TESObjectREFR?
			sd::KeepOffsetFromActor(ac, target, offset.x, offset.y, offset.z, 0, 0, angleOffset, catchupRadius, followRadius);
		}
	}
}

void Features::ClearKeepOffsetFromActor(const beth::Form &pActor) noexcept {
	if (auto ac = Get<Actor>(pActor)) {
		sd::ClearKeepOffsetFromActor(ac);
	}
}

void Features::TranslateTo(const beth::Form &pRef, const Point3D &pos, const Point3D &rot, float speed, float maxRotSpeed) noexcept {
	if (auto ref = Get<TESObjectREFR>(pRef)) {
		sd::TranslateTo(ref, pos.x, pos.y, pos.z, rot.x, rot.y, rot.z, speed, maxRotSpeed);
	}
}

void Features::SendAnimEvent(const beth::Form &pRef, const char *animEventName) noexcept {
	if (auto ref = Get<TESObjectREFR>(pRef)) {
		BSFixedString s(animEventName);
		g_animEvent = &s;
		ref->animGraphHolder.SendAnimationEvent(&s);
		g_animEvent = nullptr;
	}
}

void Features::SetWeaponDrawn(const beth::Form &pActor, bool drawn) noexcept {
	if (auto ac = Get<Actor>(pActor)) {
		if (drawn) {
			std::lock_guard l(pImpl->share3.m);
			pImpl->share3.drawn.insert(ac->formID);
		}
		else {
			std::lock_guard l(pImpl->share3.m);
			pImpl->share3.drawn.erase(ac->formID);
		}
		ac->DrawSheatheWeapon(drawn ? 1 : 0);
	}
}

Look Features::GetLook(const beth::Form &pActor) noexcept {
	auto ac = Get<Actor>(pActor);
	if (!ac) return {};
	return g_lookManager.GetFrom(ac);
}

beth::Form Features::ApplyLook(const Look &look, const beth::Form &pOptionalNpc) noexcept {
	try {
		auto optPreallocNpc = Get<TESNPC>(pOptionalNpc);
		auto npc = g_lookManager.Apply(look, optPreallocNpc, g_formIdGen);
		assert(npc);
		if (!npc) return {};
		assert(npc->formID == 0 || npc->formID == (*g_thePlayer)->baseForm->formID);
		auto id = g_fakeId.Insert(npc);
		return id;
	}
	catch (std::exception &e) {
		Console_Print("%s", e.what()); // TODO: logs
		return {};
	}
}

void Features::ApplyLookTints(const Look &look, const beth::Form &pActor) noexcept {
	if (auto ac = Get<Actor>(pActor)) {
		g_lookManager.ApplyTints(look, ac);
	}
}

void Features::ApplyLookInRuntime(const Look &look, const beth::Form &pActor) noexcept {
	if (auto ac = Get<Actor>(pActor)) {
		if (ac != *g_thePlayer) {
			g_lookManager.Apply(look, (TESNPC *)ac->baseForm, g_formIdGen);
			g_lookManager.ApplyTints(look, ac);
		}
		else {
			g_lookManager.ApplyToPlayer(look, g_formIdGen);
		}
	}
}

void Features::ApplyEquipment(const Equipment &eq, const beth::Form &pActorOrNpc) noexcept {
	if (auto actor = Get<Actor>(pActorOrNpc)) {
		assert(actor != *g_thePlayer); // Not implemented
		if (actor != *g_thePlayer) {
			g_eqManager.Apply(actor, eq);
		}
	}
	else if (auto npc = Get<TESNPC>(pActorOrNpc)) {
		npc->defaultOutfit = npc->sleepOutfit = g_eqManager.Apply(eq);
	}
}

bool Features::HasLOS(const beth::Form &pRef) noexcept {
	if (auto ref = Get<TESObjectREFR>(pRef)) {
		return sd::HasLOS(*g_thePlayer, ref);
	}
	return false;
}

void Features::ShowRaceMenu() noexcept {
	auto lookWas = GetLook(this->GetPlayer());

	static auto fsSpeedSampled = new BSFixedString("SpeedSampled");
	int i = 4;
	while (1) {
		float speed = 0.f;
		(*g_thePlayer)->animGraphHolder.GetVariableFloat(fsSpeedSampled, &speed);
		if (i <= 0 && speed <= 0.1f) {
			break;
		}
		--i;
		sd::DisablePlayerControls(true, true, true, true, true, true, true, true, 0);
		sd::Wait(0);
	}
	sd::ShowRaceMenu();
	assert(g_controls != nullptr);
	if (g_controls) {
		g_controls->RequestApply();
	}
	sd::Wait(1000);
	pImpl->raceMenuExit.reset(new RaceMenuExitEvent);
	pImpl->raceMenuExit->lookChanged = lookWas != GetLook(this->GetPlayer());
}

void Features::SetControlEnabled(beth::Control control, bool enabled, const char *systemFrom) noexcept {
	assert(g_controls != nullptr);
	if (g_controls) {
		g_controls->SetEnabled(control, enabled, systemFrom);
	}
}

beth::Form Features::GetDefaultNpc() noexcept {
	return LookupFormByID(ID_TESNPC::AADeleteWhenDoneTestJeremyRegular)->formID;
}

Point3D Features::GetNodeWorldPosition(const beth::Form &pActor, SkeletonNode node) noexcept {
	if (auto actor = Get<Actor>(pActor)) {
		const BSFixedString nodeName = GetSkeletonNodeName(node);
		const auto res = papyrusNetImmerse::GetNodeWorldPosition(0, actor, nodeName, false);
		return { res.x, res.y, res.z };
	}
	return { 0,0,0 };
}

beth::Form Features::GetProduce(const Form &pRefOrBase) noexcept {
	if (auto ref = Get<TESObjectREFR>(pRefOrBase)) {
		assert(ref->baseForm);
		if (ref->baseForm) {
			return this->GetProduce(ref->baseForm->formID);
		}
	}

	if (auto tree = Get<TESFlora>(pRefOrBase)) {
		if (!tree->produce.produce) {
			return beth::Form();
		}
		return beth::Form(tree->produce.produce->formID);
	}

	if (auto tree = Get<TESObjectTREE>(pRefOrBase)) {
		if (!tree->produce.produce) {
			return beth::Form();
		}
		return beth::Form(tree->produce.produce->formID);
	}

	return beth::Form();
}

void Features::SetSneaking(const Form &pActor, bool sneaking) noexcept {
	this->SendAnimEvent(pActor, sneaking ? "SneakStart" : "SneakStop");
}

bool Features::Load(uint32_t worldSpaceId, const Point3D &pos, float degreesRotZ) noexcept {
	std::lock_guard l(pImpl->share.m);
	try {
		GUID guid;
		char name[MAX_PATH] = { 0 };
		if (CoCreateGuid(&guid) != S_OK) {
			throw std::runtime_error("CoCreateGuid failed");
		}
		sprintf_s(name, "SKYMP-%08lX-%04hX-%04hX-%02hhX%02hhX-%02hhX%02hhX%02hhX%02hhX%02hhX%02hhX",
			guid.Data1, guid.Data2, guid.Data3,
			guid.Data4[0], guid.Data4[1], guid.Data4[2], guid.Data4[3],
			guid.Data4[4], guid.Data4[5], guid.Data4[6], guid.Data4[7]);

		fs::path path = GetPathToMyDocuments() + L"\\My Games\\Skyrim\\Saves\\" + StringToWstring(name) + L".ess";
		std::ofstream outFile(path, std::ios::out | std::ios::binary);

		ess::SaveData data;
		data.posX = pos.x;
		data.posY = pos.y;
		data.posZ = pos.z;
		data.worldSpaceId = worldSpaceId;
		data.degreesRotZ = degreesRotZ;
		const auto res = ess::Create(data);
		outFile.write((const char *)res->GetData(), res->GetDataLength());
		if (!outFile.is_open() || !outFile.good()) {
			throw std::runtime_error("unable to write into " + std::string(name));
		}
		pImpl->share.essName = name;
		pImpl->share.essGarbage.push_back(path);
		return true;
	}
	catch (std::exception &e) {
		Console_Print("CreateSave failed with exception:");
		Console_Print("%s", e.what());
		return false;
	}
}

Point3D Features::WorldPointToScreenPoint(const Point3D &point) noexcept {
	// Should we protect WorldPtToScreenPt3 from multithreaded access?
	static std::mutex m;
	std::lock_guard l(m);

	const NiPoint3 in(point.x, point.y, point.z);
	Point3D screenPoint;
	PlayerCamera::GetSingleton()->WorldPtToScreenPt3(in, screenPoint.x, screenPoint.y, screenPoint.z, 1);
	return screenPoint;
}

namespace {
	// https://www.creationkit.com/index.php?title=UI_Script
	BSFixedString *GetMenuName(beth::Menu menu) {
		auto uis = UIStringHolder::GetSingleton();
		static auto g_fsEmpty = new BSFixedString("");
		switch (menu) {
		case beth::Menu::Cursor:
			return &uis->cursorMenu;
		case beth::Menu::RaceSexMenu:
			return &uis->raceSexMenu;
		default:
			assert(0 && "Unknwon beth::Menu");
			return g_fsEmpty;
		}
	}
}

bool Features::IsMenuOpen(beth::Menu menu) const noexcept {
	std::lock_guard<std::mutex> l(g_uiM);
	const auto menuName = GetMenuName(menu);
	return MenuManager::GetSingleton()->IsMenuOpen(menuName);
}

void Features::SetMenuOpen(beth::Menu menu, bool open) const noexcept {
	std::lock_guard<std::mutex> l(g_uiM);
	const auto menuName = GetMenuName(menu);
	const auto ui = UIManager::GetSingleton();
	if (open) {
		ui->OpenMenu(menuName);
	}
	else {
		ui->CloseMenu(menuName);
	}
}

void Features::BeforeAnyTick() noexcept {
	std::lock_guard l(pImpl->share.m);
	try {
		if (pImpl->share.taskEssGarbageCollect) {
			pImpl->share.taskEssGarbageCollect = false;
			for (auto &p : pImpl->share.essGarbage) {
				if (!p.empty()) {
					std::filesystem::remove(p);
				}
				p = fs::path();
			}
		}
		pImpl->share.essGarbage.erase(
			std::remove(pImpl->share.essGarbage.begin(), pImpl->share.essGarbage.end(), fs::path()), pImpl->share.essGarbage.end()
		);
	}
	catch (std::exception &e) {
		Console_Print("BeforeTick failed with exception:");
		Console_Print("%s", e.what());
	}
}

// SKSE/GameData.cpp
extern uint32_t g_numGameLoads;

void Features::AfterAnyTick() {
	std::lock_guard l(pImpl->share.m);

	if (pImpl->share.essName.empty() == false) {
		papyrusGame::LoadGame(0, pImpl->share.essName.data());
		pImpl->share.essName.clear();
	}
}

void Features::ForceDeleteSaves() {
	std::lock_guard l(pImpl->share.m);
	pImpl->share.taskEssGarbageCollect = true;
}

void Features::SDMain() {
	g_controls.reset(new Controls);
	bool isFirstTick = true;
	while (1) {
		if (isFirstTick) {
			g_eqManager = EquipmentManager();
		}
		g_loadedRefsCache.clear();

		std::vector<std::string> anims;
		{
			std::lock_guard l(pImpl->share2.m);
			anims = std::move(pImpl->share2.animEvents);
			pImpl->share2.animEvents.clear();
		}

		this->BeforeAnyTick();
		for (auto &anim : anims) {
			pImpl->listener->OnPlayerAnimation(this, anim.data());
		}
		pImpl->listener->TickGame(this, isFirstTick);
		this->AfterAnyTick();

		assert(g_controls != nullptr);
		if (g_controls) {
			g_controls->Update();
		}

		if (pImpl->raceMenuExit) {
			pImpl->listener->OnRaceMenuExit(this, pImpl->raceMenuExit->lookChanged);
			pImpl->raceMenuExit.reset();
		}

		if (isFirstTick) {
			this->ForceDeleteSaves();
		}
		isFirstTick = false;
		sd::Wait(0);
	}
}

void Features::HelperThreadMain(void *featuresPtr) {
	const auto this_ = (Features *)featuresPtr;
	assert(this_);
	if (!this_) return;

	bool mainMenuSeen = false;
	bool isFirstTick = true;
	while (1) {
		Sleep(1);
		static auto fsMainMenu = new BSFixedString("Main Menu");
		if (MenuManager::GetSingleton()->IsMenuOpen(fsMainMenu)) {
			if (!mainMenuSeen) {
				mainMenuSeen = true;

				Hooks_SaveLoad_Commit();
				Hooks_Gameplay_Commit();
			}
		}
		if (!mainMenuSeen) continue;

		this_->BeforeAnyTick();
		this_->pImpl->listener->TickHelper(this_, isFirstTick);
		g_lookManager.TickHelper();
		this_->AfterAnyTick();
		isFirstTick = false;
	}
}

const char *Features::SendAnimEventHook(uint32_t id, const char *animEventName) {
	std::string lower = animEventName;
	std::transform(lower.begin(), lower.end(), lower.begin(), tolower);

	static auto doNothing = "";
	static std::set<std::string> badAnims = { "movestart", "movestop", "sprintstart", "sprintstop", "turnright", "turnleft", "turnstop", "cyclicfreeze", "cycliccrossblend" };
	if (badAnims.count(lower)) return animEventName;

	// Checks pointers, not values
	const bool isTriggeredBySkymp = g_animEvent && g_animEvent->data == animEventName;
	if (isTriggeredBySkymp) return animEventName;

	if (!strcmp(animEventName, "OffsetBoundStandingPlayerInstant")) {
		return doNothing;
	}

	if (id == 0x14) {
		std::lock_guard l(pImpl->share2.m);
		const bool isFalling = lower.find("jumpfall") != std::string::npos;
		if (pImpl->share2.lastAe != animEventName || !isFalling) {
			pImpl->share2.lastAe = animEventName;
			pImpl->share2.animEvents.push_back(animEventName);
		}
	}
	if (id > 0xff000000) {
		if (!memcmp(animEventName, "Idle", 4)) { // Idle*, IdleCombat*
			if (strcmp(animEventName, "IdleStop") != 0 && strcmp(animEventName, "IdleForceDefaultState") != 0) {
				return doNothing;
			}
		}
		if (!strcmp(animEventName, "MotionDrivenIdle")) {
			return doNothing;
		}
	}
	return animEventName;
}

bool Features::SetWeaponDrawnHook(uint32_t id, bool draw) {
	std::lock_guard l(pImpl->share3.m);
	return pImpl->share3.drawn.count(id) > 0;
}
