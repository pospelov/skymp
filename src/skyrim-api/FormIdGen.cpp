#include "headers.h"
#include "FormIdGen.h"

uint32_t FormIdGen::NewFormId() noexcept {
	std::lock_guard<std::mutex> l(this->mutex);
	do {
		this->id--;
	} while (LookupFormByID(this->id));
	return this->id;
}