#pragma once
#include <vector>
#include <string>
#include <memory>
#include <cstdint>
#include <stdexcept>

namespace ess {
	struct SaveData {
		float posX = 0, posY = 0, posZ = 0;
		float degreesRotZ = 0;
		uint32_t worldSpaceId = 0;
	};

	class IBinarySave {
	public:
		virtual const void *GetData() const noexcept = 0;
		virtual size_t GetDataLength() const noexcept = 0;
		virtual ~IBinarySave() = default;
	};

	class CreateError : public std::runtime_error {
	public:
		CreateError(std::string text) : runtime_error(text) {};
	};

	// Throws CreateError
	std::shared_ptr<IBinarySave> Create(const SaveData &saveData);
}