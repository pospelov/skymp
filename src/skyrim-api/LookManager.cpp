// Legacy from 2.0 (July 2018)

#include "headers.h"
#include <cassert>
#include <sstream>

#include "TintUtil.h"
#include "LookManager.h"
#include <skymp-common/Look.h>

static constexpr uint32_t g_playableRaceIdStart = 0x00013740;

void LookManager::TickHelper() noexcept {
	static bool g_started = false;

	if (g_started) {
		return;
	}
	g_started = true;
	std::thread([this] {
		while (1) {
			Sleep(1);

			static auto fsRaceSexMenu = new BSFixedString("RaceSex Menu");
			static auto fsConsole = new BSFixedString("Console");
			if (MenuManager::GetSingleton()->IsMenuOpen(fsRaceSexMenu)) {
				this->badTintsInPC = false;

				const auto allow = InputManager::GetSingleton()->allowTextInput;
				if (allow != 0) {
					if (MenuManager::GetSingleton()->IsMenuOpen(fsConsole) == false) {
						keybd_event(VK_LSHIFT, 0x11, NULL, NULL);
						keybd_event(VK_LSHIFT, 0x11, KEYEVENTF_KEYUP, NULL);
						keybd_event(VK_LSHIFT, 0x1c, NULL, NULL);
						keybd_event(VK_LSHIFT, 0x1c, KEYEVENTF_KEYUP, NULL);
					}
				}
			}
		}
	}).detach();
}

Look LookManager::GetFrom(Actor *ac) noexcept {
	std::lock_guard<std::recursive_mutex> l(this->m);

	auto npc = (TESNPC *)ac->baseForm;
	Look res;
	res.isFemale = sd::GetSex(npc) == 1;
	res.raceID = (Look::PlayableRace)(ac->race->formID - g_playableRaceIdStart);
	assert(res.raceID >= Look::PlayableRace::MIN && res.raceID <= Look::PlayableRace::MAX);
	res.weight = UInt8(npc->weight) / 10;
	memcpy(res.skinRGB.data(), &npc->color, 3);
	if (npc->headData && npc->headData->hairColor) {
		auto hairColor = npc->headData->hairColor->color;
		memcpy(res.hairRGB.data(), &hairColor, 3);
	}
	else {
		std::array<uint8_t, 3> defaultCol = { 57, 55, 40 };
		memcpy(res.hairRGB.data(), defaultCol.data(), 3);
	}
	if (npc->headData && npc->headData->headTexture)
		res.headTextureSet = npc->headData->headTexture->formID;
	if (npc->headparts) {
		for (auto it = npc->headparts; it != npc->headparts + npc->numHeadParts; ++it)
			res.headparts.push_back((*it)->formID);
	}
	if (npc->faceMorph) {
		std::copy(npc->faceMorph->option, npc->faceMorph->option + npc->faceMorph->kNumOptions,
			res.options.begin());
		std::copy(npc->faceMorph->presets, npc->faceMorph->presets + npc->faceMorph->kNumPresets,
			res.presets.begin());
	}

	// Tints:
	if (ac == *g_thePlayer) {
		if (this->badTintsInPC)
			res.tints = this->lastTints;
		else {
			for (size_t i = 0; i < (*g_thePlayer)->tintMasks.count; ++i) {
				auto &it = (*g_thePlayer)->tintMasks[i];
				Tint m;
				std::string texture_path = (it)->texture->str.data;
				std::transform(texture_path.begin(), texture_path.end(), texture_path.begin(), ::tolower);
				m.alpha = (it)->alpha;
				memcpy(&m.rgba, &(it)->color, 4);
				m.texture = sdutil::GetTintMaskTextureID(texture_path.data());
				m.type = (it)->tintType;
				res.tints.push_back(m);
			}
			this->lastTints = res.tints;
		}
	}

	return res;
}

TESNPC *LookManager::Apply(const Look &lo, TESNPC *_npc, FormIdGen &formIdGen) {
	std::lock_guard<std::recursive_mutex> l(this->m);

	auto npc = _npc;
	if (!npc) {
		npc = (TESNPC *)FormHeap_Allocate(sizeof TESNPC);
		if (!npc) {
			throw ApplyError("FormHeap_Allocate failed");
		}
		const auto srcNpc = (TESNPC *)LookupFormByID(ID_TESNPC::AADeleteWhenDoneTestJeremyRegular);
		assert(srcNpc);
		assert(srcNpc->formType == kFormType_NPC);
		if (!srcNpc || srcNpc->formType != kFormType_NPC) {
			throw ApplyError("bad srcNpc");
		}
		memcpy(npc, srcNpc, sizeof TESNPC);

		npc->actorData.voiceType = (BGSVoiceType *)LookupFormByID(0x0002F7C3);
		npc->formID = 0;

		// Doesn't work
		///const uint32_t id = formIdGen.NewFormId();
		///SetFormId(npc, id);

		npc->container.entries = nullptr;
		npc->container.numEntries = 0;
		npc->faction = nullptr;
		npc->nextTemplate = nullptr;
		npc->actorData.flags_.pcLevelMult = false;
		npc->actorData.flags_.unique = true;
		npc->actorData.flags_.simpleActor = true; // Disables face animations
		npc->spellList.unk04 = nullptr;

		// Clear AI Packages to prevent idle animations with Furniture
		auto doNothing = (TESPackage *)LookupFormByID(ID_TESPackage::DoNothing);
		auto flagsSource = (TESPackage *)LookupFormByID(ID_TESPackage::DefaultMoveToCustom02IgnoreCombat); // ignore combat && no combat alert
		doNothing->packageFlags = flagsSource->packageFlags;
		npc->aiForm.unk10.unk0 = (uint32_t)doNothing;
		npc->aiForm.unk10.next = nullptr;
	}
	assert(npc);
	if (!npc)
		throw ApplyError("npc was nullptr");
	npc->actorData.flags_.female = !!lo.isFemale;
	memcpy(&npc->color, lo.skinRGB.data(), 3);

	// Race
	const bool isPlayable = (int)lo.raceID >= (int)Look::PlayableRace::MIN && (int)lo.raceID <= (int)Look::PlayableRace::MAX;
	assert(isPlayable);
	if (!isPlayable) {
		throw ApplyError("unplayable race (raceId=" + std::to_string((int)lo.raceID) + ')');
	}
	auto race = LookupFormByID(g_playableRaceIdStart + (uint32_t)lo.raceID);
	assert(race);
	assert(race->formType == kFormType_Race);
	if (!race) 
		throw ApplyError("race was nullptr (raceId=" + std::to_string((int)lo.raceID) + ')');
	if (race->formType != kFormType_Race) 
		throw ApplyError("race->formType was " + std::to_string(race->formType));
	npc->race.race = (TESRace *)race;

	// Weight
	npc->weight = lo.weight * 10.f;

	// Headparts
	if (lo.headparts.size() > Look::g_maxHeadparts) {
		std::stringstream ss;
		ss << "too much headparts (" << lo.headparts.size() << ") ";
		ss << std::hex << "with raceId=" << (int)lo.raceID;
		throw ApplyError(ss.str());
	}
	if (lo.headparts.size() > 0) {
		npc->headparts = new BGSHeadPart*[lo.headparts.size()];
		npc->numHeadParts = (uint8_t)lo.headparts.size();
	}
	else {
		npc->headparts = nullptr;
		npc->numHeadParts = 0;
	}
	for (size_t i = 0; i != lo.headparts.size(); i++) {
		npc->headparts[i] = (BGSHeadPart *)LookupFormByID(lo.headparts[i]);
		assert(npc->headparts[i]);
		assert(npc->headparts[i]->formType == kFormType_HeadPart);
		if (npc->headparts[i] == nullptr) {
			std::stringstream ss;
			ss << "headpart " << std::hex << lo.headparts[i] << " not found";
			npc->numHeadParts = 0;
			npc->headparts = nullptr;
			throw ApplyError(ss.str());
		}
		if (npc->headparts[i]->formType != kFormType_HeadPart) {
			std::stringstream ss;
			ss << "headpart " << std::hex << lo.headparts[i] << " formType was" << npc->headparts[i]->formType;
			npc->numHeadParts = 0;
			npc->headparts = nullptr;
			throw ApplyError(ss.str());
		}
	}

	// Head data
	if (!npc->headData) npc->headData = (TESNPC::HeadData *)FormHeap_Allocate(sizeof TESNPC::HeadData);
	npc->headData->hairColor = nullptr;
	npc->headData->headTexture = nullptr;
	auto resultTextureSet = LookupFormByID(lo.headTextureSet);
	if (resultTextureSet) {
		assert(resultTextureSet->formType == kFormType_TextureSet);
		if (resultTextureSet->formType != kFormType_TextureSet) {
			std::stringstream ss;
			ss << "textureSet " << std::hex << resultTextureSet->formID << " formType was" << resultTextureSet->formType;
			throw ApplyError(ss.str());
		}
		npc->headData->headTexture = (BGSTextureSet *)resultTextureSet;
	}
	auto factory = ((IFormFactory **)0x012E57B0)[BGSColorForm::kTypeID];
	auto hair_color = (BGSColorForm *)factory->Create();
	memcpy(&hair_color->color, lo.hairRGB.data(), 3);
	hair_color->color.alpha = 255;
	npc->headData->headTexture = nullptr;
	npc->headData->hairColor = hair_color;

	// Face morph
	npc->faceMorph = new TESNPC::FaceMorphs;
	std::copy(lo.presets.begin(), lo.presets.end(), npc->faceMorph->presets);
	std::copy(lo.options.begin(), lo.options.end(), npc->faceMorph->option);

	return npc;
}

void LookManager::ApplyTints(const Look &lo_, Actor *optAc) noexcept {
	Look lo = lo_;
	std::lock_guard<std::recursive_mutex> l(this->m);

	if (optAc != nullptr) {
		optAc->baseForm->formID = (*g_thePlayer)->baseForm->formID;
	}

	if (lo.tints.empty()) {
		Tint skintone;
		skintone.alpha = 1.f;
		skintone.rgba = ~0;
		memcpy(&skintone.rgba, lo.skinRGB.data(), 3);
		skintone.texture = 189; // 189 is "skintone", see TintUtil.h
		skintone.type = TintMask::kMaskType_SkinTone;
		lo.tints.push_back(skintone);
	}
	(*g_thePlayer)->tintMasks.Allocate(lo.tints.size());

	for (size_t i = 0; i < min(lo.tints.size(), Look::g_maxTints); ++i) {
		auto _new = (TintMask *)FormHeap_Allocate(sizeof TintMask);
		auto &tint = lo.tints[i];
		memcpy(&_new->color, &tint.rgba, 4);
		_new->color.alpha = 0;
		_new->alpha = tint.alpha;
		_new->tintType = tint.type;

		auto texture = (TESTexture *)FormHeap_Allocate(sizeof TESTexture);
		texture->str = sdutil::GetTintMaskTexturePath(tint.texture);
		_new->texture = texture;
		(*g_thePlayer)->tintMasks[i] = _new;
	}
	this->badTintsInPC = true;

	if (optAc != nullptr) {
		const bool updateWeight = optAc == *g_thePlayer;
		CALL_MEMBER_FN(optAc, QueueNiNodeUpdate)(updateWeight);
	}
}

void LookManager::ApplyToPlayer(const Look &look, FormIdGen &formIdGen) {
	auto was = this->GetFrom(sd::GetPlayer());

	bool modelChanged = was.isFemale != look.isFemale || was.raceID != look.raceID || was.headparts != look.headparts;

	auto npc = (TESNPC *)(*g_thePlayer)->baseForm;
	if (modelChanged) sd::Wait(500);
	{
		std::lock_guard<std::recursive_mutex> l(this->m);
		this->Apply(look, npc, formIdGen);
		this->ApplyTints(look, (*g_thePlayer));
	}
	if (modelChanged) sd::Wait(500);
}