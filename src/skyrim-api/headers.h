#pragma once
#include <mutex>
#include <string>
#include <vector>
#include <set>
#include <unordered_set>
#include <fstream>
#include <filesystem>
#include <atomic>
#include <memory>
#include <cassert>
#include <sstream>
#include <mutex>

#include <Windows.h>
#include <process.h> // _beginthread
#include <objbase.h> // CoCreateGuid

#include <scriptdragon/enums.h>
#include <scriptdragon/invoke.h>
#include <scriptdragon/plugin.h>
#include <scriptdragon/skyscript.h>
#include <scriptdragon/obscript.h>
#include <scriptdragon/types.h>

#include <common/IPrefix.h>
#include <skse/PapyrusForm.h>
#include <skse/PapyrusGame.h>
#include <skse/PapyrusObjectReference.h>
#include <skse/PapyrusNetImmerse.h>
#include <skse/GameMenus.h>
#include <skse/Hooks_SaveLoad.h>
#include <skse/Hooks_Gameplay.h>
#include <skse/GameRTTI.h>
#include <skse/GameObjects.h>