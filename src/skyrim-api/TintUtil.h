// Legacy from 0.0.1+ (April 2017 or earlier)
// Worked in SkyMP 2.1

#pragma once
#include "headers.h"

namespace sdutil {

#define PREFIX "actors\\character\\character assets\\tintmasks\\"
#define POSTFIX ".dds"
	BSFixedString TintMaskTextures[] = {
		"null",
		PREFIX"argonianbrowridge" POSTFIX, PREFIX"argoniancheeks" POSTFIX, PREFIX"argoniancheeks01" POSTFIX, PREFIX"argoniancheeks02" POSTFIX, PREFIX"argoniancheeks03" POSTFIX,
		PREFIX"argoniancheekslower" POSTFIX, PREFIX"argonianchin" POSTFIX, PREFIX"argoniandirt" POSTFIX, PREFIX"argonianeyeliner" POSTFIX, PREFIX"argonianeyesocketlower" POSTFIX,
		PREFIX"argonianeyesockets" POSTFIX, PREFIX"argonianeyesocketupper" POSTFIX, PREFIX"argonianforehead" POSTFIX, PREFIX"argonianlaughline" POSTFIX, PREFIX"argonianlips" POSTFIX,
		PREFIX"argonianlips01" POSTFIX, PREFIX"argonianlips02" POSTFIX, PREFIX"argonianneck" POSTFIX, PREFIX"argoniannostrils01" POSTFIX, PREFIX"argonianstripes01" POSTFIX,
		PREFIX"argonianstripes02" POSTFIX, PREFIX"argonianstripes03" POSTFIX, PREFIX"argonianstripes04" POSTFIX, PREFIX"argonianstripes05" POSTFIX, PREFIX"argonianstripes06" POSTFIX,
		PREFIX"femalehead_cheeks" POSTFIX, PREFIX"femalehead_cheeks2" POSTFIX, PREFIX"femalehead_frownlines" POSTFIX, PREFIX"femaleheadblackbloodtattoo_01" POSTFIX,
		PREFIX"femaleheadblackbloodtattoo_02" POSTFIX, PREFIX"femaleheadbothiahtattoo_01" POSTFIX, PREFIX"femaleheadbreton_lips" POSTFIX, PREFIX"femaleheadbretonwarpaint_01" POSTFIX,
		PREFIX"femaleheadbretonwarpaint_02" POSTFIX, PREFIX"femaleheaddarkelfwarpaint_01" POSTFIX, PREFIX"femaleheaddarkelfwarpaint_02" POSTFIX, PREFIX"femaleheaddarkelfwarpaint_03" POSTFIX,
		PREFIX"femaleheaddarkelfwarpaint_04" POSTFIX, PREFIX"femaleheaddarkelfwarpaint_05" POSTFIX, PREFIX"femaleheaddarkelfwarpaint_06" POSTFIX, PREFIX"femaleheaddirt_01" POSTFIX,
		PREFIX"femaleheaddirt_02" POSTFIX, PREFIX"femaleheaddirt_03" POSTFIX, PREFIX"femaleheadforsworntattoo_01" POSTFIX, PREFIX"femaleheadforsworntattoo_02" POSTFIX,
		PREFIX"femaleheadforsworntattoo_03" POSTFIX, PREFIX"femaleheadforsworntattoo_04" POSTFIX, PREFIX"femaleheadhighelf_lips" POSTFIX, PREFIX"femaleheadhighelfwarpaint_01" POSTFIX,
		PREFIX"femaleheadhighelfwarpaint_02" POSTFIX, PREFIX"femaleheadhighelfwarpaint_03" POSTFIX, PREFIX"femaleheadhighelfwarpaint_04" POSTFIX, PREFIX"femaleheadhuman_chin" POSTFIX,
		PREFIX"femaleheadhuman_forehead" POSTFIX, PREFIX"femaleheadhuman_neck" POSTFIX, PREFIX"femaleheadhuman_nose" POSTFIX, PREFIX"femaleheadimperial_lips" POSTFIX,
		PREFIX"femaleheadimperialwarpaint_01" POSTFIX, PREFIX"femaleheadimperialwarpaint_02" POSTFIX, PREFIX"femaleheadimperialwarpaint_03" POSTFIX, PREFIX"femaleheadimperialwarpaint_04" POSTFIX,
		PREFIX"femaleheadimperialwarpaint_05" POSTFIX, PREFIX"femaleheadnord_lips" POSTFIX, PREFIX"femaleheadnordwarpaint_01" POSTFIX, PREFIX"femaleheadnordwarpaint_02" POSTFIX,
		PREFIX"femaleheadnordwarpaint_03" POSTFIX, PREFIX"femaleheadnordwarpaint_04" POSTFIX, PREFIX"femaleheadnordwarpaint_05" POSTFIX, PREFIX"femaleheadorcwarpaint_01" POSTFIX,
		PREFIX"femaleheadorcwarpaint_02" POSTFIX, PREFIX"femaleheadorcwarpaint_03" POSTFIX, PREFIX"femaleheadorcwarpaint_04" POSTFIX, PREFIX"femaleheadorcwarpaint_05" POSTFIX,
		PREFIX"femaleheadredguard_lips" POSTFIX, PREFIX"femaleheadredguardwarpaint_01" POSTFIX, PREFIX"femaleheadredguardwarpaint_02" POSTFIX, PREFIX"femaleheadredguardwarpaint_03" POSTFIX,
		PREFIX"femaleheadredguardwarpaint_04" POSTFIX, PREFIX"femaleheadredguardwarpaint_05" POSTFIX, PREFIX"femaleheadwarpaint_01" POSTFIX, PREFIX"femaleheadwarpaint_02" POSTFIX,
		PREFIX"femaleheadwarpaint_03" POSTFIX, PREFIX"femaleheadwarpaint_04" POSTFIX, PREFIX"femaleheadwarpaint_05" POSTFIX, PREFIX"femaleheadwarpaint_06" POSTFIX,
		PREFIX"femaleheadwarpaint_07" POSTFIX, PREFIX"femaleheadwarpaint_08" POSTFIX, PREFIX"femaleheadwarpaint_09" POSTFIX, PREFIX"femaleheadwarpaint_10" POSTFIX,
		PREFIX"femaleheadwoodelfwarpaint_01" POSTFIX, PREFIX"femaleheadwoodelfwarpaint_02" POSTFIX, PREFIX"femaleheadwoodelfwarpaint_03" POSTFIX, PREFIX"femaleheadwoodelfwarpaint_04" POSTFIX,
		PREFIX"femaleheadwoodelfwarpaint_05" POSTFIX, PREFIX"femalelowereyesocket" POSTFIX, PREFIX"femalenordeyelinerstyle_01" POSTFIX, PREFIX"femaleuppereyesocket" POSTFIX,
		PREFIX"khajiitcheekcolor" POSTFIX, PREFIX"khajiitcheekcolorlower" POSTFIX, PREFIX"khajiitchin" POSTFIX, PREFIX"khajiitdirt" POSTFIX, PREFIX"khajiiteyeliner" POSTFIX,
		PREFIX"khajiiteyesocket01" POSTFIX, PREFIX"khajiiteyesocket02" POSTFIX, PREFIX"khajiiteyesocketlower" POSTFIX, PREFIX"khajiiteyesocketupper" POSTFIX,
		PREFIX"khajiitforehead" POSTFIX, PREFIX"khajiitlaughlines" POSTFIX, PREFIX"khajiitlipcolor" POSTFIX, PREFIX"khajiitneck" POSTFIX, PREFIX"khajiitnose01" POSTFIX,
		PREFIX"khajiitpaint01" POSTFIX, PREFIX"khajiitpaint02" POSTFIX, PREFIX"khajiitpaint03" POSTFIX, PREFIX"khajiitpaint04" POSTFIX, PREFIX"khajiitstripes01" POSTFIX,
		PREFIX"khajiitstripes02" POSTFIX, PREFIX"khajiitstripes03" POSTFIX, PREFIX"khajiitstripes04" POSTFIX, PREFIX"malehead_cheeks" POSTFIX, PREFIX"malehead_cheeks2" POSTFIX,
		PREFIX"malehead_frekles_01" POSTFIX, PREFIX"malehead_frownlines" POSTFIX, PREFIX"malehead_nose" POSTFIX, PREFIX"maleheadblackbloodtattoo_01" POSTFIX,
		PREFIX"maleheadblackbloodtattoo_02" POSTFIX, PREFIX"maleheadbothiahtattoo_01" POSTFIX, PREFIX"maleheadbretonwarpaint_01" POSTFIX, PREFIX"maleheadbretonwarpaint_02" POSTFIX,
		PREFIX"maleheadbretonwarpaint_03" POSTFIX, PREFIX"maleheadbretonwarpaint_04" POSTFIX, PREFIX"maleheadbretonwarpaint_05" POSTFIX, PREFIX"maleheaddarkelfwarpaint_01" POSTFIX,
		PREFIX"maleheaddarkelfwarpaint_02" POSTFIX, PREFIX"maleheaddarkelfwarpaint_03" POSTFIX, PREFIX"maleheaddarkelfwarpaint_04" POSTFIX, PREFIX"maleheaddarkelfwarpaint_05" POSTFIX,
		PREFIX"maleheaddarkelfwarpaint_06" POSTFIX, PREFIX"maleheaddirt_01" POSTFIX, PREFIX"maleheaddirt_02" POSTFIX, PREFIX"maleheaddirt_03" POSTFIX, PREFIX"maleheadforsworntattoo_01" POSTFIX,
		PREFIX"maleheadforsworntattoo_02" POSTFIX, PREFIX"maleheadforsworntattoo_03" POSTFIX, PREFIX"maleheadforsworntattoo_04" POSTFIX, PREFIX"maleheadhuman_chin" POSTFIX,
		PREFIX"maleheadhuman_forehead" POSTFIX, PREFIX"maleheadhuman_neck" POSTFIX, PREFIX"maleheadimperialwarpaint_01" POSTFIX, PREFIX"maleheadimperialwarpaint_02" POSTFIX,
		PREFIX"maleheadimperialwarpaint_03" POSTFIX, PREFIX"maleheadimperialwarpaint_04" POSTFIX, PREFIX"maleheadimperialwarpaint_05" POSTFIX, PREFIX"maleheadnord_lips" POSTFIX,
		PREFIX"maleheadnordwarpaint_01" POSTFIX, PREFIX"maleheadnordwarpaint_02" POSTFIX, PREFIX"maleheadnordwarpaint_03" POSTFIX, PREFIX"maleheadnordwarpaint_04" POSTFIX,
		PREFIX"maleheadnordwarpaint_05" POSTFIX, PREFIX"maleheadorcwarpaint_01" POSTFIX, PREFIX"maleheadorcwarpaint_02" POSTFIX, PREFIX"maleheadorcwarpaint_03" POSTFIX,
		PREFIX"maleheadorcwarpaint_04" POSTFIX, PREFIX"maleheadorcwarpaint_05" POSTFIX, PREFIX"maleheadredguard_lips" POSTFIX, PREFIX"maleheadredguardwarpaint_01" POSTFIX,
		PREFIX"maleheadredguardwarpaint_02" POSTFIX, PREFIX"maleheadredguardwarpaint_03" POSTFIX, PREFIX"maleheadredguardwarpaint_04" POSTFIX, PREFIX"maleheadredguardwarpaint_05" POSTFIX,
		PREFIX"maleheadwarpaint_01" POSTFIX, PREFIX"maleheadwarpaint_02" POSTFIX, PREFIX"maleheadwarpaint_03" POSTFIX, PREFIX"maleheadwarpaint_04" POSTFIX, PREFIX"maleheadwarpaint_05" POSTFIX,
		PREFIX"maleheadwarpaint_06" POSTFIX, PREFIX"maleheadwarpaint_07" POSTFIX, PREFIX"maleheadwarpaint_08" POSTFIX, PREFIX"maleheadwarpaint_09" POSTFIX, PREFIX"maleheadwarpaint_10" POSTFIX,
		PREFIX"maleheadwoodelfwarpaint_01" POSTFIX, PREFIX"maleheadwoodelfwarpaint_02" POSTFIX, PREFIX"maleheadwoodelfwarpaint_03" POSTFIX, PREFIX"maleheadwoodelfwarpaint_04" POSTFIX,
		PREFIX"maleheadwoodelfwarpaint_05" POSTFIX, PREFIX"malelowereyesocket" POSTFIX, PREFIX"maleuppereyesocket" POSTFIX, PREFIX"redguardmaleeyelinerstyle_01" POSTFIX, PREFIX"skintone" POSTFIX,
	};
	enum {
		NUM_TINT_MASK_TEXTURES = 190
	};
	static_assert(sizeof TintMaskTextures == NUM_TINT_MASK_TEXTURES * sizeof size_t, "");

	UInt8 GetTintMaskTextureID(const char *path)
	{
		if (path)
			for (size_t i = 0; i != NUM_TINT_MASK_TEXTURES; ++i)
			{
				if (!strcmp(path, TintMaskTextures[i].data))
					return i;
			}
		return NULL;
	}

	BSFixedString &GetTintMaskTexturePath(UInt8 tintMaskID)
	{
		if (tintMaskID >= NUM_TINT_MASK_TEXTURES)
			tintMaskID = NULL;
		return TintMaskTextures[tintMaskID];
	}
}