#pragma once
#include <optional>
#include <atomic>
#include <skymp-common/Equipment.h>

class TESAmmo;
class Actor;
class TESNPC;
class BGSOutfit;

class EquipmentManager {
public:
	Equipment GetFrom(Actor *actor) noexcept;

	// Returns nullptr on bad allocation or empty equipment
	BGSOutfit *Apply(const Equipment &eq) noexcept;

	void Apply(Actor *ac, const Equipment &eq) noexcept;

private:
	TESAmmo *fakeIronArrow = nullptr;

	void Init();
};