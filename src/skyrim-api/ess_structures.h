// skymp3: Edited
#pragma once

#include <iostream>
#include <fstream>
#include <cstdint>
#include <cstdio>
#include <string>
#include <string.h>
#include <sstream>
#include <iomanip>
#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#include <Windows.h>
#include <tchar.h>
#else
#include <cmath>
#endif


typedef uint8_t byte;
typedef uint32_t vsval;	//Variable-sized value, the type of vsval depends on the first byte: the lowest 2 bits represents the type of the vsval:

struct refID_t { //The lower 22 bits represent the formID value itself, while the upper 2 bits are the type of formID.
    uint8_t byte0;  //The upper two bits represent the type of formID:
                    //0 = An index into the File.formIDArray. If the index value of 0 is given, the formID is 0x00000000, else, index into the array using value - 1.
                    //1 = Default (ie, came from Skyrim.esm)
                    //2 = Created (ie, plugin index of 0xFF)
                    //3 = ???
    uint8_t byte1;
    uint8_t byte2;
    };

struct refID6 {
    uint16_t type; //???
    uint32_t id; //array ID
    };

struct w32string_t{
	uint32_t length;
	uint8_t* string;
};

struct globalData_t {
    uint32_t type;
    uint32_t length;
    void* data;
    };

struct header_t {
    uint32_t version;
    uint32_t saveNumber;
    std::string playerName;
    uint32_t playerLevel;
    std::string playerLocation;
    std::string gameDate;
    std::string playerRaceEditorId;
    uint16_t playerSex; //0 - male, 1 - female
    float playerCurExp; //32 bit
    float playerLvlUpExp; //32 bit
    byte filetime[8];
    uint32_t shotWidth; //screenshot width in pixels
    uint32_t shotHeight;    //screenshot height in pixels
    };

struct pluginInfo_t {
    uint8_t pluginCount;
    std::string* plugins;	//plugins[pluginCount]
    };

struct unknown3Table_t {
    uint32_t count;
    std::string* unknown;	//unknown[count]
    };

struct fileLocationTable_t {
    uint32_t formIDArrayCountOffset;
    uint32_t unknownTable3Offset;
    uint32_t globalDataTable1Offset;
    uint32_t globalDataTable2Offset;
    uint32_t changeFormsOffset;
    uint32_t globalDataTable3Offset;
    uint32_t globalDataTable1Count;
    uint32_t globalDataTable2Count;
    uint32_t globalDataTable3Count;
    uint32_t changeFormCount;
    uint32_t unused[15];
    };

struct changeForm_t {
    refID_t formID;
    uint32_t changeFlags;
    uint8_t type;
    uint8_t version;  //Current as of Skyrim 1.9 is 74. Older values (57, 64, 73) are also valid
    uint32_t length1; //actual type depends on flags
    uint32_t length2; //actual type depends on flags. If this value is non-zero, data is compressed. This value then represents the uncompressed length.
    uint8_t* data;	//data[length1]
    };

struct saveFile_t {
    char magic[13];             //Constant: "TESV_SAVEGAME"
    uint32_t headerSize;
    header_t header;
    uint8_t* screenshotData;	//[3*header.shotWidth*header.showHeight] -> pixel data in RGB
    uint8_t formVersion;        //current as of Skyrim 1.9 is 74
    uint32_t pluginInfoSize;
    pluginInfo_t pluginInfo;
    fileLocationTable_t fileLocationTable;
    globalData_t* globalDataTable1; //Types 0 to 8
    globalData_t* globalDataTable2; //Types 100 to 114
    changeForm_t* changeForms;
    globalData_t* globalDataTable3; //Types 1000 to 1005
    uint32_t formIDArrayCount;
    //refID_t* formIDArray;
	uint32_t* formIDArray;
    uint32_t visitedWorldspaceArrayCount;
    //refID_t* visitedWorldspaceArray;
	uint32_t* visitedWorldspaceArray;
    uint32_t unknown3TableSize;
    unknown3Table_t unknown3Table;
    };

struct reference_t {
    uint32_t referenceID;
    uint16_t type;				//String table reference.
    };

struct variable_t {
    uint8_t type;
    //0 = Null(4) but not empty(4 bytes of zero)
    //1 = RefID(6)
    //2 = String(2)
    //3 = Integer(4)
    //4 = Float(4)
    //5 = Boolean(4)
    //11 = RefID Array(6, 2 bytes for type and 4 more bytes for array ID)
    //12 = String Array(4) = > Array ID
    //13 = Integer Array(4) = > Array ID
    //14 = Float Array(4) = > Array ID
    //15 = Boolean Array(4) = > Array ID
    void* data;		//depends on (uint8 type)
    };

struct miscStats {			//global data type=0
    struct miscStat_t {
        std::string name;
        uint8_t category;	//0 = General
        //1 = Quest
        //2 = Combat
        //3 = Magic
        //4 = Crafting
        //5 = Crime
        //6 = DLC Stats ? Showed up in Skyrim 1.7.7, but nothing shows up in the in - game menu.Four values observed : NumVampirePerks, NumWerewolfPerks(if Dawnguard is installed), SolstheimLocationsDiscovered and StalhrimItemsCrafted(if Dragonborn is installed).
        int32_t value;
        };

    uint32_t count;
    miscStat_t* stats;
    };

struct playerLocation_t {	//global data type=1
    uint32_t nextObjectId;	//Number of next savegame specific object id, i.e. FFxxxxxx.
    refID_t worldspace1;	//This form is usually 0x0 or a worldspace. coorX and coorY represent a cell in this worldspace.
    int32_t coorX;			//x-coordinate (cell coordinates) in worldSpace1.
    int32_t coorY;			//y-coordinate (cell coordinates) in worldSpace1
    refID_t worldspace2;	//This can be either a worldspace or an interior cell. If it's a worldspace, the player is located at the cell (coorX, coorY). posX/Y/Z is the player's position inside the cell.
    float posX;				//x-coordinate in worldSpace2
    float posY;				//y - coordinate in worldSpace2
    float posZ;				//u-coordinate in worldSpace2
    uint8_t unknown;		//vsval? It seems absent in 9th version
    };

struct tes_t {			//global data type=2
    struct unknown0_t {
        refID_t formID;
        uint16_t unknown;
        };

    vsval count1;
    unknown0_t* unknown1;
    uint32_t count2;
    refID_t* unknown2;//[count2*count2]
    vsval count3;
    refID_t* unknown3;
    };

struct globalVariables {		//global data type=3
    struct globalVariable_t {
        refID_t formID;
        float value;
        };

    uint32_t count;//vsval
    globalVariable_t* globals;
    };

struct createdObjects {		//global data type=4
    struct enchantment_t {
        struct magicEffect_t {
            struct enchInfo_t {
                float magnitude;
                uint32_t duration;
                uint32_t area;
                };

            refID_t effectID;
            enchInfo_t info;
            float price;			//Amount this enchantment adds to the base item's price.
            };

        refID_t refID;			//FormID of the enchantment. I've only seen created types, no default or array types.
        uint32_t timesUsed;		//Seems to represent the number of times the enchantment is used? However, sometimes using the same enchantment nets two different forms. Could just be a bug in Skyrim.
        vsval count;
        magicEffect_t* effects;
        };

    vsval weaponCount;
    enchantment_t* weaponEnchTable;	//List of all created enchantments that are/were applied to weapons.
    vsval armourCount;
    enchantment_t* armourEnchTable;	//List of all created enchantments that are/were applied to armour. Not sure which types of armour (Body/Gloves/Boots/Shield/etc) this encompasses.
    vsval potionCount;
    enchantment_t* potionTable;		//List of all created potions.
    vsval poisonCount;
    enchantment_t* poisonTable;		//List of all created poisons.
    };

struct effects {			//global data type=5
    struct effect_t {
        float strength;		//Value from 0 to 1 (0 is no effect, 1 is full effect)
        float timestamp;	//Time from effect beginning
        uint32_t unknown;	//May be flag. Appears when you аdd a crossfade imagespace modifier to the active list with imodcf command
        refID_t effectID;
        };

    vsval count;
    effect_t* imageSpaceModifiers;
    float unknown1;
    float unknown2;
    };

struct weather_t {			//global data type=6
    refID_t climate;
    refID_t weather;
    refID_t prevWeather;		//Only during weather transition. In other cases it equals zero.
    refID_t unknownWeather1;
    refID_t unknownWeather2;
    refID_t regnWeather;
    float curTime;				//Current in-game time in hours
    float begTime;				//Time of current weather beginning
    float weatherPct;			//A value from 0.0 to 1.0 describing how far in the current weather has transitioned
    uint32_t unknown1;
    uint32_t unknown2;
    uint32_t unknown3;
    uint32_t unknown4;
    uint32_t unknown5;
    uint32_t unknown6;
    float unknown7;
    uint32_t unknown8;
    uint8_t flags;
    void* unknown9;				//Unresearched format. Only present if flags has bit 0 set.
    void* unknown10;			//Unresearched format. Only present if flags has bit 1 set.
    };

struct audio_t {	//global data type=7
    refID_t unknown;		//Only the UIActivateFail sound descriptor has been observed here.
    vsval tracksCount;
    refID_t* tracks;		//Seems to contain music tracks (MUST records) that were playing at the time of saving, not including the background music.
    refID_t bgm;			//Background music at time of saving. Only MUST records have been observed here.
    };

struct skyCells {	//global data type=8
    struct unknown0_t {
        refID_t unknown1;
        refID_t unknown2;
        };

    vsval count;
    unknown0_t* unknown;
    };

struct processList_t {	//global data type=100
    struct crimeType {
        struct crime_t {
            uint32_t witnessNum;
            uint32_t crimeType;		//0 = Theft
            //1 = Pickpocketing
            //2 = Trespassing
            //3 = Assault
            //4 = Murder
            //5 =
            //6 = Lycanthropy

            uint8_t unknown1;
            uint32_t quantity;		//The number of stolen items (e.g. if you've stolen Gold(7), it would be equals to 7).
            //Only for thefts
            uint32_t serialNum;		//Assigned in accordance with nextNum
            uint8_t unknown2;
            uint32_t unknown3;		//May be date of crime? Little byte is equal to day
            float elapsedTime;		//Negative value measured from moment of crime
            refID_t victimID;		//The killed, forced door, stolen item etc.
            refID_t criminalID;
            refID_t itemBaseID;		//Only for thefts
            refID_t ownershipID;	//Faction, outfit etc. Only for thefts
            vsval count;
            refID_t* witnesses;
            uint32_t bounty;
            refID_t crimeFactionID;
            uint8_t isCleared;		//0 - active crime, 1 - it was atoned
            uint16_t unknown4;
            };

        vsval count;
        crime_t* crimes;
        };

    float unknown1;
    float unknown2;
    float unknown3;
    uint32_t nextNum;		//This value is assigned to the next process
    crimeType allCrimes[7];	//Crimes grouped according with their type (see below)
    };

struct positon_t {
    float x;
    float y;
    float z;
    refID_t cellID;
    };

struct combat {	//global data type=101
    struct unknown0_0_t {
        struct unknown0_0_0_t {
            refID_t unknown1;
            uint32_t unknown2;
            float unknown3;
            uint16_t unknown4;
            uint16_t unknown5;
            refID_t target;
            positon_t unknown6;
            positon_t unknown7;
            positon_t unknown8;
            positon_t unknown9;
            positon_t unknown10;
            float unknown11;
            float unknown12;
            float unknown13;
            float unknown14;
            float unknown15;
            float unknown16;
            };

        struct unknown0_0_1_t {
            refID_t unknown1;
            float unknown2;
            float unknown3;
            };

        struct unknown0_0_2_t {
            struct unknownStruct_t {
                float unknown1;
                float unknown2;
                };

            struct unknown0_0_2_0_t {
                positon_t unknown1;
                uint32_t unknown2;
                float unknown3;
                };

            struct unknown0_0_2_1_t {
                refID_t unknown1;
                refID_t unknown2;
                uint8_t unknown3;
                uint8_t unknown4;
                uint8_t unknown5;
                uint8_t unknown6;
                };

            struct unknown0_0_2_2_t {
                struct unknown0_0_2_2_0_t {
                    uint8_t unknown1;
                    uint32_t count0;
                    uint8_t* unknown2;			//[count0]
                    refID_t unknown3;
                    uint32_t unknown4;
                    };

                uint32_t unknown1;
                uint32_t unknown2;
                uint32_t count0;
                unknown0_0_2_2_0_t* unknown3;	//[count0]
                refID_t unknown4;
                float unknown5;
                float unknown6;
                float unknown7;
                float unknown8;
                float unknown9;
                refID_t unknown10;
                float unknown11;
                refID_t unknown12;
                uint16_t unknown13;
                uint8_t unknown14;
                uint8_t unknown15;
                float unknown16;
                float unknown17;
                };

            refID_t unknown1;
            unknownStruct_t unknown2;
            unknownStruct_t unknown3;
            float unknown4;
            positon_t unknown5;
            float unknown6;
            vsval count0;
            unknown0_0_2_0_t* unknown7;	//[count0]
            vsval count1;
            unknown0_0_2_1_t* unknown8;	//[count1]
            uint8_t unknownFlag;
            unknown0_0_2_2_t unknown9;	//only present if unknownFlag != 0
            };

        vsval count0;
        unknown0_0_0_t* unknown0;
        vsval count1;
        unknown0_0_1_t* unknown1;
        unknown0_0_2_t::unknownStruct_t unknown2;
        unknown0_0_2_t::unknownStruct_t unknown3;
        unknown0_0_2_t::unknownStruct_t unknown4;
        unknown0_0_2_t::unknownStruct_t unknown5;
        unknown0_0_2_t::unknownStruct_t unknown6[11];
        uint32_t unknownFlag;
        unknown0_0_2_t unknown7; //only present if unknownFlag != 0
        unknown0_0_2_t::unknownStruct_t unknown8;
        float unknown9;
        float unknown10;
        float unknown11;
        float unknown12;
        unknown0_0_2_t::unknownStruct_t unknown13;
        uint8_t unknown14;
        };

    struct unknown0_t {
        uint32_t unknown1;
        uint32_t serialNum;				//Assigned in accordance with nextNum
        unknown0_0_t unknown2;
        };

    struct unknown1_t {
        refID_t unknown1;
        float unknown2;
		refID_t unknown3;
		refID_t unknown4;
		float unknown5;
		float unknown6;
		float unknown7;
		float x;
		float y;
		float z;
		float unknown8;
		float unknown9;
		float unknown10;
		float unknown11;
		float unknown12;
		float unknown13;
		float unknown14;
		float unknown15;
		refID_t unknown16;
        };

    uint32_t nextNum;				//This value is assigned to the next combat
    vsval count0;
    unknown0_t* unknown0;	//[count0]
    vsval count1;
    unknown1_t* unknown1;
    float unknown2;
    vsval unknown3;
    vsval count2;
    refID_t* unknown4;				//[count2]
    float unknown5;
    unknown0_0_t::unknown0_0_2_t::unknownStruct_t unknown6;
    unknown0_0_t::unknown0_0_2_t::unknownStruct_t unknown7;
    };

struct animObjects_t {			//global data type=1002
    struct animObject_t {
        refID_t achr;			//RefID pointing to an actor reference.
        refID_t anim;			//RefID pointing to an animation form.
        uint8_t unknown;		//Unknown but only 0 and 1 have been observed.
        };

    uint32_t count;
    animObject_t* objects;	//Array with currently active actor reference + animation combo?
    //Haven't yet determined when these are saved.
    };

struct timer_t {				//global data type=1003
    uint32_t unknown1;
    uint32_t unknown2;
    };

struct papyrus_t {	//global data type=1001
    struct activeScript_t {
        uint32_t scriptID;	//ID is Papyrus runtime
        uint8_t scriptType; //8 or 16???
        };

    struct script_t {
        struct memberData_t {
            uint16_t memberName;	//string table reference
            uint16_t memberType;	//string table reference
            };

        uint16_t scriptName;	//string table reference
        uint16_t type;			//String table reference
        uint32_t memberCount;
        memberData_t* memberData;
        };

    struct scriptInstance_t {
        uint32_t scriptID;
        uint16_t scriptName;		//String table reference
        uint16_t unknown2bits;		//Only the first two bits are used.
        int16_t unknown;			//used value is unknown2bits*1000+unknown
        refID_t refID;				//Ignored if preceding unknown is -1.
        uint8_t unknown2;
        };

    struct arrayInfo_t {
        uint32_t arrayID;	//Reference by ScriptData members
        uint8_t type;
        //1 = RefID
        //2 = String
        //3 = Integer
        //4 = Float
        //5 = Boolean
        uint16_t refType;	//Only appear when type == 1 otherwise this variable is not present, String table reference
        uint32_t length;	//Number of elements in the array.
        };

    struct scriptData_t {
        uint32_t scriptID;		//Link with script instance ID
        uint8_t flag;			//not sure if flag
        uint16_t type;			//String Table Reference
        uint32_t unknown1;
        uint32_t unknown2;		//Appears only if (flag and 0x04) == 0x04
        uint32_t memberCount;	//Number of stored variables
        variable_t* member;
        };

    struct referenceData_t {
        uint32_t referenceID;		//Link with reference ID
        uint8_t flag;				//not sure if flag
        uint16_t type;				//String Table Reference
        uint32_t unknown1;
        uint32_t unknown2;			//Appears only if (flag and 0x04) == 0x04
        uint32_t memberCount;		//Number of stored variables
        variable_t* member;
        };

    struct arrayData_t {
        uint32_t arrayID;		//Reference by ScriptData members
        variable_t* data;		//ArrayLength from ArrayInfo with the same arrayID.
        };

    struct activeScriptData_t {
        struct unknown4_1 {
            uint32_t count;
            char* str;
            void* data;
            };
        struct unknown4_3 {
            uint32_t count;
            char* str;
            void* data;
            variable_t unknown;
            };
        struct questStage {
            refID_t refID;
            uint16_t string;
            uint8_t unknown;
            };
        struct scenePhaseResults { //same as sceneActionResults
            refID_t refID;
            uint32_t unknown;
            };
        struct sceneResults {
            refID_t refID;
            };
        struct stackframe_t {
            struct functionParam_t {
                uint16_t functionParamName;//String table reference
                uint16_t functionParamType;//String table reference
                };

            struct functionLocal_t {
                uint16_t functionLocalName;//String table reference
                uint16_t functionTypeName;//String table reference
                };

            struct opcodeData_t {
                struct parameter_t {
                    uint8_t type;	//1 and 2 = uint16 string table referece
                    //3 = uint32
                    //4 = float
                    //5 = uint8
                    void* data;
                    };

                uint8_t opcode;
                //opcode: type:           paremeters:
                //0x00  NOOP
                //0x01  Iadd                SII
                //0x02  Fadd                SFF
                //0x03  Isubtract           SII
                //0x04  Fsubtract           SFF
                //0x05  IMultiply           SII
                //0x06  FMultiply           SFF
                //0x07  IDivide             SII
                //0x08  FDivide             SFF
                //0x09  Imod                SII
                //0x0a  Not                 SA
                //0x0b  Inegate             SI
                //0x0c  Fnegate             SF
                //0x0d  Assign              SA
                //0x0e  Cast                SA
                //0x0f  CompareEQ           SAA
                //0x10  CompareLT           SAA
                //0x11  CompareLTE          SAA
                //0x12  CompareGT           SAA
                //0x13  CompareLTE          SAA
                //0x14  Jump                L
                //0x15  JumpT               AL
                //0x16  JumpF               AL
                //0x17  CallMethod          NSS *
                //0x18  CallParent          NS *
                //0x19  CallStatic          NNS *
                //0x1a  Return              A
                //0x1b  StrCat              SQQ
                //0x1c  PropGet             NSS
                //0x1d  PropSet             NSA
                //0x1e  ArrayCreate         Su
                //0x1f  ArrayLength         SS
                //0x20  ArrayGetElement     SSI
                //0x21  ArraySetElement     SIA
                //0x22  ArrayFindelement    SSAI
                //0x23  ArrayRFindElement   SSAI
                //0x24  INVALID

                //I = integer
                //F = float
                //S = string
                //A = auto ?
                //*= uint32 extra parameters count
                parameter_t* parameter;//count depends on opcode type
				parameter_t* extra_parameter;
                };

            uint32_t variablecount;
            uint8_t flag;				//??
            uint8_t functionType;//String table reference
            uint16_t scriptName;//String table reference
            uint16_t scriptBaseName;//string table reference
            uint16_t event;//String table reference
            uint16_t status;//String table reference. Appears only if (flag and 0x01) == 0x00, and function type is 0x00
            uint8_t opcodeVersion;
            uint8_t opcodeMinorVersion;
            uint16_t returnType;//String table reference
            uint16_t functionDocstring;//String table reference
            uint32_t functionUserflags;
            uint8_t functionFlags;
            uint16_t functionParameterCount;
            functionParam_t* functionParam;
            uint16_t functionLocalsCount;
            functionLocal_t* functionLocal;
            uint16_t opcodeCount;
            opcodeData_t* opcodeData;
            uint32_t unknown3;
            variable_t unknown4;
            variable_t* unknown5;
            };

        uint32_t scriptID;		//Link with active script ID
        uint8_t majorVersion;	//has to be 3
        uint8_t minorVersion;	//1 (Dawnguard, Hearthfire and Dragonborn scripts are 2)
        variable_t unknown;
        uint8_t flag;			//??
        uint8_t unknownByte;
        uint32_t unknown2;		//Appears only if (flag and 0x01) == 0x01
        uint8_t unknown3;
        void* unknown4;			//type depends on value of unknown3
        //Appears only if unknown3 is between 1 - 3
        //*If string matches one of these, here is more things :
        //String:			  Data :
        //TopicInfo           none
        //QuestStage          3 bytes refId, uint16 string ref, uint8 unknown
        //ScenePhaseResults   3 bytes refId, uint32 unknown
        //SceneActionResults  3 bytes refId, uint32 unknown
        //SceneResults        3 bytes refId
        uint32_t stackframecount;
        stackframe_t* stackframe;
        uint8_t unknown5;		//Appears only if Stackframecount is not 0
        };

    struct functionMessage_t {
        struct functionMessageData_t {
            uint8_t unknown0;
            uint16_t scriptName; //string table reference
            uint16_t event; //string table reference
            variable_t unknown1;
            uint32_t variablecount;
            variable_t* unknown2;   //variable_t unknown2[variablecount]
            };

        uint8_t unknown;        //if value >2, no id, flag and message are present
        uint32_t id;            //link with activeScriptID
        uint8_t flag;
        functionMessageData_t message;  //appears only if flag > 0
        };

    struct suspendedStack_t {
        uint32_t id;    //link with activeScriptID
        uint8_t flag;
        functionMessage_t::functionMessageData_t message;   //appears only if flag > 0
        };

    struct queudUnbind_t {
        uint32_t id;    //link with scriptInstanceID
        uint32_t unknown;
        };

	struct array1_t{
		uint32_t unknown0;
		uint32_t unknown1;
	};

	struct array1a_t {
		uint32_t unknown0;
		uint32_t unknown1;
	};

	struct array2_t{
		uint32_t unknown0;
		uint32_t unknown1;
	};

	struct array3_t{
		uint8_t type;
		uint16_t string0;
		uint16_t unknown0;
		uint16_t string1;
		uint32_t unknown1;
		uint16_t flags;
		refID_t reference;
	};

	struct array4_t{
		uint16_t string0;
		uint16_t unknown0;
		uint8_t unknown1;
		uint16_t string1;
		uint32_t unknown2;
		uint16_t flags;
		refID_t reference;
	};

	struct array4b_t{
		uint8_t unknown0;
		uint16_t unknown1;
		uint16_t unknown2;
		refID_t reference0;
		refID_t reference1;
		refID_t reference2;
		refID_t reference3;
	};

	struct array4c_t{
		uint8_t flag;
		uint32_t unknown0;
		refID_t reference0;
		uint32_t unknown1[3];			//exists if flag is 0 or 6
		uint32_t unknown2[4];			//exists if flag == 0
		uint8_t unknown3;				//exists if flag is 0 or 4
	};

	struct array4d_t{
		uint8_t flag0;
		uint32_t unknown;
		uint8_t flag1;
		refID_t reference;
	};

	struct array5_t{
		uint16_t unknown0;
		uint16_t unknown1;
		refID_t reference0;
		refID_t reference1;
		refID_t reference2;
		uint16_t unknown2;
	};

	struct array6_t	{
		uint16_t unknown;
		uint16_t flags;
		refID_t reference;
	};

	struct array7_t	{
		uint16_t unknown;
		uint16_t flags;
		refID_t reference;
	};

	struct array8_t {
		uint16_t unknown;
		uint16_t type;
		refID_t reference;
		uint32_t count0;
		uint32_t count1;
		refID_t* reference0;
		refID_t* reference1;
	};

	struct array10_t{
		struct array10element_t{
			struct array10element1_t{
				struct array10element1data_t{
					uint16_t string;	//string table reference
					uint16_t flag;
				};
				uint32_t count;
				array10element1data_t* elements;
			};
			struct array10element2_t{
				uint16_t unknown;
				uint16_t flag;
				refID_t reference;
			};
			w32string_t name;
			uint32_t count0;
			array10element1_t* elements0;
			uint32_t count1;
			array10element2_t* elements1;
		};

		refID_t reference;
		uint32_t count;
		array10element_t* elements;
	};

	struct array11_t{
		uint32_t unknown0;
		refID_t reference;
		uint32_t unknown1;
	};

	struct array12_t{
		struct element_t {
			uint32_t unknown0;
			uint8_t unknown1;
			uint16_t unknown2;
			uint16_t unknown3;
		};
		refID_t reference;
		uint32_t count;
		element_t* elements;
	};

	struct array13_t{
		struct element_t {
			struct data_t {
				uint8_t data[5];
			};
			uint32_t unknown0;
			uint32_t count0;
			uint32_t* data0;
			uint32_t count1;
			data_t* data1;
			uint32_t unknown1;
		};
		refID_t reference;
		uint32_t count;
		element_t* elements;
	};

	struct array14_t {
		struct data_t {
			uint8_t data[5];
		};
		refID_t reference;
		uint32_t count;
		data_t* elements;
		uint32_t data;
	};

	struct array15_t{
		uint32_t data;
		uint32_t count;
		uint32_t* elements;
	};

    uint16_t header;						//-1 for invalid save or Skyrim VM version
    uint16_t strCount;
	std::string* strings;
    uint32_t scriptCount;
    script_t* script;
    uint32_t scriptInstanceCount;
    scriptInstance_t* scriptInstance;
    uint32_t referenceCount;
    reference_t* reference;
    uint32_t arrayInfoCount;
    arrayInfo_t* arrayInfo;					//Array type and length
    uint32_t papyrusRuntime;				//ID for next active script
    uint32_t activeScriptCount;
    activeScript_t* activeScript;
    scriptData_t* scriptData;
    referenceData_t* referenceData;
    arrayData_t* arrayData;
    activeScriptData_t* activeScriptData;
    uint32_t functionMessageCount;
    functionMessage_t* functionMessages;
    uint32_t suspendedStackCount1;
    suspendedStack_t* suspendedStacks1;
    uint32_t suspendedStackCount2;
    suspendedStack_t* suspendedStacks2;
    uint32_t unknown1;
    uint32_t unknown2;						//Present only since Skyrim VM version 2 and if the previous field is not null
    uint32_t unknown0Count;
    uint32_t* unknowns0;
    uint32_t queuedUnbindCount;				//Present only since Skyrim VM version 4.
    queudUnbind_t* queuedUnbinds;			//Present only since Skyrim VM version 4. This is the last field of the first part of the Papyrus data.To be updated
    int16_t saveFileVersion;				//-1 marks an invalid save
	uint32_t arrayCount1;
	array1_t* array1;
	uint32_t arrayCount1a;
	array1a_t* array1a;
	uint32_t arrayCount2;
	array2_t* array2;
	uint32_t arrayCount3;
	array3_t* array3;
	uint32_t arrayCount4;
	array4_t* array4;
	uint32_t scriptListCount;
	w32string_t* scriptList;
	uint32_t arrayCount4a;
	uint32_t arrayCount4b;
	array4b_t* array4b;
	uint32_t arrayCount4c;
	array4c_t* array4c;
	uint32_t arrayCount4d;
	array4d_t* array4d;
	uint32_t arrayCount5;
	array5_t* array5;
	uint32_t arrayCount6;
	array6_t* array6;
	uint32_t arrayCount7;
	array7_t* array7;
	uint32_t arrayCount8;
	array8_t* array8;
	uint32_t arrayCount9;
	uint32_t* array9;
	uint32_t arrayCount10;
	array10_t* array10;
	uint32_t arrayCount11;
	array11_t* array11;
	uint32_t arrayCount12;
	array12_t* array12;
	uint32_t arrayCount13;
	array13_t* array13;
	uint32_t arrayCount14;
	array14_t* array14;
	uint32_t arrayCount15;
	array15_t* array15;
    };

struct interface_t {		//global data type=102
    struct unknown0_t {
        struct unknown0_0_t {
            std::string unknown1;
            std::string unknown2;
            uint32_t unknown3;
            uint32_t unknown4;
            uint32_t unknown5;
            uint32_t unknown6;
            };

        vsval count1;
        unknown0_0_t* unknown1;
        vsval count2;
        std::string* unknown2;
        uint32_t unknown3;
        };

    uint32_t shownHelpMsgCount;
    uint32_t* shownHelpMsg;		/*0xEC - HelpLockpickingShort
    0xEE - helpSmithingShort
    0xEF - HelpCookingPots
    0xF0 - HelpSmeltingShort
    0xF1 - HelpTanningShort
    0xF3 - HelpEnchantingShort
    0xF4 - HelpGrindstoneShort
    0xF5 - HelpArmorBenchShort
    0xF6 - HelpAlchemyShort
    0xF7 - HelpBarterShortPC
    0xF9 - HelpLevelingShort
    0xFA - HelpWorldMapShortPC
    0xFB - HelpJournalShortPC
    0xFF - HelpJailTutorial
    0x100 - HelpFollowerCommandTutorial
    0x102 - HelpFavoritesPCShort
    etc. (???)*/
    uint8_t unknown1;
    vsval lastUsedWeaponsCount;
    refID_t* lastUsedWeapons;
    vsval lastUsedSpellsCount;
    refID_t* lastUsedSpells;
    vsval lastUsedShoutsCount;
    refID_t* lastUsedShouts;
    uint8_t unknown2;
    unknown0_t unknown3;//This value is only present in certain situations. Undetermined when.
    };

struct actorCauses_t {		//global data type=103
    struct unknown0_t {
        float x;
        float y;
        float z;
        uint32_t serialNum;
        refID_t actorID;
        };

    uint32_t nextNum;
    vsval count;
    unknown0_t* unknown;
    };

struct detectionManager_t {	//global data type=105
    struct unknown_t {
        refID_t unknown1;
        uint32_t unknown2;
        uint32_t unknown3;
        };

    vsval count;
    unknown_t* unknown;
    };

struct locationMetaData_t {		//global data type=106
    struct unknown_t {
        refID_t unknown1;
        uint32_t unknown2;
        };

    vsval count;
    unknown_t* unknown;
    };

struct questStaticData_t {	//global data type=107
    struct unknown0_t {
		struct unknown0_1_t{
			uint32_t unknown0;
			uint32_t unknown1;
		};
        refID_t unknown0;
		vsval count;
		unknown0_1_t* unknown1;
        };
	struct QuestRunData_3{
		struct QuestRunData_3_item{
			uint32_t type;
			void* unknown; //depends on previous type - 1,2,4 -> refID; 3 -> uint32
		};
		uint32_t unknown0;
		float unknown1;
		uint32_t count;
		QuestRunData_3_item* questRunData_item;
	};
    uint32_t count0;
    QuestRunData_3* unknown0;
    uint32_t count1;
    QuestRunData_3* unknown1;
    uint32_t count2;
    refID_t* unknown2;
    uint32_t count3;
    refID_t* unknown3;
    uint32_t count4;
    refID_t* unknown4;
    vsval count5;
    unknown0_t* unknown5;
    uint8_t unknown6; //always seems to be 1
    };

struct storyTeller_t {		//global data type=108
    uint8_t flag; //0 or 1
    };

struct magicFavorites_t {	//global data type=109
    vsval count0;
    refID_t* favoritedMagics;	//Spells, shouts, abilities etc.
    vsval count1;
    refID_t* magicHotKeys;		//Hotkey corresponds to the position of magic in this array
    };

struct playerControls_t {	//global data type=110
    uint8_t unknown1;
    uint8_t unknown2;
    uint8_t unknown3;
    uint16_t unknown4;
    uint8_t unknown5;
    };

struct storyEventManager_t {	//global data type=111
    uint32_t unknown0;
    vsval count;
    //void* unknown1; //Unknown format. Possibly the same as unk0 and unk1 in Quest Static Data
	byte* data;
    };

struct ingredientShared_t {	//global data type=112
    struct ingredientsCombined_t {
        refID_t ingredient0;
        refID_t ingredient1;
        };

    uint32_t count;
    ingredientsCombined_t* ingredientsCombined;	//Pairs of failed ingredient combinations in alchemy.
    };

struct menuControls_t {	//global data type=113
    uint8_t unknown1;
    uint8_t unknown2;
    };

struct menuTopicManager {	//global data type=114
    refID_t unknown1;
    refID_t unknown2;
    };

struct tempEffects_t {	//global data type=1000
    struct unknown0_t {
        uint8_t flag;
        refID_t unknown1;
        uint32_t unknown2;//Only present if flag is non zero
        refID_t unknown3;
        refID_t unknown4;
        float unknown5[3];
        float unknown6[3];
        float unknown7;
        float unknown8;
        float unknown9;
        float unknown10[4];
        uint8_t unknown11;
        uint8_t unknown12;
        uint8_t unknown13;
        uint8_t unknown14;
        float unknown15;
        uint8_t unknown16;
        float unknown17;
        float unknown18;
        float unknown19;
        float unknown20;
        float unknown21;
        uint8_t unknown22;
        uint8_t unknown23;
        uint32_t unknown24;
        };

    struct unknown1_0_t {
        struct unknown1_def_t {
            float unknown1;
            float unknown2;
            uint8_t unknown3;
            uint32_t unknown4;
            };

        unknown1_def_t unknown1;
        uint32_t unknown2[4];
        uint32_t unknown3[3];
        uint8_t unknown4[12];
        std::string unknown5;
        refID_t unknown6;
        refID_t unknown7;
        uint32_t unknown8;
        };

    struct unknown1_6_t {
        unknown1_0_t::unknown1_def_t unknown1;
        refID_t unknown2;
        uint32_t unknown3;
        uint32_t unknown4;
        uint8_t unknown5;
        };

    struct unknown1_8_t {
        unknown1_0_t::unknown1_def_t unknown1;
        uint8_t unknown2;
        refID_t unknown3;
        uint8_t flag;
        refID_t unknown4[4];//only present if flag is non-zero
        };

    struct unknown1_9_0_0_t {
        struct unknown0_t {//{wstring, uint32, wstring, uint32, uint32}[count0]
            std::string u0;
            uint32_t u1;
            std::string u2;
            uint32_t u3;
            uint32_t u4;
            };

        struct unknown1_t { //{wstring, uint32}[count1]
            std::string u0;
            uint32_t u1;
            };

        struct unknown2_t { //{wstring, float, {wstring, uint8, uint32}[2]}[count2]
            struct u2_t {
                std::string u0;
                uint8_t u1;
                uint32_t u2;
                };

            std::string u0;
            float u1;
            u2_t u2[2];
            };

        struct unknown3_t { //{wstring, uint8}[count3]
            std::string u0;
            uint8_t u1;
            };

        struct unknown4_t { //{ wstring, wstring, uint32, uint32, uint16, uin16, uint16, uint8, uint8 }[count4]
            std::string u0;
            std::string u1;
            uint32_t u2;
            uint32_t u3;
            uint16_t u4;
            uint16_t u5;
            uint16_t u6;
            uint8_t u7;
            uint8_t u8;
            };

        struct unknown5_t { //{wstring, uint32}[count5]
            std::string u0;
            uint32_t u1;
            };

        struct unknown6_t { //{wstring, uint32}[count6]
            std::string u0;
            uint32_t u1;
            };

        struct unknown7_t { //{wstring, uint32 [count1], uint32 [count2], uint16[count1], uint16[count2]}[count7]
            std::string u0;
            uint32_t* u1;//[count1]
            uint32_t* u2;//[count2]
            uint16_t* u3;//[count1]
            uint16_t* u4;//[count2]
            };

        struct unknown8_t { //{wstring, uint8}[count8]
            std::string u0;
            uint8_t u1;
            };

        struct unknown9_t { //{wstring, uint32[4], uint32[4], uint32[4], uint32, uint32}[count9]
            std::string u0;
            uint32_t u1[4];
            uint32_t u2[4];
            uint32_t u3[4];
            uint32_t u4;
            uint32_t u5;
            };

        struct unknown10_t { //{wstring, 3 x uint32[4], 3 x uint32[4], 3 x uint32[4], uint32, uint8}[count10] ????
            std::string u0;
            uint32_t u1[4];
            uint32_t u2[4];
            uint32_t u3[4];
            uint32_t u4[4];
            uint32_t u5[4];
            uint32_t u6[4];
            uint32_t u7[4];
            uint32_t u8[4];
            uint32_t u9[4];
            uint32_t u10;
            uint8_t u11;
            };

        struct unknown11_t { //{wstring, wstring}[count11]
            std::string u0;
            std::string u1;
            };

        struct unknown12_t { //{uint16, uint32, uint32, uint32, uint8, uint32}[count12]
            uint16_t u0;
            uint32_t u1;
            uint32_t u2;
            uint32_t u3;
            uint8_t u4;
            uint32_t u5;
            };

        std::string unknownStr0;
        uint32_t count0;
        unknown0_t* unknown0;
        uint32_t count1;
        unknown1_t* unknown1;
        uint32_t count2;
        unknown2_t* unknown2;
        std::string unknownStr1;
        uint32_t count3;
        unknown3_t* unknown3;
        uint32_t count4;
        unknown4_t* unknown4;
        uint32_t count5;
        unknown5_t* unknown5;
        uint32_t count6;
        unknown6_t* unknown6;
        uint32_t count7;
        unknown7_t* unknown7;
        uint32_t count8;
        unknown8_t* unknown8;
        uint32_t count9;
        unknown9_t* unknown9;
        uint32_t count10;
        unknown10_t* unknown10;
        uint32_t count11;
        unknown11_t* unknown11;
        uint32_t count12;
        unknown12_t* unknown12;
        };

    struct unknown1_9_0_t {
        vsval length;//Length of the next data
        uint8_t unknown;
        uint32_t count0;
        unknown1_9_0_0_t* unknown0;
        };

    struct tempEffects_unknown1_9_t {
        unknown1_8_t unknown1;
        refID_t unknown2;
        unknown1_9_0_t unknown3;
        };

    struct tempEffects_unknown1_10_t {
        unknown1_8_t unknown1;
        float unknown2;
        float unknown3;
        float unknown4;
        float unknown5;
        uint32_t unknown6;
        refID_t unknown7;
        refID_t unknown8;
        uint32_t unknown9;
        };

    struct tempEffects_unknown1_11_t {
        unknown1_8_t unknown1;
        refID_t unknown2;
        uint8_t unknown3;
        uint32_t unknown4[3];
        unknown1_9_0_t unknown5;
        };

    struct unknown1_t {
        uint32_t unknown1;	//Probably type of temp effect
        void* unknown2;		//type seems to depend on previous variable
        };

    vsval count0;
    unknown0_t* unknown0;//[count0]
    uint32_t unknown;
    vsval count1;
    unknown1_t* unknown1;//[count1]
    vsval count2;
    unknown1_t* unknown2;//[count2]
    };
