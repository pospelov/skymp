#pragma once
#include <memory>
#include <functional>
#include <string>
#include <skyrim-api/headers.h>

class CApi {
	friend class CApiAccess;
public:
	static CApi &GetSingleton();

	struct Controller {
		std::function<void(const char *text)> print;
		std::function<bool(uint32_t formId, bool drawn)> setWeaponDrawnHook;
		std::function<std::string(uint32_t formId, const char *animEvent)> sendAnimationEventHook;
	};

	void SetController(Controller controller);
	void IgnoreSendAnimEvent(BSFixedString *fs, bool ignore);

private:
	CApi();

	struct Impl;
	std::shared_ptr<Impl> pImpl;
};