#include <mutex>
#include <sstream>
#include <set>
#include <nlohmann/json.hpp>

#include "CApi.h"

struct CApi::Impl {
	std::recursive_mutex m;
	Controller controller;
	std::set<BSFixedString *> ignored;
};

class CApiAccess {
public:
	static CApi::Controller &GetController() {
		std::lock_guard l(GetImpl().m);
		return CApi::GetSingleton().pImpl->controller;
	};

	static CApi::Impl &GetImpl() {
		return *CApi::GetSingleton().pImpl;
	}
};

CApi &CApi::GetSingleton() {
	static CApi capi;
	return capi;
}

CApi::CApi() {
	pImpl.reset(new Impl);
}

void CApi::SetController(Controller controller) {
	std::lock_guard l(pImpl->m);
	pImpl->controller = controller;
}

void CApi::IgnoreSendAnimEvent(BSFixedString *fs, bool ignore) {
	std::lock_guard l(pImpl->m);
	if (ignore) {
		pImpl->ignored.insert(fs);
	}
	else {
		pImpl->ignored.erase(fs);
	}
}

extern "C" {
	__declspec(dllexport) void SKYMP2_Print(const char *text) {
		if (CApiAccess::GetController().print) {
			return CApiAccess::GetController().print(text);
		}
	}

	__declspec(dllexport) const char *SKYMP2_SetWeaponDrawn(const char *jDump) {
		static const std::string g_resultDraw = nlohmann::json{ { "draw", "0x1" } }.dump();
		static const std::string g_resultSheathe = nlohmann::json{ { "draw", "0x0" } }.dump();

		auto j = nlohmann::json::parse(jDump);

		std::string ecxS = j["ecx"];
		uint32_t ecxU = 0;
		std::stringstream ss;
		ss << std::hex << ecxS;
		ss >> ecxU;
		const auto actor = reinterpret_cast<Actor *>(ecxU);
		assert(actor);
		const uint32_t formId = actor ? actor->formID : 0;

		const auto &jDraw = j["draw"];
		const bool draw = jDraw == "0x1" || jDraw == 1 || jDraw == "1";

		const auto f = CApiAccess::GetController().setWeaponDrawnHook;
		const bool resDraw = f && f(formId, draw);
		return resDraw ? g_resultDraw.data() : g_resultSheathe.data();
	}

	__declspec(dllexport) BSFixedString *SKYMP2_SendAnimationEvent(TESObjectREFR *refr, BSFixedString *animEventName) {
		{
			std::lock_guard l(CApiAccess::GetImpl().m);
			bool isTriggeredBySkymp = CApiAccess::GetImpl().ignored.count(animEventName) != 0;
			if (isTriggeredBySkymp) return animEventName;
		}

		const uint32_t id = refr ? refr->formID : 0;
		const auto f = CApiAccess::GetController().sendAnimationEventHook;
		const std::string s = f ? f(id, animEventName->data) : animEventName->data;

		thread_local std::map<std::string, BSFixedString *> g_fixedStrings;
		auto &fs = g_fixedStrings[s];
		if (!fs) {
			auto sPtr = new std::string(s);
			fs = new BSFixedString(sPtr->data());
		}
		return fs;
	}

	__declspec(dllexport) void SKYMP2_OnEquipItem(void *actor, void *tesForm, bool equip) {
		// ...
	}
}