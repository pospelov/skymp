#pragma once
#include <functional>

enum class MouseButton {
	Left, Right, Middle
};

class IInputHandler {
public:
	virtual void OnKeyStateChange(uint8_t dxKeyCode, bool down) noexcept = 0;
	virtual void OnMouseStateChange(MouseButton mouseButton, bool down) noexcept = 0;
	virtual bool OnMouseMove() noexcept = 0;
	virtual bool OnMouseWheelRotate(float delta) noexcept = 0;
	virtual bool IsKeyboardEnabled() noexcept = 0;
	virtual bool IsMouseButtonsEnabled() noexcept = 0;

	virtual ~IInputHandler() = default;
};

struct IDirect3DDevice9;

namespace MyHooks {
	using OnPresent = std::function<void(IDirect3DDevice9 *)>;

	bool HookDirectInput(IInputHandler *handler);
	void HookDirect3D9(OnPresent onPresent); // Throws
}