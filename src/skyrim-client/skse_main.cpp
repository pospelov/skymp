#include <process.h> // _beginthread
#include <cassert>
#include <nlohmann/json.hpp>

#include <viet/Client.h>
#include <VietRakNet.h>

#include "common/IPrefix.h"
#include "skse/PluginAPI.h"
#include "skse/skse_version.h"
#include "scriptdragon/plugin.h"

#include "skyrim-api/skyrim_le_api.h"
#include "beth-client/Client.h"
#include "skymp-common/Entities.h"

#include "DirectXStuff.h"
#include "ImguiContext.h"
#include "GlobalState.h"
#include "CApi.h"

constexpr auto g_pluginName = "skymp3";
HMODULE g_hModule = 0;

IDebugLog gLog("skymp3.log");

PluginHandle g_pluginHandle = kPluginHandle_Invalid;

SKSEScaleformInterface		* g_scaleform = NULL;
SKSESerializationInterface	* g_serialization = NULL;

std::shared_ptr<Client> g_client;
std::shared_ptr<skyrim_le_api::Features> g_features;
std::shared_ptr<GlobalState> g_state;

class LoggerOutput : public Viet::Logging::ILoggerOutput {
public:
	void Write(Viet::Logging::Severity severity, const char *text) noexcept override {
		assert(g_state);
		if (g_state) {
			g_state->AddLogLine(severity, text);
		}
	}
};

namespace {
	static void ExitWithError(const std::exception &e) {
		MessageBoxA(0, e.what(), "Unhandled exception", MB_ICONERROR);
		ExitProcess(-1);
	}

	void StartFrida(HMODULE skymp2hooks) {
		std::thread([=] {
			typedef int (SetUpHooks)(void);
			auto setUpHooks = (SetUpHooks *)GetProcAddress(skymp2hooks, "SetUpHooks");
			setUpHooks();
		}).detach();
	}
}

void HelperThreadMain(void *featuresPtr) {
	{
		auto skymp2hooks = LoadLibraryA(HOOKS_DLL_NAME);
		if (!skymp2hooks) throw std::runtime_error("LoadLibraryA " HOOKS_DLL_NAME " failed");
		StartFrida(skymp2hooks);
	}

	skyrim_le_api::Features::HelperThreadMain(featuresPtr);
}

std::shared_ptr<Viet::Client> CreateVietClient() {
	std::string ip = "127.0.0.1";
	int port = 7777;
	int timeoutTimeMs = 4000;

	Viet::Networking::Plugin net;
	net.clientFactory = [=] {
		return new Viet::Networking::RakClient(ip.data(), port, timeoutTimeMs);
	};

	Viet::Serialization::Plugin seri;
	seri.serializerFactory = [=] {
		return new Viet::Serialization::RakSerializer();
	};

	auto p = new Viet::Client(SkympEntities::GetEntities(), net, seri);
	return std::shared_ptr<Viet::Client>(p);
}

extern "C" {
	__declspec(dllexport) bool SKSEPlugin_Query(const SKSEInterface * skse, PluginInfo * info) {
		_MESSAGE(g_pluginName);

		// populate info structure
		info->infoVersion = PluginInfo::kInfoVersion;
		info->name = g_pluginName;
		info->version = 1;

		// store plugin handle so we can identify ourselves later
		g_pluginHandle = skse->GetPluginHandle();

		if (skse->isEditor) {
			_MESSAGE("loaded in editor, marking as incompatible");
			return false;
		}
		if (skse->runtimeVersion != RUNTIME_VERSION_1_9_32_0) {
			_MESSAGE("unsupported runtime version %08X", skse->runtimeVersion);
			return false;
		}

		// get the scaleform interface and query its version
		g_scaleform = (SKSEScaleformInterface *)skse->QueryInterface(kInterface_Scaleform);
		if (!g_scaleform) {
			_MESSAGE("couldn't get scaleform interface");
			return false;
		}

		if (g_scaleform->interfaceVersion < SKSEScaleformInterface::kInterfaceVersion) {
			_MESSAGE("scaleform interface too old (%d expected %d)", g_scaleform->interfaceVersion, SKSEScaleformInterface::kInterfaceVersion);
			return false;
		}

		// get the serialization interface and query its version
		g_serialization = (SKSESerializationInterface *)skse->QueryInterface(kInterface_Serialization);
		if (!g_serialization) {
			_MESSAGE("couldn't get serialization interface");
			return false;
		}

		if (g_serialization->version < SKSESerializationInterface::kVersion) {
			_MESSAGE("serialization interface too old (%d expected %d)", g_serialization->version, SKSESerializationInterface::kVersion);
			return false;
		}

		// ### do not do anything else in this callback
		// ### only fill out PluginInfo and return true/false

		// supported runtime version
		return true;
	}

	__declspec(dllexport) bool SKSEPlugin_Load(const SKSEInterface * skse) {
		_MESSAGE("load");
		try {
			auto vietClient = CreateVietClient();

			const std::shared_ptr<Viet::Logging::Logger> logger(new Viet::Logging::Logger(new LoggerOutput));
			g_client.reset(new Client(logger, vietClient));
			g_features.reset(new skyrim_le_api::Features(g_client));

			assert(g_state);
			if (g_state) {
				g_state->SetVietClient(vietClient);
			}

			CApi::Controller capiCont;
			capiCont.print = [logger](const char *text) {
				logger->Info("hooks.js -> '%s'", text);
			};
			capiCont.sendAnimationEventHook = [logger](uint32_t id, const char *animEvent) {
				return g_features->SendAnimEventHook(id, animEvent);
			};
			capiCont.setWeaponDrawnHook = [logger](uint32_t id, bool draw) {
				return g_features->SetWeaponDrawnHook(id, draw);
			};

			CApi::GetSingleton().SetController(capiCont);

			_beginthread(HelperThreadMain, 1024 * 1024, &*g_features);
			sd::DragonPluginInit(g_hModule);
		}
		catch (std::exception &e) {
			MessageBoxA(0, e.what(), "SkyMP startup error", MB_ICONERROR);
			return false;
		}
		return true;
	}

	__declspec(dllexport) void main() {
		assert(g_client);
		assert(g_features);
		if (!g_client || !g_features) return;

		g_features->SDMain();
	}

	__declspec(dllexport) BOOL WINAPI DllMain(HMODULE hModule, DWORD fdwReason, LPVOID lpReserved) {
		switch (fdwReason) {
		case DLL_PROCESS_ATTACH:
			g_hModule = hModule;
			try {
				g_state.reset(new GlobalState);
				bool success = MyHooks::HookDirectInput(g_state.get());
				if (!success) {
					throw std::runtime_error("HookDirectInput failed");
				}

				MyHooks::HookDirect3D9([](IDirect3DDevice9 *device) {
					static MyImGuiContext imCtx(device);
					assert(g_state);
					assert(g_client);
					g_state->Render(imCtx, device, g_client.get());
				});
			}
			catch (std::exception &e) {
				ExitWithError(e);
			}
			break;

		case DLL_THREAD_ATTACH:
			break;

		case DLL_THREAD_DETACH:
			break;

		case DLL_PROCESS_DETACH:
			break;
		}
		return TRUE;
	}

};