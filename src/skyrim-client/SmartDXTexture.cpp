#include <cassert>

#include "SmartDXTexture.h"

std::shared_ptr<IDirect3DTexture9> SmartDXTexture::GetTexture() const noexcept {
	return tex;
}

SmartDXTexture::EditRes SmartDXTexture::EditFromPng(IDirect3DDevice9 *device, const unsigned char *pngData, size_t size) noexcept {
	IDirect3DTexture9 *newTex = nullptr;
	D3DXCreateTextureFromFileInMemory(device, pngData, size, &newTex);
	if (!newTex) return EditRes::CreateTextureFailed;

	this->tex = std::shared_ptr<IDirect3DTexture9>(newTex, [](IDirect3DTexture9 *tex) {
		tex->Release();
	});
	return EditRes::Success;
}

SmartDXTexture::EditRes SmartDXTexture::Edit(IDirect3DDevice9 *device, const unsigned char *data, int width, int height) noexcept {
	assert(device);
	assert(data);
	assert(width > 0);
	assert(height > 0);
	
	if (!this->tex || !this->pWidth || !this->pHeight || *this->pWidth != width || *this->pHeight != height) {
		IDirect3DTexture9 *newTex = nullptr;
		D3DXCreateTexture(device, width, height, 1, D3DUSAGE_DYNAMIC, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, &newTex);
		if (!newTex) return EditRes::CreateTextureFailed;
		this->tex = std::shared_ptr<IDirect3DTexture9>(newTex, [](IDirect3DTexture9 *tex) {
			tex->Release();
		});
		if (!pWidth) this->pWidth.reset(new int);
		if (!pHeight) this->pHeight.reset(new int);
		*this->pWidth = width;
		*this->pHeight = height;
	}

	assert(this->tex);
	if (!this->EditTexture(this->tex.get(), data, width, height)) {
		return EditRes::EditTextureFailed;
	}

	assert(this->pWidth);
	assert(*this->pWidth > 0);
	assert(this->pHeight);
	assert(*this->pHeight > 0);
	return EditRes::Success;
}

bool SmartDXTexture::EditTexture(LPDIRECT3DTEXTURE9 texture, const void *buffer, int width, int height) {
	D3DLOCKED_RECT locked_rect = { 0 };
	if (SUCCEEDED(texture->LockRect(0, &locked_rect, NULL, 0))) {
		// Copy new data into the buffer
		int bbp = (int)(locked_rect.Pitch / width);
		int bpr = width * bbp;
		for (uint32_t i = 0; i < height; i++) {
			memcpy((char*)locked_rect.pBits + (i * locked_rect.Pitch), (uint8_t *)(buffer)+i * bpr, bpr);
		}
		texture->UnlockRect(0);
		return true;
	}
	return false;
}

int SmartDXTexture::GetWidth() const noexcept {
	return pWidth ? *pWidth : -1;
}

int SmartDXTexture::GetHeight() const noexcept {
	return pHeight ? *pHeight : -1;
}