#pragma once
#include <functional>
#include <imgui.h>
#include "imgui_impl_dx9.h"

class MyImGuiContext {
public:
	using Callback = std::function<void()>;

	MyImGuiContext(IDirect3DDevice9 *device);

	void Render(IDirect3DDevice9 *device, Callback drawWidgets, float mouseX, float mouseY, float mouseWheelAbsolute, bool allowInputs) noexcept;

	bool GetKeyPressed(unsigned char key);
};