#pragma once
#include <vector>
#include <string>
#include <utility>
#include <optional>
#include <functional>
#include <viet/Logging.h> // Logging::Severity

struct DebugPresenter {
	using ForEachCb = std::function<void(Viet::Logging::Severity, const char *)>;

	std::function<void(ForEachCb)> forEachLogLine;

	std::function<std::string()> getNotification;
};

class DebugView {
public:
	DebugView(DebugPresenter presenter) noexcept;
	~DebugView();

	void OnPresent();

private:
	struct Impl;
	Impl *const pImpl;
};