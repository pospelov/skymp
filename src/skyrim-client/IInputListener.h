#pragma once
#include <cstdint>

class IInputListener {
public:
	enum class MouseButton {
		Left = 0, Middle = 1, Right = 2
	};

	virtual void OnKeyStateChange(uint8_t vkCode, bool down) noexcept = 0;
	virtual void OnChar(wchar_t ch) noexcept = 0;
	virtual void OnMouseWheel(int32_t delta) noexcept = 0;
	virtual void OnMouseMove(float newX, float newY) noexcept = 0;
	virtual void OnMouseStateChange(float clickX, float clickY, MouseButton mouseButton, bool down) noexcept = 0;

	virtual ~IInputListener() = default;
};
