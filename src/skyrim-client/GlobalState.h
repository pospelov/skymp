#include <atomic>
#include <mutex>
#include <list>
#include <viet/Logging.h> // Viet::Logging::Severity
#include <viet/Client.h>
#include "DirectXStuff.h"
#include "ImguiContext.h"
#include "skyrim-api/headers.h"
#include <beth-client/Client.h>
#include "DebugView.h"
#include "SmartDXTexture.h"
#include "InputEmitter.h"
#include "InputConverter.h"

typedef int (SkympCef_Main)(void);
typedef uint32_t(SkympCef_GetPaintSize)(void);
typedef void(SkympCef_GetPaint)(uint8_t *out, uint32_t maxSize);
typedef void(SkympCef_SetFocus)(bool focus);
typedef void(SkympCef_OnMouseStateChange)(float x, float y, uint8_t mouseButton, bool down);
typedef void(SkympCef_OnMouseMove)(float x, float y);
typedef void(SkympCef_OnMouseWheel)(int wheelDelta);
typedef void(SkympCef_OnKeyStateChange)(uint8_t vkCode, bool down);
typedef void(SkympCef_OnChar)(wchar_t character);
typedef void(SkympCef_LoadUrl)(const wchar_t *url);
typedef void(SkympCef_ExecuteJs)(const char *jsSrc);
typedef uint32_t(SkympCef_GetCPRequiredBufSize)(void);
typedef void(SkympCef_NextOutCP)(char *outBuf, bool *outSuccess, uint32_t bufSize);

struct CefFunctions {
	SkympCef_GetPaintSize *cefGetPaintSize;
	SkympCef_GetPaint *cefGetPaint;
	SkympCef_SetFocus *cefSetFocus;
	SkympCef_OnMouseStateChange *cefOnMouseStateChange;
	SkympCef_OnMouseMove *cefOnMouseMove;
	SkympCef_OnMouseWheel *cefOnMouseWheel;
	SkympCef_OnKeyStateChange *cefOnKeyStateChange;
	SkympCef_OnChar *cefOnChar;
	SkympCef_LoadUrl *cefLoadUrl;
	SkympCef_ExecuteJs *cefExecuteJs;
	SkympCef_GetCPRequiredBufSize *cefGetCPRequiredBufSize;
	SkympCef_NextOutCP *cefNextOutCP;
};

class GlobalState : public IInputHandler {
public:
	// IInputHandler
	void OnKeyStateChange(uint8_t dxKeyCode, bool down) noexcept override;
	void OnMouseStateChange(MouseButton mouseButton, bool down) noexcept override;
	bool OnMouseMove() noexcept override;
	bool OnMouseWheelRotate(float delta) noexcept override;
	bool IsKeyboardEnabled() noexcept override;
	bool IsMouseButtonsEnabled() noexcept override;

	void AddLogLine(Viet::Logging::Severity severity, const char *text) noexcept;

	void Render(MyImGuiContext &imCtx, IDirect3DDevice9 *device, Client *client) noexcept;

	void SetVietClient(std::shared_ptr<Viet::Client> cl);

private:
	void DrawWidgets() noexcept;
	void DrawCursor(IDirect3DDevice9 *device) noexcept;
	void DrawChromium(IDirect3DDevice9 *device) noexcept;
	void PerformChromiumInputs() noexcept;
	void HandleOutCP() noexcept;
	void HandleIncomingCP() noexcept;
	void UpdateFrontFilesIfNeed() noexcept;

	float GetMouseX() noexcept;
	float GetMouseY() noexcept;
	bool IsCursorOpen() const noexcept;
	bool IsMainMenuOpen() const noexcept;
	bool IsLoadingMenuOpen() const noexcept;

	void SetCursorOpen(bool open) noexcept;
	void SetOverlayActive(bool active, Client &client) noexcept; // Cursor + ToggleControls + Chromium focus

	std::atomic<float> mouseWheelAbsolute = 0;
	std::atomic<bool> debug = false;
	std::atomic<bool> overlayActive = false;
	std::atomic<bool> cursorShownByGame = false;

	struct Share {
		std::mutex m;
		std::list<std::pair<Viet::Logging::Severity, std::string>> logs;
	} share;

	std::unique_ptr<DebugView> debugView;

	SmartDXTexture cursorTexture;
	void *cursorSprite = nullptr; // LPD3DXSPRITE

	SmartDXTexture chromiumTexture;
	void *chromiumSprite = nullptr; // LPD3DXSPRITE
	std::vector<uint8_t> paintData;

	bool cefInited = false;
	CefFunctions cefFunctions;
	std::shared_ptr<IInputListener> cefInputListener;

	InputEmitter inputEmitter;
	InputConverter inputConverter;
	bool switchLangDownWas = false;

	std::shared_ptr<Viet::Client> cl;
	int numFrontChanges = -1;

	struct {
		std::vector<char> buf;
	} outCp;
};