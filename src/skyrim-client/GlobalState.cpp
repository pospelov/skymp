#include <filesystem>
#include "assets-util/assets.h"

#include "GlobalState.h"

void GlobalState::OnKeyStateChange(uint8_t dxKeyCode, bool down) noexcept {
}

void GlobalState::OnMouseStateChange(MouseButton mouseButton, bool down) noexcept {
}

bool GlobalState::OnMouseMove() noexcept {
	return true;
}

bool GlobalState::OnMouseWheelRotate(float delta) noexcept {
	if (this->overlayActive) {
		mouseWheelAbsolute = mouseWheelAbsolute + delta;
		return false;
	}
	return true;
}

bool GlobalState::IsKeyboardEnabled() noexcept {
	return !this->overlayActive;
}

bool GlobalState::IsMouseButtonsEnabled() noexcept {
	return !this->overlayActive;
}

void GlobalState::AddLogLine(Viet::Logging::Severity severity, const char *text) noexcept {
	std::lock_guard l(this->share.m);
	this->share.logs.push_back({ severity, text });
	const size_t maxLogs = 3000;
	while (this->share.logs.size() >= maxLogs) {
		this->share.logs.pop_front();
	}
}

void GlobalState::Render(MyImGuiContext &imCtx, IDirect3DDevice9 *device, Client *client) noexcept {
	if (!this->cefInited) {
		this->cefInited = true;

		auto skymp_cef_renderer = LoadLibraryA("skymp_cef_renderer.dll");
		assert(skymp_cef_renderer);
		if (skymp_cef_renderer) {
			auto cefMain = (SkympCef_Main *)GetProcAddress(skymp_cef_renderer, "SkympCef_Main");
			assert(cefMain);
			if (cefMain) {
				std::thread([cefMain] { cefMain(); }).detach();

				this->cefFunctions.cefGetPaintSize = (SkympCef_GetPaintSize *)GetProcAddress(skymp_cef_renderer, "SkympCef_GetPaintSize");
				this->cefFunctions.cefGetPaint = (SkympCef_GetPaint *)GetProcAddress(skymp_cef_renderer, "SkympCef_GetPaint");
				this->cefFunctions.cefSetFocus = (SkympCef_SetFocus *)GetProcAddress(skymp_cef_renderer, "SkympCef_SetFocus");
				this->cefFunctions.cefOnMouseStateChange = (SkympCef_OnMouseStateChange *)GetProcAddress(skymp_cef_renderer, "SkympCef_OnMouseStateChange");
				this->cefFunctions.cefOnMouseMove = (SkympCef_OnMouseMove *)GetProcAddress(skymp_cef_renderer, "SkympCef_OnMouseMove");
				this->cefFunctions.cefOnMouseWheel = (SkympCef_OnMouseWheel *)GetProcAddress(skymp_cef_renderer, "SkympCef_OnMouseWheel");
				this->cefFunctions.cefOnKeyStateChange = (SkympCef_OnKeyStateChange *)GetProcAddress(skymp_cef_renderer, "SkympCef_OnKeyStateChange");
				this->cefFunctions.cefOnChar = (SkympCef_OnChar *)GetProcAddress(skymp_cef_renderer, "SkympCef_OnChar");
				this->cefFunctions.cefLoadUrl = (SkympCef_LoadUrl *)GetProcAddress(skymp_cef_renderer, "SkympCef_LoadUrl");
				this->cefFunctions.cefExecuteJs = (SkympCef_ExecuteJs *)GetProcAddress(skymp_cef_renderer, "SkympCef_ExecuteJs");
				this->cefFunctions.cefGetCPRequiredBufSize = (SkympCef_GetCPRequiredBufSize *)GetProcAddress(skymp_cef_renderer, "SkympCef_GetCPRequiredBufSize");
				this->cefFunctions.cefNextOutCP = (SkympCef_NextOutCP *)GetProcAddress(skymp_cef_renderer, "SkympCef_NextOutCP");
			}
		}
	}

	const bool allowInputs = this->overlayActive;
	float mouseX = this->GetMouseX();
	float mouseY = this->GetMouseY();
	imCtx.Render(device, [this, &imCtx, &client, device] {
		bool combDown = ImGui::IsKeyDown('9') && ImGui::IsKeyDown('O') && ImGui::IsKeyDown('L');
		thread_local bool g_combDown = false;
		if (combDown != g_combDown) {
			if (combDown) {
				this->debug = !this->debug;
			}
			g_combDown = combDown;
		}

		const bool mainMenuOpen = this->IsMainMenuOpen();
		if (mainMenuOpen) {
			if (!this->overlayActive) {
				this->SetOverlayActive(true, *client);
			}
			if (this->cefFunctions.cefSetFocus) {
				this->cefFunctions.cefSetFocus(true);
			}
		}
		else if (this->IsLoadingMenuOpen()) {
			if (this->overlayActive) {
				this->SetOverlayActive(false, *client);
			}
		}
		else {
			bool chatDown = imCtx.GetKeyPressed(VK_F6);
			thread_local bool g_chatDown = false;
			if (chatDown != g_chatDown) {
				if (chatDown) {
					this->SetOverlayActive(!this->overlayActive, *client);
				}
				g_chatDown = chatDown;
			}
		}

		this->DrawWidgets();
		this->PerformChromiumInputs();
	}, mouseX, mouseY, mouseWheelAbsolute, allowInputs);

	this->DrawChromium(device);
	this->DrawCursor(device);

	this->UpdateFrontFilesIfNeed();
	this->HandleOutCP();
	this->HandleIncomingCP();
}

void GlobalState::SetVietClient(std::shared_ptr<Viet::Client> cl) {
	this->cl = cl;
}

void GlobalState::DrawWidgets() noexcept {
	if (!this->debug) return;
	
	// Draw debug:

	if (!this->debugView) {
		DebugPresenter presenter;
		presenter.forEachLogLine = [this](DebugPresenter::ForEachCb cb) {
			std::lock_guard l(this->share.m);
			for (auto &[severity, text] : this->share.logs) {
				cb(severity, text.data());
			}
		};
		presenter.getNotification = [this] {
			return "";
		};

		this->debugView.reset(new DebugView(presenter));
	}
	this->debugView->OnPresent();
}

void GlobalState::DrawCursor(IDirect3DDevice9 *device) noexcept {
	const bool cursorVisible = this->IsCursorOpen();
	const auto mouseX = this->GetMouseX();
	const auto mouseY = this->GetMouseY();

	if (this->cursorTexture.GetTexture() == nullptr) {
		static const auto cursor = assets_get_file("cursor.png");
		const auto res = this->cursorTexture.EditFromPng(device, (unsigned char *)cursor.first, cursor.second);
		assert(res == SmartDXTexture::EditRes::Success);
	}
	if (this->cursorSprite == nullptr) {
		D3DXCreateSprite(device, (LPD3DXSPRITE *)&this->cursorSprite);
		assert(this->cursorSprite);
	}
	device->BeginScene();
	if (cursorVisible && this->cursorSprite) {
		auto sprite = (LPD3DXSPRITE)this->cursorSprite;
		sprite->Begin(D3DXSPRITE_ALPHABLEND);
		const D3DXVECTOR3 position(mouseX - 32, mouseY - 32, 0);
		D3DCOLOR color = -1;
		const auto cursorTexture = this->cursorTexture.GetTexture();
		assert(cursorTexture);
		if (cursorTexture) {
			const bool result = SUCCEEDED(sprite->Draw(cursorTexture.get(), NULL, NULL, &position, color));
			assert(result);
		}
		sprite->End();
	}
	device->EndScene();
}

void GlobalState::DrawChromium(IDirect3DDevice9 *device) noexcept {
	if (!this->cefFunctions.cefGetPaintSize || !this->cefFunctions.cefGetPaint) return;

	const uint32_t size = this->cefFunctions.cefGetPaintSize();
	if (!size) return;

	this->paintData.resize(size, 0);
	this->cefFunctions.cefGetPaint(&this->paintData[0], size);

	const auto &displaySize = ImGui::GetIO().DisplaySize;

	if (displaySize.x <= 0) return;
	if (displaySize.y <= 0) return;

	const auto res = this->chromiumTexture.Edit(device, &this->paintData[0], (int)displaySize.x, (int)displaySize.y);
	assert(res == SmartDXTexture::EditRes::Success);


	if (this->chromiumSprite == nullptr) {
		D3DXCreateSprite(device, (LPD3DXSPRITE *)&this->chromiumSprite);
		assert(this->chromiumSprite);
	}
	device->BeginScene();
	if (this->chromiumSprite) {
		auto sprite = (LPD3DXSPRITE)this->chromiumSprite;
		sprite->Begin(D3DXSPRITE_ALPHABLEND);
		const D3DXVECTOR3 position(0, 0, 0);
		D3DCOLOR color = -1;
		const auto tex = this->chromiumTexture.GetTexture();
		assert(tex);
		if (tex) {
			const bool result = SUCCEEDED(sprite->Draw(tex.get(), NULL, NULL, &position, color));
			assert(result);
		}
		sprite->End();
	}
	device->EndScene();
}

void GlobalState::PerformChromiumInputs() noexcept {
	// Chromium should catch all inputs even if the overlay is not active
	///if (!this->overlayActive) return;

	if (!this->cefFunctions.cefGetPaint) return;

	if (!this->cefInputListener) {
		class CefInputListener : public IInputListener {
		public:
			CefInputListener(CefFunctions *funcs_, std::atomic<bool> *isOverlayActive_) : funcs(*funcs_), isOverlayActive(*isOverlayActive_) {
			}

			void OnKeyStateChange(uint8_t vkCode, bool down) noexcept override {
				if (this->funcs.cefOnKeyStateChange)
					this->funcs.cefOnKeyStateChange(vkCode, down);
			}

			void OnChar(wchar_t ch) noexcept override {
				if (this->funcs.cefOnChar && this->isOverlayActive)
					this->funcs.cefOnChar(ch);
			}

			void OnMouseWheel(int32_t delta) noexcept override {
				if (this->funcs.cefOnMouseWheel)
					this->funcs.cefOnMouseWheel(delta);
			}

			void OnMouseMove(float newX, float newY) noexcept override {
				if (this->funcs.cefOnMouseMove)
					this->funcs.cefOnMouseMove(newX, newY);
			}

			void OnMouseStateChange(float clickX, float clickY, MouseButton mouseButton, bool down) noexcept override {
				if (mouseButton != MouseButton::Left) return;

				if (this->funcs.cefOnMouseStateChange)
					this->funcs.cefOnMouseStateChange(clickX, clickY, uint8_t(mouseButton), down);
			}

			CefFunctions &funcs;
			std::atomic<bool> &isOverlayActive;
		};
		this->cefInputListener.reset(new CefInputListener(&this->cefFunctions, &this->overlayActive));
	}

	InputEmitter::Data data;
	data.mouseDown = ImGui::GetIO().MouseDown;
	data.mouseWheelAbsolute = (int64_t)ImGui::GetIO().MouseWheel;
	data.mouseX = ImGui::GetIO().MousePos.x;
	data.mouseY = ImGui::GetIO().MousePos.y;
	data.vkCodesDown = ImGui::GetIO().KeysDown;

	this->inputEmitter.Tick(*this->cefInputListener, data, this->inputConverter);

	// Switch keyboard layout if need
	const bool switchLangDown = (ImGui::IsKeyDown(VK_CONTROL) || ImGui::IsKeyDown(VK_MENU)) && ImGui::IsKeyDown(VK_SHIFT);
	const bool switchLangPressed = switchLangDown && !this->switchLangDownWas;
	this->switchLangDownWas = switchLangDown;
	if (switchLangPressed) {
		this->inputConverter.SwitchLayout();
	}
}

void GlobalState::HandleOutCP() noexcept {
	if (!this->cefFunctions.cefGetCPRequiredBufSize || !this->cefFunctions.cefNextOutCP) return;

	const auto requiredBufSize = this->cefFunctions.cefGetCPRequiredBufSize();
	if (requiredBufSize > this->outCp.buf.size()) {
		this->outCp.buf.resize(requiredBufSize);
	}

	while (1) {
		bool success = false;
		this->cefFunctions.cefNextOutCP(this->outCp.buf.data(), &success, this->outCp.buf.size());
		if (!success) break;

		bool isNullTerminated = std::find(this->outCp.buf.begin(), this->outCp.buf.end(), 0) != this->outCp.buf.end();
		assert(isNullTerminated);

		if (isNullTerminated) {
			std::string logLine = (std::string)"Outcoming CP - '" + this->outCp.buf.data() + "'";
			this->AddLogLine(Viet::Logging::Severity::Debug, logLine.data());

			cl->SendCustomPacket(this->outCp.buf.data());
		}
	}
}

void GlobalState::HandleIncomingCP() noexcept {
	assert(this->cefFunctions.cefExecuteJs);
	if (!this->cefFunctions.cefExecuteJs) return;

	while (auto cp = cl->NextCustomPacket()) {
		std::string logLine = (std::string)"Incoming CP - '" + *cp + "'";
		this->AddLogLine(Viet::Logging::Severity::Debug, logLine.data());

		auto v = "window._skymp.cpIn ? window._skymp.cpIn.push('" + *cp + "') : window._skymp.cpIn = ['" + *cp + "']";
		this->cefFunctions.cefExecuteJs(v.data());
	}
}

void FlushFile(std::filesystem::path tmpDir, std::filesystem::path path, const void *content, size_t contentSize) {
	auto parent = std::filesystem::path(path).parent_path();
	std::filesystem::create_directories(tmpDir / parent);

	std::ofstream f(path, std::ios::binary);
	if (!f.write((const char *)content, contentSize)) {
		assert(0 && "Unable to flush a file");
		// TODO: logging
	}
}


void GlobalState::UpdateFrontFilesIfNeed() noexcept {
	try {
		if (!this->cl) return;

		int n = this->cl->GetNumFrontChanges();
		if (this->numFrontChanges == n) return;
		this->numFrontChanges = n;

		// Flush to disk
		wchar_t buf[MAX_PATH + 1] = { 0 };
		int res = GetTempPathW(MAX_PATH, buf);
		assert(res != 0);
		if (res == 0) return;

		std::filesystem::path tempDir = buf;
		auto skympFront = tempDir / "skymp_front";
		if (std::filesystem::exists(skympFront)) {
			std::filesystem::remove_all(skympFront);
		}
		std::filesystem::create_directory(skympFront);

		auto front = this->cl->GetFrontEndFiles();
		for (auto &[fileName, data] : front) {
			FlushFile(tempDir, std::filesystem::path(skympFront) / fileName, data.data(), data.size());
		}

		assert(this->cefFunctions.cefLoadUrl);
		assert(this->cefFunctions.cefExecuteJs);
		if (this->cefFunctions.cefLoadUrl && this->cefFunctions.cefExecuteJs) {
			auto wstr = L"file://" + (skympFront / "index.html").wstring();
			auto str = (skympFront / "index.html").string();
			auto logLine = "Loading url " + str;
			this->AddLogLine(Viet::Logging::Severity::Debug, logLine.data());
			this->cefFunctions.cefLoadUrl(wstr.data());

			auto &[patchCef, patchCefSize] = assets_get_file("patchCef.js");
			static std::string g_patchCef(patchCef, patchCefSize);
			this->cefFunctions.cefExecuteJs(g_patchCef.data());
		}
	}
	catch (std::exception &e) {
		assert(0);
		// TODO: logging
		
		// Retry
		this->numFrontChanges = -1;
		this->UpdateFrontFilesIfNeed();
	}
}

float GlobalState::GetMouseX() noexcept {
	if (!this->IsCursorOpen()) return -1.f;
	return *(float *)0x1b8d034;
}

float GlobalState::GetMouseY() noexcept {
	if (!this->IsCursorOpen()) return -1.f;
	return *(float *)0x1b8d038;
}

bool GlobalState::IsCursorOpen() const noexcept {
	static const auto fs = new BSFixedString("Cursor Menu");
	return MenuManager::GetSingleton()->IsMenuOpen(fs);
}

bool GlobalState::IsMainMenuOpen() const noexcept {
	static const auto fs = new BSFixedString("Main Menu");
	return MenuManager::GetSingleton()->IsMenuOpen(fs);
}

bool GlobalState::IsLoadingMenuOpen() const noexcept {
	static const auto fs = new BSFixedString("Loading Menu");
	return MenuManager::GetSingleton()->IsMenuOpen(fs);
}

void GlobalState::SetCursorOpen(bool open) noexcept {
	static const auto fsCursorMenu = new BSFixedString("Cursor Menu");
	if (open) {
		return UIManager::GetSingleton()->OpenMenu(fsCursorMenu);
	}
	return UIManager::GetSingleton()->CloseMenu(fsCursorMenu);
}

void GlobalState::SetOverlayActive(bool active, Client &client) noexcept {
	bool v = !this->overlayActive;
	if (v) {
		this->cursorShownByGame = this->IsCursorOpen();
		this->SetCursorOpen(true);
	}
	else {
		if (!this->cursorShownByGame) {
			this->SetCursorOpen(false);
		}
	}
	this->overlayActive = v;
	client.OnKeyboardEnableDisable(!v);
	assert(this->cefFunctions.cefSetFocus);
	if (this->cefFunctions.cefSetFocus) {
		this->cefFunctions.cefSetFocus(active);
	}
}