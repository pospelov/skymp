cmake_minimum_required(VERSION 2.8.12.1)

set(CMAKE_CONFIGURATION_TYPES Debug Release)
set_property(GLOBAL PROPERTY OS_FOLDERS ON)

set(CEF_ROOT ${CEF_PATH})
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CEF_ROOT}/cmake")

find_package(CEF REQUIRED)

add_subdirectory(${CEF_LIBCEF_DLL_WRAPPER_PATH} libcef_dll_wrapper)

set(CEF_LIBRARIES
  ${CEF_PATH}/$<CONFIG>/cef_sandbox.lib
  ${CEF_PATH}/$<CONFIG>/libcef.lib
  libcef_dll_wrapper
)
set(CEF_INCLUDE
  ${CEF_PATH}
)

add_library(skymp_cef_renderer SHARED
  "${CMAKE_CURRENT_LIST_DIR}/cef.cpp"
)
add_executable(skymp_cef_renderer_exe WIN32
  "${CMAKE_CURRENT_LIST_DIR}/cef.cpp"
  "${CMAKE_CURRENT_LIST_DIR}/cef_exe_main.cpp"
)

foreach(TARGET skymp_cef_renderer skymp_cef_renderer_exe)
  target_link_libraries(${TARGET} PRIVATE ${CEF_LIBRARIES})
  target_include_directories(${TARGET} PRIVATE ${CEF_INCLUDE})
  set_target_properties(${TARGET} PROPERTIES MSVC_RUNTIME_LIBRARY "MultiThreaded$<$<CONFIG:Debug>:Debug>")
endforeach()
