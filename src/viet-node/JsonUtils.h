#pragma once
#include <napi.h>
#include <viet/ApiClient.h>

inline Viet::json ToJson(Napi::Env info, Napi::Value value) {
	auto theJson = info.Global().Get("JSON").As<Napi::Object>();
	auto stringify = theJson.Get("stringify").As<Napi::Function>();
	auto dump = (std::string)stringify.Call({ value }).As<Napi::String>();
	try {
		return Viet::json::parse(dump);
	}
	catch (std::exception &) {
		assert(0);
		return {};
	}
}

inline Napi::Value ToJson(Napi::Env info, const Viet::json &j) {
	auto theJson = info.Global().Get("JSON").As<Napi::Object>();
	auto parse = theJson.Get("parse").As<Napi::Function>();
	auto res = parse.Call({ Napi::String::New(info, j.dump()) });
	return res;
}