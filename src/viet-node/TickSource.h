#pragma once
#include <napi.h>
#include <functional>
#include <cassert>

class TickSource {
public:
	TickSource(Napi::Env env, std::function<void(Napi::Env)> f) {
		auto setInterval = env.Global().Get("setInterval").As<Napi::Function>();

		auto tickF = Napi::Function::New(env, [f](const Napi::CallbackInfo &info) {
			f(info.Env());
		});
		this->tick = Napi::Persistent(tickF);

		auto interval = setInterval.Call({ tickF, Napi::Number::New(env, 0) });
		this->interval = Napi::Persistent(interval.As<Napi::Object>());

		this->isActive = true;
	}

	void Destroy(Napi::Env env) {
		auto clearInterval = env.Global().Get("clearInterval").As<Napi::Function>();
		clearInterval.Call({ this->interval.Value() });

		this->isActive = false;
	}

	~TickSource() {
		assert(!this->isActive);
	}

private:
	Napi::FunctionReference tick;
	Napi::ObjectReference interval;
	bool isActive = false;
};