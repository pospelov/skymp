if (UNIX)
  set(suffix .sh)
elseif(WIN32)
  set(suffix .bat)
endif()

if (WIN32)
  set(temp_bat "${CMAKE_CURRENT_BINARY_DIR}/temp${suffix}")
  set(npm_cmd ${temp_bat})
  set(npm_arg "")
  file(WRITE ${temp_bat} "npm i")
else()
  set(npm_cmd "npm")
  set(npm_arg "i")
endif()

set(VIET_NODE_DIR ${VIET_PLUGINS_DIR}/viet-node)

message(STATUS "[viet] Doing 'npm install'")
execute_process(COMMAND ${npm_cmd} ${npm_arg}
  WORKING_DIRECTORY ${VIET_NODE_DIR}
  RESULT_VARIABLE npm_result
  OUTPUT_VARIABLE npm_out
)
message(STATUS "[viet] Finished 'npm install' with code ${npm_result}")
if(npm_result EQUAL "1")
    message(FATAL_ERROR "[viet] Bad exit status")
endif()

if (WIN32)
  file(WRITE ${temp_bat} "npm run build_Release")
else()
  set(npm_cmd "npm")
  set(npm_arg run build_Release)
endif()

message(STATUS "[viet] Doing 'cmake-js build' to retrieve nodejs files")
execute_process(COMMAND ${npm_cmd} ${npm_arg}
  WORKING_DIRECTORY ${VIET_NODE_DIR}
  RESULT_VARIABLE npm_result
  OUTPUT_VARIABLE npm_out
)
message(STATUS "[viet] Finished 'cmake-js build' with code ${npm_result}")
if(npm_result EQUAL "1")
    message(FATAL_ERROR "[viet] Bad exit status")
endif()

set(vars_dir "${VIET_NODE_DIR}/build/vars")
foreach(var_name CMAKE_JS_INC CMAKE_JS_SRC CMAKE_JS_LIB)
  file(READ "${vars_dir}/${var_name}" ${var_name})
endforeach()

file(GLOB VIET_NODE_SRC "${VIET_NODE_DIR}/*")
add_library(viet-node SHARED ${VIET_NODE_SRC} ${CMAKE_JS_SRC})
target_include_directories(viet-node PRIVATE ${CMAKE_JS_INC})
target_link_libraries(viet-node PRIVATE ${CMAKE_JS_LIB})
set_target_properties(viet-node PROPERTIES PREFIX "" OUTPUT_NAME "skymp-api" SUFFIX ".node")

# Viet
target_link_libraries(viet-node PRIVATE nlohmann_json::nlohmann_json)
target_link_libraries(viet-node PRIVATE ${VIET_LIBS} ${VIET_LIBS_RAKNET})
target_include_directories(viet-node PRIVATE ${VIET_INCLUDE})

# Viet plugins
target_include_directories(viet-node PRIVATE "viet-raknet")
target_link_libraries(viet-node PRIVATE viet-raknet)

# skymp common (required for client api)
target_include_directories(viet-node PRIVATE "src/skymp-common")
target_link_libraries(viet-node PRIVATE skymp_common)

# for assets (api patch)
target_include_directories(viet-node PRIVATE "src/assets-util")
target_link_libraries(viet-node PRIVATE assetslib)
if (NOT TARGET assetslib)
  message(FATAL_ERROR "assetslib is not a target")
endif()

# Include N-API wrappers
execute_process(COMMAND node -p "require('node-addon-api').include"
  WORKING_DIRECTORY ${VIET_NODE_DIR}
  OUTPUT_VARIABLE NODE_ADDON_API_DIR
)
string(REPLACE "\n" "" NODE_ADDON_API_DIR ${NODE_ADDON_API_DIR})
string(REPLACE "\"" "" NODE_ADDON_API_DIR ${NODE_ADDON_API_DIR})
target_include_directories(viet-node PRIVATE ${NODE_ADDON_API_DIR})

# https://github.com/cmake-js/cmake-js/issues/84
target_include_directories(viet-node PRIVATE "${CMAKE_SOURCE_DIR}/node_modules/node-addon-api")
target_include_directories(viet-node PRIVATE "${CMAKE_SOURCE_DIR}/node_modules/node-addon-api/src")
