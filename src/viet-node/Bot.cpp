#include <viet/Client.h>
#include <VietRakNet.h>
#include <Entities.h>
#include "TickSource.h"
#include "JsonUtils.h"

#include "Bot.h"

struct Bot::Impl {
	std::shared_ptr<Viet::Client> client;
	std::unique_ptr<TickSource> tickSource;
	std::unique_ptr<Napi::ObjectReference> worldState;
};

Napi::FunctionReference Bot::constructor;

Bot::Bot(const Napi::CallbackInfo &info) : Napi::ObjectWrap<Bot>(info) {
	pImpl.reset(new Impl);
}

Napi::Object Bot::Init(Napi::Env env, Napi::Object exports) {
	Napi::Function func = DefineClass(env, "Bot", {
        InstanceMethod("connect", &Bot::Connect),
		InstanceMethod("kill", &Bot::Kill),
		InstanceMethod("processWorldState", &Bot::ProcessWorldState),
		InstanceMethod("send", &Bot::Send)
    });
    constructor = Napi::Persistent(func);
    constructor.SuppressDestruct();
    exports.Set("Bot", func);
    return exports;
}

void Bot::Connect(const Napi::CallbackInfo &info) {
	auto ip = (std::string)info[0].As<Napi::String>();
	auto port = (int)info[1].As<Napi::Number>();

	std::optional<std::string> sessionHash;
	if (info[2].IsString()) {
		sessionHash = (std::string)info[2].As<Napi::String>();
	}
	return this->Connect(info.Env(), ip.data(), port, sessionHash);
}

void Bot::Connect(Napi::Env env, const char *ip_, int port, std::optional<std::string> sessionHash) {
	std::string ip = ip_;

	Viet::Networking::Plugin netPlugin;
	netPlugin.clientFactory = [=] {
		return new Viet::Networking::RakClient(ip.data(), port, 4000);
	};

	Viet::Serialization::Plugin seriPlugin;
	seriPlugin.serializerFactory = [=] {
		return new Viet::Serialization::RakSerializer;
	};

	auto entities = SkympEntities::GetEntities();
	pImpl->client.reset(new Viet::Client(entities, netPlugin, seriPlugin, sessionHash));

	auto pImpl_ = pImpl;
	pImpl->tickSource.reset(new TickSource(env, [pImpl_](Napi::Env env) {
		assert(pImpl_->client);
		if (pImpl_->client) {
			pImpl_->client->Tick();
		}
	}));
}

void Bot::Kill(const Napi::CallbackInfo &info) {
	this->Kill(info.Env());
}

void Bot::Kill(Napi::Env env) {
	if (pImpl->tickSource) {
		pImpl->tickSource->Destroy(env);
		pImpl->tickSource.reset();
	}
	pImpl->client.reset();
}

void Bot::ProcessWorldState(const Napi::CallbackInfo &info) {
	assert(pImpl->client);
	if (!pImpl->client) return;

	if (!pImpl->worldState) {
		auto obj = Napi::Object::New(info.Env());
		pImpl->worldState.reset(new Napi::ObjectReference(Napi::Persistent(obj)));
	}

	auto obj = pImpl->worldState->Value();
	pImpl->client->ProcessWorldState([&](const Viet::IWorldState *wst) {
		std::shared_ptr<bool> valid(new bool(true));

		// Pass instances by value instead of passing by reference to prevent bad memory access
		auto newInstanceProxy = [this](Napi::Env env, auto instances, uint32_t idx, std::shared_ptr<bool> valid) {
			Napi::Object res = Napi::Object::New(env);
			res.Set("getValue", Napi::Function::New(env, [instances, idx, valid, this](const Napi::CallbackInfo &info) {
				if (!*valid || info[0].IsString() == false) {
					return info.Env().Undefined();
				}
				auto propName = (std::string)info[0].As<Napi::String>();
				auto v = instances[idx].GetValue(propName.data());
				assert(v.value);
				auto j = pImpl->client->ToApiFormat(instances.GetEntityName(), propName.data(), v.value);
				Napi::Value result = ToJson(info.Env(), j);

				return result;
			}));
			return res;
		};

		obj.Set("getInstances", Napi::Function::New(info.Env(), [valid, wst, newInstanceProxy](const Napi::CallbackInfo &info) {
			if (!*valid || !info[0].IsString()) {
				return info.Env().Undefined();
			}
			auto entityName = (std::string)info[0].As<Napi::String>();
			auto instances = wst->GetInstances(entityName.data());
			auto res = Napi::Array::New(info.Env(), instances.GetPoolSize());
			for (uint32_t i = 0; i < res.Length(); ++i) {
				if (instances[i].IsValid()) {
					res.Set(i, newInstanceProxy(info.Env(), instances, i, valid));
				}
				else {
					res.Set(i, info.Env().Null());
				}
			}
			return (Napi::Value)res;
		}));

		auto fn = info[0].As<Napi::Function>();
		try {
			fn.Call({ obj });
		}
		catch (std::exception &e) {
			Napi::Error::New(info.Env(), e.what()).ThrowAsJavaScriptException();
		}

		*valid = false;
	});
}

void Bot::Send(const Napi::CallbackInfo &info) {
	std::string propName = info[0].As<Napi::String>();
	auto newValue = ToJson(info.Env(), info[1]);
	return this->Send(propName, newValue);
}

void Bot::Send(std::string propertyName, const Viet::json &newValue) {
	assert(pImpl->client);
	if (pImpl->client) {

		std::string myEntity;
		pImpl->client->ProcessWorldState([&](const Viet::IWorldState *wst) {
			myEntity = wst->GetMyEntity();
			assert(!myEntity.empty());
		});
		auto v = pImpl->client->FromApiFormat(myEntity.data(), propertyName.data(), newValue);
		pImpl->client->SendPropertyChange(propertyName.data(), v);
	}
}