#pragma once
#include <napi.h>

class RemoteServer : public Napi::ObjectWrap<RemoteServer> {
public:
	static Napi::Object Init(Napi::Env env, Napi::Object exports);
	RemoteServer(const Napi::CallbackInfo &info);

private:
	static Napi::FunctionReference constructor;

	void Connect(const Napi::CallbackInfo &info);
	void Connect(Napi::Env env, const char *ip, int port, const char *serverId, const char *devPass, Napi::Object frontEndFiles);

	void On(const Napi::CallbackInfo &info);
	void On(const char *eventName, Napi::Function fn);

	void Kill(const Napi::CallbackInfo &info);
	void Kill(Napi::Env env);

	Napi::Value ManipulateInstance(const Napi::CallbackInfo &info);
	Napi::Value SetAttachedInstance(const Napi::CallbackInfo &info);
	Napi::Value GetAttachedInstance(const Napi::CallbackInfo &info);
	Napi::Value FindInstance(const Napi::CallbackInfo &info);
	Napi::Value SendCustomPacket(const Napi::CallbackInfo &info);

	struct Impl;
	std::shared_ptr<Impl> pImpl;
};