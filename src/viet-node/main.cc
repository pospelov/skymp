#include <napi.h>
#include <viet/Logging.h>
#include <VietRakNet.h>
#include <iostream>

#include "assets.h"
#include "RemoteServer.h"
#include "Bot.h"

//
// Used by RemoteServer.cpp
//

Viet::Networking::Plugin GetNetworkingPlugin(std::string serverIp, uint16_t serverPort) {
	constexpr auto timeoutTimeMs = 4000;

	Viet::Networking::Plugin p;
	p.clientFactory = [=] {return new Viet::Networking::RakClient(serverIp.data(), serverPort, timeoutTimeMs); };

	return p;
}

Viet::Serialization::Plugin GetSeriPlugin() {
	Viet::Serialization::Plugin p;
	p.serializerFactory = [] {return new Viet::Serialization::RakSerializer; };
	return p;
}

Viet::Logging::Plugin GetLoggingPlugin() {
	class StdLoggerOutput : public Viet::Logging::ILoggerOutput {
	public:
		void Write(Viet::Logging::Severity severity, const char *text) noexcept override {
			std::cout << '[' << Viet::Logging::GetSeverityName(severity) << "] " << text << std::endl;
		}
	};

	Viet::Logging::Plugin p;
	p.loggerFactory = [] {return new StdLoggerOutput; };
	return p;
}

//

Napi::Object Init(Napi::Env env, Napi::Object exports) {
	RemoteServer::Init(env, exports);
	Bot::Init(env, exports);

	auto eval = env.Global().Get("eval").As<Napi::Function>();;

	const auto patchNames = { "common/patch.js" };
	for (auto patchName : patchNames) {
		auto [ptr, size] = assets_get_file(patchName);
		eval.Call({ Napi::String::New(env, std::string(ptr, size)) });
	}

	auto applyPatch = env.Global().Get("applyPatch").As<Napi::Function>();
	exports = applyPatch.Call({ exports }).As<Napi::Object>();

	return exports;
}

NODE_API_MODULE(skymp, Init)
