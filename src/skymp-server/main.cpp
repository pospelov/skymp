#include <iostream>
#include <fstream>
#include <filesystem>
#include <stdexcept>
#include <mutex>
#include <thread>
#include <ctime>

#include <viet/Server.h>
#include <VietRakNet.h>
#include <VietMongocxx.h>

#include <skymp-common/Entities.h>

namespace {
	class StdLoggerOutput : public Viet::Logging::ILoggerOutput {
	public:
		void Write(Viet::Logging::Severity severity, const char *text) noexcept override {
			// Since there will be more than one instance of the logger class
			static std::mutex m;
			std::lock_guard l(m);

			if (Viet::Logging::GetSeverityName(severity) == std::string("Debug")) return;

			std::cout << "[" << Viet::Logging::GetSeverityName(severity) << "] " << text << std::endl;
		}
	};

	struct SkympServerSettings {
		std::string serverId;
		std::string devPassword;
		int maxPlayers = 0;
		int port = 0;
		std::string mongoUri;
		int gamemodesPort = 7000;

		SkympServerSettings() = default;

		SkympServerSettings(const Viet::json &j) {
			this->serverId = j.at("serverId").get<std::string>();
			this->devPassword = j.at("devPassword").get<std::string>();
			this->maxPlayers = getInt(j.at("maxPlayers"));
			this->port = getInt(j.at("port"));
			this->mongoUri = j.at("mongoUri").get<std::string>();
			if (j.count("gamemodesPort")) {
				this->gamemodesPort = getInt(j.at("gamemodesPort"));
			}
		}

		int getInt(const Viet::json &j) {
			if (j.is_string()) {
				std::string s = j.get<std::string>();
				bool isInt = !s.empty() && s[0] != '0' && s.find_first_not_of("0123456789") == std::string::npos;
				if (!isInt) {
					throw std::runtime_error("'" + s + "' is not a valid integer");
				}
				return atoi(s.data());
			}
			return j.get<int>();
		}
	};
}

int main(int argc, const char *argv[]) {
	try {
		const std::string defaultSettingsFile = "C:/projects/skympServerSettingsDefault.json";

		std::filesystem::path p = argc < 2 ? defaultSettingsFile : argv[1];

		if (argc < 2 && std::filesystem::exists(defaultSettingsFile) == false) {
			throw std::runtime_error("No settings file specified, " + defaultSettingsFile + "does not exist");
		}

		if (defaultSettingsFile == p) {
			printf("Using the default settings file\n");
		}

		if (!std::filesystem::exists(p)) {
			throw std::runtime_error("File " + p.string() + " doesn't exist");
		}

		std::ifstream t(p);
		auto j = Viet::json::parse(std::string((std::istreambuf_iterator<char>(t)),
			std::istreambuf_iterator<char>()));

		SkympServerSettings sss(j);

		int timeoutTimeMs = 4000;
		int gmTimeoutTimeMs = 4000;
		int maxGamemodes = 10;

		if (sss.port == sss.gamemodesPort) {
			throw std::runtime_error("port and gamemodesPort must not be same");
		}

		Viet::Server::Settings settings;
		settings.devPassword = (new std::string(sss.devPassword))->data();
		settings.maxPlayers = sss.maxPlayers;
		settings.entities = SkympEntities::GetEntities();
		settings.netPlugin.serverFactory = [=] {
			return new Viet::Networking::RakServer(settings.maxPlayers, sss.port, timeoutTimeMs);
		};
		settings.gamemodeNetPlugin.serverFactory = [=] {
			return new Viet::Networking::RakServer(maxGamemodes, sss.gamemodesPort, gmTimeoutTimeMs);
		};
		settings.seriPlugin.serializerFactory = [] {
			return new Viet::Serialization::RakSerializer();
		};
		settings.loggingPlugin.loggerFactory = [] {
			return new StdLoggerOutput;
		};
		settings.nosqlPlugin.databaseFactory = [=] {
			const std::string dbName = "skymp_db_" + sss.serverId;

			Viet::NoSqlDatabase::Mongo::Settings s;
			s.mongoUri = sss.mongoUri;
			s.dbName = dbName;
			s.startsWith = "skymp_db_test_server";
			s.dbIgnore = { dbName };
			s.logger.reset(new Viet::Logging::Logger(new StdLoggerOutput));

			auto db = new Viet::NoSqlDatabase::Mongo(s);
			return db;
		};

		StdLoggerOutput l;
		Viet::Server server(settings);
		clock_t activeGmMoment = clock();

		while (1) {
			std::this_thread::sleep_for(std::chrono::milliseconds(1));
			server.Tick();
			int n = server.GetNumActiveGamemodes();
			if (n > 0) activeGmMoment = clock();

			constexpr int timeoutSeconds = 5;
			if (clock() - activeGmMoment > timeoutSeconds * CLOCKS_PER_SEC) {
				std::string s = "There was no active gamemode for " +  std::to_string(timeoutSeconds) + " seconds, stopping the server";
				l.Write(Viet::Logging::Severity::Notice, s.data());
				break;
			}

		}
		l.Write(Viet::Logging::Severity::Notice, "The server exited");
		return 0;
	}
	catch (std::exception &e) {
		std::cerr << "Unhandled exception: " << e.what() << std::endl;
		return 1;
	}
}
