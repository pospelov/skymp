#
# Prepare Frida
#

set(FRIDA_INSTALLER_PATH "${CMAKE_CURRENT_BINARY_DIR}/frida-gumjs/frida-gumjs-devkit.exe")
set(FRIDA_LIB_PATH "${CMAKE_CURRENT_BINARY_DIR}/frida-gumjs/frida-gumjs.lib")
get_filename_component(FRIDA_INSTALLER_DIR ${FRIDA_INSTALLER_PATH} DIRECTORY)

message(STATUS "[Frida] trying to find Frida on your machine")
if(NOT EXISTS ${FRIDA_LIB_PATH})
  message(STATUS "[Frida] not found. installing")
  file(DOWNLOAD
    "https://github.com/frida/frida/releases/download/12.2.25/frida-gumjs-devkit-12.2.25-windows-x86.exe"
    ${FRIDA_INSTALLER_PATH}
  )
  execute_process(COMMAND ${FRIDA_INSTALLER_PATH} WORKING_DIRECTORY ${FRIDA_INSTALLER_DIR})
else()
  message(STATUS "[Frida] found")
endif()

set(FRIDA_INCLUDE ${FRIDA_INSTALLER_DIR})
link_directories(${FRIDA_INSTALLER_DIR})

#
# Add hooks target
#
add_library(skymp_hooks SHARED "${CMAKE_CURRENT_LIST_DIR}/hooks.cpp")
target_include_directories(skymp_hooks PRIVATE ${FRIDA_INCLUDE} "${CMAKE_CURRENT_SOURCE_DIR}/src")
target_link_libraries(skymp_hooks PRIVATE assetslib_mt)
set_target_properties(skymp_hooks PROPERTIES MSVC_RUNTIME_LIBRARY "MultiThreaded")
