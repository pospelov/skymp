#include <raknet/RakNetTypes.h>
#include "VietRakNet.h"
#include "IdManager.h"

using namespace Viet::Networking;
using userid = Viet::Networking::UserId;

IdManager::IdManager(userid maxConnections_) : maxConnections(maxConnections_) {
	guidById.resize(maxConnections_, RakNet::UNASSIGNED_CRABNET_GUID);
}

userid IdManager::allocateId(const RakNet::RakNetGUID &guid) noexcept {
	if (nextId >= maxConnections) return g_invalidUserId;
	const userid res = nextId++;
	if (nextId < maxConnections) {
		while (guidById[nextId] != RakNet::UNASSIGNED_CRABNET_GUID) {
			++nextId;
			if (nextId >= maxConnections) break;
		}
	}
	guidById[res] = guid;
	idByGuid[guid] = res;
	return res;
}

void IdManager::freeId(userid id) noexcept {
	const auto guid = guidById[id];
	guidById[id] = RakNet::UNASSIGNED_CRABNET_GUID;
	idByGuid.erase(guid);
	if (id < nextId) nextId = id;
}

userid IdManager::find(const RakNet::RakNetGUID &guid) const noexcept {
	try {
		return idByGuid.at(guid);
	}
	catch (...) {
		return g_invalidUserId;
	}
}

RakNet::RakNetGUID IdManager::find(userid id) const noexcept {
	return id < maxConnections ? guidById[id] : RakNet::UNASSIGNED_CRABNET_GUID;
}