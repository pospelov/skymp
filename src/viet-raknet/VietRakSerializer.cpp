#include <cassert>
#include <raknet/RakPeer.h>
#include <raknet/BitStream.h>

#include "VietRakNet.h"

using namespace Viet::Serialization;

struct RakSerializer::Impl {
	RakNet::BitStream writeBs, readBs;
	bool readBsUsed = false;
	std::unique_ptr<RakSerializer> subSeri;
};

RakSerializer::RakSerializer() {
	pImpl.reset(new Impl);
}

void RakSerializer::ResetWrite() noexcept {
	pImpl->writeBs.Reset();
}

void RakSerializer::WriteBool(bool v) noexcept {
	pImpl->writeBs.Write(v);
}

void RakSerializer::WriteInt8(int8_t v) noexcept {
	pImpl->writeBs.Write(v);
}

void RakSerializer::WriteInt16(int16_t v) noexcept {
	pImpl->writeBs.Write(v);
}

void RakSerializer::WriteInt32(int32_t v) noexcept {
	pImpl->writeBs.Write(v);
}

void RakSerializer::WriteInt64(int64_t v) noexcept {
	pImpl->writeBs.Write(v);
}

void RakSerializer::WriteFloat(float v) noexcept {
	pImpl->writeBs.Write(v);
}

void RakSerializer::WriteDouble(double v) noexcept {
	pImpl->writeBs.Write(v);
}

void RakSerializer::WriteString(const char *str) noexcept {
	auto n = (uint32_t)strlen(str);
	assert(n <= g_maxStr);
	if (n > g_maxStr) n = g_maxStr;
	pImpl->writeBs.Write(n);
	for (size_t i = 0; i < n; ++i) {
		pImpl->writeBs.Write(str[i]);
	}
}

void RakSerializer::GetWrittenData(uint8_t *&outData, size_t &outLength) noexcept {
	outData = pImpl->writeBs.GetData();
	outLength = pImpl->writeBs.GetNumberOfBytesUsed();
}

void RakSerializer::ResetRead(const uint8_t *data, size_t length) noexcept {
	if (pImpl->readBsUsed) {
		pImpl->readBs.~BitStream();
	}
	new(&pImpl->readBs) RakNet::BitStream(const_cast<uint8_t *>(data), length, false);
	pImpl->readBsUsed = true;
}

bool RakSerializer::ReadBool() noexcept {
	bool res = false;
	bool ok = pImpl->readBs.Read(res);
	assert(ok);
	return res;
}

int8_t RakSerializer::ReadInt8() noexcept {
	int8_t res = 0;
	bool ok = pImpl->readBs.Read(res);
	assert(ok);
	return res;
}

int16_t RakSerializer::ReadInt16() noexcept {
	int16_t res = 0;
	bool ok = pImpl->readBs.Read(res);
	assert(ok);
	return res;
}

int32_t RakSerializer::ReadInt32() noexcept {
	int32_t res = 0;
	bool ok = pImpl->readBs.Read(res);
	assert(ok);
	return res;
}

int64_t RakSerializer::ReadInt64() noexcept {
	int64_t res = 0;
	bool ok = pImpl->readBs.Read(res);
	assert(ok);
	return res;
}

float RakSerializer::ReadFloat() noexcept {
	float res = 0;
	bool ok = pImpl->readBs.Read(res);
	assert(ok);
	return res;
}

double RakSerializer::ReadDouble() noexcept {
	double res = 0;
	bool ok = pImpl->readBs.Read(res);
	assert(ok);
	return res;
}

std::string RakSerializer::ReadString() noexcept {
	uint32_t size = 0;
	assert(size <= g_maxStr);
	if (size > g_maxStr) size = g_maxStr;
	bool ok = pImpl->readBs.Read(size);
	assert(ok);
	std::string res(size, ' ');
	for (size_t i = 0; i < size; ++i) {
		ok = pImpl->readBs.Read(res[i]);
		assert(ok);
	}
	return res;
}

ISerializer *RakSerializer::GetSubSerializer() noexcept {
	if (pImpl->subSeri == nullptr) {
		pImpl->subSeri.reset(new RakSerializer);
	}
	return pImpl->subSeri.get();
}