#pragma once
#include <memory>
#include <viet/Networking.h>
#include <viet/Serialization.h>

namespace Viet {
	namespace Networking {
		class RakClient : public IClient {
		public:
			// Throws std::runtime_error on failure
			RakClient(const char *ip, unsigned short port, int timeoutTimeMs);
			~RakClient() override;

			void Tick(const OnPacket &onPacket) noexcept override;
			void Send(const unsigned char *packet, size_t length, bool reliable) noexcept override;

		private:
			struct Impl;
			Impl *const pImpl;
		};

		class RakServer : public IServer {
		public:
			// Throws std::runtime_error on failure
			RakServer(UserId maxConnections, unsigned short port, int timeoutTimeMs);
			~RakServer() override;

			void Tick(const OnPacket &onPacket) noexcept override;
			void Send(UserId id, const unsigned char *packet, size_t length, bool reliable) noexcept override;

		private:
			struct Impl;
			Impl *const pImpl;
		};
	}

	namespace Serialization {
		class RakSerializer : public ISerializer {
		public:
			RakSerializer();

			void ResetWrite() noexcept override;
			void WriteBool(bool v) noexcept override;
			void WriteInt8(int8_t v) noexcept override;
			void WriteInt16(int16_t v) noexcept override;
			void WriteInt32(int32_t v) noexcept override;
			void WriteInt64(int64_t v) noexcept override;
			void WriteFloat(float v) noexcept override;
			void WriteDouble(double v) noexcept override;
			void WriteString(const char *str) noexcept override;
			void GetWrittenData(uint8_t *&outData, size_t &outLength) noexcept override;
			void ResetRead(const uint8_t *data, size_t length) noexcept override;
			bool ReadBool() noexcept override;
			int8_t ReadInt8() noexcept override;
			int16_t ReadInt16() noexcept override;
			int32_t ReadInt32() noexcept override;
			int64_t ReadInt64() noexcept override;
			float ReadFloat() noexcept override;
			double ReadDouble() noexcept override;
			std::string ReadString() noexcept override;

			ISerializer *GetSubSerializer() noexcept override;

		private:
			struct Impl;
			std::shared_ptr<Impl> pImpl;
		};
	}
}