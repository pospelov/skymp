#
# mongo-cxx-driver
#

find_package(libmongocxx REQUIRED)
find_package(libbsoncxx REQUIRED)
find_package(mongo-c-driver CONFIG REQUIRED)
find_package(nlohmann_json REQUIRED)

message(MONGOC ${MONGOC_LIBRARIES})
message(MONGOCSTATIC ${MONGOC_STATIC_LIBRARIES})

function(link_and_include_mongocxx TARGET_NAME)

  #foreach(var LIBMONGOCXX_LIBRARIES LIBBSONCXX_LIBRARIES MONGOC_LIBRARIES MONGOC_STATIC_LIBRARIES)
  #  message(STATUS "\n\n\n!!!!!!!! ${var}=${${var}}\n\n\n")
  #endforeach()

  target_link_libraries(${TARGET_NAME} ${LIBMONGOCXX_LIBRARIES} ${LIBBSONCXX_LIBRARIES} ${MONGOC_LIBRARIES} ${MONGOC_STATIC_LIBRARIES})

  if(UNIX)
    find_package(OpenSSL REQUIRED)
    target_link_libraries(${TARGET_NAME} OpenSSL::SSL OpenSSL::Crypto)
    target_link_libraries(${TARGET_NAME} pthread resolv rt m sasl2)
  elseif(WIN32)
    # Do nothing - mongo-cxx-driver doesn't require OpenSSL on Windows
  endif()
endfunction()

#
# viet-mongocxx
#

file(GLOB_RECURSE VIET_MONGOCXX_SOURCES "${VIET_PLUGINS_DIR}/viet-mongocxx/*")
add_library(viet-mongocxx STATIC ${VIET_MONGOCXX_SOURCES})

target_link_libraries(viet-mongocxx nlohmann_json::nlohmann_json)
target_link_libraries(viet-mongocxx ${VIET_LIBS})

target_include_directories(viet-mongocxx PRIVATE "${VIET_INCLUDE}")

link_and_include_mongocxx(viet-mongocxx)
