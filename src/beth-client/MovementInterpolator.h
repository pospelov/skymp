#pragma once
#include <beth-api/beth_api.h>
#include <skymp-common/Movement.h>
#include <viet/Logging.h>

class MovementInterpolator {
public:
	MovementInterpolator() noexcept;
	~MovementInterpolator();

	void PushMovement(const Movement &mov) noexcept;
	void TickGame(beth::IGameThrFeatures *game, const beth::Form &actor, Viet::Logging::Logger &logger) noexcept;

private:
	struct State;
	void OnMovementChange(State &state, const Movement &movWas, const Movement &movNow) noexcept;

	struct Impl;
	Impl *const pImpl;
};
