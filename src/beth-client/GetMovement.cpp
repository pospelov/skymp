#include "GetMovement.h"

Movement GetMovement(beth::IGameThrFeatures *game, const beth::Form &actor) noexcept {
	auto mov = Movement();
	mov.pos = game->GetPosition(actor);
	mov.angleZ = game->GetRotation(actor).z;
	mov.angleHeadDirection = 360 * game->GetAnimVariableFloat(actor, "Direction");

	const float speedSampled = game->GetAnimVariableFloat(actor, "SpeedSampled");

	const auto flags = game->GetActorFlags(actor);

	if (flags.isSneaking) mov.flags |= PMovementFlags::Sneaking;
	if (flags.isWeapDrawn) mov.flags |= PMovementFlags::WeapDrawn;
	if (flags.isBlocking) mov.flags |= PMovementFlags::Blocking;
	if (flags.isDead) mov.flags |= PMovementFlags::Dead;

	if (speedSampled != 0) {
		if (flags.isSprinting)
			mov.flags |= (PMovementFlags::Walking | PMovementFlags::Running);
		else if (!flags.isBlocking && flags.isRunning)
			mov.flags |= PMovementFlags::Running;
		else
			mov.flags |= PMovementFlags::Walking;

		if ((mov.flags & PMovementFlags::Walking) && !(mov.flags & PMovementFlags::Running)
			&& flags.isBlocking && flags.isSneaking) {
			mov.flags &= ~PMovementFlags::Walking;
			mov.flags |= PMovementFlags::Running;
		}

		const float carryWeight = game->GetCurrentAV(actor, ActorValue::CarryWeight);
		const float totalItemWeight = game->GetTotalItemWeight(actor);
		const bool overWeight = totalItemWeight >= carryWeight;
		if (overWeight) {
			mov.flags &= ~PMovementFlags::Running;
			mov.flags |= PMovementFlags::Walking;
		}
	}

	mov.healthPercentage = game->GetAVPercentage(actor, ActorValue::Health);

	return mov;
}