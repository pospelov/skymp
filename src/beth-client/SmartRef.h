#pragma once
#include <skymp-common/Movement.h>
#include <skymp-common/Look.h>
#include <skymp-common/Equipment.h>
#include <beth-api/beth_api.h>
#include <viet/Logging.h>

#include "WorldCleaner.h"

class SmartRefManager {
public:
	SmartRefManager() noexcept;
	~SmartRefManager();

	struct Impl; // Must be visible for SmartRef implementation
	Impl *const pImpl;
};

class SmartRef {
public:
	SmartRef(std::shared_ptr<SmartRefManager> manager, const beth::Form &optBase, const Point3D &pos) noexcept;
	~SmartRef();
	void TickGame(beth::IGameThrFeatures *game, Viet::Logging::Logger *logger, WorldCleaner *wc) noexcept;
	void SetBase(const beth::Form &optBase) noexcept;
	void SetLook(const Look &look) noexcept;
	void SetEquipment(const Equipment &eq) noexcept;
	void SetName(const std::string &name) noexcept;
	void SetDrawDistance(float newDrawDistance) noexcept;
	void PushMovement(const Movement &mov) noexcept;
	void SendAnimationEvent(const char *aeName) noexcept;
	Point3D GetNodePos(SkeletonNode node) const noexcept;
	bool HasLOS() const noexcept;

private:
	void Spawn() noexcept;
	void Despawn() noexcept;
	bool WantApplyTints() const noexcept;
	uint32_t GetMaxTintApplies() const noexcept;

	struct Impl;
	Impl *const pImpl;
};