#pragma once
#include <beth-api/beth_api.h>
#include <viet/Logging.h>

// Use only in game thread
class WorldCleaner {
public:
	WorldCleaner() noexcept;
	~WorldCleaner();
	void TickGame(beth::IGameThrFeatures *game, Viet::Logging::Logger *logger) noexcept;
	void ModProtection(uint32_t formid, int mod) noexcept;

private:
	void OnPlayerCellChange() noexcept;
	void DealWithRef(const beth::Form &ref) noexcept;

	struct Impl;
	Impl *const pImpl;
};