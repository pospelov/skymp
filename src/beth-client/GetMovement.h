#pragma once
#include <skymp-common/Movement.h>
#include <beth-api/beth_api.h>

Movement GetMovement(beth::IGameThrFeatures *game, const beth::Form &actor) noexcept;