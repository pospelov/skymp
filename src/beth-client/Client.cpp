#include <atomic>
#include <mutex>
#include <viet/Client.h>
#include <skymp-common/Entities.h>

#include <beth-client/SmartRef.h>
#include <beth-client/GetMovement.h>

#include "Client.h"

namespace {
	struct Actor {
		std::unique_ptr<SmartRef> smartRef;
		uint64_t numAnimChanges = (uint64_t)-1;
		bool updated = false;
		clock_t lastPushMovement = 0;
		Point3D pos;
		bool drawnWas = false;

		struct {
			PPos::Value pos;
			PAngle::Value angle;
			PMovementFlags::Value flags;
			PHeadDirection::Value angleHeadDirection;
		} lastPropVals;

		bool IsEmpty() const noexcept {
			return !smartRef;
		}
	};

	struct Me {
		uint64_t numNameChanges = 0;
		uint64_t numAnimChanges = 0;
		uint64_t numPosChanges = 0;
		uint64_t numAngleChanges = 0;
		uint64_t numMovFlagsChanges = 0;
		uint64_t numCellChanges = 0;
		uint64_t numLookChanges = 0;
		uint64_t numRaceMenuChanges = 0;
	};

	struct Game {
		Me me;
		std::shared_ptr<SmartRefManager> acManager;
		std::vector<Actor> actors;
		std::vector<Actor *> acsSorted;
		clock_t lastAcsSort = 0;
		std::unique_ptr<WorldCleaner> wc;
		Look lastLook;
		bool waitsForLook = false;
		clock_t lastSendMovement = 0;
		clock_t drawnBlocker = 0;
		bool drawnBlockerValue = false;

		Game() {
			wc.reset(new WorldCleaner);
		}

		void Tick(beth::IGameThrFeatures *game, Viet::Logging::Logger *logger) {
			assert(wc != nullptr);
			assert(logger);
			if (wc) wc->TickGame(game, logger);
			for (auto &acView : actors) {
				if (acView.smartRef) acView.smartRef->TickGame(game, logger, &*wc);
			}
		}
	};

	inline bool IsEquip(const std::string &s) {
		return (s.find("Equip") != std::string::npos || s.find("equip") != std::string::npos)
			&& s.find("Unequip") == std::string::npos && s.find("unequip") == std::string::npos;
	}

	inline bool IsUnequip(const std::string &s) {
		return (s.find("Unequip") != std::string::npos || s.find("unequip") != std::string::npos);
	}

	inline bool IsTorch(const std::string &s) {
		return s.find("torch") != std::string::npos || s.find("Torch") != std::string::npos;
	}

#ifdef WIN32
#include <Windows.h>
	__forceinline bool IsResetButtonDown() {
		return GetAsyncKeyState(0x31) & 0x8000;
	}
	__forceinline bool IsShowMeButtonDown() {
		return GetAsyncKeyState(0x32) & 0x8000;
	}
#else
	bool IsResetButtonDown() {
		return false;
	}

	bool IsShowMeButtonDown() {
		return false;
	}
#endif
}

struct Client::Impl {
	struct {
		std::atomic<bool> isKeyboardEnabled = true;
	} in;

	std::vector<Viet::Entity> entities;
	std::shared_ptr<Viet::Client> cl;
	std::shared_ptr<Viet::Logging::Logger> logger;

	std::atomic<clock_t> lastGameThrUpdate = 0;

	Game game;

	struct {
	} gameThr;

	bool showMe = false;
	bool showMeBtnDownWas = false;

	struct {
		bool isSpawningStarted = false;
	} wstProcess;

	bool isKeyboardEnabled;
};

Client::Client(std::shared_ptr<Viet::Logging::Logger> logger, std::shared_ptr<Viet::Client> client) noexcept {
	assert(logger);

	pImpl.reset(new Impl);
	pImpl->entities = SkympEntities::GetEntities();
	pImpl->cl = client;
	pImpl->logger = logger;
}

void Client::TickGame(beth::IGameThrFeatures *game, bool isFirstTick) noexcept {
	if (isFirstTick) {
		pImpl->logger->Notice("The first tick in Game thread was received");
		pImpl->isKeyboardEnabled = true;
		pImpl->game = Game();
	}

	pImpl->game.Tick(game, pImpl->logger.get());
	if (IsResetButtonDown()) {
		pImpl->game.actors.clear();
	}

	bool showMeBtnDown = IsShowMeButtonDown();
	if (pImpl->showMeBtnDownWas != showMeBtnDown) {
		pImpl->showMeBtnDownWas = showMeBtnDown;
		if (showMeBtnDown) {
			pImpl->showMe = !pImpl->showMe;
		}
	}

	pImpl->lastGameThrUpdate = clock();

	// Output
	{
		// Movement
		if (clock() - pImpl->game.lastSendMovement > CLOCKS_PER_SEC * 0.15) {
			pImpl->game.lastSendMovement = clock();
			auto mov = GetMovement(game, game->GetPlayer());
			if (clock() <= pImpl->game.drawnBlocker) {
				if (pImpl->game.drawnBlockerValue)
					mov.flags |= PMovementFlags::WeapDrawn;
				else
					mov.flags &= ~PMovementFlags::WeapDrawn;
			}
			pImpl->cl->SendPropertyChange<PPos>(PPos::Value(mov.pos));
			pImpl->cl->SendPropertyChange<PAngle>(PAngle::Value({ 0,0,mov.angleZ }));
			pImpl->cl->SendPropertyChange<PMovementFlags>(PMovementFlags::Value(mov.flags));
			pImpl->cl->SendPropertyChange<PHeadDirection>(PHeadDirection::Value(mov.angleHeadDirection));
		}
	}

	// My Input
	{
		bool showRaceMenu = false;

		pImpl->cl->ProcessWorldState([&](const Viet::IWorldState *wst) {
			auto &me = pImpl->game.me;

			if (auto name = wst->GetMyPropertyValue<PName>()) {
				auto n = wst->GetMyPropertyValue(PName::name).numChanges;
				if (n != me.numNameChanges) {
					me.numNameChanges = n;
					game->SetName(game->GetPlayer(), name->value.data());
				}
			}

			auto animEvent = wst->GetMyPropertyValue<PAnimEvent>();
			if (animEvent) {
				auto n = wst->GetMyPropertyValue(PAnimEvent::name).numChanges;
				if (n != me.numAnimChanges) {
					me.numAnimChanges = n;

					if (!isFirstTick) {
						std::string lower = animEvent->value;
						std::transform(lower.begin(), lower.end(), lower.begin(), ::tolower);

						bool ignoring = false;

						if (!ignoring) {
							game->SendAnimEvent(game->GetPlayer(), animEvent->value.data());
						}
						pImpl->logger->Info("%sSendAnimEvent %s (numChanges=%d, numChangesWas=%d)", ignoring ? "ignoring" : "", animEvent->value.data(), int(n), int(pImpl->game.me.numAnimChanges));
					}
				}
			}

			// Handle it before we handle pos and angle changes
			if (auto cellOrWorld = wst->GetMyPropertyValue<PCellOrWorld>()) {
				auto n = wst->GetMyPropertyValue(PCellOrWorld::name).numChanges;
				if (n != me.numCellChanges) {
					me.numCellChanges = n;
					auto currentCell = game->GetParentCell(game->GetPlayer());
					auto currentWorld = game->GetWorldSpace(game->GetPlayer());
					if (!isFirstTick && game->GetId(currentCell) != cellOrWorld->value && game->GetId(currentWorld) != cellOrWorld->value) {
						pImpl->logger->Info("SetCellOrWorld");
						auto pos = wst->GetMyPropertyValue<PPos>();
						auto angle = wst->GetMyPropertyValue<PAngle>();
						me.numAngleChanges = wst->GetMyPropertyValue(PAngle::name).numChanges;
						me.numPosChanges = wst->GetMyPropertyValue(PPos::name).numChanges;
						if (pos && angle) {
							float angleZ = angle->z;
							game->Load(cellOrWorld->value, *pos, angleZ);
						}
					}
				}
			}

			auto pos = wst->GetMyPropertyValue<PPos>();
			if (pos) {
				auto n = wst->GetMyPropertyValue(PPos::name).numChanges;
				if (n != me.numPosChanges) {
					me.numPosChanges = n;
					if (!isFirstTick) {
						pImpl->logger->Info("SetPos");
						const auto distance = (game->GetPosition(game->GetPlayer()) - *pos).Length();
						if (distance > 1024) {
							game->SetPosition(game->GetPlayer(), *pos);
						}
						else {
							const auto rot = game->GetRotation(game->GetPlayer());
							game->TranslateTo(game->GetPlayer(), *pos, game->GetRotation(game->GetPlayer()), 90'000, 0);
						}
					}
				}
			}

			if (pos && (*pos - game->GetPosition(game->GetPlayer())).Length() < 64) {
				auto angle = wst->GetMyPropertyValue<PAngle>();
				if (angle) {
					auto n = wst->GetMyPropertyValue(PAngle::name).numChanges;
					if (n != me.numAngleChanges) {
						me.numAngleChanges = n;
						if (!isFirstTick) {
							pImpl->logger->Info("SetAngle");
							game->SetRotation(game->GetPlayer(), *angle);
						}
					}
				}
			}

			if (auto movFlags = wst->GetMyPropertyValue<PMovementFlags>()) {
				auto n = wst->GetMyPropertyValue(PMovementFlags::name).numChanges;
				if (n != me.numMovFlagsChanges) {
					me.numMovFlagsChanges = n;

					// Other flags are not implemented
					if (!isFirstTick) {
						if (movFlags->value & PMovementFlags::WeapDrawn) {
							game->SetWeaponDrawn(game->GetPlayer(), 1);
						}
						else {
							game->SetWeaponDrawn(game->GetPlayer(), 0);
						}
					}
				}
			}

			if (auto raceMenu = wst->GetMyPropertyValue<PIsRaceMenuOpen>()) {
				auto n = wst->GetMyPropertyValue(PIsRaceMenuOpen::name).numChanges;
				if (n != me.numRaceMenuChanges) {
					me.numRaceMenuChanges = n;

					if (raceMenu->value) {
						pImpl->logger->Info("ShowRaceMenu");
						showRaceMenu = true;

						// ShowRaceMenu and ApplyLook in the same time will result in a crash
						me.numLookChanges = wst->GetMyPropertyValue(PLook::name).numChanges;
					}
				}
			}

			if (auto look = wst->GetMyPropertyValue<PLook>()) {
				auto n = wst->GetMyPropertyValue(PLook::name).numChanges;
				if (n != me.numLookChanges) {
					me.numLookChanges = n;
					auto lookWas = game->GetLook(game->GetPlayer());
					if (lookWas != *look) {
						pImpl->logger->Info("Applying new look (numTints=%u) instead of old look (numTints=%u)", look->tints.size(), lookWas.tints.size());
						game->ApplyLookInRuntime(*look, game->GetPlayer());
						pImpl->logger->Info("The look has applied successfully");
					}
					else {
						pImpl->logger->Info("Skipping applying new look. Old and new versions are the same");
					}
				}
			}
		});

		if (showRaceMenu) {
			game->ShowRaceMenu();
		}
	}

	// Input 
	{
		pImpl->cl->ProcessWorldState([&](const Viet::IWorldState *wst) {
			for (auto &acView : pImpl->game.actors) {
				acView.updated = false;
			}

			auto actors = wst->GetInstances(SkympEntities::actorEntityName);

			int n = 0;
			for (size_t id = 0; id < actors.GetPoolSize(); ++id) {
				auto &acModel = actors[id];
				if (!acModel.IsValid()) continue;

				std::optional<PPos::Value> pPropPos;
				if (auto ptr = acModel.GetValue<PPos>()) {
					pPropPos = *ptr;
				}

				if (acModel.IsMe()) {
					if (pImpl->showMe && pPropPos) {
						*pPropPos += {128, 128, 0};
					}
					if (!pImpl->showMe) {
						if (pImpl->game.actors.size() > id) {
							pImpl->game.actors[id] = {};
						}
					}
				}
				if (acModel.IsMe() && !pImpl->showMe) continue;
				if (auto pPropRaceMenuOpen = acModel.GetValue<PIsRaceMenuOpen>()) {
					if (pPropRaceMenuOpen->value) {
						// Actors with active RaceSexMenu should not be visible
						continue;
					}
				}

				auto pPropAngle = acModel.GetValue<PAngle>();
				auto pPropMovementFlags = acModel.GetValue<PMovementFlags>();
				auto pPropAngleHeadDirection = acModel.GetValue<PHeadDirection>();
				if (!pPropPos) continue;
				if (!pPropAngle) {
					static PAngle::Value g_defaultAngle;
					pPropAngle = &g_defaultAngle;
				}
				if (!pPropMovementFlags) {
					static PMovementFlags::Value g_movementFlags;
					pPropMovementFlags = &g_movementFlags;
				}
				if (!pPropAngleHeadDirection) {
					static PHeadDirection::Value g_headDir;
					pPropAngleHeadDirection = &g_headDir;
				}


				if (pImpl->game.actors.size() <= id) {
					pImpl->game.actors.resize(id + 1);
				}
				auto &acView = pImpl->game.actors[id];
				if (acView.IsEmpty()) {
					acView = {}; // Reset variables to default values
					if (!pImpl->game.acManager) {
						pImpl->game.acManager.reset(new SmartRefManager);
					}
					acView.smartRef.reset(new SmartRef(pImpl->game.acManager, game->GetDefaultNpc(), *pPropPos));
				}
				assert(acView.smartRef != nullptr);
				if (!acView.smartRef) continue;
				acView.updated = true;
				n++;

				// Look
				auto pPropLook = acModel.GetValue<PLook>();
				if (pPropLook) {
					if (acView.smartRef) {
						acView.smartRef->SetLook(*pPropLook);
					}
				}

				// Name
				auto pPropName = acModel.GetValue<PName>();
				if (pPropName) {
					if (acView.smartRef) {
						acView.smartRef->SetName(pPropName->value);
					}
				}

				// Animations
				auto pPropAnim = acModel.GetValue<PAnimEvent>();
				if (pPropAnim) {
					auto n = acModel.GetValue(PAnimEvent::name).numChanges;
					if (n != acView.numAnimChanges) {
						acView.numAnimChanges = n;
						if (acView.smartRef && pPropAnim->value.size() > 0) {
							acView.smartRef->SendAnimationEvent(pPropAnim->value.data());
						}
					}
				}

				// Movement
				acView.pos = *pPropPos;

				const bool somethingChanged = acView.lastPropVals.angle != *pPropAngle
					|| acView.lastPropVals.pos != *pPropPos
					|| acView.lastPropVals.angleHeadDirection != *pPropAngleHeadDirection
					|| acView.lastPropVals.flags != *pPropMovementFlags;

				acView.lastPropVals.angle = *pPropAngle;
				acView.lastPropVals.pos = *pPropPos;
				acView.lastPropVals.angleHeadDirection = *pPropAngleHeadDirection;
				acView.lastPropVals.flags = *pPropMovementFlags;

				const auto mov = Movement(*pPropPos, *pPropAngle, *pPropAngleHeadDirection, *pPropMovementFlags);
				if (somethingChanged) {
					acView.smartRef->PushMovement(mov);
					acView.lastPushMovement = clock();
					const bool drawn = (mov.flags & PMovementFlags::WeapDrawn) != 0;
					if (drawn != acView.drawnWas) {
						acView.drawnWas = drawn;
						pImpl->logger->Info("SetWeapDrawn: %d", drawn);
					}
				}
				if (clock() - acView.lastPushMovement > CLOCKS_PER_SEC) {
					acView.smartRef->PushMovement(mov);
					acView.lastPushMovement = clock();
				}
			}

			int spawnedLimit = 100;
			bool sizeChanged = pImpl->game.acsSorted.size() != pImpl->game.actors.size();
			if (n > spawnedLimit || sizeChanged) {
				auto &acsSorted = pImpl->game.acsSorted;
				if (sizeChanged || clock() - pImpl->game.lastAcsSort > 1.5 * CLOCKS_PER_SEC) {
					pImpl->game.lastAcsSort = clock();

					acsSorted.resize(pImpl->game.actors.size());
					for (size_t i = 0; i < pImpl->game.actors.size(); ++i) {
						acsSorted[i] = &pImpl->game.actors[i];
					}
					const auto pcPos = game->GetPosition(game->GetPlayer());
					std::sort(acsSorted.begin(), acsSorted.end(), [&pcPos](const Actor *lhs, const Actor *rhs) {
						const float lDistance = (lhs && lhs->updated) ? (lhs->pos - pcPos).Length() : std::numeric_limits<float>::infinity();
						const float rDistance = (rhs && rhs->updated) ? (rhs->pos - pcPos).Length() : std::numeric_limits<float>::infinity();
						return lDistance < rDistance;
					});
				}
				for (int i = 0; i < acsSorted.size(); ++i) {
					if (i >= spawnedLimit && acsSorted[i]) {
						*acsSorted[i] = Actor();
					}
				}
			}
		});
	}

	// Disable Movement if keyboard is disabed
	const bool dest = pImpl->in.isKeyboardEnabled;
	if (pImpl->isKeyboardEnabled != dest) {
		pImpl->isKeyboardEnabled = dest;

		const bool controlEnabled = dest;
		const static std::vector<beth::Control> controls = {
			beth::Control::Movement
		};
		for (auto control : controls) {
			game->SetControlEnabled(control, controlEnabled, "Keyboard Enable Disable (Blah-Blah)");
		}
	}
}

void Client::OnPlayerAnimation(beth::IGameThrFeatures *game, const char *animEventName) noexcept {
	if (IsEquip(animEventName) && !IsTorch(animEventName)) {
		pImpl->game.lastSendMovement = 0;
		pImpl->game.drawnBlocker = clock() + int(0.85 * CLOCKS_PER_SEC);
		pImpl->game.drawnBlockerValue = true;
		pImpl->logger->Info("IsEquip('%s') == true", animEventName);
	}
	else if (IsUnequip(animEventName) && !IsTorch(animEventName)) {
		pImpl->game.lastSendMovement = 0;
		pImpl->game.drawnBlocker = clock() + int(0.85 * CLOCKS_PER_SEC);
		pImpl->game.drawnBlockerValue = false;
		pImpl->logger->Info("IsUnequip('%s') == true", animEventName);
	}
	else {
		pImpl->cl->SendPropertyChange<PAnimEvent>(PAnimEvent::Value(animEventName));
	}
}

void Client::OnRaceMenuExit(beth::IGameThrFeatures *game, bool lookChanged) noexcept {
	pImpl->logger->Info("OnRaceMenuExit (lookChanged=%d)", lookChanged);
	pImpl->cl->SendPropertyChange<PIsRaceMenuOpen>(PIsRaceMenuOpen::Value(false));

	if (lookChanged) {
		auto look = game->GetLook(game->GetPlayer());
		pImpl->cl->SendPropertyChange<PLook>(look);

		pImpl->logger->Info("Sending a new look to the server (numTints=%u)", look.tints.size());
	}
}

void Client::TickHelper(beth::IHelperThrFeatures *helper, bool isFirstTick) noexcept {
	if (isFirstTick) {
		pImpl->logger->Notice("The first tick in Helper thread was received");
		//helper->Load(0x3c, { 0,0,0 }, 180);
	}

	if (clock() - pImpl->lastGameThrUpdate > CLOCKS_PER_SEC / 5) {
		pImpl->cl->ProcessWorldState([&](const Viet::IWorldState *wst) {
			auto myPos = wst->GetMyPropertyValue<PPos>();
			auto myCell = wst->GetMyPropertyValue<PCellOrWorld>();
			auto myAngle = wst->GetMyPropertyValue<PAngle>();

			if (myPos && myCell) {
				if (!pImpl->wstProcess.isSpawningStarted) {
					pImpl->wstProcess.isSpawningStarted = true;
					helper->Load(myCell->value, *myPos, myAngle ? myAngle->z : 0);

					pImpl->logger->Notice("myPos %f %f %f", myPos->x, myPos->y, myPos->z);
				}
			}

			if (auto raceMenu = wst->GetMyPropertyValue<PIsRaceMenuOpen>()) {
				if (!raceMenu->value && helper->IsMenuOpen(beth::Menu::RaceSexMenu)) {
					helper->SetMenuOpen(beth::Menu::RaceSexMenu, false);
				}
			}
		});
	}

	pImpl->cl->Tick();
}

void Client::OnKeyboardEnableDisable(bool isEnabled) noexcept {
	pImpl->in.isKeyboardEnabled = isEnabled;
}