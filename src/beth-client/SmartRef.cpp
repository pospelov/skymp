#include <cassert>
#include <ctime>
#include <optional>
#include <third-party/MakeID.h>

#include "SmartRef.h"
#include "MovementInterpolator.h"

enum TintDelays {
	GLOBAL_DELAY_MS = 250,
	SPAWN_DELAY_TICKS = 40,
	TICKS_3D_ISNT_LOADED = 100
};

constexpr auto g_spawnsIntervalMs = -1;

namespace {
	class ProtectionGuard {
	public:
		ProtectionGuard(uint32_t id_, WorldCleaner *wc_) : id(id_), wc(wc_) {
			assert(id > 0);
			assert(wc);
			if (wc && id) {
				wc->ModProtection(id, 1);
			}
		}

		~ProtectionGuard() {
			if (wc && id) {
				wc->ModProtection(id, -1);
			}
		}

	private:
		const uint32_t id;
		WorldCleaner *const wc;
	};

	struct SmartRefInfo {
		float importanceFactor = -1;
		bool wantApplyTints = false;
		bool valid = false;
	};

	using NpcRecycle = std::vector<beth::Form>;

	class RecycleGuard {
	public:
		RecycleGuard(NpcRecycle *parent_, beth::Form form_) : parent(parent_), form(form_) {
			assert(parent_ != nullptr);
			assert(form_.IsEmpty() == false);
		}

		~RecycleGuard() {
			if (!form.IsEmpty() && parent) {
				parent->push_back(form);
			}
		}

	private:
		NpcRecycle *const parent;
		const beth::Form form;
	};
}

struct SmartRefManager::Impl {
	std::unique_ptr<MakeID> makeId;
	clock_t tintsRequest = 0;
	clock_t tintsApply = 0;
	clock_t lastSpawn = 0;
	std::vector<SmartRefInfo> smartRefs;
	uint32_t topId = ~0;
	uint32_t numRefs = 0;
	uint32_t queueSize = 0;

	NpcRecycle npcRecycle;
};

SmartRefManager::SmartRefManager() noexcept : pImpl(new Impl) {
	pImpl->makeId.reset(new MakeID(UINT32_MAX));
}

SmartRefManager::~SmartRefManager() {
	delete pImpl;
}

using Ticks = uint64_t;

namespace {
	struct SpawnedState {
		std::string nameApplied;
		bool lookChanged = false;
		bool lookChangedHard = false;
		bool tintsApplied = true; // means doesn't need to be applied
		bool damageDisabled = false;
		clock_t lastEqUpdate = clock() + CLOCKS_PER_SEC; // don't update the equipment until 1s after spawn
	};
}

struct SmartRef::Impl {
	std::shared_ptr<SmartRefManager> manager;
	uint32_t managedId = ~0;

	beth::IGameThrFeatures *game = nullptr;
	Viet::Logging::Logger *logger = nullptr;
	WorldCleaner *wc = nullptr;

	Point3D pos;
	beth::Form ref, base;
	std::optional<Look> look;
	Equipment eq;
	std::string name;
	std::unique_ptr<ProtectionGuard> protection;
	std::unique_ptr<RecycleGuard> recycleGuard;
	std::unique_ptr<MovementInterpolator> mi;
	SpawnedState spawnedState;
	float drawDistance = 7000.f;
	Ticks spawnMoment = 0;
	Ticks now = 0;
	uint32_t numTintApplies = 0;
	bool hasLos = false;
	clock_t lastHasLosCall = 0;
};

SmartRef::SmartRef(std::shared_ptr<SmartRefManager> manager, const beth::Form &optBase, const Point3D &pos) noexcept : pImpl(new Impl) {
	assert(manager);
	pImpl->manager = manager;
	pImpl->pos = pos;
	pImpl->base = optBase;
	
	assert(manager->pImpl != nullptr);
	assert(manager->pImpl->makeId != nullptr);
	if (manager->pImpl && manager->pImpl->makeId) {
		const bool success = manager->pImpl->makeId->CreateID(pImpl->managedId);
		assert(success);
		if (success) {
			if (manager->pImpl->smartRefs.size() <= pImpl->managedId) {
				manager->pImpl->smartRefs.resize(pImpl->managedId + 1);
			}
			auto &inf = manager->pImpl->smartRefs[pImpl->managedId];
			inf = SmartRefInfo();
			inf.valid = true;
		}
	}
}

SmartRef::~SmartRef() {
	if (pImpl->manager && pImpl->manager->pImpl && pImpl->manager->pImpl->makeId) {
		const bool success = pImpl->manager->pImpl->makeId->DestroyID(pImpl->managedId);
		assert(success);
		auto &refs = pImpl->manager->pImpl->smartRefs;
		assert(refs.size() > pImpl->managedId);
		if (refs.size() > pImpl->managedId) {
			refs[pImpl->managedId] = SmartRefInfo();
		}
		while (!refs.empty() && !refs.back().valid) {
			refs.pop_back();
		}
	}
	if (pImpl->logger) { // logger will be nullptr here if Tick() wasn't called
		pImpl->logger->Notice("SmartRef dtor");
	}
	delete pImpl;
}

void SmartRef::TickGame(beth::IGameThrFeatures *game, Viet::Logging::Logger *logger, WorldCleaner *wc) noexcept {
	const auto pcPos = game->GetPosition(game->GetPlayer());

	pImpl->game = game;
	pImpl->logger = logger;
	pImpl->wc = wc;
	pImpl->now++;
	assert(pImpl->game != nullptr);
	assert(pImpl->logger != nullptr);
	assert(pImpl->wc != nullptr);
	if (pImpl->game && pImpl->logger && pImpl->wc) {
		const Point3D pcPos = game->GetPosition(game->GetPlayer());
		const auto distance = (pImpl->pos - pcPos).QuickLength();

		if (!pImpl->ref) {
			if (distance < pImpl->drawDistance) {
				bool forceSpawn = false;

				assert(pImpl->manager != nullptr);
				assert(pImpl->manager->pImpl != nullptr);
				if (!pImpl->manager || !pImpl->manager->pImpl) {
					forceSpawn = true;
				}
				else {
					auto manager = pImpl->manager->pImpl;
					if (clock() - manager->lastSpawn > g_spawnsIntervalMs) {
						manager->lastSpawn = clock();
						forceSpawn = true;
					}
				}

				if (forceSpawn) {
					this->Spawn();
				}
			}
		}
		else {
			const auto base = game->GetBase(pImpl->ref);
			const auto world = game->GetWorldSpace(pImpl->ref);
			const auto pcWorld = game->GetWorldSpace(game->GetPlayer());
			const auto cell = game->GetParentCell(pImpl->ref);
			const auto pcCell = game->GetParentCell(game->GetPlayer());
			const bool pcInInterior = game->IsInterior(pcCell);

			if (distance > pImpl->drawDistance) {
				logger->Info("Despawning: too much distance");
				this->Despawn();
			}
			else if (game->Exist(pImpl->ref) == false) {
				logger->Info("Despawning: doesn't exist");
				this->Despawn();
			}
			else if (pImpl->base && game->GetId(pImpl->base) != game->GetId(base)) {
				logger->Info("Despawning: base changed from %x to %x", 
					game->GetId(pImpl->base), game->GetId(base));
				this->Despawn();
			}
			else if (pImpl->spawnedState.lookChanged) {
				pImpl->spawnedState.lookChanged = false;
				pImpl->spawnedState.lookChangedHard = false;
				if (pImpl->look && !pImpl->spawnedState.lookChangedHard) {
					game->ApplyLookInRuntime(*pImpl->look, pImpl->ref);
					game->ApplyLookTints(*pImpl->look, pImpl->ref);
				}
				else {
					logger->Info("Despawning: look changed");
					this->Despawn();
				}
			}
			else if (game->GetId(world) != game->GetId(pcWorld)) {
				logger->Info("Despawning: different worlds (world=%x, pcWorld=%x)", 
					game->GetId(world), game->GetId(pcWorld));
				this->Despawn();
			}
			else if (pcInInterior && game->GetId(cell) != game->GetId(pcCell)) {
				logger->Info("Despawning: different interior cells (cell=%x, pcCell=%x)",
					game->GetId(cell), game->GetId(pcCell));
				this->Despawn();
			}
			else if (pImpl->now - pImpl->spawnMoment > TICKS_3D_ISNT_LOADED && !game->Is3DLoaded(pImpl->ref)) {
				logger->Info("Despawning: 3D isn't loaded");
				this->Despawn();
			}
			else {
				if (pImpl->mi) {
					pImpl->mi->TickGame(game, pImpl->ref, *logger);
				}
				if (clock() - pImpl->spawnedState.lastEqUpdate > 0.5 * CLOCKS_PER_SEC) {
					pImpl->spawnedState.lastEqUpdate = clock();
					pImpl->game->ApplyEquipment(pImpl->eq, pImpl->ref);
				}
				if (pImpl->name != pImpl->spawnedState.nameApplied) {
					pImpl->spawnedState.nameApplied = pImpl->name;
					game->SetName(pImpl->ref, pImpl->name.data());
					logger->Debug("|||%s|||", pImpl->name.data());
				}
				
				assert(pImpl->manager != nullptr);
				assert(pImpl->manager->pImpl != nullptr);
				if (!pImpl->manager || !pImpl->manager->pImpl) return;
				auto manager = pImpl->manager->pImpl;

				assert(manager->smartRefs.size() > pImpl->managedId);
				if (manager->smartRefs.size() > pImpl->managedId) {
					auto &inf = manager->smartRefs[pImpl->managedId];
					inf.wantApplyTints = this->WantApplyTints();

					float f = (1.f / (pImpl->pos - pcPos).Length()); // Lower distance => greater factor
					f /= pImpl->numTintApplies + 1;
					f /= pImpl->hasLos ? 1 : 3;
					inf.importanceFactor = f;
				}

				if (clock() - manager->tintsRequest > GLOBAL_DELAY_MS) {
					uint32_t bestId = ~0;
					manager->queueSize = 0;
					manager->numRefs = 0;
					float bestFactor = -1;
					for (uint32_t i = 0; i != (uint32_t)manager->smartRefs.size(); ++i) {
						auto &inf = manager->smartRefs[i];
						if (inf.valid) {
							manager->numRefs++;
							if (inf.wantApplyTints) {
								manager->queueSize++;
								if (inf.importanceFactor > bestFactor) {
									bestFactor = inf.importanceFactor;
									bestId = i;
								}
							}
						}
					}
					manager->topId = bestId;
					manager->tintsRequest = clock();
				}

				if (clock() - manager->tintsApply > GLOBAL_DELAY_MS) {
					if (manager->topId == pImpl->managedId && this->WantApplyTints()) {
						game->ApplyLookTints(*pImpl->look, pImpl->ref);
						pImpl->logger->Info("ApplyLookTints (N=%u, queueSize=%u)", manager->numRefs, manager->queueSize);
						pImpl->spawnedState.tintsApplied = true;
						pImpl->numTintApplies++;
						manager->tintsApply = clock();
					}
				}
				if (!pImpl->spawnedState.damageDisabled) {
					pImpl->spawnedState.damageDisabled = true;
					game->SetCurrentAV(pImpl->ref, ActorValue::AttackDamageMult, 0.02f);
					game->SetCurrentAV(pImpl->ref, ActorValue::UnarmedDamage, 0.02f);
				}

				if (clock() - pImpl->lastHasLosCall > 0*CLOCKS_PER_SEC / 2) {
					pImpl->lastHasLosCall = clock();
					const bool hasLos = game->HasLOS(pImpl->ref);
					if (pImpl->hasLos != hasLos) {
						pImpl->hasLos = hasLos;
						if (hasLos) pImpl->spawnedState.tintsApplied = false;
					}
				}
			}
		}
	}
}

void SmartRef::SetBase(const beth::Form &optBase) noexcept {
	pImpl->base = optBase;
}

void SmartRef::SetLook(const Look &look) noexcept {
	if (!pImpl->look || look != *pImpl->look) {
		pImpl->spawnedState.lookChanged = true;

		Look was = pImpl->look ? *pImpl->look : Look();
		bool modelChanged = was.isFemale != look.isFemale || was.raceID != look.raceID || was.headparts != look.headparts;
		pImpl->spawnedState.lookChangedHard = modelChanged;

		pImpl->look = look;
		pImpl->base = {};
	}
}

void SmartRef::SetEquipment(const Equipment &eq) noexcept {
	pImpl->eq = eq;
}

void SmartRef::SetName(const std::string &name) noexcept {
	pImpl->name = name;
}

void SmartRef::SetDrawDistance(float drd) noexcept {
	pImpl->drawDistance = drd;
}

void SmartRef::PushMovement(const Movement &mov) noexcept {
	pImpl->pos = mov.pos;
	if (pImpl->mi) {
		pImpl->mi->PushMovement(mov);
	}
}

void SmartRef::SendAnimationEvent(const char *aeName) noexcept {
	if (pImpl->game && pImpl->ref) { // I think pImpl->game may be nullptr here
		pImpl->game->SendAnimEvent(pImpl->ref, aeName);
	}
}

Point3D SmartRef::GetNodePos(SkeletonNode node) const noexcept {
	if (pImpl->ref && pImpl->game) {
		return pImpl->game->GetNodeWorldPosition(pImpl->ref, node);
	}
	return Point3D(0.f, 0.f, 0.f);
}

bool SmartRef::HasLOS() const noexcept {
	return pImpl->ref && pImpl->hasLos;
}

void SmartRef::Spawn() noexcept {
	pImpl->spawnedState = {};
	if (pImpl->logger)
		pImpl->logger->Info("spawn");
	auto game = pImpl->game;
	auto refToPlaceAt = game->GetPlayer();

	const bool isActor = game->GetFormType(pImpl->base) == TESFormType::NPC;
	const auto pcCell = game->GetParentCell(game->GetPlayer());
	const bool pcInInterior = pcCell && game->IsInterior(pcCell);

	std::vector<beth::Form> refs;
	if (pcInInterior)
		refs = game->GetLoadedRefs(pcCell);
	else
		refs = game->GetLoadedRefs();

	float mDistance = 1'000.f;
	for (auto &ref : refs) {
		float distance = (game->GetPosition(ref) - (pImpl->pos)).Length();
		if (distance < mDistance) {
			mDistance = distance;
			refToPlaceAt = ref;
		}
	}


	beth::Form base = pImpl->base;
	bool baseRecyclable = false;
	if (pImpl->look) {
		assert(pImpl->manager != nullptr);
		assert(pImpl->manager->pImpl != nullptr);
		if (pImpl->manager && pImpl->manager->pImpl) {
			auto manager = pImpl->manager->pImpl;
			if (!manager->npcRecycle.empty()) {
				base = game->ApplyLook(*pImpl->look, manager->npcRecycle.back());
				baseRecyclable = true;
				manager->npcRecycle.pop_back();
				pImpl->logger->Info("Use recycled npc");
			}
			else {
				base = game->ApplyLook(*pImpl->look, {});
				baseRecyclable = true;
			}
			if (base && baseRecyclable) {
				pImpl->recycleGuard.reset(new RecycleGuard(&manager->npcRecycle, base));
			}
		}
		if (!base) {
			pImpl->logger->Error("ApplyLook returned None");
		}
	}
	if (base) {
		pImpl->game->ApplyEquipment(pImpl->eq, base);
		pImpl->ref = game->PlaceAtMe(refToPlaceAt, base);
		assert(pImpl->ref);
		if (pImpl->ref) {
			pImpl->protection.reset(
				new ProtectionGuard(game->GetId(pImpl->ref), pImpl->wc)
			);
			if (pImpl->game->GetFormType(pImpl->ref) == TESFormType::Character) {
				pImpl->mi.reset(new MovementInterpolator);
			}
			pImpl->spawnMoment = pImpl->now;
			pImpl->numTintApplies = 0;
		}
	}
}

void SmartRef::Despawn() noexcept {
	pImpl->ref = {};
	pImpl->protection.reset();
}

bool SmartRef::WantApplyTints() const noexcept {
	return !pImpl->spawnedState.tintsApplied
		&& pImpl->numTintApplies < this->GetMaxTintApplies()
		&& pImpl->look 
		&& pImpl->now - pImpl->spawnMoment > SPAWN_DELAY_TICKS;
}

uint32_t SmartRef::GetMaxTintApplies() const noexcept {
	assert(pImpl->manager != nullptr);
	assert(pImpl->manager->pImpl != nullptr);
	if (pImpl->manager && pImpl->manager->pImpl) {
		auto manager = pImpl->manager->pImpl;
		if (manager->numRefs > 200) return 2;
		return 5;
	}
	return 2;
}