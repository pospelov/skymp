#include "WorldCleaner.h"
#include <unordered_map>

struct WorldCleaner::Impl {
	beth::Form cell;
	std::unordered_map<uint32_t, int> protection;

	// use only inside TickGame
	beth::IGameThrFeatures *game = nullptr;
	Viet::Logging::Logger *logger = nullptr;
};

WorldCleaner::WorldCleaner() noexcept : pImpl(new Impl) {
}

WorldCleaner::~WorldCleaner() {
	delete pImpl;
}

void WorldCleaner::TickGame(beth::IGameThrFeatures *game, Viet::Logging::Logger *logger) noexcept {
	assert(logger);
	if (!logger) return;

	auto cell = game->GetParentCell(game->GetPlayer());
	pImpl->game = game;
	pImpl->logger = logger;
	if (game->GetId(cell) != game->GetId(pImpl->cell)) {
		logger->Info("Cell changed from %x to %x", game->GetId(pImpl->cell), game->GetId(cell));
		pImpl->cell = cell;
		this->OnPlayerCellChange();
	}
	const auto pos = game->GetPosition(game->GetPlayer());
	for (int i = 0; i != 50; ++i) {
		auto ac = game->FindRandomActor(pos, 4096);
		if (ac) {
			this->DealWithRef(ac);
		}
	}
}

void WorldCleaner::ModProtection(uint32_t formid, int mod) noexcept {
	bool erased = false;

	auto it = pImpl->protection.find(formid);
	if (it != pImpl->protection.end()) {
		it->second += mod;
		if (it->second == 0) {
			pImpl->protection.erase(it);
			erased = true;
		}
	}
	else {
		pImpl->protection[formid] = mod;
	}

	assert(pImpl->logger);
	if (pImpl->logger)
		pImpl->logger->Info("Mod protection %x %d (erased=%d)", formid, mod, int(erased));
	// Force OnPlayerCellChange() on next TickGame()
	pImpl->cell = {};
}

void WorldCleaner::OnPlayerCellChange() noexcept {
	if (!pImpl->cell) return;
	auto refs = pImpl->game->GetLoadedRefs();
	for (auto &ref : refs) {
		this->DealWithRef(ref);
	}
}

void WorldCleaner::DealWithRef(const beth::Form &ref) noexcept {
	auto game = pImpl->game;
	if (!game) return;
	if (!ref || game->GetId(ref) == game->GetId(game->GetPlayer())) return;

	const auto type = game->GetFormType(ref);
	const auto base = game->GetBase(ref);
	const auto baseType = game->GetFormType(base);
	const auto baseId = game->GetId(base);
	const auto id = game->GetId(ref);

	if (type == TESFormType::Character) {
		game->BlockActivation(ref, true);
		if (pImpl->protection[id] <= 0) {
			game->Delete(ref);
		}
		return;
	}

	if (type == TESFormType::Water) return;
	if (baseType == TESFormType::Water) return;
	if (baseId >= 0xff000000) return; // To prevent water deletion

	if (id == 0x000af671) { /* helgen exit blocker */
		return game->Delete(ref);
	}

	switch (baseType) {
	case TESFormType::Door:
	case TESFormType::Container:
		game->SetLockLevel(ref, beth::LockLevel::None);
		game->BlockActivation(ref, true);
		break;
	case TESFormType::Tree:
	case TESFormType::Flora:
	case TESFormType::Ammo:
	case TESFormType::Armor:
	case TESFormType::Book:
	case TESFormType::Potion:
	case TESFormType::Ingredient:
	case TESFormType::Key:
	case TESFormType::Misc:
	case TESFormType::SoulGem:
	case TESFormType::Weapon:
	case TESFormType::Light:
	case TESFormType::ScrollItem:
		if (baseType == TESFormType::Flora || baseType == TESFormType::Tree) {
			if (game->GetProduce(ref).IsEmpty()) {
				break;
			}
		}
		game->BlockActivation(ref, true);
		game->SetMotionType(ref, beth::MotionType::Keyframed);
		break;
	case TESFormType::MovableStatic:
		if (baseId == 0x1C0C3) {
			game->Delete(ref);
		}
		break;
	}
}