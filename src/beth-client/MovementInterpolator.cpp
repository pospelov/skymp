#include <ctime>
#include <optional>
#include <functional>
#include <cassert>
#include <algorithm>
#include <map>
#include <skymp-common/Entities.h>

#include "MovementInterpolator.h"

constexpr float g_pi = 3.14159265358979323846f;

struct MovementInterpolator::State {
	clock_t lastSneakRepair = 0;
	clock_t lastBlockRepair = 0;
	clock_t lastDrawnRepair = 0;
	bool firstChangeHealth = true;
};

struct MovementInterpolator::Impl {
	std::optional<Movement> movement;

	std::vector<std::function<void(beth::Form)>> tasks;

	clock_t lastPush = 0;
	clock_t setMotionTypeMoment = 0;
	bool lastStopLooking = false;

	State state; // To modify inside tasks only

	beth::IGameThrFeatures *game = nullptr;
	Viet::Logging::Logger *logger = nullptr;
};

MovementInterpolator::MovementInterpolator() noexcept : pImpl(new Impl) {
}

MovementInterpolator::~MovementInterpolator() {
	delete pImpl;
}

void MovementInterpolator::PushMovement(const Movement &mov) noexcept {
	if (pImpl->movement.has_value()) {
		this->OnMovementChange(pImpl->state, *pImpl->movement, mov);
	}
	else {
		pImpl->tasks.push_back([=](beth::Form ref) {
			pImpl->game->SetPosition(ref, mov.pos);
		});
	}
	pImpl->movement = mov;
	pImpl->lastPush = clock();
}

void MovementInterpolator::TickGame(beth::IGameThrFeatures *game, const beth::Form &actor, Viet::Logging::Logger &logger) noexcept {
	pImpl->game = game;
	pImpl->logger = &logger;
	if (pImpl->movement.has_value()) {
		for (auto task : pImpl->tasks) task(actor);
		pImpl->tasks.clear();
	}
}

void MovementInterpolator::OnMovementChange(State &state, const Movement &was, const Movement &now) noexcept {
	assert(pImpl->lastPush != 0);

	auto ms = clock() - pImpl->lastPush;
	auto seconds = ms / 1000.f;

	auto speed = std::max((was.pos - now.pos).Length() / seconds, 75.0f) * 0.975f;
	if ((now.flags & PMovementFlags::Sneaking) && (now.flags & PMovementFlags::Walking) && !(now.flags & PMovementFlags::Running))
		speed = 50;

	pImpl->tasks.push_back([=, &state](beth::Form ac) {
		const bool isWalking = (now.flags & PMovementFlags::Walking) && !(now.flags & PMovementFlags::Running);
		const bool isStanding = !(now.flags & PMovementFlags::Walking) && !(now.flags & PMovementFlags::Running);
		const bool isSprinting = (now.flags & PMovementFlags::Walking) && (now.flags & PMovementFlags::Running);

		const bool wasSprinting = (was.flags & PMovementFlags::Walking) && (was.flags & PMovementFlags::Running);

		pImpl->game->SetHeadTracking(ac, false);
		pImpl->game->ClearLookAt(ac);
		pImpl->game->StopCombat(ac);

		// Dead
		if (now.flags & PMovementFlags::Dead) {
			if (pImpl->game->GetActorFlags(ac).isDead == false) {
				pImpl->game->SetDeferredKill(ac, false);
				pImpl->game->Kill(ac);
			}
			return;
		}
		else {
			if (pImpl->state.firstChangeHealth || abs(now.healthPercentage - was.healthPercentage) >= 0.00001f) {
				pImpl->state.firstChangeHealth = false;
				constexpr float hpFull = 1'000'000.f;
				pImpl->game->SetBaseAV(ac, ActorValue::Health, hpFull);
				const auto currentPercentage = pImpl->game->GetAVPercentage(ac, ActorValue::Health);
				const auto destPercentage = std::max(now.healthPercentage, 0.02f);
				const auto diffPercentage = currentPercentage - destPercentage;
				pImpl->game->ModCurrentAV(ac, ActorValue::Health, -diffPercentage * hpFull);
			}
		}
		// To respawn dead actors
		if (!(now.flags & PMovementFlags::Dead) && (was.flags & PMovementFlags::Dead)) {
			pImpl->game->DisableNoWait(ac, false);
		}
		pImpl->game->SetDeferredKill(ac, true);

		const bool stay = (pImpl->game->GetPosition(ac) - now.pos).Length() <= 1.0
			&& abs(pImpl->game->GetRotation(ac).z - now.angleZ) <= 1.f;
		if (!stay) {

			auto getUnitsOffset = [](uint32_t flags) {
				if (!(flags & PMovementFlags::Walking) && !(flags & PMovementFlags::Running)) {
					return 0.f;
				}
				return 3.f;
			};

			auto getOffsetZ = [](uint32_t flags) {
				if ((flags & PMovementFlags::Walking) && (flags & PMovementFlags::Running)) {
					return 0.f;
				}
				if (flags & PMovementFlags::Walking) {
					return -512.f;
				}
				if (flags & PMovementFlags::Running) {
					return -1024.f;
				}
				return 0.f;
			};
			const Point3D offset(
				getUnitsOffset(now.flags) * std::sin(now.angleHeadDirection / 180.f * g_pi),
				getUnitsOffset(now.flags) * std::cos(now.angleHeadDirection / 180.f * g_pi),
				getOffsetZ(now.flags)
			);


			float offsetAngleZ = 0;
			if (isStanding) {
				float diff = 0; //now.angleZ - pImpl->game->GetRotation(ac).z;
				if (abs(diff) < 10) {
					pImpl->game->ClearKeepOffsetFromActor(ac);
					pImpl->game->KeepOffsetFromActor(ac, ac, { 0.f,0.f,0.f }, 0.f, 1, 1);
					offsetAngleZ = 0;
				}
				else offsetAngleZ = diff * 0.5f;
			}
			pImpl->game->KeepOffsetFromActor(ac, ac, offset, offsetAngleZ, isWalking ? 2048.f : 1.f, 1.f);


			const bool forceStopLooking = true;
			/*const float angleAbs = abs(now.angleZ - was.angleZ);
			const bool isLooking = angleAbs >= 5.0f;
			const bool stopLooking = ac && !isLooking && isStanding;
			const bool forceStopLooking = stopLooking && !pImpl->lastStopLooking;
			pImpl->lastStopLooking = stopLooking;
			if (ac && forceStopLooking) {
				pImpl->game->ClearKeepOffsetFromActor(ac);
				pImpl->game->KeepOffsetFromActor(ac, ac, { 0.f,0.f,0.f }, 0.f, 1, 1);
			}*/

			auto p = now.pos;
			auto realPos = pImpl->game->GetPosition(ac);
			auto distance = (realPos - p).Length();
			auto distanceNoZ = (Point3D(realPos.x, realPos.y, 0) -= {p.x, p.y, 0}).Length();
			auto rotSpeed = abs((was.angleZ - now.angleZ) / seconds);
			if (ac && rotSpeed < 10) rotSpeed = 0;
			if (forceStopLooking) rotSpeed = 200;

			const bool shouldTranslateActor = forceStopLooking || !isStanding
				|| distanceNoZ > 8.0 || distance > 64.0
				|| pImpl->game->GetAnimVariableBool(ac, "bInJumpState");
			if (shouldTranslateActor) {
				pImpl->game->TranslateTo(ac, p,
					{ 0.f, 0.f, now.angleZ }, (distance <= 256) ? speed : 200'000, rotSpeed);
			}
		}


		if (isSprinting) {
			pImpl->game->SendAnimEvent(ac, "SprintStart");
		}
		if (isStanding || (wasSprinting && !isSprinting)) {
			pImpl->game->SendAnimEvent(ac, "SprintStop");
		}

		// Sneaking
		if (clock() - pImpl->state.lastSneakRepair > 2000) {
			pImpl->state.lastSneakRepair = clock();
			pImpl->game->SendAnimEvent(ac, (now.flags & PMovementFlags::Sneaking) ? "SneakStart" : "SneakStop");
		}

		// Blocking
		if (clock() - pImpl->state.lastBlockRepair > 1000) {
			pImpl->state.lastBlockRepair = clock();
			pImpl->game->SendAnimEvent(ac, (now.flags & PMovementFlags::Blocking) ? "BlockStart" : "BlockStop");
		}

		// Combat
		if (clock() - pImpl->state.lastDrawnRepair > 100) {
			pImpl->state.lastDrawnRepair = clock();
			const bool isDrawnReal = ac && pImpl->game->GetActorFlags(ac).isWeapDrawn;
			const bool isWeapDrawn = now.flags & PMovementFlags::WeapDrawn;
			if (isWeapDrawn && !isDrawnReal) {
				pImpl->game->SetWeaponDrawn(ac, true);
			}
			if (!isWeapDrawn && isDrawnReal) {
				pImpl->game->SetWeaponDrawn(ac, false);
			}
		}
	});
}