#pragma once
#include <memory>
#include <viet/Logging.h>
#include <viet/Client.h>

#include <beth-api/beth_api.h>

class Client : public beth::IListener {
public:
	Client(std::shared_ptr<Viet::Logging::Logger> logger, std::shared_ptr<Viet::Client> client) noexcept;
	void TickGame(beth::IGameThrFeatures *game, bool isFirstTick) noexcept override;
	void OnPlayerAnimation(beth::IGameThrFeatures *game, const char *animEventName) noexcept override;
	void OnRaceMenuExit(beth::IGameThrFeatures *game, bool lookChanged) noexcept override;

	void TickHelper(beth::IHelperThrFeatures *helper, bool isFirstTick) noexcept override;

	void OnKeyboardEnableDisable(bool isEnabled) noexcept;

private:
	struct Impl;
	std::shared_ptr<Impl> pImpl;
};