#pragma once
#include <string>
#include <memory>
#include <vector>
#include <variant>
#include <optional>

#include <skymp-common/ActorValue.h>
#include <skymp-common/TESFormType.h>
#include <skymp-common/ActorValue.h>
#include <skymp-common/Point3D.h>
#include <skymp-common/Look.h>
#include <skymp-common/SkeletonNode.h>
#include <skymp-common/Equipment.h>

namespace beth {
	enum class Control {
		Movement = 0,
		Fighting = 1,
		CamSwitch = 2,
		Looking = 3,
		Sneaking = 4,
		Menu = 5,
		Activate = 6,
		JournalTabs = 7,
		SaveGame = 8,
		Wait = 9,
		FastTravel = 10,
		Console = 11, // Unused
		BeastForm = 12,
		COUNT = 13,
	};

	struct ActorFlags {
		bool isSneaking = false;
		bool isWeapDrawn = false;
		bool isBlocking = false;
		bool isDead = false;
		bool isSprinting = false;
		bool isRunning = false;
	};

	enum class Menu {
		Cursor,
		RaceSexMenu
	};

	enum class MouseButton {
		Left, Right, Middle
	};

	enum class LockLevel {
		None,
		Novice,
		Apprentice,
		Adept,
		Expert,
		Master,
		RequiresKey
	};

	enum MotionType {
		Keyframed,
		Dynamic
	};

	struct Form {
		Form() = default;
		Form(std::nullptr_t) {};

		Form(uint32_t id_) : id(id_) {
		}

		// For internal use
		template <class T> 
		Form(T *form) {
			this->id = form ? form->formID : 0;
		}

		uint32_t id = 0;

		bool IsEmpty() const noexcept {
			return id == 0;
		}

		operator bool() const noexcept {
			return this->IsEmpty() == false;
		}

		bool operator==(const Form &rhs) const noexcept {
			return rhs.id == this->id;
		}

		bool operator!=(const Form &rhs) const noexcept {
			return !(*this == rhs);
		}
	};

	class IGameThrFeatures;

	class IAnyThrFeatures {
	public:
		virtual bool Load(uint32_t cellOrWorld, const Point3D &pos, float degreesRotZ) noexcept = 0;
		virtual bool IsMenuOpen(Menu menu) const noexcept = 0;
		virtual void SetMenuOpen(Menu menu, bool open) const noexcept = 0;
		virtual Point3D WorldPointToScreenPoint(const Point3D &point) noexcept = 0;
	};

	class IGameThrFeatures : public IAnyThrFeatures {
	public:
		virtual void PrintNote(const char *fmt, ...) noexcept = 0;
		virtual Form GetPlayer() noexcept = 0;
		virtual Form GetParentCell(const Form &ref) noexcept = 0;
		virtual std::vector<Form> GetLoadedRefs(const Form &filterCell = {}) noexcept = 0;
		virtual TESFormType GetFormType(const Form &form) noexcept = 0;
		virtual Form GetBase(const Form &form) noexcept = 0;
		virtual uint32_t GetId(const Form &form) noexcept = 0;
		virtual void DisableNoWait(const Form &ref, bool fadeOut) noexcept = 0;
		virtual void Delete(const Form &ref) noexcept = 0;
		virtual void SetLockLevel(const Form &ref, LockLevel lockLevel) noexcept = 0;
		virtual void BlockActivation(const Form &ref, bool block) noexcept = 0;
		virtual void SetMotionType(const Form &ref, MotionType mt) noexcept = 0;
		virtual Point3D GetPosition(const Form &ref) noexcept = 0;
		virtual Point3D GetRotation(const Form &ref) noexcept = 0;
		virtual Form FindRandomActor(const Point3D &pos, float radius) noexcept = 0;
		virtual bool IsInterior(const Form &cell) noexcept = 0;
		virtual Form PlaceAtMe(const Form &refToPlaceAt, const Form &baseForm) noexcept = 0;
		virtual bool Exist(const Form &form) noexcept = 0;
		virtual Form GetWorldSpace(const Form &ref) noexcept = 0;
		virtual bool Is3DLoaded(const Form &ref) noexcept = 0;
		virtual bool GetAnimVariableBool(const Form &ref, const char *varName) noexcept = 0;
		virtual int GetAnimVariableInt(const Form &ref, const char *varName) noexcept = 0;
		virtual float GetAnimVariableFloat(const Form &ref, const char *varName) noexcept = 0;
		virtual ActorFlags GetActorFlags(const Form &ref) noexcept = 0;
		virtual float GetCurrentAV(const Form &actor, ActorValue actorValue) noexcept = 0;
		virtual void SetCurrentAV(const Form &actor, ActorValue actorValue, float newValue) noexcept = 0;
		virtual void ModCurrentAV(const Form &actor, ActorValue actorValue, float mod) noexcept = 0;
		virtual void SetBaseAV(const Form &actor, ActorValue actorValue, float newValue) noexcept = 0;
		virtual float GetAVPercentage(const Form &actor, ActorValue actorValue) noexcept = 0;
		virtual float GetTotalItemWeight(const Form &ref) noexcept = 0;
		virtual void SetPosition(const Form &ref, const Point3D &pos) noexcept = 0;
		virtual void SetRotation(const Form &ref, const Point3D &rot) noexcept = 0;
		virtual void SetHeadTracking(const Form &actor, bool headTraking) noexcept = 0;
		virtual void SetName(const Form &ref, const char *name) noexcept = 0;
		virtual void ClearLookAt(const Form &actor) noexcept = 0;
		virtual void StopCombat(const Form &actor) noexcept = 0;
		virtual void SetDeferredKill(const Form &actor, bool deferredKill) noexcept = 0;
		virtual void Kill(const Form &actor) noexcept = 0;
		virtual void KeepOffsetFromActor(const Form &actor, const Form &target, const Point3D &offset, float angleOffset, float catchupRadius, float followRadius) noexcept = 0;
		virtual void ClearKeepOffsetFromActor(const Form &actor) noexcept = 0;
		virtual void TranslateTo(const Form &ref, const Point3D &pos, const Point3D &rot, float speed, float maxRotSpeed) noexcept = 0;
		virtual void SendAnimEvent(const Form &ref, const char *animEventName) noexcept = 0;
		virtual void SetWeaponDrawn(const Form &actor, bool drawn) noexcept = 0;
		virtual Look GetLook(const Form &actor) noexcept = 0;
		virtual Form ApplyLook(const Look &look, const Form &optionalNpc) noexcept = 0;
		virtual void ApplyLookTints(const Look &look, const Form &actor) noexcept = 0;
		virtual void ApplyLookInRuntime(const Look &look, const Form &actor) noexcept = 0;
		virtual void ApplyEquipment(const Equipment &eq, const Form &npcOrActor) noexcept = 0;
		virtual bool HasLOS(const Form &ref) noexcept = 0;
		virtual void ShowRaceMenu() noexcept = 0;
		virtual void SetControlEnabled(Control control, bool enabled, const char *systemFrom) noexcept = 0;
		virtual beth::Form GetDefaultNpc() noexcept = 0;
		virtual Point3D GetNodeWorldPosition(const Form &actor, SkeletonNode node) noexcept = 0;
		virtual beth::Form GetProduce(const Form &refOrBase) noexcept = 0;
		virtual void SetSneaking(const Form &actor, bool sneaking) noexcept = 0;
	};

	class IHelperThrFeatures : public IAnyThrFeatures {
	public:
	};

	class IListener {
	public:
		// Game thread
		virtual void TickGame(IGameThrFeatures *game, bool isFirstTick) noexcept = 0;
		virtual void OnPlayerAnimation(IGameThrFeatures *game, const char *animEventName) noexcept {};
		virtual void OnRaceMenuExit(IGameThrFeatures *game, bool lookChanged) noexcept {};

		// Helper thread
		virtual void TickHelper(IHelperThrFeatures *helper, bool isFirstTick) noexcept = 0;
	};
}
