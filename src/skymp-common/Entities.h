#pragma once
#include <vector>
#include <viet/Entity.h>
#include <viet/JsonWrapper.h>

#include "Look.h"
#include "Equipment.h"
#include "Point3D.h"

template <class T>
class JsonCastsDefault {
public:
	Viet::json operator()(const T &v) const {
		return v;
	}

	T operator()(const Viet::json &v) const {
		return v;
	}
};

template <class T, class JsonCasts = JsonCastsDefault<T>>
class SimplePropertyValue : public Viet::PropertyValue<SimplePropertyValue<T, JsonCasts>> {
public:

	SimplePropertyValue() = default;
	SimplePropertyValue(const T &v_) : value(v_) {
	}

	T value;

	template <class Archive>
	void serialize(Archive &a, int) {
		a & value;
	}

	static Viet::json ToJson(const SimplePropertyValue<T> &v) {
		return JsonCasts{}(v.value);
	}

	static SimplePropertyValue<T> FromJson(const Viet::json &j) {
		SimplePropertyValue<T> res;
		res.value = JsonCasts{}(j);
		return res;
	}

	inline friend bool operator==(const SimplePropertyValue<T> &lhs, const SimplePropertyValue<T> &rhs) noexcept {
		return lhs.value == rhs.value;
	}

	inline friend bool operator!=(const SimplePropertyValue<T> &lhs, const SimplePropertyValue<T> &rhs) noexcept {
		return !(lhs == rhs);
	}
};

class PPos {
public:
	static constexpr auto name = "pos";
	using Value = Point3D;
	static Viet::Property Build();
};

class PAngle {
public:
	static constexpr auto name = "angle";
	using Value = Point3D;
	static Viet::Property Build();
};

class PLook {
public:
	static constexpr auto name = "look";
	using Value = Look;
	static Viet::Property Build();
};

class PMovementFlags {
public:
	static constexpr auto name = "movementFlags";
	using Value = SimplePropertyValue<uint8_t>;

	enum {
		Walking = 1 << 0,
		Running = 1 << 1,
		// Standing is when !Walking && !Running
		// Sprinting is whan Walking && Running

		WeapDrawn = 1 << 2,
		Blocking = 1 << 3,
		Sneaking = 1 << 4,
		Dead = 1 << 5
	};

	static Viet::Property Build();
};

class PHeadDirection {
public:
	static constexpr auto name = "headDirection";
	using Value = SimplePropertyValue<float>;
	static Viet::Property Build();
};

class PVerticalAngle {
public:
	static constexpr auto name = "verticalAngle";
	using Value = SimplePropertyValue<float>;
	static Viet::Property Build();
};

class PCellOrWorld {
public:
	static constexpr auto name = "cellOrWorld";
	using Value = SimplePropertyValue<uint32_t>;
	static Viet::Property Build();
};

class PIsEnabled {
public:
	static constexpr auto name = "isEnabled";
	using Value = SimplePropertyValue<bool>;
	static Viet::Property Build();
};

class PEquipment {
public:
	static constexpr auto name = "equipment";
	using Value = Equipment;
	static Viet::Property Build();
};

class PStorage {
public:
	static constexpr auto name = "storage";
	using Value = SimplePropertyValue<Viet::JsonWrapper>;
	static Viet::Property Build();
};

class PAnimEvent {
public:
	static constexpr auto name = "animEvent";
	using Value = SimplePropertyValue<std::string>;
	static Viet::Property Build();
};

class PName {
public:
	static constexpr auto name = "name";
	using Value = SimplePropertyValue<std::string>;
	static Viet::Property Build();
};

class PIsRaceMenuOpen {
public:
	static constexpr auto name = "isRaceMenuOpen";
	using Value = SimplePropertyValue<bool>;
	static Viet::Property Build();
};

class SkympEntities {
public:
	static constexpr auto actorEntityName = "Actor";

	static std::vector<Viet::Entity> GetEntities();
};