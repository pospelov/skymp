#pragma once
#include <cstdint>

enum class SkeletonNode : uint32_t {
	NPC_Root, //NPC Root[Root]
	NPC_LookNode, // NPC LookNode[Look]
	NPC_Translate, // NPC Translate[Pos]
	NPC_Rotate, // NPC Rotate[Rot]
	NPC_COM, // NPC COM[COM]
	NPC_Pelvis, // NPC Pelvis[Pelv]
	NPC_L_Thigh, // NPC L Thigh[LThg]
	NPC_L_Calf, // NPC L Calf[LClf]
	NPC_L_Foot, // NPC L Foot[Lft]
	NPC_R_Thigh, // NPC R Thigh[RThg]
	NPC_R_Calf, // NPC R Calf[RClf]
	NPC_R_Foot, // NPC R Foot[Rft]
	x_SkirtRBone01, // x_SkirtRBone01
	x_SkirtRBone02, // x_SkirtRBone02
	x_SkirtRBone03, // x_SkirtRBone03
	x_SkirtLBone01, // x_SkirtLBone01
	x_SkirtLBone02, // x_SkirtLBone02
	x_SkirtLBone03, // x_SkirtLBone03
	x_SkirtBBone01, // x_SkirtBBone01
	x_SkirtBBone02, // x_SkirtBBone02
	x_SkirtBBone03, // x_SkirtBBone03
	x_SkirtFBone01, // x_SkirtFBone01
	x_SkirtFBone02, // x_SkirtFBone02
	x_SkirtFBone03, // x_SkirtFBone03
	NPC_Spine, // NPC Spine[Spn0]
	NPC_Spine1, // NPC Spine1[Spn1]
	NPC_Spine2, // NPC Spine2[Spn2]
	NPC_L_Clavicle, // NPC L Clavicle[LClv]
	NPC_L_UpperArm, // NPC L UpperArm[LUar]
	NPC_L_Forearm, // NPC L Forearm[LLar]
	NPC_R_Clavicle, // NPC R Clavicle[RClv]
	NPC_R_UpperArm, // NPC R UpperArm[RUar]
	NPC_R_Forearm, // NPC R Forearm[RLar]
	AnimObjectA, // AnimObjectA
	AnimObjectB, // AnimObjectB
	NPC_Neck, // NPC Neck[Neck]
	NPC_Head, // NPC Head[Head]
	NPCEyeBone, // NPCEyeBone
	NPC_L_Hand, // NPC L Hand[LHnd]
	NPC_R_Hand, // NPC R Hand[RHnd]
	AnimObjectL, // AnimObjectL
	AnimObjectR, // AnimObjectR
	Shield, // Shield
	Weapon, // Weapon
	NPC_L_Pauldron, // NPC L Pauldron
	NPC_R_Pauldron, // NPC R Pauldron
	MagicEffectsNode, // MagicEffectsNode
	NPC_L_MagicNode, // NPC L MagicNode[LMag]
	NPC_R_MagicNode, // NPC R MagicNode[RMag]
	NPC_Head_MagicNode, // NPC Head MagicNode[Hmag]
	NPC_L_Toe0, // NPC L Toe0[LToe]
	NPC_R_Toe0, // NPC R Toe0[RToe]
	NPC_L_ForearmTwist1, // NPC L ForearmTwist1[LLt1]
	NPC_L_ForearmTwist2, // NPC L ForearmTwist2[LLt2]
	NPC_L_UpperarmTwist1, // NPC L UpperarmTwist1[LUt1]
	NPC_L_UpperarmTwist2, // NPC L UpperarmTwist2[LUt2]
	NPC_R_ForearmTwist1, // NPC R ForearmTwist1[RLt1]
	NPC_R_ForearmTwist2, // NPC R ForearmTwist2[RLt2]
	NPC_R_UpperarmTwist1, // NPC R UpperarmTwist1[RUt1]
	NPC_R_UpperarmTwist2, // NPC R UpperarmTwist2[RUt2]
	Quiver, // Quiver
	WeaponAxe, // WeaponAxe
	WeaponBack, // WeaponBack
	WeaponBow, // WeaponBow
	WeaponDagger, // WeaponDagger
	WeaponMace, // WeaponMace
	WeaponSword, // WeaponSword
	NPC_L_Finger00, // NPC L Finger00[LF00]
	NPC_L_Finger01, // NPC L Finger01[LF01]
	NPC_L_Finger02, // NPC L Finger02[LF02]
	NPC_L_Finger10, // NPC L Finger10[LF10]
	NPC_L_Finger11, // NPC L Finger11[LF11]
	NPC_L_Finger12, // NPC L Finger12[LF12]
	NPC_L_Finger20, // NPC L Finger20[LF20]
	NPC_L_Finger21, // NPC L Finger21[LF21]
	NPC_L_Finger22, // NPC L Finger22[LF22]
	NPC_L_Finger30, // NPC L Finger30[LF30]
	NPC_L_Finger31, // NPC L Finger31[LF31]
	NPC_L_Finger32, // NPC L Finger32[LF32]
	NPC_L_Finger40, // NPC L Finger40[LF40]
	NPC_L_Finger41, // NPC L Finger41[LF41]
	NPC_L_Finger42, // NPC L Finger42[LF42]
	NPC_R_Finger00, // NPC R Finger00[RF00]
	NPC_R_Finger01, // NPC R Finger01[RF01]
	NPC_R_Finger02, // NPC R Finger02[RF02]
	NPC_R_Finger10, // NPC R Finger10[RF10]
	NPC_R_Finger11, // NPC R Finger11[RF11]
	NPC_R_Finger12, // NPC R Finger12[RF12]
	NPC_R_Finger20, // NPC R Finger20[RF20]
	NPC_R_Finger21, // NPC R Finger21[RF21]
	NPC_R_Finger22, // NPC R Finger22[RF22]
	NPC_R_Finger30, // NPC R Finger30[RF30]
	NPC_R_Finger31, // NPC R Finger31[RF31]
	NPC_R_Finger32, // NPC R Finger32[RF32]
	NPC_R_Finger40, // NPC R Finger40[RF40]
	NPC_R_Finger41, // NPC R Finger41[RF41]
	NPC_R_Finger42, // NPC R Finger42[RF42]
	MIN = NPC_Root,
	MAX = NPC_R_Finger42,
	INVALID
};

const char *GetSkeletonNodeName(SkeletonNode node);
SkeletonNode GetSkeletonNodeByName(const char *name);