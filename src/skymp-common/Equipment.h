#pragma once
#include <viet/Property.h>


class Equipment : public Viet::PropertyValue<Equipment> {
public:
	Equipment() noexcept {
		special.fill(0);
	}

	std::vector<uint32_t> armor;

	enum SpecialSlot {
		RightHand = 0,
		LeftHand = 1,
		COUNT
	};
	std::array<uint32_t, SpecialSlot::COUNT> special;

	friend bool operator==(const Equipment &lhs, const Equipment &rhs) noexcept {
		return lhs.armor == rhs.armor && lhs.special == rhs.special;
	}

	friend bool operator!=(const Equipment &lhs, const Equipment &rhs) noexcept {
		return !(lhs == rhs);
	}

	template <class Archive>
	void serialize(Archive &ar, unsigned int version) {
		ar & armor & special;
	}

	static Viet::json ToJson(const Equipment &equipment);
	static Equipment FromJson(const Viet::json &j);
};