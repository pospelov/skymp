#pragma once
#include <string>

enum class ActorValue {
	Invalid = -1,
	CarryWeight,
	AttackDamageMult,
	UnarmedDamage,
	Health,

	COUNT
};

namespace ActorValueUtils {
	const char *GetActorValueName(ActorValue av) noexcept;
	ActorValue GetActorValueByName(std::string name) noexcept;
}