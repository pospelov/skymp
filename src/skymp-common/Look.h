#pragma once
#include <viet/Property.h>

struct Tint {
	uint8_t texture = 0;
	uint32_t rgba = 0;
	float alpha = 0;
	uint32_t type = 0;

	Tint() = default;

	explicit Tint(uint8_t tex, uint32_t rgba_, float a, uint32_t t) :
		texture(tex), rgba(rgba_), alpha(a), type(t) {
	}

	friend bool operator==(const Tint &rhs, const Tint &lhs) {
		return rhs.texture == lhs.texture && rhs.rgba == lhs.rgba && rhs.type == lhs.type && abs(rhs.alpha - lhs.alpha) < 0.005f;
	}

	friend bool operator!=(const Tint &rhs, const Tint &lhs) {
		return !(rhs == lhs);
	}

	template <class Archive>
	void serialize(Archive &ar, unsigned int version) {
		ar & texture & rgba & alpha & type;
	}
};

class Look : public Viet::PropertyValue<Look> {
public:
	static constexpr auto g_name = "look";
	static constexpr size_t g_maxTints = 128;
	static constexpr size_t g_maxHeadparts = 48;

	Look() noexcept {
		this->options.fill(0);
		this->presets.fill(0);
	}

	enum {
		NumOptions = 19,
		NumPresets = 4
	};

	enum class PlayableRace : int8_t {
		Invalid = -1,
		ArgonianRace,
		BretonRace,
		DarkElfRace,
		HighElfRace,
		ImperialRace,
		KhajiitRace,
		NordRace,
		OrcRace,
		RedguardRace,
		WoodElfRace,

		MIN = ArgonianRace,
		MAX = WoodElfRace
	};

	uint8_t isFemale = 0;
	PlayableRace raceID = PlayableRace::Invalid;
	uint8_t weight = 0;
	std::array<uint8_t, 3> skinRGB = { 0,0,0 };
	std::array<uint8_t, 3> hairRGB = { 0,0,0 };
	std::vector<uint32_t> headparts;
	uint32_t headTextureSet = 0;
	std::array<float, NumOptions> options;
	std::array<uint32_t, NumPresets> presets;
	std::vector<Tint> tints;

	bool IsEmpty() const noexcept {
		return this->raceID == PlayableRace::Invalid;
	}

	friend bool operator==(const Look &rhs, const Look &lhs) {
		return rhs.isFemale == lhs.isFemale
			&& rhs.raceID == lhs.raceID
			&& rhs.weight == lhs.weight
			&& rhs.skinRGB == lhs.skinRGB
			&& rhs.hairRGB == lhs.hairRGB
			&& rhs.headparts == lhs.headparts
			&& rhs.headTextureSet == lhs.headTextureSet
			&& rhs.options == lhs.options
			&& rhs.presets == lhs.presets
			&& rhs.tints == lhs.tints;
	}

	friend bool operator!=(const Look &rhs, const Look &lhs) {
		return !(rhs == lhs);
	}

	template <class Archive>
	void serialize(Archive &ar, unsigned int version) {
		ar & isFemale & raceID & weight & skinRGB & hairRGB & headparts & headTextureSet & options & presets & tints;
	}

	static Viet::json ToJson(const Look &);
	static Look FromJson(const Viet::json &);
};