#pragma once
#include <skymp-common/Entities.h>

struct Movement {
	Movement() = default;

	Movement(PPos::Value pos_, PAngle::Value rot_, PHeadDirection::Value angleHeadDirection_, PMovementFlags::Value movementFlags_) {
		this->pos = pos_;
		this->angleZ = rot_.z;
		this->angleHeadDirection = angleHeadDirection_.value;
		this->flags = movementFlags_.value;
		// TODO: health percentage
	}

	Point3D pos;
	float angleZ = 0;
	float angleHeadDirection = 0;
	uint8_t flags;
	float healthPercentage = 1.0f;
};