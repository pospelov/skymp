#include <algorithm>

#include "ActorValue.h"

const char *ActorValueUtils::GetActorValueName(ActorValue av) noexcept {
	switch (av) {
	case ActorValue::CarryWeight:
		return "CarryWeight";
	case ActorValue::AttackDamageMult:
		return "AttackDamageMult";
	case ActorValue::UnarmedDamage:
		return "UnarmedDamage";
	case ActorValue::Health:
		return "Health";
	}
	return "";
}

ActorValue ActorValueUtils::GetActorValueByName(std::string name) noexcept {
	std::transform(name.begin(), name.end(), name.begin(), tolower);
	for (int i = 0; i < (int)ActorValue::COUNT; ++i) {
		std::string thisName = GetActorValueName(ActorValue(i));
		std::transform(thisName.begin(), thisName.end(), thisName.begin(), tolower);
		if (thisName == name) {
			return ActorValue(i);
		}
	}
	return ActorValue::Invalid;
}