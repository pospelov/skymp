#include "Look.h"

using json = Viet::json;

namespace {
	const char *GetPlayableRaceName(Look::PlayableRace race) noexcept {
		const int i = (int)race;
		if (i < (int)Look::PlayableRace::MIN || i >(int)Look::PlayableRace::MAX) {
			return "Invalid";
		}
		static const char *const names[] = { "Argonian", "Breton", "DarkElf", "HighElf", "Imperial", "Khajiit", "Nord", "Orc", "Redguard", "WoodElf" };
		return names[i];
	}

	Look::PlayableRace GetPlayableRaceByName(const char *name) noexcept {
		for (int i = (int)Look::PlayableRace::MIN; i < (int)Look::PlayableRace::MAX; ++i) {
			const auto race = Look::PlayableRace(i);
			if (!strcmp(name, GetPlayableRaceName(race)))
				return race;
		}
		return Look::PlayableRace::Invalid;
	}
}

json Look::ToJson(const Look &look) {
	if (look.IsEmpty()) return nullptr;

	auto res = json::object();
	res["sex"] = look.isFemale ? "Female" : "Male";
	res["race"] = GetPlayableRaceName(look.raceID);
	res["weight"] = (float)look.weight;

	auto skinColor = json::array(), hairColor = json::array();
	for (int i = 0; i < 3; ++i) {
		skinColor.push_back((float)look.skinRGB[i]);
		hairColor.push_back((float)look.hairRGB[i]);
	}
	res["skinColor"] = skinColor;
	res["hairColor"] = hairColor;

	auto headparts = json::array();
	for (int i = 0; i < (int)look.headparts.size(); ++i) {
		headparts.push_back(look.headparts[i]);
	}
	res["headparts"] = headparts;
	res["headTextureSet"] = look.headTextureSet;

	auto options = json::array();
	for (int i = 0; i < (int)look.options.size(); ++i) {
		options.push_back(look.options[i]);
	}
	res["options"] = options;

	auto presets = json::array();
	for (int i = 0; i < (int)look.presets.size(); ++i) {
		presets.push_back(look.presets[i]);
	}
	res["presets"] = presets;

	auto tints = json::array();
	for (int i = 0; i < (int)look.tints.size(); ++i) {
		auto color = json::array();

		union {
			uint32_t rgba = 0;
			uint8_t rgb[3];
		};
		rgba = look.tints[i].rgba;
		for (int i = 0; i < 3; ++i) {
			color.push_back(rgb[i]);
		}

		auto tint = json::object();
		tint["textureId"] = look.tints[i].texture;
		tint["color"] = color;
		tint["alpha"] = look.tints[i].alpha;
		tint["type"] = look.tints[i].type;
		tints.push_back(tint);
	}
	res["tints"] = tints;

	return res;
}

Look Look::FromJson(const json &j) {
	if (j.is_null()) return Look();

	Look res;
	res.isFemale = j.count("sex") && j.at("sex").is_string() && j.at("sex").get<std::string>() == "Female" ? 1 : 0;

	if (j.count("race") && j.at("race").is_string()) {
		auto s = j.at("race").get<std::string>();
		res.raceID = GetPlayableRaceByName(s.data());
	}

	if (j.count("weight")) {
		res.weight = (uint8_t)j.at("weight").get<float>();
	}

	if (j.count("skinColor")) {
		auto &jSkinColor = j.at("skinColor");
		assert((int)jSkinColor.size() == 3);
		if ((int)jSkinColor.size() >= 3) {
			for (int i = 0; i < 3; ++i) {
				res.skinRGB[i] = (uint8_t)(float)jSkinColor.at(i).get<float>();
			}
		}
	}

	if (j.count("hairColor")) {
		auto &jHairColor = j.at("hairColor");
		assert((int)jHairColor.size() == 3);
		if ((int)jHairColor.size() >= 3) {
			for (int i = 0; i < 3; ++i) {
				res.hairRGB[i] = (uint8_t)(float)jHairColor.at(i).get<float>();
			}
		}
	}

	if (j.count("headparts")) {
		auto &jHeadparts = j.at("headparts");
		for (int i = 0; i < (int)jHeadparts.size(); ++i) {
			res.headparts.push_back(jHeadparts.at(i).get<uint32_t>());
		}
	}

	if (j.count("headTextureSet")) {
		res.headTextureSet = j.at("headTextureSet").get<uint32_t>();
	}

	if (j.count("options")) {
		auto &jOptions = j.at("options");
		if ((size_t)jOptions.size() >= std::size(res.options)) {
			for (size_t i = 0; i < std::size(res.options); ++i) {
				res.options[i] = (float)jOptions.at(i).get<float>();
			}
		}
	}

	if (j.count("presets")) {
		auto &jPresets = j.at("presets");
		if ((size_t)jPresets.size() >= std::size(res.presets)) {
			for (size_t i = 0; i < std::size(res.presets); ++i) {
				res.presets[i] = (uint32_t)jPresets.at(i).get<uint32_t>();
			}
		}
	}

	if (j.count("tints")) {
		auto &jTints = j.at("tints");
		for (int i = 0; i < (int)jTints.size(); ++i) {
			auto &jTint = jTints.at(i);
			res.tints.push_back({});
			res.tints.back().texture = (int)jTint.at("textureId").get<int>();
			res.tints.back().alpha = (float)jTint.at("alpha").get<float>();
			res.tints.back().type = (int)jTint.at("type").get<int>();
			union {
				uint32_t rgba = 0;
				uint8_t rgb[3];
			};
			auto jColor = jTint.at("color");
			assert((int)jColor.size() == 3);
			for (size_t i = 0; i < 3; ++i) {
				if (jColor.size() > i) rgb[i] = (int)jColor.at(i).get<int>();
			}
			res.tints.back().rgba = rgba;
		}
	}

	return res;
}