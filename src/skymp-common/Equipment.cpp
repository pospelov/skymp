#include "Equipment.h"

Viet::json Equipment::ToJson(const Equipment &equipment) {
	Viet::json res;
	{
		auto jArmor = Viet::json::array();
		for (auto v : equipment.armor) {
			jArmor.push_back(v);
		}
		res["armor"] = jArmor;
	}
	{
		auto jSpecial = Viet::json::array();
		for (auto v : equipment.special) {
			jSpecial.push_back(v);
		}
		res["special"] = jSpecial;
	}
	return res;
}

Equipment Equipment::FromJson(const Viet::json &j) {
	Equipment res;

	if (j.count("armor") && j.at("armor").is_array()) {
		auto &jArmor = j.at("armor");
		for (int i = 0; i < (int)jArmor.size(); ++i) {
			res.armor.push_back(jArmor[i].get<uint32_t>());
		}
	}

	if (j.count("special") && j.at("special").is_array()) {
		auto &jSpecial = j.at("special");
		for (size_t i = 0; i < jSpecial.size(); ++i) {
			if (i >= res.special.size()) break;
			res.special[i] = jSpecial[i].get<uint32_t>();
		}
	}

	return res;
}