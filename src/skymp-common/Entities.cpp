#include "Entities.h"

Viet::Property PPos::Build() {
	return Viet::Property().Register<PPos>()
		.AttachToGrid([](const PPos::Value &pos) { return std::pair<int16_t, int16_t>(int16_t(pos.x / 4096), int16_t(pos.y / 4096)); })
		.SetSaveCondition([](const PPos::Value &prevPos, const PPos::Value &newPos) { return (prevPos- newPos).Length() > 1024.f; })
		.RemoveFlags(Viet::PropertyFlags::ReliableNetworking);
}

Viet::Property PAngle::Build() {
	return Viet::Property().Register<PAngle>()
		.RemoveFlags(Viet::PropertyFlags::ReliableNetworking);
}

Viet::Property PLook::Build() {
	return Viet::Property().Register<PLook>();
}

Viet::Property PMovementFlags::Build() {
	return Viet::Property().Register<PMovementFlags>();
}

Viet::Property PHeadDirection::Build() {
	return Viet::Property().Register<PHeadDirection>()
		.RemoveFlags(Viet::PropertyFlags::ReliableNetworking);
}

Viet::Property PVerticalAngle::Build() {
	return Viet::Property().Register<PVerticalAngle>()
		.RemoveFlags(Viet::PropertyFlags::ReliableNetworking);
}

Viet::Property PCellOrWorld::Build() {
	return Viet::Property().Register<PCellOrWorld>()
		.AddFlags(Viet::PropertyFlags::AffectsCellProcessSelection)
		.RemoveFlags(Viet::PropertyFlags::ClientWritable);
}

Viet::Property PIsEnabled::Build() {
	return Viet::Property().Register<PIsEnabled>()
		.RemoveFlags(Viet::PropertyFlags::ClientWritable)
		.SetValueForcesCaching(PIsEnabled::Value(true));
}

Viet::Property PEquipment::Build() {
	return Viet::Property().Register<PEquipment>()
		.RemoveFlags(Viet::PropertyFlags::SavedToDb | Viet::PropertyFlags::ReliableNetworking);
}

Viet::Property PStorage::Build() {
	return Viet::Property().Register<PStorage>()
		.RemoveFlags(Viet::PropertyFlags::ClientReadable | Viet::PropertyFlags::ClientWritable | Viet::PropertyFlags::VisibleToNeighbour);
}

Viet::Property PAnimEvent::Build() {
	return Viet::Property().Register<PAnimEvent>()
		.RemoveFlags(Viet::PropertyFlags::SavedToDb | Viet::PropertyFlags::ReliableNetworking);
}

Viet::Property PName::Build() {
	return Viet::Property().Register<PName>()
		.RemoveFlags(Viet::PropertyFlags::ClientWritable);
}

Viet::Property PIsRaceMenuOpen::Build() {
	return Viet::Property().Register<PIsRaceMenuOpen>();
}

std::vector<Viet::Entity> SkympEntities::GetEntities() {
	auto actorProperties = {
		PPos::Build(),
		PAngle::Build(),
		PLook::Build(),
		PMovementFlags::Build(), 
		PHeadDirection::Build(),
		PVerticalAngle::Build(),
		PCellOrWorld::Build(),
		PIsEnabled::Build(),
		PEquipment::Build(),
		PStorage::Build(),
		PAnimEvent::Build(),
		PName::Build(),
		PIsRaceMenuOpen::Build()
	};

	std::vector<Viet::Entity> res;
	res.push_back(
		Viet::Entity(actorEntityName, actorProperties)
	);
	return res;
}