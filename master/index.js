let fs = require('fs');
let LocalStorage = require('node-localstorage').LocalStorage;

const Koa = require('koa');
const Router = require('koa-router');
const bodyParser = require('koa-bodyparser');
const logger = require('koa-logger');
const ServerRunner = require('./server-runner');

function getMasterSetting(setting) {
  let envVal = process.env[setting];
  if (envVal) return envVal;

  // For tests only, do not use in production
  // It's unsafe to store secrets in the filesystem
  if (fs.existsSync('config.js')) {
    let cfg = JSON.parse(fs.readFileSync('config.js', 'utf8'));
    if (cfg[setting]) return cfg[setting];
  }

  return null;
};

let mongoUri = getMasterSetting('MONGO_URI');
let serverPath = getMasterSetting('SERVER_PATH');
console.log({ mongoUri: 'length=' + mongoUri.length, serverPath });

if (!fs.existsSync(serverPath)) {
  throw new Error(`${serverPath} doesn't exist`);
}

let port = 3000;
let app = new Koa;
let router = new Router;
let svrRunner = new ServerRunner(mongoUri, serverPath);
let localStorage = new LocalStorage('./master_tmp/local_storage');

router.get('/', async (ctx) => {
  ctx.body = 'the master server is running';
});

router.get('/servers', async (ctx) => {
  ctx.body = svrRunner.getServers();
});

router.post('/servers', async (ctx) => {
  console.log({body: ctx.request.body});
  let { maxPlayers, serverId, devPassword } = ctx.request.body;

  let existingServerData = svrRunner.getServers().find(data => data.serverId === serverId);
  if (existingServerData) {
    ctx.body = {
      ip: existingServerData.ip,
      gamemodesPort: existingServerData.gamemodesPort,
      serverId, devPassword
    };
    return;
  }

  let passCache = localStorage.getItem('server_' + serverId);

  let res;
  if (serverId && devPassword) {
    if (!passCache || passCache === devPassword) {
      res = await svrRunner.add(maxPlayers, serverId, devPassword);
    }
    else {
      return ctx.throw(403, 'bad devPassword');
    }
  }
  else {
    res = await svrRunner.add(maxPlayers);
  }
  ctx.body = res;
  localStorage.setItem('server_' + serverId, devPassword);
});

app.use(bodyParser())
  .use(logger())
  .use(router.routes())
  .use(router.allowedMethods());
app.listen(port, () => console.log('Ready'));
