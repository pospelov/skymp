let path = require('path');
let childProcess = require('child_process');
let fs = require('fs');
let crypto = require('crypto');

// https://stackoverflow.com/questions/20273128/how-to-get-my-external-ip-address-with-node-js
var g_myPublicIp = null;
function getMyPublicIpImpl() {
  const http = require('http');
  var options = { host: 'ipv4bot.whatismyipaddress.com', port: 80, path: '/' };
  return new Promise((resolve, reject) => {
    http.get(options, function (res) {
      res.on('data', (buf) => { resolve(buf.toString('ascii')); });
    }).on('error', reject);
  });
}
async function getMyPublicIp() {
  let isLocalTest = process.platform === 'win32';
  if (isLocalTest) return '127.0.0.1';
  if (!g_myPublicIp) g_myPublicIp = await getMyPublicIpImpl();
  return g_myPublicIp;
}

// https://gist.github.com/hyamamoto/fd435505d29ebfa3d9716fd2be8d42f0
function hashCode(s) {
  let h;
  for (let i = 0; i < s.length; i++) {
    h = Math.imul(31, h) + s.charCodeAt(i) | 0;
  }
  return h;
}

function getPort(serverId) {
  // Valid ports are from 7000 to 10000
  return 7000 + Math.abs(hashCode(serverId) % 3000);
}

class ServerRunner {
  constructor(mongoUri, serverPath) {
    if (typeof mongoUri !== 'string') {
      throw new TypeError('mongoUri must be string');
    }
    if (typeof serverPath !== 'string') {
      throw new TypeError('serverPath must be string');
    }
    this.mongoUri = mongoUri;
    this.serverPath = serverPath;
    this.nextPort = 3001;
    this.servers = {};
  }

  getServers() {
    let servers = [];
    for (let [serverId, data] of Object.entries(this.servers)) {
      let pub = JSON.parse(JSON.stringify(data.public));
      pub.serverId = serverId;
      servers.push(pub);
    }
    return servers;
  }

  async add(maxPlayers, serverId, devPassword) {
    serverId = serverId || crypto.randomBytes(10).toString('hex');
    devPassword = devPassword || crypto.randomBytes(20).toString('hex');
    let cfgname = 'cfg_' + serverId + '.json';
    if (!fs.existsSync('master_tmp')) {
      await fs.mkdirSync('master_tmp');
    }

    let gamemodesPort = this.nextPort++;
    if (this.nextPort == 3999) this.nextPort = 3001;

    let port = getPort(serverId);

    let ip = await getMyPublicIp();

    let configPath = path.join('master_tmp', cfgname);
    let mongoUri = this.mongoUri;
    let cfg = { maxPlayers, gamemodesPort, port, serverId, devPassword, mongoUri };
    fs.writeFileSync(configPath, JSON.stringify(cfg));

    let p = childProcess.spawn(this.serverPath, [configPath], {});
    this.servers[serverId] = {
      processHandle: p,
      public: { ip, port, gamemodesPort, maxPlayers }
    };
    this._addHandlers(serverId);

    return {
      ip,
      gamemodesPort,
      serverId,
      devPassword
    };
  }

  _addHandlers(serverId) {
    let p = this.servers[serverId].processHandle;
    [p.stdout, p.stderr].forEach(stream => {
      stream.on('data', (data) => {
        let s = data.toString().trim();
        if (!s.length) return;
        console.log(`${serverId} -> ${s}`);
      })
    });
    p.on('exit', (code) => {
      if (code === null) {
        // terminated by script
      }
      else {
        console.log(`${serverId} exited with code ${code}`);
        delete this.servers[serverId];
      }
    });
  }
};

module.exports = ServerRunner;
