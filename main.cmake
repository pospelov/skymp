# Options
option(VIET_DIST_PATH "Path to Viet distribution" OFF)
option(SKYRIM_DIR "Path to Skyrim" "C:\\Program Files (x86)\\Steam\\steamapps\\common\\Skyrim")
option(TEST_MONGO_URI "mongoUri used for tests (do NOT use production database uri here)" OFF)
option(CEF_PATH "path to CEF distribution" OFF)
option(NO_CEF "Set to ON to skip building of the CEF stuff" OFF)

if (WIN32)
  if (NOT CEF_PATH)
    message(FATAL_ERROR "Bad CEF_PATH")
  endif()
endif()
if (NOT VIET_DIST_PATH)
  message(FATAL_ERROR "Bad VIET_DIST_PATH")
endif()
if (WIN32)
  if (NOT SKYRIM_DIR)
    message(FATAL_ERROR "Bad SKYRIM_DIR")
  endif()
endif()
if (NOT TEST_MONGO_URI)
  message(FATAL_ERROR "Bad TEST_MONGO_URI")
endif()

if(MSVC)
  include(cmake/win32/msvc_settings.cmake)
endif()

include(cmake/common/skymp.cmake)

# It's not in viet.cmake's plugins section because it requires skymp_common to build
include(${CMAKE_CURRENT_SOURCE_DIR}/src/viet-node/VietNode.cmake)
set_target_properties(viet-node PROPERTIES
  ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib"
  LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib"
  RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin"
)

if(WIN32)
  if (NOT ${NO_CEF})
    include(src/cef/cef.cmake)
  else()
    message(STATUS "[skymp] Skipping cef.cmake")
  endif()
  include(src/frida/frida.cmake)
  include(cmake/win32/vcpkg.cmake)
  include(cmake/win32/third-party.cmake)
  include(cmake/win32/skymp.cmake)
  include(cmake/win32/pack.cmake)

  if (NOT ${NO_CEF})
    list(APPEND targets_to_bin skymp_cef_renderer skymp_cef_renderer_exe)
  endif()
  list(APPEND targets_to_bin skyrim_client skymp_hooks)

  set_target_properties(${targets_to_bin} PROPERTIES
    ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib"
    LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib"
    RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin"
  )
  target_compile_definitions(skyrim_client PRIVATE HOOKS_DLL_NAME=\"skymp_hooks.dll\")
endif()

include(tests/tests.cmake)
