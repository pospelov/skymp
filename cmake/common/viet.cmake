message(STATUS "[skymp] Using local Viet distribution ${VIET_PATH}")
message(STATUS "[skymp] VIET_DIST_PATH is ${VIET_DIST_PATH}")

set(VIET_INCLUDE
  ${VIET_DIST_PATH}/include
  ${CMAKE_CURRENT_SOURCE_DIR}/src/viet-raknet
  ${CMAKE_CURRENT_SOURCE_DIR}/src/viet-node
  ${CMAKE_CURRENT_SOURCE_DIR}/src/viet-mongocxx
)
message(STATUS "VIET_INCLUDE=${VIET_INCLUDE}")

# VIET_LIBS
set(libs
  vietlib
)

list(APPEND VIET_LIBS nlohmann_json::nlohmann_json)

if (WIN32)
  list(APPEND VIET_LIBS debug)
  foreach(lib ${libs})
    list(APPEND VIET_LIBS "${VIET_DIST_PATH}/lib_dbg/${CMAKE_STATIC_LIBRARY_PREFIX}${lib}${CMAKE_STATIC_LIBRARY_SUFFIX}")
  endforeach()
  list(APPEND VIET_LIBS optimized)
  foreach(lib ${libs})
    list(APPEND VIET_LIBS "${VIET_DIST_PATH}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}${lib}${CMAKE_STATIC_LIBRARY_SUFFIX}")
  endforeach()
endif()

if (UNIX)
  if (${CMAKE_BUILD_TYPE} STREQUAL "Debug")
    set(VIET_LIB_DIR_SUFFIX "_dbg")
  endif()
  foreach(lib ${libs})
    list(APPEND VIET_LIBS "${VIET_DIST_PATH}/lib${VIET_LIB_DIR_SUFFIX}/${CMAKE_STATIC_LIBRARY_PREFIX}${lib}${CMAKE_STATIC_LIBRARY_SUFFIX}")
  endforeach()
endif()

# Plugins
set(VIET_PLUGINS_DIR ${CMAKE_CURRENT_SOURCE_DIR}/src)
include(${CMAKE_CURRENT_SOURCE_DIR}/src/viet-mongocxx/VietMongocxx.cmake)
include(${CMAKE_CURRENT_SOURCE_DIR}/src/viet-raknet/VietRaknet.cmake)

# Note: VietNode.cmake is included into another file since it requires skymp_common library to build

message(STATUS "[skymp] Found VIET_LIBS: ${VIET_LIBS}")
