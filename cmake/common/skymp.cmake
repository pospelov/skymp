# common skymp library
file(GLOB skymp_common_src "src/skymp-common/*")
add_library(skymp_common STATIC ${skymp_common_src})

# server
file(GLOB skymp_server_src "src/skymp-server/*")
add_executable(skymp_server ${skymp_server_src})
target_link_libraries(skymp_server PRIVATE skymp_common viet-mongocxx ${VIET_LIBS_RAKNET})

foreach(target skymp_common skymp_server)
  target_link_libraries(${target} PRIVATE ${VIET_LIBS})
  target_include_directories(${target} PRIVATE ${VIET_INCLUDE})
  target_include_directories(${target} PRIVATE "${CMAKE_SOURCE_DIR}/src")
endforeach()
