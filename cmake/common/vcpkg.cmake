include("${CMAKE_BINARY_DIR}/external/pmm/src/pmm/pmm.cmake")
set(VCPKG_REVISION "722c49e09e2042e525a8c560736fd7b6457d181f")

if (UNIX)
  # see https://github.com/microsoft/vcpkg/commit/6c7f1c76738be50529fa8f1857db426fd3ef1d56
  set(VCPKG_REVISION "6c7f1c76738be50529fa8f1857db426fd3ef1d56")
endif()

pmm(VCPKG REVISION ${VCPKG_REVISION} REQUIRES
  openssl
  mongo-cxx-driver[boost]
  nlohmann-json
  zlib
)

find_package(nlohmann_json)
find_package(ZLIB REQUIRED)
