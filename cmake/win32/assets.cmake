include("${CMAKE_BINARY_DIR}/external/cmrc/src/cmrc/CMakeRC.cmake")
set(ASSET_FILES
  ${CMAKE_SOURCE_DIR}/assets/cursor.png
  ${CMAKE_SOURCE_DIR}/assets/hooks.js
  ${CMAKE_SOURCE_DIR}/assets/common/patch.js
  ${CMAKE_SOURCE_DIR}/assets/patchCef.js
  ${CMAKE_SOURCE_DIR}/assets/template.ess
)

cmrc_add_resource_library(assetslib ${ASSET_FILES} ALIAS assets NAMESPACE assets WHENCE assets)

cmrc_add_resource_library(assetslib_mt ${ASSET_FILES} ALIAS assets NAMESPACE assets WHENCE assets)
set_target_properties(assetslib_mt PROPERTIES MSVC_RUNTIME_LIBRARY "MultiThreaded")
