set(PACK ${CMAKE_BINARY_DIR}"/pack")

add_custom_target(_pack ALL
  COMMAND ${CMAKE_COMMAND} -E remove_directory ${PACK}
  COMMAND ${CMAKE_COMMAND} -E copy_directory "${CMAKE_BINARY_DIR}/bin/$<CONFIG>" ${PACK}
  COMMAND ${CMAKE_COMMAND} -E copy ${PACK}/$<TARGET_FILE_NAME:skyrim_client> ${PACK}/Data/skse/plugins/$<TARGET_FILE_NAME:skyrim_client>
  COMMAND ${CMAKE_COMMAND} -E remove ${PACK}/$<TARGET_FILE_NAME:skyrim_client>
  COMMAND ${CMAKE_COMMAND} -E copy_directory ${CEF_PATH}/Release ${PACK}
  COMMAND ${CMAKE_COMMAND} -E copy_directory ${CEF_PATH}/Resources ${PACK}
)

list(APPEND pack_deps skyrim_client viet-node)
if (NOT ${NO_CEF})
  list(APPEND pack_deps skymp_cef_renderer skymp_cef_renderer_exe)
else()
  message(STATUS "[skymp] Skipping CEF dependencies of _pack target")
endif()
add_dependencies(_pack ${pack_deps})

message(STATUS "[skymp] SKYRIM_DIR=" ${SKYRIM_DIR})
add_custom_target(_install ALL
  COMMAND ${CMAKE_COMMAND} -E copy_directory ${PACK} ${SKYRIM_DIR}
)
add_dependencies(_install _pack)
