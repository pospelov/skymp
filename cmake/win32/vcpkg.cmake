include("${CMAKE_BINARY_DIR}/external/pmm/src/pmm/pmm.cmake")

if (NOT VCPKG_REVISION)
  message(FATAL_ERROR "Hey! VCPKG_REVISION should not be empty")
endif()

pmm(VCPKG REVISION ${VCPKG_REVISION} REQUIRES
  imgui
  mhook
  minhook
  cpp-httplib
)

find_package(imgui CONFIG REQUIRED)

find_library(mhook_lib_dbg mhook PATH_SUFFIXES debug)
set(mhook_lib_rel ${mhook_lib_dbg})
string(REPLACE "/debug" "" mhook_lib_rel ${mhook_lib})
string(REPLACE "\\debug" "" mhook_lib_rel ${mhook_lib})
set(mhook_lib optimized ${mhook_lib_rel} debug ${mhook_lib_dbg})
