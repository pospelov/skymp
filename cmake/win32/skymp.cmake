# beth client
file(GLOB client_src "src/beth-client/*")
add_library(beth_client STATIC ${client_src})

# skyrim api
file(GLOB skyrim_api_src "src/skyrim-api/*")
add_library(skyrim_api STATIC ${skyrim_api_src})
target_link_libraries(skyrim_api PRIVATE assetslib)
target_include_directories(skyrim_api PRIVATE ${CMAKE_CURRENT_BINARY_DIR}/external/skse/src/skse)
target_include_directories(skyrim_api PRIVATE "src/assets-util")

# skyrim client
file(GLOB skymp_le_client_src "src/skyrim-client/*")
add_library(skyrim_client SHARED ${skymp_le_client_src})
target_link_libraries(skyrim_client PRIVATE
  beth_client
  skyrim_api
  skymp_common
  ${COMMON_TARGET}
  ${SKSE_TARGET}
  ${SD_TARGET}
  ZLIB::ZLIB
  ${mhook_lib}
  imgui::imgui
  ${VIET_LIBS_RAKNET}
  ${DXSDK_LIBS}
  assetslib
)
target_include_directories(skyrim_client PRIVATE ${DXSDK_INCLUDE})
set_target_properties(skyrim_client PROPERTIES PREFIX "" OUTPUT_NAME "skymp" SUFFIX ".dll")

foreach(target beth_client skyrim_api skyrim_client)
  target_link_libraries(${target} PRIVATE ${VIET_LIBS})
  target_include_directories(${target} PRIVATE ${VIET_INCLUDE})
  target_include_directories(${target} PRIVATE "${CMAKE_SOURCE_DIR}/src")
endforeach()
