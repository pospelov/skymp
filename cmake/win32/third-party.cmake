if(MSVC)
  set(THIRD_PARTY_DIR ${CMAKE_CURRENT_BINARY_DIR}/external/skse/src/skse)
  set(DXSDK_DIR ${CMAKE_CURRENT_BINARY_DIR}/external/dxsdk/src/dxsdk)

  # ScriptDragon
  set(SD_TARGET ScriptDragon)
  set(SD_INCLUDE "${THIRD_PARTY_DIR}/scriptdragon")
  file(GLOB SD_SRC "${THIRD_PARTY_DIR}/scriptdragon/*")
  add_library(${SD_TARGET} STATIC ${SD_SRC})

  # common
  set(COMMON_TARGET common)
  set(COMMON_INCLUDE "${THIRD_PARTY_DIR}/common")
  file(GLOB COMMON_SRC "${THIRD_PARTY_DIR}/common/*")
  add_library(${COMMON_TARGET} ${COMMON_SRC})
  set_target_properties(${COMMON_TARGET} PROPERTIES COMPILE_FLAGS /FI"IPrefix.h")
  target_include_directories(${COMMON_TARGET} PUBLIC "${THIRD_PARTY_DIR}")

  # SKSE
  set(SKSE_TARGET SKSE)
  set(SKSE_INCLUDE "${THIRD_PARTY_DIR}/skse")
  file(GLOB SKSE_SRC "${THIRD_PARTY_DIR}/skse/*")
  add_library(${SKSE_TARGET} ${SKSE_SRC})
  set_target_properties(${SKSE_TARGET} PROPERTIES COMPILE_FLAGS /FI"../common/IPrefix.h")
  target_include_directories(${SKSE_TARGET} PUBLIC "${THIRD_PARTY_DIR}")
  target_compile_definitions(${SKSE_TARGET} PUBLIC RUNTIME=1 RUNTIME_VERSION=0x09200000)

  # DirectX
  set(DXSDK_TARGET "D3dx9")
  set(DXSDK_INCLUDE "${DXSDK_DIR}/Include")
  set(DXSDK_LIBS
    "${DXSDK_DIR}/Lib/x86/d3d9.lib"
    "${DXSDK_DIR}/Lib/x86/D3dx9.lib"
  )
endif()
