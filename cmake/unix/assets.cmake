include("${CMAKE_BINARY_DIR}/external/cmrc/src/cmrc/CMakeRC.cmake")
set(ASSET_FILES
  ${CMAKE_SOURCE_DIR}/assets/common/patch.js
)

cmrc_add_resource_library(assetslib ${ASSET_FILES} ALIAS assets NAMESPACE assets WHENCE assets)
cmrc_add_resource_library(assetslib_mt ${ASSET_FILES} ALIAS assets NAMESPACE assets WHENCE assets)
