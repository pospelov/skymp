enable_testing()

set(TESTS_DIR ${CMAKE_CURRENT_LIST_DIR})


set(node_api_test_dirname "node_api_test")

set(NODE_WORKDIR ${TESTS_DIR}/${node_api_test_dirname})

if (UNIX)
  set(suffix .sh)
elseif(WIN32)
  set(suffix .bat)
endif()
set(temp_bat "${CMAKE_CURRENT_BINARY_DIR}/temp2${suffix}")
set(npm_cmd ${temp_bat})
set(npm_arg "")

message(STATUS "[tests] Doing 'npm install'")
file(WRITE ${temp_bat} "npm i")
execute_process(COMMAND ${npm_cmd} ${npm_arg}
  WORKING_DIRECTORY ${NODE_WORKDIR}
  RESULT_VARIABLE npm_result
  OUTPUT_VARIABLE npm_out
)
message(STATUS "[tests] Finished 'npm install' with code ${npm_result}")
if(npm_result EQUAL "1")
    message(FATAL_ERROR "[tests] Bad exit status")
endif()

set(name ${node_api_test_dirname})
add_test(NAME ${name} COMMAND
  ${CMAKE_COMMAND}
  -DSKYMP_TEST_SERVER_PATH=$<TARGET_FILE:skymp_server>
  -DSKYMP_TEST_NODE_API_DIR=$<TARGET_FILE_DIR:viet-node>
  -DSKYMP_TEST_TEMP_DIR=${CMAKE_CURRENT_BINARY_DIR}/test_tmp
  -DSKYMP_TEST_MONGO_URI=${TEST_MONGO_URI}
  -DSKYMP_TEST_NODE_WORKDIR=${NODE_WORKDIR}
  -P ${NODE_WORKDIR}/run_test.cmake
  WORKING_DIRECTORY ${TESTS_DIR}
)
set_property(TEST ${name} APPEND PROPERTY
  FAIL_REGULAR_EXPRESSION "(.*CMake Error at.*)|(.*Error: .*)"
  # CMake and NodeJS errors
)
set_tests_properties(${name} PROPERTIES TIMEOUT 300)
