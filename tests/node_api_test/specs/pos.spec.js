it ('sends pos from client', async () => {
  await setup();
  let bot = newBot();

  let [user] = await nextEvent(remoteServer, 'userEnter', 1000);

  let actor = await remoteServer.createActor()
    .setPos([1, 2, 100])
    .setAngle([0, 0, 120])
    .setCellOrWorld(0x3c)
    .setEnabled(true);
  await user.setActor(actor);

  expect(await actor.getPos()).to.eql([1, 2, 100]);
  await see(bot, 'Actor', { pos: [1, 2, 100] });

  bot.send('pos', [111, 222, 200]);
  await see(bot, 'Actor', { pos: [111, 222, 200], isEnabled: true });
});

it ('finds by pos', async () => {
  await setup();

  let ac0 = await remoteServer.createActor().setPos([1, 2, 3]);
  let ac1 = await remoteServer.createActor().setPos([1, 2, 0]);
  let ac2 = await remoteServer.createActor().setPos([1, 0, 3]);

  for (let i = 0; i < 2; ++i) {
    let r0 = await remoteServer.findActors({ pos: [1, 2, 3] }, 1);
    let r0_ = await remoteServer.findActor({ pos: [1, 2, 3] });
    expect(r0[0].getId()).to.eql(ac0.getId());
    expect(r0_.getId()).to.eql(ac0.getId());

    let r1 = await remoteServer.findActors({ pos: [1, 2, 0] }, 1);
    expect(r1[0].getId()).to.eql(ac1.getId());

    let r2 = await remoteServer.findActors({ pos: [1, 0, 3] }, 1);
    expect(r2[0].getId()).to.eql(ac2.getId());

    let r2a = await remoteServer.findActors({ pos: [1, 0, 3], angle: [0,0,0], 'angle.0': 0 }, 1);
    let r2a_ = await remoteServer.findActors({ pos: [1, 0, 3], 'angle.0': { $greater: -1, $lessOrEqual: 0 } }, 1);
    let r2a__ = await remoteServer.findActors({ pos: [1, 0, 3], 'angle.0': { $greater: 1, $lessOrEqual: 10 } }, 1);
    expect(r2a[0].getId()).to.eql(ac2.getId());
    expect(r2a_[0].getId()).to.eql(ac2.getId());
    expect(r2a__).to.eql([]);

    let r3 = await remoteServer.findActors({ 'pos.0': { $equal: 1 } }, 3);
    expect(r3.length).to.eql(3);

    let r3a = await remoteServer.findActors({ 'pos.0': { $greater: 0, $less: 2 } }, 3);
    expect(r3a.length).to.eql(3);

    let r3b = await remoteServer.findActors({ 'pos.0': { $greaterOrEqual: 1, $lessOrEqual: 1 } }, 3);
    expect(r3b.length).to.eql(3);

    await Promise.all([ac0, ac1, ac2].map(ac => ac.setEnabled(true)));
    expect(
      await Promise.all([ac0, ac1, ac2].map(ac => ac.isEnabled()))
    ).to.eql([true, true, true]);
  }
});
