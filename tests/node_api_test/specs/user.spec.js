it('emits userEnter and userExit', async () => {
  await setup();

  let bot = newBot();

  let userEnter = await nextEvent(remoteServer, 'userEnter', 1000);
  let userId = userEnter[0].getId();

  bot.kill();

  let userExit = await nextEvent(remoteServer, 'userExit', 10000);
  expect(userExit[0].getId()).to.eql(userId);
});

it('restores a session', async () => {
  await setup();

  let bot = newBot();

  let userEnter = await nextEvent(remoteServer, 'userEnter');
  let userId = userEnter[0].getId();

  bot.kill();
  await serverLogLine(server, '.*Disconnected 0.*', 10000);

  bot = newBot(userId);

  let thrown = false;
  try {
    await nextEvent(remoteServer, 'userEnter', 1000);
  }
  catch(e) {
    thrown = true;
  }
  expect(thrown).to.eql(true);

  bot.kill();

  let userExit = await nextEvent(remoteServer, 'userExit', 10000);
  expect(userExit[0].getId()).to.eql(userId);
});
