it('getActor returns Actor passed to setActor', async () => {
  await setup();
  let bot = newBot();

  let [user] = await nextEvent(remoteServer, 'userEnter', 1000);

  let actor = await remoteServer.createActor()
    .setPos([1, 2, 3])
    .setAngle([0, 0, 89]);

  expect(await user.getActor()).to.eql(null);

  await user.setActor(actor);

  let ac = await user.getActor();

  expect(ac.getId()).to.eql(actor.getId());

  await user.setActor(null);
  expect(await user.getActor()).to.eql(null);
});

it('Calling setActor on enabled actor allows bot to see WorldState', async () => {
  await setup();
  let bot = newBot();

  let [user] = await nextEvent(remoteServer, 'userEnter', 1000);

  let actor = await remoteServer.createActor()
    .setPos([1, 2, 100])
    .setAngle([0, 0, 120])
    .setCellOrWorld(0x3c)
    .setEnabled(true);

  await user.setActor(actor);
  await see(bot, 'Actor', {
    pos: [1, 2, 100]
  });
});

it('Calling setEnabled on actor allows bot to see WorldState', async () => {
  await setup();
  let bot = newBot();

  let [user] = await nextEvent(remoteServer, 'userEnter', 1000);

  let actor = await remoteServer.createActor()
    .setPos([1, 2, 100])
    .setAngle([0, 0, 120])
    .setCellOrWorld(0x3c);

  await user.setActor(actor);

  await actor.setEnabled(true);
  await see(bot, 'Actor', { pos: [1, 2, 100] });

  await actor.setAngle([99, 99, 99]);
  await see(bot, 'Actor', { pos: [1, 2, 100] });
});
