it('setEnabled works', async () => {
  await setup();
  let bot = newBot();

  let [user] = await nextEvent(remoteServer, 'userEnter', 1000);

  let actor = await remoteServer.createActor()
    .setPos([1, 2, 100])
    .setAngle([0, 0, 120])
    .setCellOrWorld(0x3c);
  await user.setActor(actor);

  for (let i = 0; i < 10; ++i) {
    await actor.setEnabled(true);

    expect(await actor.isEnabled()).to.eql(true);
    await see(bot, 'Actor', { pos: [1, 2, 100] });

    await actor.setEnabled(false);

    expect(await actor.isEnabled()).to.eql(false);
    await notSee(bot, 'Actor', { pos: [1, 2, 100] });
  }

  await actor.setEnabled(true);
  await see(bot, 'Actor', { pos: [1, 2, 100] });

  await actor.setEnabled(false);
  await notSee(bot, 'Actor', { pos: [1, 2, 100] });

  await actor.setEnabled(true).setPos(1, 2, 10);
  await see(bot, 'Actor', { pos: [1, 2, 10] });
});

it ('send enabled does not work', async () => {
  await setup();
  let bot = newBot();

  let [user] = await nextEvent(remoteServer, 'userEnter', 1000);

  let actor = await remoteServer.createActor()
    .setPos([111, 222, 200])
    .setAngle([0, 0, 120])
    .setCellOrWorld(0x3c)
    .setEnabled(true);
  await user.setActor(actor);

  await see(bot, 'Actor', { pos: [111, 222, 200] });

  let error;

  // Try to set isEnabled to false from the client
  bot.send('isEnabled', false);
  error = false;
  try {
    await notSee(bot, 'Actor', { pos: [111, 222, 200], isEnabled: true });
  }
  catch (e) {
    error = true;
  }
  expect(error).to.eql(true);
  expect(await actor.isEnabled()).to.eql(true);

  // Set isEnabled to false by the gamemode
  await actor.setEnabled(false);
  await notSee(bot, 'Actor', { pos: [111, 222, 200] });

  // Try to set isEnabled to true from the client
  bot.send('isEnabled', true);
  error = false;
  try {
    await see(bot, 'Actor', { pos: [111, 222, 200] });
  }
  catch (e) {
    error = true;
  }
  expect(error).to.eql(true);
  expect(await actor.isEnabled()).to.eql(false);
});
