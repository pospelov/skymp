// from masser/scripts/lib/util.js
var __strStorage = {};
var __totalRamUsed = 0;
function cstr(string) {
    if (typeof string == 'object') {
        string = JSON.stringify(string, null, 2);
    }
    string = '' + string;
    if (__strStorage[string] === undefined) {
        __strStorage[string] = Memory.allocUtf8String(string);
        __totalRamUsed += string.length;
    }
    return __strStorage[string];
}

var ptr_ = null;

ptr_ = Module.findExportByName(null, 'SKYMP2_Print');
var SKYMP2_Print = new NativeFunction(ptr_, 'void', ['pointer']);

ptr_ = Module.findExportByName(null, 'SKYMP2_SetWeaponDrawn');
var SKYMP2_SetWeaponDrawn = new NativeFunction(ptr_, 'pointer', ['pointer']);

ptr_ = Module.findExportByName(null, 'SKYMP2_SendAnimationEvent');
var SKYMP2_SendAnimationEvent = new NativeFunction(ptr_, 'pointer', ['pointer', 'pointer']);

ptr_ = Module.findExportByName(null, 'SKYMP2_OnEquipItem');
var SKYMP2_OnEquipItem = new NativeFunction(ptr_, 'void', ['pointer', 'pointer', 'uint8']);

var GetLeftHand = new NativeFunction(ptr(0x0054C810), 'pointer', []);
var GetRightHand = new NativeFunction(ptr(0x0054C840), 'pointer', []);

var print = function(text) {
  SKYMP2_Print(cstr('' + text));
};

// SetWeaponDrawn
Interceptor.attach(ptr(0x006B2050), {
  onEnter: function(args) {
    var msg = {};
    msg['ecx'] = this.context.ecx;
    msg['draw'] = args[0];
    var res = SKYMP2_SetWeaponDrawn(cstr(JSON.stringify(msg)));
    res = Memory.readAnsiString(res);
    res = JSON.parse(res);
    args[0] = ptr(res['draw']);
  }
});

// SendAnimationEvent
Interceptor.attach(ptr(0x0064F7D0), {
  onEnter: function(args) {
    args[0] = SKYMP2_SendAnimationEvent(this.context.ecx.add(-0x20), args[0]);
  }
});

// Equip
Interceptor.attach(ptr(0x006EF3E0), {
  onEnter: function(args) {
    var msg = {};

    // Prevent equipping weapon to left hand
    if (args[4].toInt32() == GetLeftHand().toInt32()) args[4] = GetRightHand();

    SKYMP2_OnEquipItem(args[0], args[1], 1);
  }
});

// Unequip
Interceptor.attach(ptr(0x006EE560), {
  onEnter: function(args) {
    SKYMP2_OnEquipItem(args[0], args[1], 0);
  }
});

var cursorPtr = null;
var cursorViewPtr = null;
var cursorMenuCtorPtr = ptr(0x00859B70);
Interceptor.attach(cursorMenuCtorPtr, {
  onEnter: function(args) {
  },
  onLeave: function(retval) {
    cursorPtr = ptr(retval);
    cursorViewPtr = cursorPtr.add(0x08);
  }
});
var mainMenuPtr = null;
var mainMenuViewPtr = null;
var mainMenuCtorPtr = ptr(0x00876FC0);
Interceptor.attach(mainMenuCtorPtr, {
  onEnter: function(args) {
  },
  onLeave: function(retval) {
    mainMenuPtr = ptr(retval);
    mainMenuViewPtr = mainMenuPtr.add(0x08);
  }
});

var renderPtr = ptr(0x00847000);
Interceptor.attach(renderPtr, {
  onEnter: function(args) {
    if (cursorPtr && cursorPtr.toInt32()) {
      if (this.context.ecx.toInt32() == cursorPtr.toInt32()) {
        Memory.writeU32(cursorViewPtr, 0);
      }
    }
    if (mainMenuPtr && mainMenuPtr.toInt32()) {
      if (this.context.ecx.toInt32() == mainMenuPtr.toInt32()) {
        Memory.writeU32(mainMenuViewPtr, 0);
      }
    }
  },
  onExit: function(retVal) {

  }
});
