let _skymp = window._skymp;
let _callbacks = {};

window.skymp = {
  sendCustomPacket: function (data) {
    return _skymp.sendCustomPacket(JSON.stringify(data));
  },

  on: function(eventName, cb) {
    if (!_callbacks[eventName]) _callbacks[eventName] = [];
    _callbacks[eventName].push(cb);
  }
}

setInterval(() => {
  if (_skymp.cpIn) {
    this._skymp.cpIn.forEach(cp => {
      let cpObj;
      try {
        cpObj = JSON.parse(cp);
      }
      catch(e) {
        cpObj = {};
      }
      if (_callbacks['customPacket']) {
        _callbacks['customPacket'].forEach(f => f(cpObj));
      }
    });
    this._skymp.cpIn = [];
  }
}, 1);
