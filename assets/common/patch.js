let require = global.require || global.process.mainModule.constructor._load;
let EventEmitter = require('events');

function uppercaseFirstLetter(param) {
  if (typeof param === 'string') {
    return param.charAt(0).toUpperCase() + param.slice(1);
  }
  return param;
}

function validateFindCondition(cond) {
  if (typeof cond !== 'object') {
    throw new TypeError('find condition must be object');
  }
  for (let [key, value] of Object.entries(cond)) {
    if (value === undefined) {
      throw new TypeError(`find condition must not contain undefined (${key})`);
    }
  }
}

let RemoteServerImpl;
let instanceForward;

class User {
  constructor(ei, svr) {
    this._ei = ei;
    this._svr = svr;

    for (let [entityName, propNames] of Object.entries(svr._entityList)) {
      this['set' + uppercaseFirstLetter(entityName)] = (instance) => {
        if (instance && !instance._ei) {
          throw new TypeError(`instance must be a valid ${entityName} or null but it's ${typeof instance}`);
        }
        return svr._impl.setAttachedInstance(ei, entityName, instance ? instance._ei : null);
      };

      this['get' + uppercaseFirstLetter(entityName)] = () => {
        return new Promise((resolve, reject) => {
          let p = svr._impl.getAttachedInstance(ei, entityName);
          p.then(instEi => resolve(instEi ? new instanceForward({ ei: instEi, svr, entityName, propNames }) : null));
          p.catch(reject);
        });
      };

      this['sendCustomPacket'] = (value) => {
        return svr._impl.sendCustomPacket(ei, JSON.stringify(value));
      };
    }
  }

  getId() {
    return this._ei;
  }
}

class InstanceStream {
  constructor(propNames, svr) {
    this._actionsHolder = { actions: [] };

    propNames.forEach(propName => {
      let isBoolean = false;
      if (propName.startsWith('is') && propName[2].toUpperCase() === propName[2]) {
        isBoolean = true;
      }

      let set = 'set' + uppercaseFirstLetter(isBoolean ? propName.substr(2) : propName);
      let _this = this;
      this[set] = function (newValue) {

        if (arguments.length > 1) {
          newValue = Array.from(arguments);
        }

        _this._actionsHolder.actions.push({ type: 'Set', name: propName, value: newValue });
        if (!this.then && this._then) this.then = this._then;
        return this;
      };

      let get = (isBoolean ? propName : 'get' + uppercaseFirstLetter(propName));
      this[get] = () => {
        this._actionsHolder.actions.push({ type: 'Get', name: propName });
        if (!this.then && this._then) this.then = this._then;
        return this;
      };
    });
  }

  _moveActions() {
    let res = this._actionsHolder.actions;
    this._actionsHolder.actions = [];
    return res;
  }
}

class Instance extends InstanceStream {
  constructor(options) {
    super(options.propNames, options.svr);
    this._ei = options.ei;
    this._svr = options.svr;
    this._entityName = options.entityName;
  }

  getId() {
    return this._ei;
  }

  _then(onFulfilled, onRejected) {
    let actions = this._moveActions();
    let p = this._svr._impl.manipulateInstance(this._entityName, actions, this._ei);
    p.then((results) => {
      if (results.instance !== this._ei) {
        throw new Error('Wow!');
      }
      onFulfilled(results.returnedValues.length === 1 ? results.returnedValues[0] : results.returnedValues);
    }).catch(onRejected);
  }
}

instanceForward = Instance;

class RemoteServer extends EventEmitter {
  constructor() {
    super();
    this._impl = new RemoteServerImpl;

    this._impl.on('connect', (err, entityList) => {
      this._processEntityList(entityList);
      this.emit('connect', err);
    });

    this._impl.on('userEnter', (userEi) => {
      this.emit('userEnter', new User(userEi, this));
    });

    this._impl.on('userExit', (userEi) => {
      this.emit('userExit', new User(userEi, this));
    });

    this._impl.on('userCustomPacket', (userEi, contentStr) => {
      let content;
      try {
        content = JSON.parse(contentStr);
      }
      catch(e) {
        content = {};
      }
      this.emit('userCustomPacket', new User(userEi, this), content);
    });
  }

  connect(options) {
    if (typeof options !== 'object') {
      throw new TypeError('Expected options object');
    }
    let { ip, port, serverId, devPassword, frontEnd } = options;
    if (typeof ip !== 'string') {
      throw new TypeError('options.ip must be string');
    }
    if (typeof port !== 'number') {
      throw new TypeError('options.port must be number');
    }
    if (typeof serverId !== 'string') {
      throw new TypeError('options.serverId must be string');
    }
    if (typeof devPassword !== 'string') {
      throw new TypeError('options.devPassword must be string');
    }
    if (frontEnd && typeof frontEnd !== 'object') {
      throw new TypeError('options.frontEnd must be object');
    }

    this._impl.connect(ip, port, serverId, devPassword, frontEnd)
  }

  kill() {
    this._impl.kill();
  }

  _processEntityList(entityList) {
    this._entityList = entityList;
    for (let [entityName, propNames] of Object.entries(entityList)) {

      let find = 'find' + uppercaseFirstLetter(entityName) + 's';
      this[find] = (condition, limit) => {
        validateFindCondition(condition);
        if (!limit) limit = 0;
        let p = this._impl.findInstance(entityName, condition, limit);
        return new Promise((resolve, reject) => {
          p.then((instancesFound) => {
            resolve(instancesFound.map(ei => new Instance({ ei, entityName, propNames, svr: this })));
          }).catch(reject);
        });
      };

      let findOne = 'find' + uppercaseFirstLetter(entityName);
      this[findOne] = async (condition) => {
        return (await (this[find])(condition, 1))[0];
      };

      let create = 'create' + uppercaseFirstLetter(entityName);
      this[create] = () => {
        let stream = new InstanceStream(propNames, this);
        stream.then = (onFulfilled, onRejected) => {
          let p = this._impl.manipulateInstance(entityName, stream._moveActions());
          p.then((results) => {
            let inst = new Instance({ ei: results.instance, entityName, propNames, svr: this });
            onFulfilled(inst);
          }).catch(onRejected);
        };
        return stream;
      }
    }
  }
};

applyPatch = (moduleExports) => {
  RemoteServerImpl = moduleExports.RemoteServer;
  moduleExports.RemoteServer = RemoteServer;
  return moduleExports;
};
