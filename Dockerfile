FROM viet:1.2.0

WORKDIR /usr/src/skymp3

COPY ./CMakeLists.txt .

COPY ./cmake/common/external ./cmake/common/external
COPY ./cmake/common/vcpkg.cmake ./cmake/common

RUN mkdir -p build && cd build && mkdir -p Debug && mkdir -p Release
RUN cd ./build/Release && \
  cmake -DONLY_VCPKG_INSTALL=ON -DCMAKE_BUILD_TYPE=Release ../.. && \
  cmake --build . && \
  cd ../Debug && \
  cmake -DONLY_VCPKG_INSTALL=ON -DCMAKE_BUILD_TYPE=Debug ../.. && \
  cmake --build .

COPY ./src/viet-raknet ./src/viet-raknet
COPY ./src/viet-mongocxx ./src/viet-mongocxx
COPY ./src/third-party ./src/third-party
COPY ./src/assets-util ./src/assets-util
COPY ./assets/common/ ./assets/common
COPY ./cmake/unix ./cmake/unix
COPY ./cmake/common/viet.cmake ./cmake/common

RUN cd ./build/Release && \
  cmake -DVIET_DIST_PATH=/usr/src/viet/build/Release/dist -DONLY_DEPS_INSTALL=ON -DONLY_VCPKG_INSTALL=OFF -DCMAKE_BUILD_TYPE=Release ../.. && \
  cmake --build . && \
  cd ../Debug && \
  cmake -DVIET_DIST_PATH=/usr/src/viet/build/Debug/dist -DONLY_DEPS_INSTALL=ON -DONLY_VCPKG_INSTALL=OFF -DCMAKE_BUILD_TYPE=Debug ../.. && \
  cmake --build .

COPY ./src/skymp-common ./src/skymp-common
COPY ./src/skymp-server ./src/skymp-server
COPY ./src/viet-node ./src/viet-node
COPY ./main.cmake .
COPY ./cmake/common/skymp.cmake ./cmake/common
COPY ./tests ./tests

ARG TEST_MONGO_URI

RUN cd ./build/Release && \
  cmake -DTEST_MONGO_URI=$TEST_MONGO_URI -DONLY_DEPS_INSTALL=OFF -DCMAKE_BUILD_TYPE=Release ../.. && \
  cmake --build . && \
  cd ../Debug && \
  cmake -DTEST_MONGO_URI=$TEST_MONGO_URI -DONLY_DEPS_INSTALL=OFF -DCMAKE_BUILD_TYPE=Debug ../.. && \
  cmake --build .

ENV MONGO_URI=$TEST_MONGO_URI
ENV SERVER_PATH=/usr/src/skymp3/build/Release/skymp_server

WORKDIR /usr/src/skymp3-master
COPY ./master/package*.json ./
RUN npm install
COPY ./master .
CMD ["npm", "run", "start"]
